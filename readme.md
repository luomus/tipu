Tipu - Rengastustoimiston päätietojärjestelmä
=============================================

### Installation

1. Place rengastus.properties config file to <catalina.base>/app-conf/
2. Place ojdbc6.jar to <catalina.base>/lib
3. Deploy rengastus.war

rengastus.properties example
~~~
SystemID = rengastus

DevelopmentMode = NO
StagingMode = YES

LintuvaaraURL = https://

DBdriver = oracle.jdbc.OracleDriver
DBurl = 
DBusername = 
DBpassword =  

TipuAPI_URI = /tipu-api
TipuAPI_Username = 
TipuAPI_Password =  

LOYDOS_URI = /loydos/
LOYDOS_USERNAME = 
LOYDOS_PASSWORD =  

BaseURL = https://localhost/rengastus
StaticURL = https://localhost/rengastus/static

#All other folders are relative to basefolder
BaseFolder = /home/tomcat/tomcat/

LanguageFileFolder = webapps/rengastus/locales
TemplateFolder     = webapps/rengastus/template
LogFolder          = application-logs/rengastus

LanguageFiles = locale
SupportedLanguages = fi

ErrorReporting_SMTP_Host = localhost
ErrorReporting_SMTP_Username =
ErrorReporting_SMTP_Password =
ErrorReporting_SMTP_SendTo = 
ErrorReporting_SMTP_SendFrom = 
ErrorReporting_SMTP_Subject = Rengastus Dev Error Report
~~~