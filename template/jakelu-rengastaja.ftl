<#include "luomus-header.ftl">
<#include "macro.ftl">

<h4>Rengastajan ${rowStructure.get("event.ringer").getEnumerationDescription(resourceID)} jakelu</h4>

<form action="${baseURL}/jakelu" method="get" class="changeResource">
 	<ul class="fieldBox fewFields">
 	<li><label for="j__ringer.id">Vaihda rengastajaa</label> <@selectWithID "j" "ringer.id" /> </li> 
 	<li> <input type="submit" value="Näytä" class="resourceSelectSubmit" /> </li>
 	</ul>
</form>


<#list distributedAndUsedRings as series>
	<h5>${series.getString("series")}</h5>
	<table class="distTable">
		<tr>
			<th>Jakopvm.</th>
			<th>Renkaat</th>
		</tr>
		<#list series.getArray("distributionsByDate").iterateAsObject() as distribution>
		<tr>
			<td>${distribution.getString("date")}:</td>
			<#list distribution.getArray("batches").iterateAsObject() as batch>
				<td <#if batch.getBoolean("used")>style="background-color: rgb(200,255,200);" class="ringBatch usedRingBatch" title="Käytetty"<#else>class="ringBatch" title="Käytämättä"</#if>  >
					<#if batch.getInteger("start") == batch.getInteger("end")>${batch.getInteger("start")}<#else>${batch.getInteger("start")} - ${batch.getInteger("end")}</#if>
				</td>
			</#list>
		</tr>
		</#list>
	</table>
</#list>
<#if !distributedAndUsedRings?has_content>
	<p>Ei jaettuja renkaita.</p>
</#if>

<#if distributedAndUsedRings?has_content>
<h4>Väritunnisteet</h4>
<#list distributedAndUsedFieldReadables as dist>
	<h5>${dist.getString("species")}</h5>
	<table class="distTable">
		<#list dist.getArray("fieldReadables").iterateAsObject() as batch>
			<tr>
				<td <#if batch.getBoolean("used")>style="background-color: rgb(200,255,200);" class="ringBatch usedRingBatch" title="Käytetty"<#else>class="ringBatch" title="Käytämättä"</#if>  >
				${batch.getString("code")} ${batch.getString("backgroundColor")} 
				</td>
			</tr>
		</#list>
	</table>
</#list>
<#else>
	<p>Ei jaettuja väritunnisteita.</p>
</#if>

<#include "luomus-footer.ftl">