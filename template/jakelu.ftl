<#include "luomus-header.ftl">
<#include "macro.ftl">

<h4>Renkaat ja jakelu</h4>

<div class="services">
<h5>Jakelun visualisointi</h5>
<form action="${baseURL}/jakelu" method="get">
 	<ul class="fieldBox fewFields">
 	<li><label for="distributionVisualization.ringer">Rengastaja</label> <@select "distributionVisualization.ringer" /> </li> 
 	<li> <input type="submit" value="Näytä" class="resourceSelectSubmit" /> </li>
 	</ul>
</form>

<h5>Kenelle tämä on jaettu?</h5>
<form action="${baseURL}/kenelle-jaettu" method="get">
 	<ul class="fieldBox fewFields">
 	<li><label for="distributed.ringStart">Rengas</label> <@input "distributed.ringStart" /></li>
 	<li>  <input type="submit" value="Kerro"  class="resourceSelectSubmit" /> 
 	</ul>
</form>

<h5>Palautus</h5>
<form action="${baseURL}/palautus" method="post">
 	<ul class="fieldBox fewFields">
 	<li><label for="returnRings.ringer">Rengastaja</label> <@select "returnRings.ringer" /> </li> 
 	<li><label for="returnRings.ringStart">Rengasväli</label> <@input "returnRings.ringStart" /> - <@input "returnRings.ringEnd" /> </li>
 	<li> <input type="submit" value="Poista jakelusta" /> </li>
 	</ul>
</form>

<h5>Tuhoutuneeksi/hävinneeksi merkitseminen</h5>
<form action="${baseURL}/tuhoutunut-havinnyt" method="get">
 	<ul class="fieldBox fewFields">
 	<li><label for="th__event.ringer">Rengastaja</label> <@selectWithID "th" "event.ringer" /> </li> 
 	<li><label for="th__event.ringStart">Rengasväli</label> <@inputWithID "th" "event.ringStart" /> - <@inputWithID "th" "event.ringEnd" /> </li>
 	<li><label for="th__event.ringNotUsedReason">Syy</label> <@selectWithID "th" "event.ringNotUsedReason" /> </li>
 	<li><label for="th__event.eventDate">Pvm</label> <@dateWithID "th" "event.eventDate" /> </li>
 	<li> <input type="submit" value="Merkitse" /> </li>
 	</ul>
</form>
</div>

<div class="services">
<h5>Jaettavissa olevat renkaat</h5>
<form action="${baseURL}/ring/haku" method="post">
 	<ul class="fieldBox fewFields">
 	<li><label for="ring.ringStart">Rengasväli</label> <@input "ring.ringStart" /> - <@input "ring.ringEnd" /> </li>
 	<li>  <input type="submit" name="search" value="Etsi" /> <a href="${baseURL}/ring/uusi" >&raquo; Renkaiden lisäykseen</a> </li>
 	</ul>
</form>

<h5>Jakelu</h5>
<form action="${baseURL}/distributed/haku" method="post">
 	<ul class="fieldBox fewFields">
 	<li><label for="distributed.distributedTo">Rengastaja</label> <@select "distributed.distributedTo" /> </li> 
 	<li><label for="distributed.ringStart">Rengasväli</label> <@input "distributed.ringStart" /> - <@input "distributed.ringEnd" /> </li>
 	<li><label for="distributed.distributedDate">Jakopvm</label> <@desiredInput "distributed.distributedDate" /> </li>
 	<li>  <input type="submit" name="search" value="Etsi" /> <a href="${baseURL}/distributed/uusi" >&raquo; Jakelun lisäykseen</a> </li>
 	</ul>
</form>

<h5>Väritunnisteiden jakelu</h5>
<form action="${baseURL}/fieldReadable" method="get">
 	<ul class="fieldBox fewFields">
 	<li><label for="fieldReadable.distributedTo">Rengastaja</label> <@select "fieldReadable.distributedTo" /> </li> 
 	<li><label for="fieldReadable.code">Tunnus</label> <@input "fieldReadable.code" /> </li>
 	<li><label for="fieldReadable.mainColor">Tausta</label> <@select "fieldReadable.mainColor" /> </li>
 	<li><label for="fieldReadable.forSpecies">Laji</label> <@select "fieldReadable.forSpecies" /> </li>
 	<li><label for="fieldReadable.distributedDate">Jakopvm</label> <@desiredInput "fieldReadable.distributedDate" /> </li>
 	<li><label for="fieldReadable.doubleDistributedCount">Jakolkm (tuplat)</label> <@desiredInput "fieldReadable.doubleDistributedCount" /> </li>
 	<li>  <input type="submit" name="search" value="Etsi" /> <a href="${baseURL}/fieldReadable/uusi" >&raquo; Värijakelun lisäykseen</a> </li>
 	<li> <a href="${baseURL}/fieldReadableUpload">&raquo; Värirenkaiden lisäys/muokkaus tiedostolla</a> </li>
 	</ul>
</form>
</div>

<div class="services">
<h5>Sopivuudet lajeille</h5>
<form action="${baseURL}/series" method="get">
 	<ul class="fieldBox fewFields">
 	<li><label for="series.series">Sarja</label> <@select "series.series" /> </li>
 	<li><label for="series.species">Laji</label> <@select "series.species" />  </li>
 	<li>  <input type="submit" name="search" value="Etsi" /> <a href="${baseURL}/series/uusi" >&raquo; Sopivuuksien lisäykseen</a> </li>
 	</ul>
</form>

<@searchInstructions />

</div>




<#include "luomus-footer.ftl">