<#include "luomus-header.ftl">
<#include "macro.ftl">

 <div style="float: right; white-space: nowrap;">
 	<form action="${baseURL}/loydos-tuonti" method="get">
 	<table class="largeTable optionsTable">
 	<tr><th><h6>Näytä...</h6></th></tr>
 	<tr><td><input type="checkbox" name="onlyTypes" <#if onlyTypes?seq_contains("received")>checked="checked"</#if> value="received" id="onlyReceived"><label for="onlyReceived">Vastaanotetut</label> </td></tr>
 	<tr><td><input type="checkbox" name="onlyTypes" <#if onlyTypes?seq_contains("deleted")>checked="checked"</#if> value="deleted" id="onlyDeleted"><label for="onlyDeleted">Poistetut</label> </td></tr>
 	<tr><td>Sivu <input type="number" id="loydosPaging" name="page" value="${pagingParam}" min="1" /> / ${pageCount}</td></tr>
 	<tr><td><input type="submit" value="Päivitä" /> </td></tr>
 	</table>
 	</form>
 </div>
 
 <h4>Tuo löydöksestä</h4>
 
 

 <div class="clear"></div>
 
 <#if forms?has_content>
 	<table class="largeTable loydosTable">
 		<caption>
 			<#if !onlyTypes?has_content>
 				<h5>Lomakkeet joita ei ole vastaanotettu tai poistettu</h5>
 			<#else>
 				<h5>
 					Lomakkeet jotka on 
 					<#if onlyTypes?seq_contains("received")> vastaanotettu
 					<#if onlyTypes?seq_contains("deleted")> ja poistettu </#if>
 				<#elseif onlyTypes?seq_contains("deleted")> poistettu </#if>
 				</h5>
 			</#if>	
 		</caption>
 		<thead>
 			<tr>
 				<th>Tiedot</th>
 				<th>Lähetysaika</th>
 				<th>Renkaan tiedot</th>
 				<th>Paikka</th>
 				<th>Lähettäjä</th>
 				<th>Kuvat</th>
 				<th>Kommentit</th>
 				<th>&nbsp;</th>
 			</tr>
 		</thead>
 		<tbody>
 		<#list forms as form>
 			<tr>
 				<td><button id="${form.id}" class="showInfoButton">Näytä</button></td>
 				<td>${form.submissionTime}
 					<#if !form.validated><div class="error">Validoimaton</div></#if>
 					<#if form.received><div class="warning">Vastaanotettu</div></#if>
 					<#if form.deleted><div class="warning">Poistettu</div></#if>
 				</td>
 				<td style="max-width: 200px">${(form.ringInfo!"")?html}</td>
 				<td><#if form.row.get("event.municipality").value?has_content>${form.row.get("event.municipality").value?html}<#else>${form.row.get("event.locality").value?html}</#if></td>
 				<td>${(form.submitter.fullName!"")?html}</td>
 				<td>
 					<#list form.attachments as attachment>
 						<a href="${loydosURI}auth?username=${loydosUsername}&amp;password=${loydosPassword}&amp;next=admin%2Fapi%2Fsubmissions%2Frengasloyto%2F${form.id}%2Fattachments%2F${attachment}"
 						target="kuva">Kuva</a>
 					</#list>
 				</td>
 				<td><input class="notesInput" type="text" value="${(form.tipuNotes!"")?html}" /><button class="button saveNotesButton" data-id="${form.id}">Tallenna</button></td>
 				<td style="white-space: nowrap;">
 					<a href="${baseURL}/loydos-tuonti?tapaamiseen=${form.id}" class="button loydosActionButton">Vie tapaamisiin</a>
 					<a target="ulko" href="${baseURL}/loydos-tuonti?ulkoon=${form.id}" class="button loydosActionButton">Vie ULKOon</a>
 					<#if (form.submitter.email)?has_content>
 						<a href="${baseURL}/loydos-tuonti?email=${form.id}" class="button loydosActionButton">Email</a>
 					<#else>
 						<span class="button button-disabled">Email</span>
 					</#if>
 					<a href="#" class="button removeButton" data-id="${form.id}">Poista</a>
 				</td>
 			</tr>
		 </#list>
		 </tbody>
 	</table>
 <#else>
 	<div class="warning">Ei mitään näytettävää!</div>
 </#if>
 
 <div style="display: none;">
 <#list forms as form>
 	<@loydosInfo form />	
 </#list>
 </div>
 
 
 <script>
 	$('.removeButton').click(function(){
 		if (!confirm('Merkitäänkö varmasti poistetuksi?')) return false;
 		var formId = $(this).data('id');
 		var button = $(this);
 		$.post("${baseURL}/loydos-tuonti?remove="+formId, function( data ) {
			if (data == 'ok') {
				button.closest('tr').hide(200); 
			} else { 
				alert('Poisto epäonnistui!'); 
			}
		}).fail(function() {
			alert('Poisto epäonnistui!');
		});
 		return false;
 	});
 	var infoDialogOpen = null;
 	$('.showInfoButton').on('click', function() {
 		var infoContainerId = $(this).attr('id') + '_infoContainer';
 		if (infoDialogOpen) {
 			var infoContainer = $('#'+infoDialogOpen);
 			var same = infoContainerId === infoDialogOpen;
 			infoContainer.dialog('close');
 			if (same) return;
 		}
		var infoContainer = $('#'+infoContainerId);
		infoContainer.dialog({
			resizable: false,
			left: 150,
			top: 0,
			height: $(document).height(),
   			width: $(document).width()-150,
   			open: function() {
      			$('body').css('overflow','hidden');
      			$(this).focus();
      		},
   			beforeClose: function() {
   				$('body').css('overflow', '');
   				infoDialogOpen = null;
				return true;
			}
    	});
 		infoDialogOpen = infoContainerId;
 	});	
 	$('.saveNotesButton').on('click', function() {
 		var button = $(this);
 		var formId = $(this).data('id');
 		var notes = button.parent().find('.notesInput').val();
 		$.post("${baseURL}/loydos-tuonti?addNotes="+formId+"&notes="+encodeURIComponent(notes), function( data ) {
			if (data == 'ok') {
				button.fadeTo('slow', 0, function() { button.fadeTo('slow', 1); });
			} else { 
				alert('Kommentin tallennus epäonnistui!'); 
			}
		}).fail(function() {
			alert('Kommentin tallennus epäonnistui!');
		});
 		return false;
 	});
 	 $('.loydosActionButton').on('click', function() {
 		$(this).closest('tr').hide(200);
 	});
 </script>
<#include "luomus-footer.ftl">

