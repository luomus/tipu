 
 <h5>Valitse hakukriteerinä käytettävät kentät / hakutulokseen liitettävät kentät</h5>
 
 <table class="largeTable advancedSearchFieldSelectTable">
 	<thead>
 		<tr>
 		<#list rowStructure.getTablesGroupsNames("event") as groupName>
 			<#if groupName != "coordinates">
 			<th>${rowStructure.getGroupDesc("event", groupName)}</th>
 			</#if>
 		</#list>
 		</tr>
 	</thead>
 	<tbody>
 		<tr>
 		<#list rowStructure.getTablesGroupsNames("event") as groupName>
 			<#if groupName != "coordinates">
 			<td>
 				<ul>
 				<#list rowStructure.getTableGroupsFields("event", groupName) as field>
 				    <#if field.name != "event.updateRing">
 					<li>
 						<input type="checkbox" name="${field.name}" id="fieldSelector_${field.name}"
 							<#if resultFieldsNames?? && resultFieldsNames?seq_contains(field.name)>
 								<#if allFields == false || (param(field.name)?length > 0)>
 									checked="checked"
 								</#if>
 							</#if> 
 						/>
 						<label for="fieldSelector_${field.name}">${field.commonName}</label></li>
 					</#if>
 				</#list>
 				</ul>
 			</td>
 			</#if>
 		</#list>
 		</tr>
 	</tbody>
 </table>

 <form action="${baseURL}/haku" method="post" id="advancedSearchForm">
 <input type="hidden" name="showAdvancedSearch" value="true" />
 <input type="hidden" name="resultColumns" id="resultColumnsHiddenInput" value="" />
 
 <h5>Syötä hakukriteerit</h5>
 <table id="searchCriteriaAndSelectedFields">
 	<#if resultFields??>
 		<#list resultFields as field>
 			<#if allFields == false || (param(field.name)?length > 0)>
 				<tr id="${"selectedFieldItem_" + field.name?replace(".", "_")}"><@generateAdvancedSearchField field /></tr>
 			</#if>
 		</#list>
 	</#if>
 </table>
 
  
 <h5>Tulokset...</h5>
 <p><input type="submit" name="toPage" value="Näytölle" /> <input type="submit" name="toFile" value="Tiedostoon" />
 &nbsp;&nbsp;  
 <label for="allFields">Ota hakutulokseen kaikki kentät</label>
 <input type="checkbox" name="allFields" id="allFields" <#if allFields == true>checked="checked"</#if> /> 
 <span class="ui-icon ui-icon-info" title="Suositeltava käytettäväksi vain tulostettaessa tiedostoon."></span> 
 
 </form>
 
 <script>
 $(function() {
	
 	$('.advancedSearchFieldSelectTable :checkbox').on('change', function() {
 		var fieldNameId = 'selectedFieldItem_' + $(this).attr('name').replace('.','_');
 		if ($('#'+fieldNameId).length !== 0) {
 			$('#'+fieldNameId).remove();
 			return;
 		}
 		$.get('${baseURL}/advancedSearchField?fieldName='+$(this).attr('name'), function( data ) {
			$('#searchCriteriaAndSelectedFields').append('<tr id="'+fieldNameId+'">'+data+'</tr>');
			$('#'+fieldNameId +' select').chosen({ search_contains: true });
		});
 	});
 	$.fn.exists = function () {
    	return this !== 0;
	}
	$('#advancedSearchForm').on('submit', function() {
		var selectedFields = '';
		$('#searchCriteriaAndSelectedFields :input').each(function() {
			var name = $(this).attr('name');
			if (name) selectedFields += name + ',';
		});
		$('#resultColumnsHiddenInput').val(selectedFields);
	});
});
</script>
 

 