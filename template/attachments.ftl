<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<script src="${staticURL}/jquery-1.9.1.js"></script>
	<link href="${staticURL}/jquery-ui/css/cupertino/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" /> 
	<script src="${staticURL}/jquery-ui/js/jquery-ui-1.10.2.min.js"></script>
	<link href="${staticURL}/luomus.css?${staticContentTimestamp}" rel="stylesheet" />
	<link href="${staticURL}/rengastus.css?${staticContentTimestamp}" rel="stylesheet" />
	<script>
		$(function() {
			$("input[type=submit], button, .button").button();
			$(".removeAttachmentLinK").on('click', function() {
				var filename = $(this).parent().find('a').first().html();
				return confirm('Poistetaanko ' + filename + '?');
			});
		});
	</script>
</head>

<body id="main-area">
<h5>Tapauksen liitetiedostot</h5>

<ul id="attachmentsList">
	<#if !attachments?has_content>
		<li>Ei liitteitä</li>
	</#if>
	<#list attachments as attachment>
		<li><a target="_blank" href="${baseURL}/attachments/show/${attachment.fileId}">${attachment.fileName}</a> &mdash; <a href="${baseURL}/attachments/delete/${attachment.fileId}" class="removeAttachmentLinK">poista</a></li>
	</#list>
</ul>

<div class="newFileContainer">
	<h6>Lisää uusi liite</h6>
	<form action="${baseURL}/attachments/${eventId}" method="POST" enctype="multipart/form-data" onsubmit="return validateAttachment()">
		<input type="file" name="attachment" id="attachmentSelect" />	
		<input type="submit" value="Lisää" />
		<div id="error" class="error hidden">&nbsp;</div>
	</form>
	<#if error?has_content><div class="message error">${error}</div><#assign hasErrors=true /></#if>
</div>

<script>
	function validateAttachment() {
		var file = $("#attachmentSelect").val();
		if (!file) {
			$("#error").html('Valitse tiedosto!').show();
			return false;
		}
		return true;
	}
</script>


</body>
</html>
