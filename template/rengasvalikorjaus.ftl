<#include "luomus-header.ftl">
<#include "macro.ftl">

<h4>Väärin ilmoitetun rengasvälin korjaus</h4>

<br />

<#if done??>
   <p><a href="${baseURL}/rengasvalikorjaus">Korjaa uusi väli</a></p>
   <p><a href="${baseURL}/etusivu">Etusivulle</a></p>
   
   <#if successRows??>
   	<h5>Seuraavat rivit päivitettiin onnistuneesti:</h5>
<pre>
${successRows?html}
   </pre>
   </#if>
   
<#else>

	<form action="${baseURL}/rengasvalikorjaus" method="post" enctype="multipart/form-data" class="fieldBox">
		<label>Sarja:</label> 			<input type="text" name="series" />	<br/>
		<label>Nykyinen väli:</label> 	<input type="number" name="currentStart" /> -	<input type="number" name="currentEnd" />	<br/>
		<label>Oikea väli:</label>		<input type="number" name="newStart" />	-		<input type="number" name="newEnd" />		<br/> <br/>
		<input type="submit" value="Korjaa" />
	</form>
	
</#if>

<#include "luomus-footer.ftl">