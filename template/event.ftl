<#include "luomus-header.ftl">
<#include "macro.ftl">

<div class="eventPikahaku">
<h5>Pikahaku</h5>
<form action="${baseURL}/haku" method="post">
 	<ul class="fieldBox fewFields">
 	<li> <input type="text" name="event.diario" placeholder="Diario" size="8" /> <span class="ui-icon ui-icon-info" title="Voi käyttää %"></span> </li> 
 	<li> <input type="text" name="event.legRing" placeholder="Jalkarengas" size="11" /> <span class="ui-icon ui-icon-info" title="Voi käyttää % mutta silloin renkaan oltava muodossa XX001234% (X 1234% ei toimi, mutta toisaalta X%1234% toimii)"></span> </li>
 	<li> <input type="submit" value="Hae" /> </li>
 	<li><a href="${baseURL}/uusi-rengastus">&raquo; Uusi rengastus</a></li>
 	<li><a href="${baseURL}/uusi-tapaaminen">&raquo; Uusi tapaaminen</a></li>
 	</ul>
 </form>
</div>

<iframe id="eventAttachments" src="${baseURL}/attachments/${params.get("event.id").value}"></iframe>
<script>
$(document).ready(function(){
    $("#eventAttachments").load( function () {
        $(this).height($(this).contents().height());
    });
});
</script>

<div class="clear"></div>

<#if params.get("event.type").value == "ringing">
	<#assign typeText = "Rengastuksen"/>
	<#assign idByType = params.get("event.nameRing").value>
<#else>
	<#assign typeText = "Tapaamisen"/>
	<#assign idByType = params.get("event.diario").value>
</#if>

<#if action == "UPDATE">
 	<h5>${typeText} ${idByType} muokkaus</h5>
	<form action="${baseURL}/muokkaa/${params.get("event.id").value}" method="post" style="" id="eventForm" class="mainSubmitForm">
 <#elseif loydosForm??>
 	<h5>Tapaamisen tuonti Löydöksestä</h5>
 	<form action="${baseURL}/uusi" method="post" id="eventForm" class="mainSubmitForm">
 	<input type="hidden" name="loydosID" value="${loydosForm.id}" />
 	<input type="hidden" name="loydosNotes" value="${(loydosForm.tipuNotes!"")?html}" />
 <#else>
 	<h5>${typeText} lisäys</h5>
 	<form action="${baseURL}/uusi" method="post" id="eventForm" class="mainSubmitForm">
 </#if>

 <#if acceptWarnings??>
 	<input type="hidden" name="acceptWarnings" value="true" />
 </#if>

 <#if fromBin??>
 	<input type="hidden" name="fromBin" value="true" />
 </#if>

 <#if (action == "INSERT" && hasErrors!false) || fromBin??>
 	<#if fromBin??>
 		<div>
 			<button id="toWaitingBin" name="toWaitingBin">Palauta muokattuna odottaviin <#if params.get("event.type").value == "ringing">rengastuksiin<#else>tapaamisiin</#if></button>
 			<button id="toTrashBin" name="toTrashBin">Palauta muokattuna <#if params.get("event.type").value == "ringing">rengastuksien<#else>tapaamisten</#if> roskikseen</button>
 			<button id="toDeletedBin" name="toDeletedBin">Palauta muokattuna poistettuihin <#if params.get("event.type").value == "ringing">rengastuksiin<#else>tapaamisiin</#if></button>
 			<a href="${baseURL}/etusivu" class="button">Poistu tekemättä mitään</a>
 		</div>
 	<#else>
 		<div>
 			<button id="toWaitingBin" name="toWaitingBin">Siirrä odottaviin <#if params.get("event.type").value == "ringing">rengastuksiin<#else>tapaamisiin</#if></button>
 			<button id="toTrashBin" name="toTrashBin">Siirrä <#if params.get("event.type").value == "ringing">rengastuksien<#else>tapaamisten</#if> roskikseen</button>
 			<button id="toDeletedBin" name="toDeletedBin">Siirrä poistettuhin <#if params.get("event.type").value == "ringing">rengastuksiin<#else>tapaamisiin</#if> </button>
 		</div>
 	</#if>
 	<textarea name="binMoveNotes" rows="2" cols="60" placeholder="Roskiskommentit"><#if binMoveNotes??>${binMoveNotes!""}</#if></textarea>
 	<div class="clear">&nbsp;</div>
 </#if>
 
 <#if history??>
 <#assign deadConditions = ["1", "2", "3"]>
 <#list history as row>
 	<#if deadConditions?seq_contains(row.get("event.birdConditionEURING").value)>
 		<p>Lintu ilmoitettu kuolleeksi</p>
 	</#if>
 </#list>
 </#if>
 
 <div id="eventGrid">

 
 <#if loydosForm??>	
 	<#if action == "UPDATE">
 		<@loydosInfo loydosForm "eventUpdate" />
 	<#else>
  		<@loydosInfo loydosForm "eventInsert" />
  	</#if>	
 </#if>
 
 <#if history??>
 <div class="birdHistory fieldGroup">
 	<h5>Linnun historia</h5>
 	<table class="largeTable">
 		<tr>
 			<th>Diario</th>
 			<th>Jalkar.</th>
 			<th>Vaihtor.</th>
 			<th>Värit.</th>
 			<th>Tausta</th>
 			<th>Laji</th>
 			<th>Kunto</th>
 			<th>Pvm</th>
 			<th>Paikka</th>
 			<th>Rengastaja</th>
 			<th>Maallikko</th>
 			<th>Aika, matka, suunta</th>
 			<th>&nbsp;</th>
 			<th>&nbsp;</th>
 		</tr>
 	<#assign okToRemove = false>
	<#list history as row>
		<#if row.get("event.type").value == "recovery">
			<#assign okToRemove = !row.get("event.newLegRing").hasValue()>
			<tr <#if params.get("event.id").value == row.get("event.id").value>class="highlight"</#if>>
				<td><a href="${baseURL}/muokkaa/${row.get("event.id").value}">${row.get("event.diario").value}</a></td>
 				<td>${row.get("event.legRing").value}</td>
 		<#else>
 			<tr <#if params.get("event.id").value == row.get("event.id").value>class="highlight"</#if>>
 				<td>&nbsp;</td>
 				<td><a href="${baseURL}/muokkaa/${row.get("event.id").value}">${row.get("event.legRing").value}</a></td>
 		</#if>
 		<#if !row_has_next><#assign okToRemove = true></#if>
 			<td>${row.get("event.newLegRing").value}</td>
 			<td>${row.get("event.fieldReadableCode").value}</td>
 			<td><@plaintextValueForRow row "event.mainColor" /></td>
 			<td><@plaintextValueForRow row "event.species" /> </td>
 			<td><@plaintextValueForRow row "event.birdConditionEURING" /></td>
 			<td>${row.get("event.eventDate").value}</td>
 			<td><#if row.get("event.municipality").hasValue()><@plaintextValueForRow row "event.municipality" /><#else><@plaintextValueForRow row "event.countryCode" /> <@plaintextValueForRow row "event.province" /></#if></td>
 			<td><@plaintextValueForRow row "event.ringer" /></td>
 			<td>${row.get("event.layman").value}</td>
 			<td><#if row.get("event.type").value == "recovery"> 
 				${row.get("event.timeToRingingInDays").value}d
 				${row.get("event.distanceToRingingInKilometers").value}km
 				${row.get("event.directionToRingingInDegrees").value}°
 				</#if>
 			</td>
 			<td><a href="${baseURL}/kopioi/${row.get("event.id").value}">Kopioi</a></td>
 			<#if okToRemove>
 				<td><a href="${baseURL}/poista/${row.get("event.id").value}" onclick="return confirm('Poistetaanko varmasti?')">Poista</a></td>
 			<#else>
 				<td>&nbsp;</td>
 			</#if>
		</tr>
	</#list>
	</table>
  </div>
</#if>

 <#list rowStructure.getTablesGroupsNames("event") as groupName>
 	<#if params.get("event.type").value == "recovery" || groupName != "calculatedValues"> <#-- Don't show calculatedValues for ringing -->
 	<div class="fieldGroup">
 		<h5>${rowStructure.getGroupDesc("event", groupName)}</h5>
 		<ul class="fieldBox">
 			<#if params.get("event.type").value == "recovery">
 				<@printRecoveryFields groupName />
			<#else>
				<@printRingingFields groupName />
 			</#if>
 			<#if groupName != "calculatedValues" && groupName != "convertedCoordinates" && groupName != "additionalPlaceInfo">
 				<li><input type="submit" value="Tallenna" /></li>
 			</#if>
 			<#if groupName == "coordinates">
 				<li>
 					<button id="mapButton">Kartta</button>
 				</li>
 			</#if>
		</ul>
	</div>
	</#if>
</#list>

<#macro printRingingFields groupName>
	<#list rowStructure.getTableGroupsFields("event", groupName) as field>
		<#if field.showForRingingMode() && field.name != "event.id" && field.name != "event.updateRing">
			<#if field.inputForRingingMode() && (action == "INSERT" || (field.name != "event.ringStart" && field.name != "event.ringEnd")) > <#-- Don't allow edit of ringStart/End for update -->
				<li><label for="${field.name}" title="${field.desc?html}">${field.commonName}</label> <@desiredInput field.name /> </li>
			<#else>
				<li><label title="${field.desc?html}">${field.commonName}</label> <@plaintextValue field.name /> <@hidden field.name /> </li>
			</#if>
		<#else>
			<li class="hidden"><@hidden field.name /></li>								
		</#if>									
	</#list>
</#macro>

<#macro printRecoveryFields groupName>
	<#list rowStructure.getTableGroupsFields("event", groupName) as field>
		<#if field.showForRecoveryMode() && field.name != "event.id" >
			<#if field.inputForRecoveryMode()>
				<li><label for="${field.name}" title="${field.desc?html}">${field.commonName}</label> <@desiredInput field.name /> </li>
			<#else>
				<li><label title="${field.desc?html}">${field.commonName}</label> <span><@plaintextValue field.name /></span> <@hidden field.name />
					<#if field.name == "event.diario" && action == "INSERT" && params.get("event.diario").value?has_content><button onclick="return clearDiario(this);">Tyhjennä</button></#if> 
				</li>
			</#if>
		<#else>
			<li class="hidden"><@hidden field.name /></li>								
		</#if>
	</#list>

	<#if groupName == "persons">
		<li><button id="eventMallikkoEditButton">Maallikon muokkaus/lisäys</button></li>
		<li><div id="eventMallikkoEdit" class="hidden">
				<h5>Mallikon muokkaus/lisäys</h5>
				<div>
					<ul class="fieldBox">
						<#list rowStructure.getTableFields("layman") as field>
							<#if field.name == "layman.id">
								<li><label title="Uuden maallikon numero generoidaan automaattisesti">Numero</label> ${params.get("layman.id").value} <@hidden "layman.id" /> </li>
							<#elseif field.name != "layman.created" && field.name != "layman.modified" && field.name != "layman.modifiedBy" && field.name != "layman.rowid">
								<li><label for="${field.name}" title="${field.desc?html}">${field.commonName}</label> <@desiredInput field.name /> </li>
							</#if>
						</#list>
					</ul>
				</div>
			</div>
		</li>
	</#if>
	
</#macro>


</div>
<div class="clear"></div>


</form>

<div id="mapContainer" style="height: 500px; width: 800px;">
	<iframe style="width: 100%; height: 98%; margin: 0; padding: 0; border-style: none;" id="mapframe" name="map" src="https://kielo.luomus.fi/wkartta/map.html"></iframe>
</div>


<script>
	
	 $('#toWaitingBin').button({ icons: { primary: "ui-icon-folder-open"} });
	 $('#toTrashBin').button({ icons: { primary: "ui-icon-trash"} });
	 $('#toDeletedBin').button({ icons: { primary: "ui-icon-disk"} });
	  
	 var clearValueClearContentText = function(e) {
	 	$(this).val(''); // clear input value
	 	$(this).parent().contents().filter(function(){return this.nodeType == 3;})[0].nodeValue = ''; // clear content text for hidden input values 
	 }
	 
	 $('#eventMallikkoEditButton').click(function(){ 
	 	$('#eventMallikkoEdit').removeClass('hidden').dialog({
	 		width:600, height: 650, 
	 		resizable: false,
        	modal: true,
	 		appendTo: "#eventForm", 
	 		buttons: {
        		'Sulje': function() { $(this).dialog('close'); },
        		'Tyhjennä': function() {
        			$('input[name^="layman."], select[name^="layman."], textarea[name^="layman."]').each(clearValueClearContentText);
        			$('input[name^="event.layman"]').each(clearValueClearContentText);
        		}
        	}
        });
	 	return false; 
	 });
	 
	 $("#mapContainer").dialog({
		autoOpen: false,
		height: 600,
		width: 900,
		resizable: false,
        modal: true,
		show: { effect: "fadeIn", duration: 300 },
		hide: { effect: "fadeOut", duration: 300},
		buttons: {
        	'Sulje': function() { $(this).dialog('close'); }
        }
	});
	
	
	var map;
	
	$('#mapButton').on('click', function() {
		try {
			$('#mapContainer').dialog('open');
			
			// Initialize map
			var mapElement = document.getElementById('mapframe');
    		map = new (new WK()).Map(mapElement, {
        		single: true, annotate: false, mapTypes: ['osm','mm'],
        		mapOptions: { scale: true, center: [65,26], maxBounds: null, maxZoom: 16, minZoom: 3, zoom: 5 }
    		});
   			updateMap();
   			map.onChange(mapClicked);
		} catch(err) {
			alert(err);
		}
		return false;
	});
	
	function updateMap() {
		var lat = $('#event\\.lat').val().replace(',', '.');
   		var lon =  $('#event\\.lon').val().replace(',', '.');
   		var system = $('#event\\.coordinateSystem').val();
   		if (lat && lon && system) {
   			if (system == 'WGS84') {
   				map.update(JSON.parse('[{"locationType": "POINT","location": ['+lat+','+lon+']}]'));
   			} else {
   				$.get('${baseURL}/convert-coordinates?lat='+encodeURIComponent(lat)+'&lon='+encodeURIComponent(lon)+'&system='+encodeURIComponent(system))
   				.done(function(data) {
   					if (data['conversion-response'].pass === false) { alert('invalid coordinates'); return; }
   					var wgs84Lat = data['conversion-response'].wgs84.lat;
   					var wgs84Lon = data['conversion-response'].wgs84.lon;
   					map.update(JSON.parse('[{"locationType": "POINT","location": ['+wgs84Lat+','+wgs84Lon+']}]'));
   				})
   				.fail(function() {
   					alert("error");
   				});
   			}
   		}
	}
	
	function mapClicked(entries) {
		if (!confirm('Asetetaanko paikka lomakkeelle?')) return;
		var lat = String(entries[0]["location"][0]).replace('.', ',').substring(0, 10);
		var lon = String(entries[0]["location"][1]).replace('.', ',').substring(0, 10);
		$('#event\\.lat').val(lat);
   		$('#event\\.lon').val(lon);
   		$('#event\\.coordinateSystem').val('WGS84').trigger('liszt:updated');
   		$('#event\\.coordinateMeasurementMethod').val('KARTTA').trigger('liszt:updated');
   		$('#event\\.coordinateAccuracy').val('1').trigger('liszt:updated');
    }
 
    $('#event\\.birdStation').on('change', function() {
    	var birdStationValue = $(this).val();
    	if (!birdStationValue) return;
    	$.get('${baseURL}/coordinates-by-bird-station?bird-station='+birdStationValue)
   				.done(function(data) {
   					var override = null;
   					$.each(data, function(key, val) {
   						if ($('#event\\.'+key).val() && override === null) {
   							override = confirm('Tietoja on jo annettu. Yliajetaanko ne lintuaseman tiedoilla?');
   						}
   					});
   					if (override === false) return;
   					$.each(data, function(key, val) {
   						$('#event\\.'+key).val(val).trigger('liszt:updated');
   					});
   				})
   				.fail(function() {
   					alert("error");
   				});
    });
    
    function clearDiario(e) {
    	if (!confirm('Tyhjennetäänkö roskiksesta tuotu diarionumero? Yleensä näin halutaan tehdä vain jos kyseinen diario on jo käytetty.')) return false;
    	$(e).closest('li').find('span').html('');
    	$(e).closest('li').find('input').val('');
    	$(e).hide();
    	return false
    }
</script>


					
<#include "luomus-footer.ftl">