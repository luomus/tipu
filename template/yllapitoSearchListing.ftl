<#include "luomus-header.ftl">
<#include "macro.ftl">

<div class="yllapito">

<h4>Hakutulokset</h4>

<a href="${baseURL}/${resourceName}/haku">&raquo; Uusi haku</a> 

<table class="largeTable" id="resultTable">
	<thead>
		<tr>
			<#list rowStructure.getTableFields(resourceName) as field>
				<#if field.name?ends_with(".rowid")>
				<#elseif field.idField>
					<th class="idField" title="${field.desc?html}">${field.commonName?html}</th>
				<#else>
					<th title="${field.desc?html}">${field.commonName?html}</th>
				</#if>
			</#list>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	<#list rows as row>
		<tr>
			<#list rowStructure.getTableFields(resourceName) as field>
				<#if field.name?ends_with(".rowid")>
				<#elseif field.idField>
					<td class="idField">
						<a href="${baseURL}/${resourceName}/${row.get(resourceFieldName).value}" class="button"><@plaintextValueForRow row field.name /></a>
					</td>
				<#else>
					<td><@searchTableValueForRow row field.name /></td>
				</#if>
			</#list>
			<td>
				<a href="${baseURL}/${resourceName}/${row.get(resourceFieldName).value}" class="button">Muokkaa</a>
				<a href="${baseURL}/${resourceName}/poista/${row.get(resourceFieldName).value}" class="button" onclick="return confirm('Poistetaanko varmasti?');">Poista</a>
			</td>
		</tr>
	</#list>
</table>
<script>
	$(document).ready(function() {
    	$('#resultTable').dataTable({"language": {"decimal": ","}, "iDisplayLength": 50, "aaSorting": []});
	});
 </script>
</div>

<a href="${baseURL}/${resourceName}/haku">&raquo; Uusi haku</a> 

<#include "luomus-footer.ftl">