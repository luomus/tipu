<#include "luomus-header.ftl">
<#include "macro.ftl">

<#if failedEmails?? && failedEmails?has_content>
	<div class="message error">
		<p>Seuraaviin osoitteisiin ei yritetty lähettää emailia koska osoite on epäkelpo</p>
		<#list failedEmails as email>${email} </#list>
	</div>
</#if>
 
<h4>Massapostitus</h4>

<div class="services">
<form action="${baseURL}/massapostitus" method="post">
 	<ul class="fieldBox fewFields">
 	
 	<li><label for="massEmail.receiverType" title="Valitse joko ryhmä tai voit antaa listan rengastajista">Vastaanottajat (ryhmät)</label> 
 		<@desiredInputWithID "" "massEmail.receiverType" "Valitse..." true /> 
 	</li>
 	
 	<li><label for="massEmail.receiverList" title="Voit valita ryhmän tai antaa pilkulla erotellun listan rengastajanumeroista">Rengastaja- numerot (pilkulla eroteltuna)</label> 
 		<@desiredInputWithID "" "massEmail.receiverList" /> 
 	</li>
 	<li><hr /></li>
 	<li><label for="massEmail.subject">Otsikko</label> 
 		<@input "massEmail.subject" /> 
 	</li> 
 	
 	<li><label for="massEmail.message">Viesti</label> 
 		<@textarea "massEmail.message" />  
 	</li>
 	<li>  <input type="submit" value="Lähetä" onclick="return confirm('Lähetetäänkö varmasti?');" /> </li>
 	</ul>
</form>
</div>



<#include "luomus-footer.ftl">