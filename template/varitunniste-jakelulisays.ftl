<#include "luomus-header.ftl">
<#include "macro.ftl">

<h4>Väritunnisteiden lisäys/muokkaus tiedostosta</h4>

<form action="${baseURL}/fieldReadableUpload" method="post" enctype="multipart/form-data">

<ol id="fieldReadableUploadInstructions">
	<li>	Lataa <a href="${staticURL}/Lisäys_väritunnisteiden_jakeluun.xlsx">Lisäys_väritunnisteiden_jakeluun.xlsx</a>	</li>
	<li>	Sarakkeiden täytyy olla seuraavanlaiset: <br /> <img src="${staticURL}/img/jakelu_listays_columns.png" alt="" /> </li>
	<li>	Tallenna tab separated -muodossa: <br /> <img src="${staticURL}/img/jakelu_listays_save.png" alt="" />	</li>
	<li>	Tiedostossa saa olla otsakerivi, jonka täytyy alkaa "Merkit" </li>
	<li>	Jos tiedostossa on [merkit, väri, laji] yhdistelmä, joka on jo tietokannassa, kyseinen rivi päivitetään uusilla tiedoilla. </li>
	<li> 	Valitse tallennettu .txt: <input type="file" name="file" /> <br /> <input type="submit" value="Lisää" /></li> 
</ol>	

</form>

<#include "luomus-footer.ftl">