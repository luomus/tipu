<#include "luomus-header.ftl">
<#include "macro.ftl">

<div class="yllapito">

<#if addNew??>
<h4>${resourceCommonName} lisäys</h4>
<form action="${baseURL}/${resourceName}/uusi" method="post" class="mainSubmitForm">
<#else>
<h4>${resourceCommonName} muokkaus</h4>
<form action="${baseURL}/${resourceName}/muokkaa" method="post" class="mainSubmitForm">
<a href="${baseURL}/${resourceName}/kopioi?<#list resourceIdFieldNames as idField>${idField}=${params.get(idField).value?url}<#if idField_has_next>&amp;</#if></#list>" class="button">Kopioi</a>
</#if>
 	
 	<#list rowStructure.getTablesGroupsNames(resourceName) as groupName>
 		<div class="fieldGroup">
 		<h5>${rowStructure.getGroupDesc(resourceName, groupName)}</h5>
 		<ul class="fieldBox">
 			<#list rowStructure.getTableGroupsFields(resourceName, groupName) as field>
 				<#if field.name?ends_with(".rowid")> <li class="hidden"> <@hidden field.name /> </li>
 				<#elseif resourceIdFieldNames?seq_contains(field.name) && !addNew??>
 					<li><@hidden field.name /> <label for="${field.name}" title="${field.desc?html}">${field.commonName}</label> <@plaintextValue field.name /> </li>		 
 				<#elseif groupName == "dataHistory">
 					<li><label title="${field.desc?html}">${field.commonName}</label> <@plaintextValue field.name /> </li>
 				<#else>
 					<li><label for="${field.name}" title="${field.desc?html}">${field.commonName}</label> <@desiredInput field.name /> </li>
 				</#if>
 			</#list>
 			<li><input type="submit" value="Tallenna" /></li>
 		</ul>
 		</div>
 	</#list>
</form>

<hr />
<a href="${baseURL}/${resourceName}/kopioi?<#list resourceIdFieldNames as idField>${idField}=${params.get(idField).value?url}<#if idField_has_next>&amp;</#if></#list>&amp;${listingByFieldName}=${params.get(listingByFieldName).value?url}" class="button">Kopioi</a>
<span style="display: inline-block; width: 90px;">&nbsp;</span>
<a href="${baseURL}/${resourceName}/poista?<#list resourceIdFieldNames as idField>${idField}=${params.get(idField).value?url}<#if idField_has_next>&amp;</#if></#list>&amp;${listingByFieldName}=${params.get(listingByFieldName).value?url}" class="button" onclick="return confirm('Poistetaanko varmasti?');">Poista</a>


</div>
<#include "luomus-footer.ftl">