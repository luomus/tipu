<#include "luomus-header.ftl">
<#include "macro.ftl">

 <h4>SQL</h4>
 
 <div style="float: left;">
 
 <form id="sqlForm" action="${baseURL}/sql" method="post">
 
 <textarea id="sqlTextArea" style="padding: 10px; width:700px;" name="sql" rows="20"  <@error "sql" /> >${sql}</textarea>
 
 <br />
 
 <input type="submit" name="toDisplay" value="Näytölle" />
 <input type="submit" name="toFile" value="Tiedostoon" onclick="setFormTarget();"/>
 
 </form>
 
 <#if queryResults??>
 	<table id="sqlResultTable" class="largeTable">
 	<thead>
 		<tr>
 			<#list colNames as col>
 				<th>${col!""}</th>
 			</#list>
 		</tr>
 	</thead>
 	<tbody>
 	<#list queryResults as row>
 		<tr>
 		<#list row as col>
 			<td>${col!""}</td>
 		</#list>
 		</tr>
 	</#list>
 	</tbody>
 	</table>
 </#if>
 
 <h5>Raportit</h5>
 <ul class="reports">
 <#list reports as report>
 	<li>
 		${report.name}
 		<form action="${baseURL}/sql" method="post" target="_blank">
  		<input type="hidden" name="reportId" value="${report.id}" />
 		<#list report.distinctParameters as parameter>
 			<@desiredInputWithID ("report"+report.id) parameter.name parameter.commonName false />
 		</#list>
 		<input type="submit" name="toFile" value="Tee raportti" />
 		</form>
 	</li>	
 </#list>
 </ul>
 
 </div>
 
 <div style="float: left; padding-left: 5px; width: 550px;">
 	<h5>Taulut ja kentät</h5>
	<div id="tables">
 		<#list rowStructure.tableNames as tableName>
 			<#if tableName != "distributionVisualization" && tableName != "returnRings">
 			<h6>${tableName}</h6>
 			<div>
 				<p><button class="selectSQLButton">select * from ...</button></p>
 				<div class="hidden selectAllSQL"><@generateSelectAllSQL tableName /></div>
 				<table style="width: 500px; font-size: 70%;">
 					<#list rowStructure.getActualDataTableFields(tableName) as field>
 						<tr><td>${field.dbName}</td><td>${field.commonName}</td><td>${field.dbType}</td></tr>
 					</#list>
 				</table>
 			</div>
 			</#if>
 		</#list>
 	</div>
 </div>
 
 <script>
	$(document).ready(function() {
    	$('#sqlResultTable').dataTable({"iDisplayLength": 50, "aaSorting": [] });
    	$('#tables').accordion({active: false, collapsible: true, header: "h6", heightStyle: "content"});
    	$('#sqlForm').on('submit', function() {
			if (formTarget) {
				$('#sqlForm').prop("target", formTarget);
			}
			$('#sqlForm').submit();
			return false;
		});
		$('.selectSQLButton').on('click', function() {
			if (!confirm('Yliajetaanko SQL select * from -lauseella?')) return;
			var sql = $(this).closest('div').find('.selectAllSQL').first().text();
			$('#sqlTextArea').val(sql);
		});
	});
	
	var formTarget = undefined;
	
	function setFormTarget() {
		formTarget = '_blank';
	}
	
 </script>

<#macro generateSelectAllSQL tableName>
SELECT   <#list rowStructure.getActualDataTableFields(tableName) as field>${field.dbName}<#if field_has_next>, </#if></#list>
FROM     ${tableName}
</#macro>


<#include "luomus-footer.ftl">