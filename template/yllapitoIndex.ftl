<#include "luomus-header.ftl">
<#include "macro.ftl">

<#macro resourceSelect tableName idName="id">
	<form action="${baseURL}/${tableName}" method="get">
	 	<ul class="fieldBox fewFields">
 			<li> <@select tableName+"."+idName /> <input type="submit" value="Muokkaa" class="resourceSelectSubmit" /> &nbsp; <a href="${baseURL}/${tableName}/haku" >&raquo; Haku</a> </li> 
 			<li> <a href="${baseURL}/${tableName}/uusi" >&raquo; Luo uusi</a>  </li>
 		</ul>
 	</form>
</#macro>

<div class="yllapito">


 <h4>Ylläpito</h4>

 <div class="services">
 	<h5>Henkilöt</h5>
 	<ul class="fieldBox fewFields">
 		<li>
 			<h6>Rengastajat</h6>
 			<@resourceSelect "ringer" />
 		</li>
 		<li>
 			<h6>Maallikkot</h6>
 			<form action="${baseURL}/layman" method="get">
			 	<ul class="fieldBox fewFields">
		 		<li> <@input "layman.id" "ID" /> <input type="submit" value="Muokkaa" class="resourceSelectSubmit" /> &nbsp; <a href="${baseURL}/layman/haku" >&raquo; Haku</a> </li>
		 		<li> <a href="${baseURL}/layman/uusi" >&raquo; Luo uusi</a>  </li> 
		 		</ul>
 			</form>
 		</li>
 	</ul>
 	
 	<h5>Muut</h5>
 	<ul class="fieldBox fewFields">
 		<li>
 			<h6>Lajit</h6>
 			<@resourceSelect "species" />
 		</li>
 		<li>
 			<h6>Sanasto</h6>
 			<form action="${baseURL}/codes" method="get">
 				<input type="hidden" name="search" value="Etsi" />
	 			<ul class="fieldBox fewFields">
 					<li> <@select "codes.code" /> <input type="submit" value="Muokkaa" /> </li> 
 				</ul>
 			</form>
 		</li>
 	</ul>
 </div>

 
 <div class="services">
 	<h5>Kotimaiset paikat</h5>
 	<ul class="fieldBox fewFields">
 		<li>
 			<h6>Kunnat</h6>
 			<@resourceSelect "municipality" />
 		</li>
 		<li>
 			<h6>Lintuasemat</h6>
 			<@resourceSelect "birdStation" />
 		</li>
 		<li>
 			<h6>ELY-keskukset</h6>
 			<@resourceSelect "elyCentre" />
 		</li>
 		<li>
 			<h6>Maakunnat</h6>
 			<@resourceSelect "finnishProvince" />
 		</li>
 		<li>
 			<h6>Vanhat läänit</h6>
 			<@resourceSelect "finnishOldCounty" />
 		</li>
 		<li>
 			<h6>LYL-alueet</h6>
 			<@resourceSelect "lylArea" />
 		</li>
 	</ul>
 </div>
 

 <div class="services">
 	<h5>Ulkomaiset paikat</h5>
 	<ul class="fieldBox fewFields">
 		<li>
 			<h6>Schemet</h6>
 			<@resourceSelect "scheme" />
 		</li>
 		<li>
 			<h6>EURING-provinssit</h6>
 			<@resourceSelect "euringProvince" />
 		</li>
 		<li>
 			<h6>Maat</h6>
 			<@resourceSelect "country" />
 		</li>
 	</ul>
 </div>
 

 



</div>
<#include "luomus-footer.ftl">