 <form action="${baseURL}/haku" method="post">
 	<ul class="fieldBox">
 	<li><label for="event.diario">Diario</label> <@input "event.diario" /> <span class="ui-icon ui-icon-info" title="Voi käyttää %"></span> </li> 
 	<li><label for="event.nameRing">Nimirengas</label> <@input "event.nameRing" /> <span class="ui-icon ui-icon-info" title="Voi käyttää % mutta silloin renkaan oltava muodossa XX001234% (X 1234% ei toimi, mutta toisaalta X%1234% toimii)"></span> </li>
 	<li><label for="event.legRing">Jalkarengas</label> <@input "event.legRing" />  <span class="ui-icon ui-icon-info" title="Voi käyttää % mutta silloin renkaan oltava muodossa XX001234% (X 1234% ei toimi, mutta toisaalta X%1234% toimii)"></span> </li>
 	<li><label for="event.fieldReadableCode">Väritunniste</label>
 		<@input "event.fieldReadableCode" "Tunniste" />
 		<@select "event.mainColor" "Tausta" />
 		<@select "event.species" "Laji" />
 	</li>
 	<li><label for="event.type">Tyyppi</label> <@select "event.type" "Rajaa tyypillä..." /> 
 	<li><label for="event.ringer">Rengastaja</label> <@select "event.ringer" "Rajaa rengastajalla..." /> </li>
 	<li><label for="event.eventYear">Vuosi</label> <@input "event.eventYear" /> </li>
 	<li> <input type="submit" value="Hae" />  </li>
 	</ul>
 </form>
 
 <div class="clear"></div>