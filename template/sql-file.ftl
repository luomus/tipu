<#if showSQL><pre>${sql}</pre></#if>
<pre>Ajettu ${currentTime}</pre>

<pre><#if queryResults??><#list colNames as col>${col!""}<#if col_has_next>|</#if></#list>
<#list queryResults as row>
<#list row as col>${((col!"")?replace("\n", " ")?replace("\r", " "))?html}<#if col_has_next>|</#if></#list>
</#list></#if></pre>