<#include "luomus-header.ftl">
<#include "macro.ftl">

 <#if !binType??>
 	<h4>Roskikset</h4>
 <#elseif binType =="WAITING">
 	<img class="binImage" src="${staticURL}/img/waiting.png" alt="Tik tak.." />
 <#elseif binType =="TRASH">
 	<img class="binImage" src="${staticURL}/img/trash.gif" alt="Roskis.." />
 <#elseif binType =="DELETED">
 	<img class="binImage" src="${staticURL}/img/shoveled.png" alt="Dunkattu.." />
 </#if>
 
 <table id="selectBinTable" class="optionsTable largeTable">
 	<tr><th>Rengastukset</th><th class="empty">&nbsp;</th><th>Tapaamiset</th></tr>
 	<tr>
 		<td><a href="${baseURL}/odottavat/rengastukset"><span class="ui-icon ui-icon-folder-open"></span> &nbsp; Odottavat (${binCounts.waitingBinRingingsCount})</a></td>
 		<td class="empty">&nbsp;</td>
 		<td><a href="${baseURL}/odottavat/tapaamiset"><span class="ui-icon ui-icon-folder-open"></span> &nbsp; Odottavat (${binCounts.waitingBinRecoveriesCount})</a></td>
 	</tr>
 	<tr>
 		<td><a href="${baseURL}/roskis/rengastukset"><span class="ui-icon ui-icon-trash"></span> &nbsp; Roskis (${binCounts.trashBinRingingsCount})</a></td>
 		<td class="empty">&nbsp;</td>
 		<td><a href="${baseURL}/roskis/tapaamiset"><span class="ui-icon ui-icon-trash"></span> &nbsp; Roskis (${binCounts.trashBinRecoveriesCount})</a></td>
 	</tr>
 	<tr>
 		<td><a href="${baseURL}/haudatut/rengastukset"><span class="ui-icon ui-icon-disk"></span> &nbsp; Haudatut (${binCounts.deletedBinRingingsCount})</a></td>
 		<td class="empty">&nbsp;</td>
 		<td><a href="${baseURL}/haudatut/tapaamiset"><span class="ui-icon ui-icon-disk"></span> &nbsp; Haudatut (${binCounts.deletedBinRecoveriesCount})</a></td>
 	</tr>
 </table>


 <#if binType??>
 	<#if binType == "WAITING">
 		<h4>Odottavat <#if eventType == "ringing">rengastukset<#else>tapaamiset</#if></h4>
 		<#if !validated??><a href="${baseURL}/odottavat/<#if eventType == "ringing">rengastukset<#else>tapaamiset</#if>/validointi" class="button" onclick="return confirm('Ajetaanko tarkistukset?')">Aja tarkistukset</a></#if>
 	<#elseif binType =="TRASH">
 		<h4> <#if eventType == "ringing">Rengastusten<#else>Tapaamisten</#if> roskis</h4>
 		<#if !validated??><a href="${baseURL}/roskis/<#if eventType == "ringing">rengastukset<#else>tapaamiset</#if>/validointi" class="button" onclick="return confirm('Ajetaanko tarkistukset?')">Aja tarkistukset</a></#if>
 	<#else>
 		<h4> Haudatut <#if eventType == "ringing">rengastukset<#else>tapaamiset</#if> </h4>
 	</#if>
 </#if>

 <#if validated??>
 	<ul class="fieldBox fewFields">
 		<li>Hyväksyttäisiin <span style="font-size: 110%;color:green">${quessedStatusCounts["OK"]}</span> riviä.</li>
 		<li>Muita virheitä <span style="font-size: 110%;color:#CBD600">${quessedStatusCounts["SHOULD_TRY"]}</span> rivillä (ei rengasvirheitä, voisi mennä läpi jos muut virheet korjataan).</li>
 		<li>Rengasvirheitä <span style="font-size: 110%;color:red">${quessedStatusCounts["RING_ERRORS"]}</span> rivillä.</li>
 	</ul>
 </#if>
			
 <#if binEvents?has_content>
 <table id="binContentsTable" class="largeTable">
 	<thead>
 		<tr>
 			<th <#if validated??>style="min-width: 400px;"</#if>>&nbsp;</th>
 			<#if validated??><th>Tila</th></#if>
 			<#if eventType == "recovery"><th>Diario</th></#if>
  			<th>Jalkar.</th>
 			<th>Vaihtor.</th>
 			<th>Värit.</th>
 			<th>Tausta</th>
 			<th>Laji</th>
 			<th>Pvm</th>
 			<th>Paikka</th>
 			<th>Rengastaja</th>
 			<th>Maallikko</th>
 			<th>Tapauksen kommentit</th>
 			<th>Roskiinsiirto -kommentit</th>
 			<th>Muokattu</th>
 			<th>Muokkaaja</th>
 			<th>&nbsp;</th>
 		</tr>
 	</thead>
 	<tbody>
		<#list binEvents as event>
		<tr>
 			<td <#if validated??>class="binstatus_${quessedStatuses[event.eventId]}"</#if> >
 				<a class="button" <#if validated??>target="_blank" onclick="hideRow(this);"</#if> href="${baseURL}/odottava-kasittelyyn/${event.eventId}">Syöttöön</a>
 				<#if validated?? && rowErrors[event.eventId]??>
 					<ul class="binErrors">
 					<#list rowErrors[event.eventId] as error>
 						<li>${error}</li>
 					</#list> 
 					</ul>
 				</#if>
 			</td>
 			<#if validated??><td class="binstatus_${quessedStatuses[event.eventId]}">${sortableStatuses[quessedStatuses[event.eventId]]}</th></#if>
 			<#if eventType == "recovery"><td><@searchTableValueForRow event.row "event.diario" /></td></#if>
  			<td><@searchTableValueForRow event.row "event.legRing" />
  				<@searchTableValueForRow event.row "event.ringStart" />
  				<@searchTableValueForRow event.row "event.ringEnd" />
  			</td>
 			<td><@searchTableValueForRow event.row "event.newLegRing" /></td>
 			<td><@searchTableValueForRow event.row "event.fieldReadableCode" /></td>
 			<td><@searchTableValueForRow event.row "event.mainColor" /></td>
 			<td><@searchTableValueForRow event.row "event.species" /></td>
 			<td><@searchTableValueForRow event.row "event.eventDate" /></td>
 			<td><@searchTableValueForRow event.row "event.municipality" /> 
 				<@searchTableValueForRow event.row "event.euringProvinceCode" />
 			</td>
 			<td><@searchTableValueForRow event.row "event.ringer" /></td>
 			<td><@searchTableValueForRow event.row "event.laymanID" /></td>
 			<td><@searchTableValueForRow event.row "event.notes" /></td>
 			<td class="nonEventColumn">${event.binMoveNotes!""}</td>
 			<td class="nonEventColumn">${event.modifiedDate}</td>
 			<td class="nonEventColumn">${event.lastModifiedUserId!""}</td>
 			<td><#if binType !="DELETED">
 				<a class="button" href="${baseURL}/roskikset/poisto/${event.eventId}" onclick="return confirm('Siiretäänkö haudattuihin?');">Poista</a>
 			</#if></td>
 		</tr>
 		</#list>
 	</tbody>
 </table>
 </#if>

<script>
	$(document).ready(function() {
		<#if validated??>
			$('#binContentsTable').dataTable({"language": {"decimal": ","}, "iDisplayLength": 50, "aaSorting": [[ 1, "asc" ]], "columnDefs": [ { "targets": 0, "orderable": false } ]});
		<#else>
    		$('#binContentsTable').dataTable({"language": {"decimal": ","}, "iDisplayLength": 50, "aaSorting": [], "columnDefs": [ { "targets": 0, "orderable": false } ]});
    	</#if>
	});
	function hideRow(e) {
		$(e).closest("tr").fadeOut('slow');
	}
 </script>

<#include "luomus-footer.ftl">