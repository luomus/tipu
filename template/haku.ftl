<#include "luomus-header.ftl">
<#include "macro.ftl">

 <#if !showAdvancedSearch??>
 <h4>Haku</h4>
 <#include "pikahaku.ftl">
 <#else>
 	<h4>Laaja haku</h4>
 </#if>
 
 <#if searchResult??>
 	<table id="resultTable" class="largeTable">
 		<thead>
 			<tr>
 				<th>&nbsp;</th>
 				<#list resultFields as field>
 					<th title="${field.desc}">${field.commonName}</th>
 				</#list>
 			</tr>
 		</thead>
 		<tbody>
 		<#list searchResult.results as row>
 			<tr>
 				<td><a href="${baseURL}/muokkaa/${row.get("event.id").value}" class="button muokkaa">Muokkaa</a></td>
 				<#list resultFields as field>
 					<td><@searchTableValueForRow row field.name /></td>
 				</#list>
 			</tr>
 		</#list>
 		</tbody>
 	</table>
 	<script>
		$(document).ready(function() {
    		$('#resultTable').dataTable({"language": {"decimal": ","}, "iDisplayLength": 50, "aaSorting": [], "columnDefs": [ { "targets": 0, "orderable": false } ]});
		});
 	</script>
 </#if>
 
 <#if searchResult?? && showAdvancedSearch??>
 	<a id="showToolsLink" href="#" onclick="showSearch(); return false;">&raquo; Muuta hakua</a>
 	<div class="hidden" id="searchToolsContainer">
 
		<#include "laajahaku.ftl">
	
		<@searchInstructions />
		
	</div>
 <#else>
 	<#if showAdvancedSearch??>
 		<#include "laajahaku.ftl">
 	<#else>
		<ul class="servicelist">
			<li><a href="${baseURL}/haku?showAdvancedSearch=true">Näytä laaja haku</a></li>
		</ul>
	</#if>
 </#if>
  
 <script>
 	function showSearch() {
 		$("#showToolsLink").hide();
 		$("#searchToolsContainer").removeClass('hidden');
 	}
 </script>
 
<#include "luomus-footer.ftl">