<#include "luomus-header.ftl">
<#include "macro.ftl">

 <h5>Pikahaku</h5>
 <#include "pikahaku.ftl">
 <ul class="servicelist">
 	<li><a href="${baseURL}/haku?showAdvancedSearch=true">Näytä laaja haku</a></li>
 </ul>
 
 <div class="services">
 <h5>Syöttö</h5>
 <ul class="servicelist">
 	<li><a href="${baseURL}/uusi-rengastus">Uusi rengastus</a></li>
 	<li><a href="${baseURL}/uusi-tapaaminen">Uusi tapaaminen</a></li>
 	<li><a href="${baseURL}/loydos-tuonti">Tuo löydöksestä (${loydosCount})</a></li>
 	<li><a href="${baseURL}/jakelu">Tuhoutuneeksi /<br/>hävinneeksi merkitseminen</a></li>
 	<li><a href="${baseURL}/massatuonti">Löytöjen massatuonti</a></li>
 	<li><a href="${baseURL}/massapaivitys">Massapäivitys</a></li>
 	<li><a href="${baseURL}/rengasvalikorjaus">Väärin ilmoitetun rengasvälin korjaus</a></li>
 </ul>
 </div>
 
 <div class="services">
 <h5>Pikalinkit</h5>
 <ul class="servicelist">
 	<li><a href="${lintuvaaraURL}external_services/sulka?locale=fi">Sulka</a></li>
 	<li><a href="${lintuvaaraURL}external_services/rengaskirje?locale=fi">Rengaskirje</a></li>
 	<li><a href="${lintuvaaraURL}external_services/kihla?locale=fi">Kihla</a></li>
 	<li><a href="${lintuvaaraURL}external_services/tulospalvelu?locale=fi">Tulospalvelu</a></li>
 	<li><a href="${lintuvaaraURL}">Lintuvaara</a></li>
 </ul>
 </div>
 
<div class="services">
<h5>Työkaluja</h5>
<ul class="servicelist">
	<li><a href="${baseURL}/massapostitus">Massapostitus</a></li>
	<li><a target="_blank" href="https://fmnh-ws-prod.it.helsinki.fi/tipu-api/coordinates">Koordinaattimuunnokset</a></li>
</ul>
</div>

<div class="services">
<h5>Päivitys</h5>
<ul class="servicelist">
	<li><a href="${baseURL}/refresh-rowstructure" id="refreshLaunchRowStucture">Päivitä alasvetovalikot</a> <span class="ui-icon ui-icon-info" title="Vaikuttaa Tipuun ja Sulkaan: päivittää muuttujalistat ja niiden kuvaukset."></span> </li>
	<li><a href="${baseURL}/refresh-validations" id="refreshLaunchValidations">Päivitä validointidatat (jakelut, sopivuudet, biometriset tiedot)</a> <span class="ui-icon ui-icon-info" title="Päivittää rengastajan jakelun sen jälkeen kun siihen on tehty muutoksia, jotta muutokset tulevat voimaan validoinneissa ja rengastajan jakelun visualisoinnissa. Vaikuttaa myös vastaavaan toimintoon Sulassa. Päivittää myös Sulan 'seuraava rengas' -ominaisuuden. Jos tätä ei paineta, jakelutiedot päivittyvät itsestään kuuden tunnin välein. Päivittää myös sopivuudet ja väritunnusjakelun. Lisäksi päivittää lintujen biometriset tiedot."></span> </li>
</ul>
</div>



<#include "luomus-footer.ftl">