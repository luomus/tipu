<#include "luomus-header.ftl">
<#include "macro.ftl">

<div class="yllapito">

<h4>${resourceCommonName} muokkaus</h4>

<p>
<a href="${baseURL}/${resourceName}/uusi?${listingByFieldName}=${params.get(listingByFieldName).value?url}" >&raquo; Luo uusi</a>
</p>

<table class="largeTable" id="resultTable">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<#list rowStructure.getTableFields(resourceName) as field>
				<#if field.name?ends_with(".rowid")>
				<#elseif resourceIdFieldNames?seq_contains(field.name)>
					<th class="idField" title="${field.desc?html}">${field.commonName?html}</th>
				<#else>
					<th title="${field.desc?html}">${field.commonName?html}</th>
				</#if>
			</#list>
		</tr>
	</thead>
	<tbody>
	<#list rows as row>
		<tr>
			<td>
				<a href="${baseURL}/${resourceName}/muokkaa?<#list resourceIdFieldNames as idField>${idField}=${row.get(idField).value?url}<#if idField_has_next>&amp;</#if></#list>" class="button">Muokkaa</a>
				<a href="${baseURL}/${resourceName}/poista?<#list resourceIdFieldNames as idField>${idField}=${row.get(idField).value?url}<#if idField_has_next>&amp;</#if></#list>" class="button" onclick="return confirm('Poistetaanko varmasti?');">Poista</a>
			</td>
			<#list rowStructure.getTableFields(resourceName) as field>
				<#if field.name?ends_with(".rowid")>
				<#elseif resourceIdFieldNames?seq_contains(field.name)>
					<td class="idField"><@plaintextValueForRow row field.name /></td>
				<#else>
					<td><@searchTableValueForRow row field.name /></td>
				</#if>
			</#list>
		</tr>
	</#list>
</table>
<script>
	$(document).ready(function() {
    	$('#resultTable').dataTable({"language": {"decimal": ","}, "iDisplayLength": 50, "aaSorting": []});
	});
 </script>
<p>
<a href="${baseURL}/${resourceName}/uusi?${listingByFieldName}=${params.get(listingByFieldName).value?url}" >&raquo; Luo uusi</a>
</p>

</div>
<#include "luomus-footer.ftl">