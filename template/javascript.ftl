<script>


function blockingLoader() {
	$('body').append('<div id="blockingLoader" class="ui-widget-overlay">Hetki!</div>');
}
	
$(function() {
	
	if ($("body").attr("class") === "oldie") {
		var text = 'Your browser is not supported. Please use Mozilla Firefox, Google Chrome  (both are reccomended) or newer version of IE (10,11) (not reccomended)'; 
		$("html").html(text);
		alert(text);
	}
	
	$(document).tooltip({
		position: {
			 my: "left bottom", 
			 at: "left top-7"
		}
	});
			
	$("input[type=submit], button, .button").button();
	
	$(".chosen-search-contains").not('#eventMallikkoEdit .chosen-search-contains').chosen({ search_contains: true, allow_single_deselect: true });
	$(".chosen").not('#eventMallikkoEdit .chosen').chosen({ allow_single_deselect: true });
	
	$('input, textarea').placeholder();
	
	$("form.mainSubmitForm").on('submit', blockingLoader);
		
	$('#refreshLaunchRowStucture').on('click', function() {
		if (confirm('Päivitetäänkö varmasti? Kysy ettei muilla ole juuri syöttö kesken.')) {
			blockingLoader();
			return true;
		} else {
			return false;
		}
	});
	$('#refreshLaunchValidations').on('click', function() {
		if (confirm('Aloitetaanko validointitietojen ja jakelun päivitys?')) {
			blockingLoader();
			return true;
		} else {
			return false;
		}
	});
	
	$("button.acceptWarnings").on('click', function() {
		$(this).hide();
		$("form.mainSubmitForm").append('<input type="hidden" name="acceptWarnings" value="true" />');
		$(this).parent().hide(300);
		<#if !error?has_content>
			$("form.mainSubmitForm").submit();
		</#if>
	});
	
	$(".resourceSelectSubmit").on('click', function() {
		var form = $(this).closest('form');
		var baseAction = form.attr('action');
		var resourceInput = form.find(':input').first();
		var resourceId = resourceInput.val();
		if (resourceId == '') return false;
		resourceInput.attr('disabled', 'disabled');
		var resourceAction = baseAction + '/'+ encodeURIComponent(resourceId);
		form.attr('action', resourceAction);
		return true; 
	});
	
	$("#eventGrid").freetile({
		scoreFunction: function(o) {
   			return -(o.TestedTop) * 8 - (o.TestedLeft);
		}
	});

});

<#if locale == "fi">
/*
* Translated default messages for the jQuery validation plugin.
* Locale: FI (Finnish; suomi, suomen kieli)
*/
(function ($) {
        $.extend($.validator.messages, {
                required: "T&auml;m&auml; kentt&auml; on pakollinen.",
                email: "Sy&ouml;t&auml; oikea s&auml;hk&ouml;postiosoite.",
                url: "Sy&ouml;t&auml; oikea URL osoite.",
                date: "Sy&ouml;t&auml; oike p&auml;iv&auml;m&auml;&auml;r&auml;.",
                dateISO: "Sy&ouml;t&auml; oike p&auml;iv&auml;m&auml;&auml;r&auml; (VVVV-MM-DD).",
                number: "Sy&ouml;t&auml; numero.",
                creditcard: "Sy&ouml;t&auml; voimassa oleva luottokorttinumero.",
                digits: "Sy&ouml;t&auml; pelk&auml;st&auml;&auml;n numeroita.",
                equalTo: "Sy&ouml;t&auml; sama arvo uudestaan.",
                maxlength: $.validator.format("Voit sy&ouml;tt&auml;&auml; enint&auml;&auml;n {0} merkki&auml;."),
                minlength: $.validator.format("V&auml;hint&auml;&auml;n {0} merkki&auml;."),
                rangelength: $.validator.format("Sy&ouml;t&auml; v&auml;hint&auml;&auml;n {0} ja enint&auml;&auml;n {1} merkki&auml;."),
                range: $.validator.format("Sy&ouml;t&auml; arvo {0} ja {1} v&auml;lilt&auml;."),
                max: $.validator.format("Sy&ouml;t&auml; arvo joka on pienempi tai yht&auml; suuri kuin {0}."),
                min: $.validator.format("Sy&ouml;t&auml; arvo joka on yht&auml; suuri tai suurempi kuin {0}.")
        });
}(jQuery));
</#if>

		
</script>
