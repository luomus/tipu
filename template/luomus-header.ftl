<!DOCTYPE html>

<!--[if lt IE 9 ]> <body class="oldie"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="${locale}"> <!--<![endif]-->

<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for supporting devices (iOS, Android etc) -->
	<meta name="viewport" content="initial-scale=1.0" />

	<title>${text["title_"+page]} | ${text.title}</title>
	<link href="${staticURL}/favicon.ico?${staticContentTimestamp}" type="image/ico" rel="shortcut icon" />
	
	<script src="${staticURL}/jquery-1.9.1.js"></script>
	
	<link href="${staticURL}/jquery-ui/css/cupertino/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" /> 
	<script src="${staticURL}/jquery-ui/js/jquery-ui-1.10.2.min.js"></script>
	
	<script src="${staticURL}/jquery.validate.min.js?${staticContentTimestamp}"></script>

	<script src="${staticURL}/jquery-placeholder/jquery.placeholder.js?${staticContentTimestamp}"></script>
	
	<script src="${staticURL}/chosen/chosen.jquery.min.js?${staticContentTimestamp}"></script>
	<link href="${staticURL}/chosen/chosen.min.css?${staticContentTimestamp}" rel="stylesheet" />
	
	<script src="${staticURL}/collapse/collapse.js?${staticContentTimestamp}"></script>
	<link href="${staticURL}/collapse/collapse.css?${staticContentTimestamp}" rel="stylesheet" />
	
	<link href="${staticURL}/luomus.css?${staticContentTimestamp}" rel="stylesheet" />
	<link href="${staticURL}/rengastus.css?${staticContentTimestamp}" rel="stylesheet" />
	
	<link href="${staticURL}/DataTables-1.10.2/media/css/jquery.dataTables.css" rel="stylesheet" />
	<script src="${staticURL}/DataTables-1.10.2/media/js/jquery.dataTables.js"></script>

	<script src="${staticURL}/freetile/jquery.freetile.min.js?${staticContentTimestamp}"></script>
	
	<script src="${staticURL}/wkartta_consumer.js?${staticContentTimestamp}"></script>
	
	<#include "javascript.ftl">
	
</head>

<body>

	<div id="masthead" role="banner">
		<div id="masthead-inner">
		
			<div id="userinfo">
				<ul>
           			<li><a href="${baseURL}/logout" class="button">Kirjaudu ulos</a></li>
           			<li>Kirjautuneena <span class="name">${username}</span> (${userid})</li>
       			</ul>
			</div>
			
			<a href="${baseURL}/etusivu">
				<div id="logo">&nbsp;</div>
			</a>

			<a href="${baseURL}/etusivu">
				<div id="sitetitle">
					${text.title}
				</div>
			</a>
			
			<#if inStagingMode>     <span class="devmode">TEST ENVIROMENT</span></#if>	
    		<#if inDevelopmentMode> <span class="devmode">DEV ENVIROMENT</span></#if>	
			
			<div id="navigation-wrap" role="navigation">
				<nav id="mainmenu" role="navigation">
					<ul class="nav-bar" role="menu">
						<li role="menuitem"><a href="${baseURL}/etusivu">${text.title_index}</a></li>
						<li role="menuitem"><a href="${baseURL}/haku">${text.title_haku}</a></li>
						<li role="menuitem"><a href="${baseURL}/loydos-tuonti">${text.title_loydos} (${loydosCount})</a></li>
						<li role="menuitem"><a href="${baseURL}/roskikset">${text.title_bin}</a></li>
						<li role="menuitem"><a href="${baseURL}/jakelu">${text.title_jakelu}</a></li>
						<li role="menuitem"><a href="${baseURL}/yllapito">${text.title_yllapito}</a></li>
						<li role="menuitem"><a href="${baseURL}/sql">${text.title_sql}</a></li>
					</ul>
				</nav>
		    </div>
		</div>
	</div>
	
	<!-- Content section -->
	<div id="main-area" role="main">
		<div id="content-wrapper">
		
		<div id="content" class="page-content">
		
		<#if searchBrowserEntry??>
			<div id="searchBrowserEntry">
				<#if searchBrowserEntry.previous??>
					<#if searchBrowserEntry.idColumns?size == 1>
						<a href="${baseURL}/${searchBrowserEntry.resourceName}/${searchBrowserEntry.previous.get(searchBrowserEntry.idColumns?first).value}">
					<#else>
						<a href="${baseURL}/${searchBrowserEntry.resourceName}/muokkaa?<#list searchBrowserEntry.idColumns as idCol>${idCol}=${searchBrowserEntry.previous.get(idCol).value}&</#list>">
					</#if> 
						&laquo; Edellinen 
						<#if searchBrowserEntry.resourceName != "event">
							<#if searchBrowserEntry.idColumns?size == 1>
								(${searchBrowserEntry.previous.get(searchBrowserEntry.idColumns?first).value})
							<#else>
								(<#list searchBrowserEntry.idColumns as idCol>${searchBrowserEntry.previous.get(idCol).value?html} </#list>)
							</#if>
						<#else>
							<#if searchBrowserEntry.previous.get("event.type").value == "ringing">
								(${searchBrowserEntry.previous.get("event.legRing").value})
							<#else>
								(${searchBrowserEntry.previous.get("event.diario").value})
							</#if>
						</#if>
						</a>
				<#else>
					<span class="grey">&laquo; Edellinen</span>
				</#if>
				|
					<#if searchBrowserEntry.resourceName != "event">
						<a href="${baseURL}/${searchBrowserEntry.resourceName}/haku?historiasta=true">Hakutuloksiin</a>
					<#else>
						<a href="${baseURL}/haku?historiasta=true">Hakutuloksiin</a>
					</#if>
				|
				<#if searchBrowserEntry.next??>
					<#if searchBrowserEntry.idColumns?size == 1>
						<a href="${baseURL}/${searchBrowserEntry.resourceName}/${searchBrowserEntry.next.get(searchBrowserEntry.idColumns?first).value}">
					<#else>
						<a href="${baseURL}/${searchBrowserEntry.resourceName}/muokkaa?<#list searchBrowserEntry.idColumns as idCol>${idCol}=${searchBrowserEntry.next.get(idCol).value}&</#list>">
					</#if>
						Seuraava
						<#if searchBrowserEntry.resourceName != "event">
							<#if searchBrowserEntry.idColumns?size == 1>
								(${searchBrowserEntry.next.get(searchBrowserEntry.idColumns?first).value})
							<#else>
								(<#list searchBrowserEntry.idColumns as idCol>${searchBrowserEntry.next.get(idCol).value?html} </#list>)
							</#if>
						<#else>
							<#if searchBrowserEntry.next.get("event.type").value == "ringing">
								(${searchBrowserEntry.next.get("event.legRing").value})
							<#else>
								(${searchBrowserEntry.next.get("event.diario").value})
							</#if>
						</#if> 
						&raquo; </a>
				<#else>
					<span class="grey">Seuraava &raquo;</span>
				</#if>
			</div>
		</#if>
		
		<#if message?has_content><div class="message">${message}</div></#if>
		<#if error?has_content><div class="message error">${error}</div><#assign hasErrors=true /></#if>
		
		<#if !acceptWarnings?? || acceptWarnings != "true">
			<#if warning?has_content><div class="message warning">${warning} <button class="acceptWarnings">Kuittaan!</button></div></#if>
		</#if>
		
		<#if success?has_content><div class="message success">${success}</div></#if>
		
