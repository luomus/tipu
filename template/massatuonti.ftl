<#include "luomus-header.ftl">
<#include "macro.ftl">

<h4>Löytöjen massatuonti</h4>

<#if successCount??>
   <p>Diariot: ${diarioMin!""} - ${diarioMax!""}</p>
   <p><a href="${baseURL}/massatuonti">Lataa uusi tiedosto</a></p>
   <p><a href="${baseURL}/etusivu">Etusivulle</a></p>
<#elseif validations??>
	<table id="massUploadValidation">
		<thead>
			<tr>
				<th>Rivin nro</th>
				<th>Virheet</th>
				<th>Varoitukset</th>
			</tr>
		</thead>
		<tbody>
			<#list validations?keys as rowNumber>
				<#assign validationResponse = validations[rowNumber]>
				<#if validationResponse.hasErrors() || validationResponse.hasWarnings()>
					<tr>
						<td>${rowNumber}</td>
						<td>
							<#list validationResponse.errors as error>
								<label>${fieldNames[error.fieldName]}</label> ${error.errorText}<#if error_has_next><br/></#if>
							</#list>
						</td>
						<td>
							<#list validationResponse.warnings as warning>
								<label>${fieldNames[warning.fieldName]}</label>${warning.errorText}<#if warning_has_next><br/></#if>
							</#list>
						</td>
					</tr>
				</#if>
			</#list>
		</tbody>
	</table>
	<form action="${baseURL}/massatuonti" method="post" enctype="multipart/form-data">
	<ol id="fieldReadableUploadInstructions">
		<#if showApproveWarnings>
			<li>	Hyväksy varoitukset <input type="checkbox" name="approveWarnings" value="true" /> </li> 
		</#if>
		<li> 	Lataa tiedosto <b>uudelleen</b> <input type="file" name="file" /> <input type="submit" value="Tuo" /></li>
	</ol>	
	</form>
<#else>
	<form action="${baseURL}/massatuonti" method="post" enctype="multipart/form-data">
	<ol id="fieldReadableUploadInstructions">
		<li>	Lataa <a href="${staticURL}/Massatuonti.xlsx">Massatuonti.xlsx</a>	</li>
		<li>	Sarakkeita saa vapaasti poistaa ja lisätä. Otsikkorivi on säilytettävä. Sarakkeita lisättäessä käytä tietokantanimiä (kts. SQL ja raportit > Event). </li>
		<li>    Syötä data. Tallenna tiedot (esim päivämäärät) samassa muodossa kuin Tipun käyttöliittymässä käyttäen samoja koodeja.</li>
		<li>	Tallenna tab separated -muodossa: <br /> <img src="${staticURL}/img/jakelu_listays_save.png" alt="" />	</li>
		<li>	Avaa .txt tiedosto Notepad++ ohjelmassa ja muuta Encoding > Convert to UTF-8 </li> 
		<li> 	Valitse tiedosto <input type="file" name="file" /> <input type="submit" value="Tuo" /></li>
	</ol>	
	</form>
</#if>

<#include "luomus-footer.ftl">