
<#macro select fieldName placeholder="Valitse..." multipleSelect=false>
	<@selectWithID "" fieldName placeholder multipleSelect />
</#macro>

<#macro input fieldName placeholder="">
	<@inputWithID "" fieldName placeholder />
</#macro>

<#macro textarea fieldName placeholder="">
	<@textareaWithID "" fieldName placeholder />
</#macro>

<#macro date fieldName>
	<@dateWithID "" fieldName />
</#macro>

<#macro hidden fieldName>
	<@hiddenWithID "" fieldName />
</#macro>

<#macro hiddenWithID idPrefix fieldName>
	<input type="hidden" id="${id(idPrefix, fieldName)}" name="${fieldName}" value="${param(fieldName)}" />
</#macro>

<#macro selectWithID idPrefix fieldName placeholder="Valitse..." multipleSelect=false>
	<#assign field = rowStructure.get(fieldName)>
	<#assign selectedValue = params.get(fieldName).value>
	<#if selectedValue?has_content && !multipleSelect && !field.hasEnumerationDefined(selectedValue)>
		<@inputWithID idPrefix fieldName placeholder /> (Arvo epäkelpo valitalistalle!)
	<#else>
		<#assign enums = rowStructure.get(fieldName).enumerationValues>
		<#assign chosenClass = "chosen">
		<#if (enums?size > 100)><#assign chosenClass = "chosen-search-contains"></#if>
		<#if (enums?size <= 6) && !multipleSelect><#assign chosenClass = "no-chosen"></#if>
		<div <@error fieldName /> style="display: inline-block;">
		<select id="${id(idPrefix, fieldName)}" name="${fieldName}" <@error fieldName chosenClass /> data-placeholder="${placeholder?html}" <#if multipleSelect>multiple="multiple"</#if> >
			<option></option>
			<#list enums as e>
				<#if multipleSelect> <#assign selected = params.get(fieldName).selectedValues?seq_contains(e.value)> <#else> <#assign selected = selectedValue?has_content && selectedValue == e.value> </#if>
				<option value="${e.value?html}" <#if selected>selected="selected"</#if>>${e.value?html}: ${e.desc?html}</option>
			</#list>
		</select>
		</div>
	</#if>
</#macro>

<#macro inputWithID idPrefix fieldName placeholder="">
	<#assign field = rowStructure.get(fieldName)>
	<#assign value = param(fieldName)>
	<#assign sizeN = field.inputSize >
    <#if (sizeN > 50)> <#assign sizeN = 50 ></#if>
    <#if (sizeN <= 1) && value?has_content> <#assign sizeN = value?length> </#if>
	<input id="${id(idPrefix, fieldName)}" type="text" placeholder="${placeholder?html}" name="${fieldName}" size="${sizeN}" autocomplete="off" value="${value}" <@error fieldName /> />
</#macro>

<#macro textareaWithID idPrefix fieldName placeholder="">
    <#assign rowsN = (param(fieldName)?length / 50)?ceiling >
    <#if (rowsN < 2)> <#assign rowsN = 2 ></#if>
	<textarea id="${id(idPrefix, fieldName)}" name="${fieldName}" rows="${rowsN}" cols="50" <@error fieldName /> >${param(fieldName)}</textarea>
</#macro>

<#macro dateWithID idPrefix fieldName>
<input id="${id(idPrefix, fieldName)}" type="text" placeholder="pp" name="${fieldName}_dd" size="2" autocomplete="off" value="${params.get(fieldName).dateValue.day?html}" <@error fieldName "dateinput" /> />.<input id="${id(idPrefix, fieldName)}_mm" type="text" placeholder="kk" name="${fieldName}_mm" size="2" autocomplete="off" value="${params.get(fieldName).dateValue.month?html}" <@error fieldName "dateinput" /> />.<input id="${id(idPrefix, fieldName)}_yyyy" type="text" placeholder="vuosi" name="${fieldName}_yyyy" size="4" autocomplete="off" value="${params.get(fieldName).dateValue.year?html}" <@error fieldName "dateinput" /> />
</#macro>

<#function id idPrefix fieldName>
  <#if idPrefix?has_content><#return idPrefix + "__" + fieldName></#if><#return fieldName>
</#function>

<#function param fieldName >
  <#return params.get(fieldName).value?html>
</#function>

<#macro error fieldName additionalClasses="">
	<#assign errorText = .data_model["error_"+fieldName]!"">
	<#assign warningText = .data_model["warning_"+fieldName]!"">
	<#if errorText?has_content || warningText?has_content> class="<#if errorText?has_content>erroneousField<#else>warningField</#if> ${additionalClasses}" title="${errorText?html} ${warningText?html}" <#else>class="${additionalClasses}"</#if>
</#macro>

<#macro desiredInput fieldName>
	<@desiredInputWithID "" fieldName />
</#macro>

<#macro desiredInputWithID idPrefix fieldName placeholder="Valitse..." multipleSelect=false>
	<#assign field = rowStructure.get(fieldName)>
	<#if field.isEnumeration()><@selectWithID idPrefix fieldName placeholder multipleSelect />
	<#elseif field.isDate()><@dateWithID idPrefix fieldName />
	<#elseif (field.inputSize > 80)><@textareaWithID idPrefix fieldName />
	<#else>
		<#if placeholder != "Valitse...">
			<@inputWithID idPrefix fieldName placeholder />
		<#else>
			<@inputWithID idPrefix fieldName />
		</#if>
	</#if>
</#macro>

<#macro loydosInfo form class="">
 	<div id="${form.id}_infoContainer" class="loydosFormInfoContainer ui-widget-content ui-corner-all ${class}">
		<h5>Löydös lähetyksen tiedot</h5>
 		<#if !form.validated><div class="error">Validoimaton!</div></#if>
 		<div class="person"><h4>Lähettäjä</h4><@person form.submitter /></div>
 		<div class="person"><h4>Löytäjä (jos eri kuin lähettäjä)</h4><@person form.discoverer /></div>
 		<div class="clear"></div>
 		<h4>Tiedot</h4>
 		<table>
		<tr><td><label>Rengas</label></td><td>${(form.ringInfo!"")?html} </td></tr>
		<tr><td><label>Laji</label> </td><td> ${(form.species!"")?html} </td></tr>
		<#list form.row.iterator() as c>
			<#if c.hasValue()>
				<tr <#if c.fieldName != "event.notes" && c.fieldName != "event.locality" && c.fieldName != "event.coordinateAccuracy">class="loydosRowResolvedValue"</#if>><td><label>${c.field.commonName}</label></td><td> ${c.value?html}</td></tr>
			</#if>
		</#list>		
		<tr><td><label>Ikä</label></td><td> ${(form.age!"")?html} </td></tr>

	<#if form.attachments?has_content>
		<tr><td><label>Kuvat</label></td><td>
			<#list form.attachments as attachment>
 				<a href="${loydosURI}auth?username=${loydosUsername}&amp;password=${loydosPassword}&amp;next=admin%2Fapi%2Fsubmissions%2Frengasloyto%2F${form.id}%2Fattachments%2F${attachment}"
 					target="kuva">${attachment}</a>
 				</#list>
 			</td></tr>
 		</#if>
 		</table>
 		<h5>Kommentit</h5>
 		<p>${(form.tipuNotes!"")?html}</p>
 	</div>
 </#macro>
 	
 <#macro person data>
 	<table>
	<tr><td><label>Nimi</label></td><td> ${(data.fullName!"")?html}</td></tr>
	<tr><td><label>Osoite</label></td><td> ${(data.address!"")?html} ${data.postCode?html} ${data.city?html}</td></tr>
	<tr><td><label>Email</label></td><td> ${(data.email!"")?html}</td></tr>
	<tr><td><label>Puhelin</label></td><td> ${(data.phone!"")?html}</td></tr>
	</table>
 </#macro>
 
 <#macro plaintextValue fieldName>
 	<@plaintextValueForRow params fieldName />
 </#macro>
 
 <#macro plaintextValueForRow row fieldName>
 	<#assign value = row.get(fieldName).value>
 	<#if value != "">
 	 	<#assign field = rowStructure.get(fieldName)>
 		<#if field.isEnumeration()>
 			<#if !field.hasEnumerationDefined(value)>
 				<abbr title="Epäkelpo valintalistan arvo!">${value?html}</abbr>
 			<#else>
 				<abbr title="${rowStructure.get(fieldName).getEnumerationDescription(value)?html}">${value?html}</abbr>
 			</#if>
		<#else>
			${value?html}
		</#if>
 	</#if>
 </#macro>
 
 <#macro searchTableValueForRow row fieldName>
 	<#assign field = rowStructure.get(fieldName)>
 	<#if field.isDate()>
 		<#assign dateValue = row.get(fieldName).dateValue>
 		${dateValue.year?html}-${dateValue.month?html}-${dateValue.day?html}
 	<#else>
 		<@plaintextValueForRow row fieldName />
 	</#if> 
 </#macro>

 <#macro generateAdvancedSearchField field>
 	<th><label title="${field.desc}" for="s__${field.name}">${field.commonName}</label></th><td><@desiredInputWithID "s" field.name "Valitse..." true /></td>
 </#macro>
 
 <#macro searchInstructions>
 	<div class="searchInstructions">
 		<h6>Hakuohjeita</h6>
 		<p><b>Tekstimuotoisten</b> kenttien osalta voidaan aina käyttää jokerimerkkejä % (kuinka monta merkkiä tahansa) ja _ (jokin yksi merkki).</p>
 		<p><b>Numeeristen</b> kenttien tapauksessa voidaan jokerimerkkien lisäksi käyttää /-merkkiä merkitsemään arvoväliä, esim. -5/10.</p>
 		<p><b>Päiväyksiä</b> voidaan hakea täsmällisellä päivällä (esim. 1.1.2000) tai vain vuodella ja kuukaudella ( .1.2000) tai ainoastaan vuodella (..2000).
 			Lisäksi päiväyksiä voidaan hakea väleillä, esim ..2000/2005 (vuosi välillä 2000-2005) tai 1/5.2/5.2000/2005 
 			(päivä välillä 1-5, kuukausi välillä 2-5 ja vuosi välillä 2000-2005).
 		</p>
 	</div>
 </#macro>