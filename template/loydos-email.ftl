<#include "luomus-header.ftl">
<#include "macro.ftl">

 
 <h4>Lähetä sähköpostia</h4>
  
 <br />
 
 <div>
 	<@loydosInfo form />	
 </div>
 
 <p>
 	<label>Vaihda kieltä</label>
 	<a href="${baseURL}/loydos-tuonti?email=${form.id}&lang=fi">Suomi</a>
 	<a href="${baseURL}/loydos-tuonti?email=${form.id}&lang=en">Englanti</a>
 </p>
 
 <h5>Email</h5>
 
 <form action="${baseURL}/loydos-tuonti" method="post" onsubmit="return confirm('Lähetä?');">
 <input type="hidden" name="email" value="${form.id}" />

<ul class="fieldBox fewFields">
 	
 	<li><label>Vastaanottaja</label> 
 		<input size=50" type="text" name="receiver" value="${form.submitter.email?html}" /> 
 	</li>
 	
 	<li><label>Otsikko</label> 
 		<#if lang == "fi">
 			<input size=50" type="text" name="subject" value="Rengastetusta linnusta" />
 		<#else>
 			<input size=50" type="text" name="subject" value="Your bird ring discovery" />
 		</#if>
 	</li>
 	
 	<li><label>Viesti</label> 
 		
 	
<#if lang == "fi">
<textarea rows="30" cols="80" name="emailBody">Hyvä ${(form.submitter.fullName!"")?html},
<#else>
<textarea rows="30" cols="80" name="emailBody">Dear ${(form.submitter.fullName!"")?html},
</#if>

${letterTexts["C_DENSE_RING"]}: ${(form.ringInfo!"")?html}
${letterTexts["F_SUMMARY_SPECIES"]}: ${(form.species!"")?html}
<#list form.row.iterator() as c><#if c.hasValue()>
${c.field.commonName}: ${c.value?html}
</#if></#list>		
${letterTexts["C_DENSE_AGE"]}: ${(form.age!"")?html}
		
${letterTexts["EMAIL_SENDERNAME"]}
${letterTexts["HEADER_SENDERADDRESS"]}
</textarea>

 	</li>

 	<li>  <input type="submit" name="send" value="Lähetä email" /> </li>
 	<li>  <input type="submit" name="sendAndRemove" value="Lähetä email ja siirrä Löydös lomake poistettuihin" /> </li>
 	</ul>

</form>

 <#include "luomus-footer.ftl">