<#include "luomus-header.ftl">
<#include "macro.ftl">

<h4>Massapäivitys</h4>

<br />

<#if done??>
   <p><a href="${baseURL}/massapaivitys">Lataa uusi tiedosto</a></p>
   <p><a href="${baseURL}/etusivu">Etusivulle</a></p>
<#else>

	<form action="${baseURL}/massapaivitys" method="post" enctype="multipart/form-data">
	
	<ol id="fieldReadableUploadInstructions">
		<li>	Luo tiedosto (tekstitiedosto tai Excel taulukko), jossaensimmäisellä rivillä on sarekkeiden nimet ja jälkimmäisillä riveillä sarakkeita vastaavat arvot.	</li>
		<li>	Tekstitiedoston osalta sarake-eroitin on TAB tai putki | </li>
		<li>	Suositeltavaa on ladata nykyiset tiedot SQL-osiossa tiedostoon, ja tehdä korjaukset siihen. </li>
		<li>	Jos käytät Exceliä, tallenna tiedosto tab separated -muodossa: <br /> <img src="${staticURL}/img/jakelu_listays_save.png" alt="" />	</li>
		<li>	Sarakkeissa täytyy olla mukana taulun ID-sarake. Se on event ja ringer tapauksessa molemmille "id". Rengastajan id on rengastajanumero, event.id on sisäinen juokseva numero. </li>
		<li>	Muokattavissa sarakkeissa ei saa olla mukana tiettyjä kenttiä, kuten renkaan tunnuksia tai laskettavia arvoja (suunta, etäisyys, aika, koordinaattimuunnokset). </li>
		<li>	Muokattaessa tapahtuman aikaa tai koordinaatteja: muokkausten jälkeen järjestelmä laskee laskettavat arvot uudelleen (suunnan, etäisyyden, ajan, koordinaattimuunnokset). </li>
		<li>	Tallenna tiedot (esim päivämäärät, dd.mm.yyyy) samassa muodossa kuin Tipun käyttöliittymässä käyttäen samoja koodeja (alasvetovalikon kirjaimet/numerot).</li>
		<li>	Voit myös poistaa jonkin sarakkeen arvon tietokannasta jättämällä arvon tyhjäksi. </li>
		<li>	Koordinaatteja muokattaessa täytyy  mukana olla kolme saraketta: coordinateSystem (arvoksi YKJ,EUREF,WGS84,WGS84-DMS) JA kyseistä koordinaattityppiä vastaavat lev/pit/N/E sarakkeet. (Katso lisäohje sivun alta). </li>
		<li>	<b>Avaa .txt tiedosto Notepad++ ohjelmassa ja muuta Encoding > Convert to UTF-8 </b></li> 
		<li>	Valitse muokattava taulu.	</li>
		<li>	Lataa tiedosto ja Aja päivitys.	</li>
	</ol>

	
	<h5>Valitse taulu</h5>
	<select name="table">
		<option value="">Valitse taulu</option>
		<option value="event">Event: Rengastus/Tapaaminen</option>
		<option value="ringer">Ringer: Rengastaja</option>
	</select>
	<br />
	<h5>Lataa tiedosto</h5>
	<input type="file" name="file" /> <input type="submit" value="Aja päivitys" />

	</form>
	
	
	<br /><br /><hr/>
	
	
	<div class="disclaimer">
		<p>Huomaa, että tämä osio ei aja riveille validointeja! Tässä osiossa tarkistetaan vain, että tiedot ovat muodollisesti oikein (oikeaa tietotyyppiä / alasvetovalikon arvo täsmää sarakkeen sallittuihin arvoihin).</p>
		<p>Tämän osion käyttö on suositeltavinta tietojen täydentämiselle: esim lisätään uuusia biometriikkatietoja/mittauksia.</p>
	</div>	
	
	
	<h6>Koordinaattikentät</h6>
	<table id="massupdateInfoTable" class="largeTable">
		<thead>
			<tr><th>coordinateSystem</th><th>latitude/leveys/N</th><th>longitude/pituus/E</th><th>Esim arvo</th></tr>
		</thead>
		<tbody>
			<tr><td>YKJ</td><td>uniformLat</td><td>uniformLon</td><td>6701194 3075598 (pituudessa etukolmonen mukaan)</td></tr>
			<tr><td>EUREF</td><td>eurefLat</td><td>eurefLon</td><td>6698380 75601</td></tr>
			<tr><td>WGS84</td><td>wgs84DecimalLat</td><td>wgs84DecimalLon</td><td>60,2345 19,3333</td></tr>
			<tr><td>WGS84-DMS</td><td>wgs84DegreeLat</td><td>wgs84DegreeLon</td><td>601200 192000</td></tr>
		</tbody>
	</table>
	
</#if>




<#include "luomus-footer.ftl">