<pre>${searchCriteriaText}</pre>

<pre>Rivien lukumäärä: ${searchResult.results?size}</pre>

<pre>
<#list resultFields as field>${field.commonName}<#if field_has_next>|</#if></#list>
<#list searchResult.results as row>
<#list resultFields as field>${row.get(field.name).value?html}<#if field_has_next>|</#if></#list>
</#list>
</pre>

