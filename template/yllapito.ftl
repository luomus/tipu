<#include "luomus-header.ftl">
<#include "macro.ftl">

<div class="yllapito">


<div class="changeResource">
<form action="${baseURL}/${resourceName}" method="get">
 	<ul class="fieldBox fewFields">
 	<#if rowStructure.get(resourceFieldName).enumerationValues?has_content>
 					<li> <@selectWithID "resourceID" resourceFieldName /> <input type="submit" value="Vaihda" class="resourceSelectSubmit" /> </li>
 	</#if>
 	<#if !search??> <li> <a href="${baseURL}/${resourceName}/haku">&raquo; ${resourceCommonName} haku</a> </li> </#if>
 	<#if !addNew??> <li> <a href="${baseURL}/${resourceName}/uusi">&raquo; ${resourceCommonName} lisäys</a> </li> </#if>
 	<#if resourceName == "ringer" && !addNew?? && !search??>
 					<li> <a target="lintuvaara" href="${LintuvaaraURL}user_accounts/ringers/${resourceID}/edit">&raquo; Rengastajan tiedot Lintuvaarassa</a></li> 
 					<li> <a target="pdf" class="button" href="${baseURL}/permitsheet/${resourceID}">Tulosta rengastajakortti</a> </li>
 	</#if>
 	</ul>
</form>
</div>


<#if addNew??>
	<h4>${resourceCommonName} lisäys</h4>
<#elseif search??>
	<h4>${resourceCommonName} haku</h4>
	<@searchInstructions />
<#elseif rowStructure.get(resourceFieldName).enumerationValues?has_content>
	<h4>${resourceCommonName} muokkaus: ${rowStructure.get(resourceFieldName).getEnumerationDescription(resourceID)} (${resourceID})</h4>
<#else>
	<h4>${resourceCommonName} muokkaus: ID ${resourceID}</h4>
</#if>


<#if addNew??>
<form action="${baseURL}/${resourceName}/uusi" method="post" class="mainSubmitForm">
<#elseif search??>
<form action="${baseURL}/${resourceName}/haku" method="post" class="mainSubmitForm">
<#else>
<form action="${baseURL}/${resourceName}/${resourceID}" method="post" class="mainSubmitForm">
<a href="${baseURL}/${resourceName}/kopioi/${params.get(resourceFieldName).value}" class="button" >Kopioi</a>

</#if>
 
 <#if ringerSynonyms??>
 	<div class="fieldGroup">
 		<h5>Synonyymit</h5>
 		<#list ringerSynonyms as synonym>
 			<input name="ringerSynonym" value="${synonym}" type="text" size="5"/>
 		</#list>
 		<input name="ringerSynonym" value="" type="text" size="5" placeholder="Uusi" />
 	</div>
 </#if>
 
 	<#list rowStructure.getTablesGroupsNames(resourceName) as groupName>
 		<div class="fieldGroup">
 		<h5>${rowStructure.getGroupDesc(resourceName, groupName)}</h5>
 		<ul class="fieldBox">
 			<#list rowStructure.getTableGroupsFields(resourceName, groupName) as field>
 				<#if (field.isIdField() && !addNew?? && !search??) || (field.isSequenceGivenIdField() && !search??) || field.name?ends_with(".rowid")>
 					<li class="hidden"><@hidden field.name /> </li>		 
 				<#elseif groupName == "dataHistory">
 					<li><label title="${field.desc?html}">${field.commonName}</label> <@plaintextValue field.name /> <@hidden field.name /> </li>
 				<#else>
 					<li><label for="${field.name}" title="${field.desc?html}">${field.commonName}</label> <@desiredInput field.name /> </li>
 				</#if>
 			</#list>
 			<#if search??>
 				<li><input type="submit" value="Hae" /></li>
 			<#else>
 				<li><input type="submit" value="Tallenna" /></li>
 			</#if>
 		</ul>
 		</div>
 	</#list>
</form>

<#if !addNew?? && !search??>
<hr />
<a href="${baseURL}/${resourceName}/kopioi/${params.get(resourceFieldName).value}" class="button" >Kopioi</a>
<span style="display: inline-block; width: 90px;">&nbsp;</span>
<a href="${baseURL}/${resourceName}/poista/${params.get(resourceFieldName).value}" class="button" onclick="return confirm('Poistetaanko varmasti?');">Poista</a>
</#if>

</div>
<#include "luomus-footer.ftl">