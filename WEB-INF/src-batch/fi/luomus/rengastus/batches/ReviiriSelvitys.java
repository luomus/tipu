package fi.luomus.rengastus.batches;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.config.ConfigReader;
import fi.luomus.commons.reporting.ErrorReporingToSystemErr;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.dao.DAO.CustomSQLTarget;
import fi.luomus.rengastus.dao.DAOImple;
import fi.luomus.rengastus.dao.DatasourceDefinition;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.tomcat.jdbc.pool.DataSource;

@SuppressWarnings("unused")
public class ReviiriSelvitys {

	private static final int HAKUSADE = 1000;
	private static final String SQL = "" + 
			" SELECT 	to_char(eventDate, 'yyyy') as year, uniformlat, uniformlon, count(1), max(clutchNumber), max(ringer) " + 
			" FROM	 	event " + 
			" WHERE 	species = 'BUBBUB' " + 
			" AND		coordinateSystem = 'YKJ' " +
			" AND		coordinateAccuracy IN ('1', '10', '100') " + 
			" AND 		(eventDateAccuracy IS NULL OR eventDateAccuracy NOT IN ('-', '8', '9'))" +
			" AND		to_number(to_char(eventDate, 'yyyy')) between 1980 AND 2015 " +
			" AND		type = 'ringing' " + 
			" AND 		age like 'P%' " + 
			" AND		(wingLengthInMillimeters IS NOT NULL OR wingLengthMinMeasurement IS NOT NULL )" + 

// " and ( (uniformlat = 6723500 and uniformlon = 3502800)  " + 
// " OR (uniformlat = 7080800 and uniformlon = 3291200)  " + 
// " OR (uniformlat = 6790600 and uniformlon = 3321600)  " + 
// " OR (uniformlat = 6798000 and uniformlon = 3216000)  " + 
// " OR (uniformlat = 7018800 and uniformlon = 3330000)  " + 
// " OR (uniformlat = 7020800 and uniformlon = 3329500)  " + 
// " OR (uniformlat = 6760100 and uniformlon = 3480900)  " + 
// " OR (uniformlat = 7125600 and uniformlon = 3373100)  " + 
// " OR (uniformlat = 7047200 and uniformlon = 3266400)  " + 
// " OR (uniformlat = 6723200 and uniformlon = 3503700)  " + 
// " OR (uniformlat = 7020900 and uniformlon = 3329200)  " + 
// " OR (uniformlat = 7080000 and uniformlon = 3292000)  " + 
// " OR (uniformlat = 6734600 and uniformlon = 3313600)  " + 
// " OR (uniformlat = 6764100 and uniformlon = 3492000)  " + 
// " OR (uniformlat = 6952500 and uniformlon = 3209300)  " + 
// " OR (uniformlat = 6951700 and uniformlon = 3209200)  " + 
// " OR (uniformlat = 6789600 and uniformlon = 3321600)  " + 
// " OR (uniformlat = 6735400 and uniformlon = 3313100)  " + 
// " OR (uniformlat = 6953100 and uniformlon = 3257800)  " + 
// " OR (uniformlat = 6767700 and uniformlon = 3273500)  " + 
// " OR (uniformlat = 6788500 and uniformlon = 3322100)  " + 
// " OR (uniformlat = 6761100 and uniformlon = 3481000)  " + 
// " OR (uniformlat = 6951600 and uniformlon = 3209200)  " + 
// " OR (uniformlat = 7018800 and uniformlon = 3330200)  " + 
// " OR (uniformlat = 6691700 and uniformlon = 3322300)  " + 
// " OR (uniformlat = 6770100 and uniformlon = 3469800)  " + 
// " OR (uniformlat = 6868400 and uniformlon = 3323800)  " + 
// " OR (uniformlat = 6790600 and uniformlon = 3321500)  " + 
// " OR (uniformlat = 6789900 and uniformlon = 3322400)  " + 
// " OR (uniformlat = 7014500 and uniformlon = 3235100)  " + 
// " OR (uniformlat = 6867800 and uniformlon = 3323400)  " + 
// " OR (uniformlat = 7080700 and uniformlon = 3291300)  " + 
// " OR (uniformlat = 6761100 and uniformlon = 3482000)  " + 
// " OR (uniformlat = 7125100 and uniformlon = 3373000)  " + 
// " OR (uniformlat = 7019600 and uniformlon = 3329300)  " + 
// " OR (uniformlat = 7215100 and uniformlon = 3442000)  " + 
// " OR (uniformlat = 7013100 and uniformlon = 3235600)  " + 
// " OR (uniformlat = 7014600 and uniformlon = 3236600)  " + 
// " OR (uniformlat = 6867900 and uniformlon = 3323500)  " + 
// " OR (uniformlat = 7019700 and uniformlon = 3329600)  " + 
// " OR (uniformlat = 6769800 and uniformlon = 3469600)  " + 
// " OR (uniformlat = 7047300 and uniformlon = 3266600)  " + 
// " OR (uniformlat = 7018800 and uniformlon = 3330100)  " + 
// " OR (uniformlat = 7015100 and uniformlon = 3236200)  " + 
// " OR (uniformlat = 7214000 and uniformlon = 3441000)  " + 
// " OR (uniformlat = 7013400 and uniformlon = 3236700)  " + 
// " OR (uniformlat = 6739000 and uniformlon = 3304800)  " + 
// " OR (uniformlat = 7214700 and uniformlon = 3441300)  " + 
// " OR (uniformlat = 7081700 and uniformlon = 3291600)  " + 
// " OR (uniformlat = 6734500 and uniformlon = 3313600)  " + 
// " OR (uniformlat = 6789900 and uniformlon = 3322300)  " + 
// " OR (uniformlat = 6738800 and uniformlon = 3304900)  " + 
// " OR (uniformlat = 7012600 and uniformlon = 3236600)  " + 
// " OR (uniformlat = 7020600 and uniformlon = 3329500)  " + 
// " OR (uniformlat = 6761100 and uniformlon = 3482100)  " + 
// " OR (uniformlat = 6734700 and uniformlon = 3313700)  " + 
// " OR (uniformlat = 7020900 and uniformlon = 3329700)  " + 
// " OR (uniformlat = 6738900 and uniformlon = 3304500)  " + 
// " OR (uniformlat = 6951700 and uniformlon = 3209400)  " + 
// " OR (uniformlat = 6761200 and uniformlon = 3482100)  " + 
// " OR (uniformlat = 7013700 and uniformlon = 3236500)  " + 
// " OR (uniformlat = 6723800 and uniformlon = 3503900)  " + 
// " OR (uniformlat = 6916200 and uniformlon = 3254500)  " + 
// " OR (uniformlat = 7014200 and uniformlon = 3235700)  " + 
// " OR (uniformlat = 7019800 and uniformlon = 3329500)  " + 
// " OR (uniformlat = 6789900 and uniformlon = 3322500)  " + 
// " OR (uniformlat = 6790200 and uniformlon = 3321800)  " + 
// " OR (uniformlat = 6951900 and uniformlon = 3209200)  " + 
// " OR (uniformlat = 6951600 and uniformlon = 3209300)  " + 
// " OR (uniformlat = 6952500 and uniformlon = 3209200)  " + 
// " OR (uniformlat = 7032000 and uniformlon = 3649600)  " + 
// " OR (uniformlat = 7124900 and uniformlon = 3373500)  " + 
// " OR (uniformlat = 7231100 and uniformlon = 3445300)  " + 
// " OR (uniformlat = 7021000 and uniformlon = 3329000)  " + 
// " OR (uniformlat = 7020700 and uniformlon = 3329300)  " + 
// " OR (uniformlat = 6771800 and uniformlon = 3523000)  " + 
// " OR (uniformlat = 7019800 and uniformlon = 3329900)  " + 
// " OR (uniformlat = 7231000 and uniformlon = 3445400)  " + 
// " OR (uniformlat = 7012700 and uniformlon = 3236600)  " + 
// " OR (uniformlat = 6691100 and uniformlon = 3322200)  " + 
// " OR (uniformlat = 6723600 and uniformlon = 3502700)  " + 
// " OR (uniformlat = 6768200 and uniformlon = 3274300)  " + 
// " OR (uniformlat = 6953800 and uniformlon = 3257500)  " + 
// " OR (uniformlat = 6691600 and uniformlon = 3322300)  " + 
// " OR (uniformlat = 6867800 and uniformlon = 3323600)  " + 
// " OR (uniformlat = 7230900 and uniformlon = 3445000)  " + 
// " OR (uniformlat = 6758900 and uniformlon = 3291900)  " + 
// " OR (uniformlat = 6759900 and uniformlon = 3481700)  " + 
// " OR (uniformlat = 7019000 and uniformlon = 3329500)  " + 
// " OR (uniformlat = 7013700 and uniformlon = 3236200)  " + 
// " OR (uniformlat = 6771200 and uniformlon = 3523000)  " + 
// " OR (uniformlat = 6769600 and uniformlon = 3469600)  " + 
// " OR (uniformlat = 6760400 and uniformlon = 3480900)  " + 
// " OR (uniformlat = 7020100 and uniformlon = 3330000)  " + 
// " OR (uniformlat = 6798000 and uniformlon = 3215000)  " + 
// " OR (uniformlat = 6916100 and uniformlon = 3254500)  " + 
// " OR (uniformlat = 6724100 and uniformlon = 3503800)  " + 
// " OR (uniformlat = 6759800 and uniformlon = 3481800)  " + 
// " OR (uniformlat = 7047200 and uniformlon = 3265700)  " + 
// " OR (uniformlat = 6953100 and uniformlon = 3257700)  " + 
// " OR (uniformlat = 6734800 and uniformlon = 3313500)  " + 
// " OR (uniformlat = 6767800 and uniformlon = 3272700)  " + 
// " OR (uniformlat = 6788500 and uniformlon = 3321700)  " + 
// " OR (uniformlat = 7014500 and uniformlon = 3235000)  " + 
// " OR (uniformlat = 6759800 and uniformlon = 3480600)  " + 
// " OR (uniformlat = 7013700 and uniformlon = 3235800)  " + 
// " OR (uniformlat = 6759800 and uniformlon = 3481200)  " + 
// " OR (uniformlat = 6952600 and uniformlon = 3209200)  " + 
// " OR (uniformlat = 6690400 and uniformlon = 3321800)  " + 
// " OR (uniformlat = 6735500 and uniformlon = 3313200)  " + 
// " OR (uniformlat = 7124800 and uniformlon = 3373200)  " + 
// " OR (uniformlat = 6758300 and uniformlon = 3291900)  " + 
// " OR (uniformlat = 7014600 and uniformlon = 3234800)  " + 
// " OR (uniformlat = 6789500 and uniformlon = 3321600)  " + 
// " OR (uniformlat = 6867600 and uniformlon = 3323200)  " + 
// " OR (uniformlat = 6769900 and uniformlon = 3469600)  " + 
// " OR (uniformlat = 7032000 and uniformlon = 3649500)  " + 
// " OR (uniformlat = 7018200 and uniformlon = 3329300)  " + 
// " OR (uniformlat = 6734800 and uniformlon = 3313600)  " + 
// " OR (uniformlat = 6723900 and uniformlon = 3503900)  " + 
// " OR (uniformlat = 6760200 and uniformlon = 3480800)  " + 
// " OR (uniformlat = 7018900 and uniformlon = 3329900)  " + 
// " OR (uniformlat = 6769800 and uniformlon = 3469700)  " + 
// " OR (uniformlat = 7019400 and uniformlon = 3329300)  " + 
// " OR (uniformlat = 7018400 and uniformlon = 3329100)  " + 
// " OR (uniformlat = 7019500 and uniformlon = 3329300)  " + 
// " OR (uniformlat = 7230800 and uniformlon = 3445200)  " + 
// " OR (uniformlat = 6723000 and uniformlon = 3502300)  " + 
// " OR (uniformlat = 6723900 and uniformlon = 3503800)  " + 
// " OR (uniformlat = 7015400 and uniformlon = 3235900)  " + 
// " OR (uniformlat = 6764200 and uniformlon = 3491900)  " + 
// " OR (uniformlat = 6738900 and uniformlon = 3304900)  " + 
// " OR (uniformlat = 6952300 and uniformlon = 3208500)  " + 
// " OR (uniformlat = 7125100 and uniformlon = 3373200)  " + 
// " OR (uniformlat = 6759500 and uniformlon = 3480600)  " + 
// " OR (uniformlat = 6951700 and uniformlon = 3209300)  " + 
// " OR (uniformlat = 6867600 and uniformlon = 3323300)  " + 
// " OR (uniformlat = 6788600 and uniformlon = 3321700)  " + 
// " OR (uniformlat = 6764100 and uniformlon = 3492100) ) " + 
			 " GROUP BY 	to_char(eventDate, 'yyyy'), uniformlat, uniformlon ";

	public static void main(String[] args) throws Exception {
		DataSource dataSource = null;
		try {
			Config config = new ConfigReader("c:/apache-tomcat/app-conf/rengastus_TUOTANTO.properties");
			ErrorReporter errorReporter = new ErrorReporingToSystemErr();
			dataSource = DatasourceDefinition.initDataSource(config);
			DAO dao = new DAOImple(config, errorReporter, dataSource, "admin_ep");
			executeWithDao(dao);
		} finally {
			if (dataSource != null) dataSource.close();
		}
		System.out.println("done");
	}

	private static void executeWithDao(DAO dao) throws SQLException {
		Pesat pesat = haePesienPesinnat(dao);
		for (Pesa pesa : pesat) {
			System.out.println(pesa.id);
		}
		List<Reviiri> reviirit = selvitaReviirit(pesat);


//		for (Reviiri reviiri : reviirit) {
//			if (!onSamojaVuosia(reviiri)) {
//				System.out.println("Reviiri " + reviiri.id + ": ");
//				for (Pesa pesa : reviiri.pesat()) {
//					Utils.debug(pesa.lev, pesa.pit, pesa.pesintaVuodet, pesa.poikuenNro);
//				}
//				System.out.println();
//			}
//		}
	}

	private static boolean onSamojaVuosia(Reviiri reviiri) {
		Set<Integer> reviirinPesintaVuodet = new HashSet<>();
		for (Pesa pesa : reviiri.pesat()) {
			for (int vuosi : pesa.pesintaVuodet) {
				if (reviirinPesintaVuodet.contains(vuosi)) {
					return true;
				}
				reviirinPesintaVuodet.add(vuosi);
			}
		}
		return false;
	}

	private static List<Reviiri> selvitaReviirit(Pesat pesat) {
		List<Reviiri> reviirit = new ArrayList<>();

		Reviiri reviiri = new Reviiri();
		for (Pesa pesa : pesat) {
			if (pesa.onJoLisattyReviiriin()) continue;
			reviiri.lisaaPesa(pesa);
			pesa.merkitaanLisatyksiReviiriin();

			while (true) {
				List<Pesa> laheisetPesat = etsiLaheisetPesatJoitaEiOleVielaMerkittyReviiriin(reviiri, pesat);
				if (laheisetPesat.isEmpty()) break;
				for (Pesa laheinen : laheisetPesat) {
					reviiri.lisaaPesa(laheinen);
				}
			}
			reviirit.add(reviiri);
			reviiri = new Reviiri();
		}
		if (!reviiri.pesat().isEmpty()) {
			reviirit.add(reviiri);
		}
		return reviirit;
	}

	private static List<Pesa> etsiLaheisetPesatJoitaEiOleVielaMerkittyReviiriin(Reviiri reviiri, Pesat pesat) {
		List<Pesa> laheiset = new ArrayList<>();
		for (Pesa reviirinPesa : reviiri.pesat()) {
			if (reviirinPesa.onJoEtsittyLaheiset()) continue;
			for (Pesa kandidaatti : pesat) {
				if (kandidaatti.onJoLisattyReviiriin()) continue;
				if (pythagoras(reviirinPesa.lev, reviirinPesa.pit, kandidaatti.lev, kandidaatti.pit) <= HAKUSADE) {
					kandidaatti.merkitaanLisatyksiReviiriin();
					laheiset.add(kandidaatti);
				}
			}
			reviirinPesa.merkitaanLaheisetEtsityksi();
		}
		return laheiset;
	}

	private static double pythagoras(int lev1, int pit1, int lev2, int pit2) {
		return Math.sqrt(Math.pow(lev1 - lev2, 2) + Math.pow(pit1 - pit2, 2));
	}

	private static Pesat haePesienPesinnat(DAO dao) throws SQLException {
		List<Pesinta> pesinnat = haePesinnat(dao);
		Pesat pesat = new Pesat();
		for (Pesinta pesinta : pesinnat) {
			pesat.lisaa(pesinta);
		}
		return pesat;
	}

	private static List<Pesinta> haePesinnat(DAO dao) throws SQLException {
		List<List<String>> rows = dao.executeCustomSQLSearch(SQL, CustomSQLTarget.FILE, new ArrayList<String>());
		List<Pesinta> pesinnat = new ArrayList<>();
		for (List<String> row : rows) {
			Pesinta pesinta = parsiPesinta(row);
			pesinnat.add(pesinta);
		}
		return pesinnat;
	}

	private static Pesinta parsiPesinta(List<String> row) {
		int vuosi = iVal(row.get(0)); 
		int lev = iVal(row.get(1));
		int pit = iVal(row.get(2));
		int poikastenLkm = iVal(row.get(3));
		String poikuenro = row.get(4);
		String rengastaja = row.get(5);
		Pesinta pesinta = new Pesinta(vuosi, lev, pit, poikastenLkm, poikuenro, rengastaja);
		return pesinta;
	}

	private static int iVal(String string) {
		return Integer.valueOf(string);
	}


	private static class Pesat implements Iterable<Pesa> {
		Map<Integer, Map<Integer, Pesa>> pesaHakuMap = new HashMap<Integer, Map<Integer,Pesa>>();
		List<Pesa> pesat = new ArrayList<>();

		public void lisaa(Pesinta pesinta) {
			if (!pesaHakuMap.containsKey(pesinta.lev)) {
				pesaHakuMap.put(pesinta.lev, new HashMap<Integer, Pesa>());
			}
			if (!pesaHakuMap.get(pesinta.lev).containsKey(pesinta.pit)) {
				Pesa pesa = new Pesa(pesinta.lev, pesinta.pit, pesinta.poikuenro);
				pesaHakuMap.get(pesinta.lev).put(pesinta.pit, pesa);
				pesat.add(pesa);
			}
			pesaHakuMap.get(pesinta.lev).get(pesinta.pit).lisaaPesinta(pesinta);
		}

		@Override
		public Iterator<Pesa> iterator() {
			return pesat.iterator();
		}
	}
	private static class Pesa {
		private static int idGenerator = 1;
		private final int id;
		private final int lev;
		private final int pit;
		private final String poikuenNro;
		private final TreeSet<Integer> pesintaVuodet = new TreeSet<Integer>();
		private boolean lisattyReviriin = false;
		private boolean laheisetEtsitty = false;
		public Pesa(int lev, int pit, String poikueNro) {
			this.id = idGenerator++;
			this.lev = lev;
			this.pit = pit;
			this.poikuenNro = poikueNro;
		}
		public boolean onJoEtsittyLaheiset() {
			return laheisetEtsitty;
		}
		public void merkitaanLaheisetEtsityksi() {
			laheisetEtsitty = true;
		}
		public boolean onJoLisattyReviiriin() {
			return lisattyReviriin;
		}
		public void merkitaanLisatyksiReviiriin() {
			this.lisattyReviriin = true;
		}
		public void lisaaPesinta(Pesinta pesinta) {
			pesintaVuodet.add(pesinta.vuosi);
		}
		@Override
		public String toString() {
			return Integer.toString(id);
		}
	}

	private static class Pesinta {
		private final int vuosi;
		private final int lev;
		private final int pit;
		private final int poikastenLkm;
		private final String poikuenro;
		
		public Pesinta(int vuosi, int lev, int pit, int poikastenLkm, String poikuenro, String rengastaja) {
			this.vuosi = vuosi;
			this.lev = lev;
			this.pit = pit;
			this.poikastenLkm = poikastenLkm;
			this.poikuenro = rengastaja + ":" + poikuenro;
		}
	}

	private static class Reviiri {
		private static int idGenarator = 1;

		private final int id;
		private final List<Pesa> pesat = new ArrayList<>();

		public Reviiri() {
			id = idGenarator++;
		}

		public void lisaaPesa(Pesa pesa) {
			pesat.add(pesa);
		}

		public List<Pesa> pesat() {
			return pesat;
		}

	}
}
