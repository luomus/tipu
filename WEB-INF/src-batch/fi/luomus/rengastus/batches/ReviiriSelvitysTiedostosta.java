package fi.luomus.rengastus.batches;


import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.luomus.commons.utils.FileUtils;
import fi.luomus.commons.utils.Utils;

public class ReviiriSelvitysTiedostosta {

	private static final int HAKUSADE = 1000;

	public static void main(String[] args) throws Exception {
		File file = new File("C:/temp/rengastetut_pesat_vainYKJ_KARSITTU.txt");
		Pesat pesat = haePesienPesinnat(file);
		//		for (Pesa pesa : pesat) {
		//			Utils.debug(pesa.id, pesa.pesinnat);
		//		}
		List<Reviiri> reviirit = selvitaReviirit(pesat);

		for (Reviiri reviiri : reviirit) {
			if (onSamojaVuosia(reviiri)) {
				System.out.println("Reviiri " + reviiri.id + ": ");
				for (Pesa pesa : reviiri.pesat()) {
					Utils.debug(pesa.lev, pesa.pit, pesa.pesinnat);
				}
				System.out.println();
			}
		}

		System.out.println("done");
	}

	private static boolean onSamojaVuosia(Reviiri reviiri) {
		Set<Integer> reviirinPesintaVuodet = new HashSet<>();
		for (Pesa pesa : reviiri.pesat()) {
			for (Pesinta pesinta : pesa.pesinnat) {
				int vuosi = pesinta.vuosi;
				if (reviirinPesintaVuodet.contains(vuosi)) {
					return true;
				}
				reviirinPesintaVuodet.add(vuosi);
			}
		}
		return false;
	}

	private static List<Reviiri> selvitaReviirit(Pesat pesat) {
		List<Reviiri> reviirit = new ArrayList<>();

		Reviiri reviiri = new Reviiri();
		for (Pesa pesa : pesat) {
			if (pesa.onJoLisattyReviiriin()) continue;
			reviiri.lisaaPesa(pesa);
			pesa.merkitaanLisatyksiReviiriin();

			while (true) {
				List<Pesa> laheisetPesat = etsiLaheisetPesatJoitaEiOleVielaMerkittyReviiriin(reviiri, pesat);
				if (laheisetPesat.isEmpty()) break;
				for (Pesa laheinen : laheisetPesat) {
					reviiri.lisaaPesa(laheinen);
				}
			}
			reviirit.add(reviiri);
			reviiri = new Reviiri();
		}
		if (!reviiri.pesat().isEmpty()) {
			reviirit.add(reviiri);
		}
		return reviirit;
	}

	private static List<Pesa> etsiLaheisetPesatJoitaEiOleVielaMerkittyReviiriin(Reviiri reviiri, Pesat pesat) {
		List<Pesa> laheiset = new ArrayList<>();
		for (Pesa reviirinPesa : reviiri.pesat()) {
			if (reviirinPesa.onJoEtsittyLaheiset()) continue;
			for (Pesa kandidaatti : pesat) {
				if (kandidaatti.onJoLisattyReviiriin()) continue;
				if (pythagoras(reviirinPesa.lev, reviirinPesa.pit, kandidaatti.lev, kandidaatti.pit) <= HAKUSADE) {
					kandidaatti.merkitaanLisatyksiReviiriin();
					laheiset.add(kandidaatti);
				}
			}
			reviirinPesa.merkitaanLaheisetEtsityksi();
		}
		return laheiset;
	}

	private static double pythagoras(int lev1, int pit1, int lev2, int pit2) {
		return Math.sqrt(Math.pow(lev1 - lev2, 2) + Math.pow(pit1 - pit2, 2));
	}

	private static Pesat haePesienPesinnat(File file) throws Exception {
		List<Pesinta> pesinnat = haePesinnat(file);
		Pesat pesat = new Pesat();
		for (Pesinta pesinta : pesinnat) {
			pesat.lisaa(pesinta);
		}
		return pesat;
	}

	private static List<Pesinta> haePesinnat(File file) throws Exception {
		List<Pesinta> pesinnat = new ArrayList<>();
		for (String line : FileUtils.readLines(file)) {
			Pesinta pesinta = parsiPesinta(line);
			pesinnat.add(pesinta);
		}
		return pesinnat;
	}

	private static Pesinta parsiPesinta(String line) {
		// ID	Vv	Yhtlev	Yhtpit
		String[] parts = line.split("\t");
		int id = iVal(parts[0]);
		int vuosi = iVal(parts[1]);
		int lev = iVal(parts[2]);
		int pit = iVal(parts[3]);

		Pesinta pesinta = new Pesinta(id, vuosi, lev, pit);
		return pesinta;
	}

	private static int iVal(String string) {
		return Integer.valueOf(string);
	}


	private static class Pesat implements Iterable<Pesa> {
		Map<Integer, Map<Integer, Pesa>> pesaHakuMap = new HashMap<>();
		List<Pesa> pesat = new ArrayList<>();

		public void lisaa(Pesinta pesinta) {
			if (!pesaHakuMap.containsKey(pesinta.lev)) {
				pesaHakuMap.put(pesinta.lev, new HashMap<Integer, Pesa>());
			}
			if (!pesaHakuMap.get(pesinta.lev).containsKey(pesinta.pit)) {
				Pesa pesa = new Pesa(pesinta);
				pesaHakuMap.get(pesinta.lev).put(pesinta.pit, pesa);
				pesat.add(pesa);
			}
			pesaHakuMap.get(pesinta.lev).get(pesinta.pit).lisaaPesinta(pesinta);
		}

		@Override
		public Iterator<Pesa> iterator() {
			return pesat.iterator();
		}
	}
	private static class Pesa {
		private static int idGenerator = 1;
		private final int id;
		private final int lev;
		private final int pit;
		private final List<Pesinta> pesinnat = new ArrayList<>();
		private boolean lisattyReviriin = false;
		private boolean laheisetEtsitty = false;
		public Pesa(Pesinta pesinta) {
			this.id = idGenerator++;
			this.lev = pesinta.lev;
			this.pit = pesinta.pit;
		}
		public boolean onJoEtsittyLaheiset() {
			return laheisetEtsitty;
		}
		public void merkitaanLaheisetEtsityksi() {
			laheisetEtsitty = true;
		}
		public boolean onJoLisattyReviiriin() {
			return lisattyReviriin;
		}
		public void merkitaanLisatyksiReviiriin() {
			this.lisattyReviriin = true;
		}
		public void lisaaPesinta(Pesinta pesinta) {
			pesinnat.add(pesinta);
		}
		@Override
		public String toString() {
			return Integer.toString(id);
		}
	}

	private static class Pesinta {
		private final int id;
		private final int vuosi;
		private final int lev;
		private final int pit;

		public Pesinta(int id, int vuosi, int lev, int pit) {
			this.id = id;
			this.vuosi = vuosi;
			this.lev = lev;
			this.pit = pit;
		}
		@Override
		public String toString() {
			return "Pesinta " + id + " vuonna " + vuosi;
		}
	}

	private static class Reviiri {
		private static int idGenarator = 1;

		private final int id;
		private final List<Pesa> pesat = new ArrayList<>();

		public Reviiri() {
			id = idGenarator++;
		}

		public void lisaaPesa(Pesa pesa) {
			pesat.add(pesa);
		}

		public List<Pesa> pesat() {
			return pesat;
		}

	}
}
