package fi.luomus.rengastus.dao;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Test;

public class BinRowParserTests {

	@Test
	public void test() {
		String data = " { event.id: 18471586 }  { event.type: ringing }  { event.measurer: eli siis (ei haittaa) mitkään, kummat = merkit } { event.eiooarvotontamuttatestataan:} { eiooarvotontamuttatestataantoinenkin:   }  ";
		Map<String, String> parsedData = BinRowParser.parse(data);
		assertEquals("18471586", parsedData.get("event.id"));
		assertEquals("ringing", parsedData.get("event.type"));
		assertEquals("eli siis (ei haittaa) mitkään, kummat = merkit", parsedData.get("event.measurer"));
		assertEquals("", parsedData.get("event.eiooarvotontamuttatestataan"));
		assertEquals("", parsedData.get("eiooarvotontamuttatestataantoinenkin"));
	}

}
