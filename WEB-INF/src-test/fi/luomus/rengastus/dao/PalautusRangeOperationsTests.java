package fi.luomus.rengastus.dao;

import static org.junit.Assert.assertEquals;

import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.model.Range;
import fi.luomus.rengastus.services.PalautusServlet;
import fi.luomus.rengastus.services.PalautusServlet.RangeOperations;

import java.util.List;

import org.junit.Test;

public class PalautusRangeOperationsTests {

	@Test
	public void test_1() {
		List<Range> distributions = Utils.list(new Range(1, 10));
		RangeOperations operations = PalautusServlet.resolveRangeOperations(1, 10, distributions);
		assertEquals("[1-10]", operations.rangesToDelete.toString());
		assertEquals("{}", operations.rangesToModify.toString());
		assertEquals("{}", operations.rangesToCreate.toString());
	}

	@Test
	public void test_2() {
		List<Range> distributions = Utils.list(new Range(1, 9), new Range(10, 19), new Range(20, 29), new Range(30, 39), new Range(40, 49), new Range(50, 59));
		RangeOperations operations = PalautusServlet.resolveRangeOperations(15, 45, distributions);
		assertEquals("[20-29, 30-39]", operations.rangesToDelete.toString());
		assertEquals("{10=10-14, 40=46-49}", operations.rangesToModify.toString());
		assertEquals("{}", operations.rangesToCreate.toString());
	}
	
	@Test
	public void test_3() {
		List<Range> distributions = Utils.list(new Range(1, 30));
		RangeOperations operations = PalautusServlet.resolveRangeOperations(10, 20, distributions);
		assertEquals("[]", operations.rangesToDelete.toString());
		assertEquals("{1=1-9}", operations.rangesToModify.toString());
		assertEquals("{1=21-30}", operations.rangesToCreate.toString());
	}
	
	@Test
	public void test_4() {
		List<Range> distributions = Utils.list(new Range(1, 30));
		RangeOperations operations = PalautusServlet.resolveRangeOperations(1, 15, distributions);
		assertEquals("[]", operations.rangesToDelete.toString());
		assertEquals("{1=16-30}", operations.rangesToModify.toString());
		assertEquals("{}", operations.rangesToCreate.toString());
	}
	
	@Test
	public void test_5() {
		List<Range> distributions = Utils.list(new Range(1, 30));
		RangeOperations operations = PalautusServlet.resolveRangeOperations(15, 30, distributions);
		assertEquals("[]", operations.rangesToDelete.toString());
		assertEquals("{1=1-14}", operations.rangesToModify.toString());
		assertEquals("{}", operations.rangesToCreate.toString());
	}
	
	@Test
	public void test_6() {
		List<Range> distributions = Utils.list(new Range(1, 9), new Range(10, 19));
		RangeOperations operations = PalautusServlet.resolveRangeOperations(1, 14, distributions);
		assertEquals("[1-9]", operations.rangesToDelete.toString());
		assertEquals("{10=15-19}", operations.rangesToModify.toString());
		assertEquals("{}", operations.rangesToCreate.toString());
	}
	
	@Test
	public void test_7() {
		List<Range> distributions = Utils.list(new Range(1, 9), new Range(10, 19));
		RangeOperations operations = PalautusServlet.resolveRangeOperations(5, 19, distributions);
		assertEquals("[10-19]", operations.rangesToDelete.toString());
		assertEquals("{1=1-4}", operations.rangesToModify.toString());
		assertEquals("{}", operations.rangesToCreate.toString());
	}
	
}
