package fi.luomus.rengastus.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.collect.Lists;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.dao.DAO.EventOperation;
import fi.luomus.rengastus.model.BinRow;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.validators.BaseValidator;

@WebServlet(urlPatterns = {"/odottavat/*", "/roskis/*", "/haudatut/*", "/roskikset/*"})
public class BinServlet extends TipuBaseServlet {

	private static final long serialVersionUID = 4572094894068155329L;

	public static enum BinType {WAITING, TRASH, DELETED}

	private static final Map<String, String> SORTABLE_STATUSES = initSortableStatusesMap();
	private static Map<String, String> initSortableStatusesMap() {
		Map<String, String> map = new HashMap<>();
		map.put("OK", "1 - OK");
		map.put("SHOULD_TRY", "2 - Ehkä ok?");
		map.put("RING_ERRORS", "3 - Rengasvirhe");
		return map;
	}
	
	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) throws Exception {
		return commonData.setViewName("bin");
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return validateAndExecute(req, res, Executor.EMPTY_EXECUTOR, new BinValidator(), new BinExecutor(), new BinAfterExecutor());
	}

	private final class BinAfterExecutor implements Executor {

		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			BinType binType = resolveBinType(req);
			String eventType = resolveEventType(req);
			responseData.setData("binCounts", dao.getBinCounts());
			responseData.setData("binType",  binType == null ? null : binType.toString());
			responseData.setData("eventType",  eventType);
			return responseData;
		}

	}

	private final class BinExecutor implements Executor {

		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			log(req);
			if (req.getRequestURI().contains("/poisto")) {
				String id = getResourceId(req);
				BinRow binRow = dao.getEventFromBin(id);
				dao.updateEventToDeletedBin(binRow.getRow(), "Siiretty haudattuihin. " + binRow.getBinMoveNotes());
				return responseData.setData("success", "Siiretty haudattuihin.");
			}

			BinType binType = resolveBinType(req);
			String eventType = resolveEventType(req);
			if (given(binType, eventType)) {
				List<BinRow> binEvents = getBinEvents(dao, binType, eventType);
				responseData.setData("binEvents", binEvents);
				
				if (req.getRequestURI().contains("/validointi")) {
					validate(binEvents, eventType, responseData, dao);
				}
			}

			return responseData;
		}

		private List<BinRow> getBinEvents(DAO dao, BinType binType, String eventType) throws Exception {
			switch (binType) {
			case WAITING: return dao.getWaitingBinEvents(eventType);
			case TRASH: return dao.getTrashBinEvents(eventType);
			case DELETED: return dao.getDeletedBinEvents(eventType);
			}
			throw new IllegalArgumentException("Unknown bin type: " + binType);
		}
		
		private void validate(List<BinRow> binEvents, String eventType, ResponseData responseData, DAO dao) throws Exception {
			Map<String, List<String>> rowErrors = new HashMap<>();
			Map<String, String> rowQuessedStatus = new HashMap<>();
			Map<String, Integer> quessedStatusCounts = new HashMap<>();
			quessedStatusCounts.put("OK", 0);
			quessedStatusCounts.put("SHOULD_TRY", 0);
			quessedStatusCounts.put("RING_ERRORS", 0);
			responseData.setData("rowErrors", rowErrors);
			responseData.setData("quessedStatuses", rowQuessedStatus);
			responseData.setData("quessedStatusCounts", quessedStatusCounts);
			responseData.setData("sortableStatuses", SORTABLE_STATUSES);
			List<List<BinRow>> chunks = Lists.partition(binEvents, 500);
			for (List<BinRow> chunk : chunks) {
				validate(chunk, eventType, dao, rowErrors, rowQuessedStatus, quessedStatusCounts);				
			}
			
			responseData.setData("validated", true);
		}

		private void validate(List<BinRow> binEvents, String eventType, DAO dao, Map<String, List<String>> rowErrors, Map<String, String> rowQuessedStatus, Map<String, Integer> quessedStatusCounts) throws Exception {
			JSONObject data = new JSONObject();
			for (BinRow binRow : binEvents) {
				EventUpdateServlet.rowToTipuApiJSON(binRow.getEventId(), binRow.getRow(), data);
			}
			JSONObject validationResponse = dao.validateEventUsingAPI(data, eventType.equals("ringing") ? "ringings" : "recoveries", EventOperation.INSERT);

			Row rowStructure = dao.newRow();
			for (String eventId : validationResponse.getObject("rows").getKeys()) {
				JSONObject errors = validationResponse.getObject("rows").getObject(eventId).getObject("errors");
				boolean hasRingErrors = false;
				for (String errorColumn : errors.getKeys()) {
					if (errorColumn.toLowerCase().contains("ring") || errorColumn.toLowerCase().contains("fieldreadablecode")) hasRingErrors = true;
					if (!rowErrors.containsKey(eventId)) rowErrors.put(eventId, new ArrayList<String>());
					String columnCommonName = rowStructure.get("event."+errorColumn).getField().getCommonName();
					for (JSONObject error : errors.getArray(errorColumn).iterateAsObject()) {
						String localizedErrorText = error.getString("localizedErrorText");
						rowErrors.get(eventId).add(columnCommonName + ": " + localizedErrorText);
					}
				}
				boolean hasErrors = errors.getKeys().length > 0;
				String quessedStatus = hasErrors ? "SHOULD_TRY" : "OK";
				if (hasRingErrors) quessedStatus = "RING_ERRORS";
				rowQuessedStatus.put(eventId, quessedStatus);
				quessedStatusCounts.put(quessedStatus, quessedStatusCounts.get(quessedStatus) + 1);
			}
		}
	}

	private BinType resolveBinType(HttpServletRequest req) {
		if (req.getRequestURI().contains("/odottavat")) {
			return BinType.WAITING;
		} else if (req.getRequestURI().contains("/roskis")) {
			return BinType.TRASH;
		} else if (req.getRequestURI().contains("/haudatut")) {
			return BinType.DELETED;
		}
		return null;
	}

	private String resolveEventType(HttpServletRequest req) {
		if (req.getRequestURI().contains("/rengastukset")) {
			return "ringing";
		} else if (req.getRequestURI().contains("/tapaamiset")) {
			return "recovery";
		}
		return null;
	}

	private static class BinValidator extends BaseValidator {

		@Override
		protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
			if (!req.getRequestURI().contains("/poisto")) return;
			String eventId = getResourceId(req);
			if (!given(eventId)) {
				setCustomFieldError("ID on annettava.");
				return;
			}
			if (!isInteger(eventId)) {
				setCustomFieldError("Epäkelpo ID.");
				return;
			}
			if (dao.getEventFromBin(eventId) == null) {
				setCustomFieldError("Tällä ID:llä ei löydy mitään poistettavaa.");
			}
		}

	}

}
