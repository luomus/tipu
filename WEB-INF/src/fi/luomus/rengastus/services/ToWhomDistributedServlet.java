package fi.luomus.rengastus.services;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Ring;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.Validator;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/kenelle-jaettu/*")
public class ToWhomDistributedServlet extends TipuBaseServlet {

	private static final long serialVersionUID = 5313424716919033627L;

	private static class AfterExecutor implements Executor {

		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			String param = getResourceId(req);
			if (!given(param)) {
				return responseData.setData("error", "Anna rengas!");
			}
			row.get("distributed.ringStart").setValue(param); // Set and get from row: will remove special chars and do other ring column handling
			Ring ring = new Ring(row.get("distributed.ringStart").getValue());
			if (!ring.validFormat()) {
				return responseData.setData("error", "Epäkelpo rengas '"+ param +"'!");
			}
			Row distribution = dao.toWhomDistributed(ring);
			if (distribution == null) {
				return responseData.setData("message", "Rengasta " + param + " ei ole jaettu.");
			}
			Column ringStart = distribution.get("distributed.ringStart");
			Column ringEnd = distribution.get("distributed.ringEnd");
			Column distributedTo = distribution.get("distributed.distributedTo");
			String ringerName = distributedTo.getField().getEnumerationDescription(distributedTo.getValue());
			String message = param + " on jaettu rengastajalle " + ringerName + ". Väli: " + ringStart.getValue() + " - " + ringEnd.getValue();
			return responseData.setData("success", message);
		}
		
	}
	
	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		return validateAndExecute(req, res, Executor.EMPTY_EXECUTOR, Validator.EMPTY_VALIDATOR, Executor.EMPTY_EXECUTOR, new AfterExecutor());
	}

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) throws Exception {
		return commonData.setViewName("jakelu");
	}

}
