package fi.luomus.rengastus.services;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.dao.DAO.EventOperation;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.ValidationResponse;
import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.validators.EventValidator;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/poista/*")
public class EventDeleteServlet extends TipuBaseServlet {

	private static final long serialVersionUID = -3938299585754694216L;

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) {
		return commonData;
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		return validateAndExecute(req, res, Executor.EMPTY_EXECUTOR, new EventDeleteValidator(), new EventDeleteExecutor(), Executor.EMPTY_EXECUTOR);
	}

	private static class EventDeleteValidator implements Validator {

		@Override
		public ValidationResponse validate(Row row, HttpServletRequest req, DAO dao) {
			ValidationResponse validationResponse = new ValidationResponse();
			String id = getResourceId(req);
			if (!given(id)) {
				validationResponse.setError("event.id", "ID on annettava kun poistetaan.");
				return validationResponse.setViewIfErrors("haku");
			}
			Row rowToDelete = null;
			try {
				rowToDelete = dao.getEventById(id);
			} catch (Exception e) {
				return validationResponse.setError("event.id", "Tällä ID:llä ei löydy mitään.");
			}
			return new EventValidator(id, EventOperation.DELETE).validate(rowToDelete, req, dao);
		}

	}

	private static class EventDeleteExecutor implements Executor {

		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			String eventId = getResourceId(req);
			Row rowToDelete = dao.getEventById(eventId);
			JSONObject response = dao.deleteEventUsingAPI(eventId);
			if (response.getBoolean("success")) {
				dao.insertEventToDeletedBin(rowToDelete, "Poistettu tietokannasta käyttöliittymällä " + DateUtils.getCurrentDateTime("dd.MM.yyyy"));
				return responseData.setData("success", "Poisto onnistui tietokannasta ja tapaus tallennettiin haudattuihin. ");
			}
			return responseData.setData("error", response.getString("error")).setRedirectLocation(config.baseURL() +"/muokkaa/"+eventId);
		}

	}

}
