package fi.luomus.rengastus.services;

import fi.luomus.commons.services.ResponseData;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.Validator;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/yllapito")
public class ResourceIndexPageServlet extends TipuBaseServlet {

	private static final long serialVersionUID = 6951900196961482768L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return validateAndExecute(req, res,  Executor.EMPTY_EXECUTOR, Validator.EMPTY_VALIDATOR, Executor.EMPTY_EXECUTOR,  Executor.EMPTY_EXECUTOR);
	}

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) {
		return commonData.setViewName("yllapitoIndex");
	}
	
}
