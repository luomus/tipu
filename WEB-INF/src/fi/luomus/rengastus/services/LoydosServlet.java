package fi.luomus.rengastus.services;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.EmailUtil;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.LoydosForm;
import fi.luomus.rengastus.model.LoydosForm.Person;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.Validator;

@WebServlet("/loydos-tuonti")
public class LoydosServlet extends TipuBaseServlet {

	private static final long serialVersionUID = -1764161489643800191L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		return validateAndExecute(req, res, Executor.EMPTY_EXECUTOR, Validator.EMPTY_VALIDATOR, new LoydosExecutor(), Executor.EMPTY_EXECUTOR);
	}

	private class LoydosExecutor implements Executor {

		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			String loydosID = req.getParameter("tapaamiseen");
			if (given(loydosID)) {
				return proceedToEventInsert(loydosID, responseData, dao);
			}
			loydosID = req.getParameter("ulkoon");
			if (given(loydosID)) {
				return proceedToUlkoInsert(loydosID, responseData, dao);
			}
			loydosID = req.getParameter("email");
			if (given(loydosID)) {
				String lang = req.getParameter("lang");
				return proceedToLoydosEmail(loydosID, lang, responseData, dao);
			}
			Set<String> onlyTypes = Utils.set(req.getParameterValues("onlyTypes"));
			int page = getPage(req);
			int pageCount = dao.getLoydosFormPageCount(onlyTypes);
			return responseData
					.setData("forms", dao.getLoydosForms(onlyTypes, page))
					.setData("onlyTypes", onlyTypes)
					.setData("pagingParam", "" +(page + 1))
					.setData("pageCount", ""+pageCount);
		}

		private int getPage(HttpServletRequest req) {
			String page = req.getParameter("page");
			if (page == null) {
				page = getSession(req).get("loydosPage");
			}
			if (page == null) return 0;
			try {
				int i = Integer.valueOf(page) - 1;
				if (i < 0) return 0;
				getSession(req).put("loydosPage", ""+(i + 1));
				return i;
			} catch (Exception e) {
				return 0;
			}
		}

		private ResponseData proceedToLoydosEmail(String loydosID, String lang, ResponseData responseData, DAO dao) throws Exception {
			LoydosForm form = dao.getLoydosForm(loydosID);
			if (lang == null) lang = "fi";
			Map<String, String> texts = getLetterTexts(lang, dao);
			return responseData.setViewName("loydos-email").setData("form", form).setData("lang", lang).setData("letterTexts", texts);
		}

		private Map<String, String> getLetterTexts(String lang, DAO dao) throws Exception {
			Map<String, String> texts = new HashMap<>();
			Document response = dao.getTipuApiClient().getAsDocument(TipuAPIClient.CODES, "700");
			for (Node codeNode : response.getRootNode()) {
				String textId = codeNode.getNode("code").getContents();
				String text = "";
				for (Node descNode : codeNode.getChildNodes("desc")) {
					if (descNode.getAttribute("lang").toLowerCase().equals(lang)) {
						text = descNode.getContents();
						break;
					}
				}
				texts.put(textId, text);
			}
			return texts;
		}

		private ResponseData proceedToUlkoInsert(String loydosID, ResponseData responseData, DAO dao) throws Exception {
			dao.markLoydosFormTemporarilyReceived(loydosID);
			return responseData.setRedirectLocation("https://rengastus.helsinki.fi/seuranta/Ulko?page=receive&loydosid="+loydosID);
		}

		private ResponseData proceedToEventInsert(String loydosID, ResponseData responseData, DAO dao) throws Exception {
			LoydosForm form = dao.getLoydosForm(loydosID);
			Row row = form.getRow();
			row.get("event.id").setValue(dao.getNextEventId());
			row.get("event.type").setValue("recovery");
			String ringInfo = form.getRingInfo();
			if (ringInfo.length() <= 10) {
				row.get("event.legRing").setValue(ringInfo);
			}
			row.get("event.recoveryMethodAccuracy").setValue("0");
			row.get("event.sourceOfRecovery").setValue("2");
			if (row.get("event.lat").hasValue()) {
				row.get("event.coordinateAccuracy").setValue("1");
			}
			tryToFillLayman(dao, form, row);
			return responseData.setViewName("event").setData("params", row).setData("loydosForm", form).setData("action", "INSERT");
		}

		private void tryToFillLayman(DAO dao, LoydosForm form, Row row) throws Exception {
			Column discovererRinger = row.get("event.ringer");
			Column intermediaryRinger = row.get("event.intermediaryRinger");
			Person laymanCandidate = null;
			if (!discovererRinger.hasValue() && !intermediaryRinger.hasValue()) {
				// Kumpikaan ei ole rengastaja joten otetaan maallikkoehdokkaaksi ensisijaisesti löytäjä tai jos sitä ei ole annettu niin lomakkeen lähettäjä
				laymanCandidate = form.getDiscoverer().hasValues() ? form.getDiscoverer() : form.getSubmitter();
			} else if (intermediaryRinger.hasValue()) {
				// Jos välittäjä on annettu mutta löytäjän rengastajanumeroa ei ole annettu, maallikoksi löytäjä, mikäli tiedot annettu
				laymanCandidate = form.getDiscoverer();
			}
			if (laymanCandidate != null && laymanCandidate.hasValues()) {
				dao.fillExistingLaymanOrInsertAndFill(laymanCandidate, row);
			}
		}
	}


	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) {
		return commonData.setViewName("loydos");
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		String idToRemove = req.getParameter("remove");
		String idToAddNotes = req.getParameter("addNotes");
		String idToEmail = req.getParameter("email");
		if (idToRemove != null) {
			getDao(req).markDeletedToLoydos(idToRemove);
		} else if (idToAddNotes != null) {
			String notes = req.getParameter("notes");
			getDao(req).saveLoydosNotes(idToAddNotes, notes);
		} else if (idToEmail != null) {
			String receiver = req.getParameter("receiver");
			String subject = req.getParameter("subject");
			String emailBody = req.getParameter("emailBody");
			String sendAndRemove = req.getParameter("sendAndRemove");
			sendEmail(receiver, subject, emailBody);
			String notes = getDao(req).getLoydosNotes(idToEmail);
			if (notes == null) notes = "";
			notes += " " + emailBody;
			notes = notes.trim();
			if (notes.length() > 3500) {
				notes = notes.substring(0, 3500);
			}
			getDao(req).saveLoydosNotes(idToEmail, notes);
			if (sendAndRemove != null) {
				getDao(req).markDeletedToLoydos(idToEmail);
			}
			return new ResponseData().setRedirectLocation(getConfig().baseURL()+"/loydos-tuonti");
		} else {
			throw new IllegalStateException();
		}
		res.setContentType("text/plain");
		res.getWriter().write("ok");
		return new ResponseData().setOutputAlreadyPrinted();
	}

	private void sendEmail(String receiver, String subject, String emailBody) throws AddressException, MessagingException {
		if (getConfig().developmentMode()) {
			Utils.debug("Send email", receiver, subject, emailBody);
		} else {
			EmailUtil emailUtil = new EmailUtil("localhost");
			emailUtil.send(receiver, "rengastustoimisto@helsinki.fi", subject, emailBody);
		}
	}

}
