package fi.luomus.rengastus.services;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.client.methods.HttpGet;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.session.SessionHandler;
import fi.luomus.commons.utils.URIBuilder;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;

@WebServlet("/login")
public class LoginServlet extends TipuBaseServlet {

	private static final long serialVersionUID = -7698153657687791555L;

	@Override
	protected boolean authorized(HttpServletRequest req) {
		return true;
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String key = req.getParameter("key");
		String iv = req.getParameter("iv");
		String data = req.getParameter("data");
		if (!given(key, iv, data)) return notAuthorizedRequest(req, res);

		HttpClientService client = null;
		try {
			Config config = getConfig();
			String tipuApiUri = config.get(Config.TIPU_API_URI);
			String tipuApiUser = config.get(Config.TIPU_API_USERNAME);
			String tipuApiPassword = config.get(Config.TIPU_API_PASSWORD);
			client = new HttpClientService(tipuApiUri, tipuApiUser, tipuApiPassword);
			URIBuilder uri = new URIBuilder(tipuApiUri + "/lintuvaara-authentication-decryptor");
			uri.addParameter("key", key);
			uri.addParameter("iv", iv);
			uri.addParameter("data", data);
			HttpGet request = new HttpGet(uri.toString());
			Node response = client.contentAsDocument(request).getRootNode();
			if (!response.getAttribute("pass").equals("true")) {
				throw new IllegalArgumentException(response.getNode("error").getContents());
			}
			if (!response.getAttribute("type").equals("admin")) throw new IllegalStateException("Not admin! : " + response.toString());
			if (!response.getNode("auth_for").getContents().startsWith(config.systemId())) throw new IllegalStateException("Not for this system! : " + response.toString());
			String userid = response.getNode("login_id").getContents();
			String name = response.getNode("name").getContents();
			SessionHandler session = getSession(req);
			session.authenticateFor(config.systemId());
			session.setUserId(userid);
			session.setUserName(name);
			session.setTimeout(6*60); // 6 hours
			session.put("internal_session_id", Utils.generateGUID());
			String service_uri = req.getParameter("service_uri");
			if (given(service_uri)) {
				return redirectTo(service_uri);
			}
			return redirectTo(config.baseURL()+"/etusivu");
		} catch (Exception e) {
			getErrorReporter().report("Login attempt: " + Utils.debugS("key", key, "iv", iv, "data", data), e);
			return notAuthorizedRequest(req, res);
		}
		finally {
			if (client != null) client.close();
		}
	}

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) {
		return commonData;
	}

}
