package fi.luomus.rengastus.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.io.IOUtils;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.dao.RowBatch;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.ValidationResponse;
import fi.luomus.rengastus.model.Validator;

@WebServlet("/fieldReadableUpload/*")
@MultipartConfig
public class FieldReadableUploadServlet extends TipuBaseServlet {

	private static final long serialVersionUID = 6085627217008719195L;

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) throws Exception {
		return commonData.setViewName("varitunniste-jakelulisays");
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return validateAndExecute(req, res, Executor.EMPTY_EXECUTOR, new Validator() {

			@Override
			public ValidationResponse validate(Row row, HttpServletRequest req, DAO dao) {
				try {
					Part filePart = req.getPart("file");
					if (filePart == null || filePart.getSize() == 0) {
						return new ValidationResponse().setError("file", "Anna tiedosto!");
					}
					return new ValidationResponse();
				} catch (Exception e) {
					return new ValidationResponse().setError("file", e.getMessage());
				}
			}

		}, new Executor() {

			@Override
			public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
				InputStream stream = null;
				try {
					Part filePart = req.getPart("file");
					stream = filePart.getInputStream();
					List<String> lines = getLines(stream);
					int count = execute(lines, dao);
					return responseData.setData("success", "Lisätty/muokattu väritunnisteiden jakeluun onnistuneesti " + count + " riviä.");
				} catch (Exception e) {
					getErrorReporter().report("Upload fieldreadable", e);
					return responseData.setData("error", e.getMessage());
				}
				finally {
					if (stream !=  null) stream.close();
				}
			}

			private int execute(List<String> lines, DAO dao) throws Exception {
				int count = 0;
				RowBatch batch = new RowBatch();
				for (String line : lines) {
					if (Utils.removeWhitespace(line).length() < 10) continue;
					if (line.startsWith("Merkit")) continue;
					String[] parts = line.split("\t");
					Row row = dao.newRow();
					row.get("fieldReadable.code").setValue(parts[0]);
					row.get("fieldReadable.mainColor").setValue(parts[1]);
					row.get("fieldReadable.forSpecies").setValue(parts[3]);
					row.get("fieldReadable.doubleDistributedCount").setValue("1");
					row.get("fieldReadable.distributedTo").setValue(intValue(parts[4]));
					row.get("fieldReadable.distributedDate").setValue(DateUtils.format(DateUtils.convertToDate(parts[6], "yyyy-MM-dd"), "dd.MM.yyyy"));
					row.get("fieldReadable.attachmentPoint").setValue(parts[5]);
					row.get("fieldReadable.codeColor").setValue(parts[2]);
					row.get("fieldReadable.codeAlign").setValue(parts[7]);
					row.get("fieldReadable.codeRepeatedCount").setValue(intValue(tryget(parts, 8)));
					row.get("fieldReadable.heightInMillimeters").setValue(doubleValue(tryget(parts, 9)));
					String diameter = doubleValue(tryget(parts, 10));
					String notes = "";
					if (diameter.equals("3k")) { // kolmikulmainen
						notes = "Kolmikulmainen.";
					} else {
						row.get("fieldReadable.diameterInMillimeters").setValue(diameter);
					}
					row.get("fieldReadable.material").setValue(tryget(parts, 11));
					row.get("fieldReadable.otherInfo").setValue(tryget(parts, 12));
					row.get("fieldReadable.font").setValue(tryget(parts, 13));
					row.get("fieldReadable.checksumCharacterFormula").setValue(tryget(parts, 14));
					notes = (notes + " " + tryget(parts, 15)).trim();
					row.get("fieldReadable.notes").setValue(notes);

					batch.add(row, row.get("fieldReadable.code"), row.get("fieldReadable.mainColor"), row.get("fieldReadable.forSpecies"));
					count++;
				}
				dao.storeBatchToDB(batch);
				return count;
			}

			private String intValue(String s) {
				if (s.length() == 0) return "";
				return Integer.valueOf(s).toString();
			}

			private String doubleValue(String s) {
				if (s.length() == 0) return "";
				if (s.equals("3k")) return s;
				return Double.valueOf(s.replace(",", ".")).toString();
			}

			private String tryget(String[] parts, int i) {
				try {
					return parts[i];
				} catch (ArrayIndexOutOfBoundsException e) {
					return "";
				}
			}

			private List<String> getLines(InputStream stream) throws IOException {
				List<String> lines = IOUtils.readLines(stream, "utf-8");
				return lines;
			}

		}, Executor.EMPTY_EXECUTOR);
	}


}
