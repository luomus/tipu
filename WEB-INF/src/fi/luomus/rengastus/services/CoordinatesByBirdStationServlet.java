package fi.luomus.rengastus.services;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.client.methods.HttpGet;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;

@WebServlet("/coordinates-by-bird-station")
public class CoordinatesByBirdStationServlet extends TipuBaseServlet {

	private static final long	serialVersionUID	= -4120767499385586005L;

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) {
		return commonData;
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		res.setContentType("application/json; charset=utf-8");
		PrintWriter out = res.getWriter();
		String birdStation = req.getParameter("bird-station");
		HttpClientService client = null;
		try {
			client = getDao(req).getTipuApiAuthenenticatedClient();
			HttpGet request = new HttpGet(getConfig().get(Config.TIPU_API_URI)+"/bird-stations/"+birdStation);
			Document doc = client.contentAsDocument(request);
			Node stationInfo = doc.getRootNode().getNode("bird-station");
			JSONObject json = new JSONObject();
			json.setString("lat", stationInfo.getAttribute("centerpoint-lat"));
			json.setString("lon", stationInfo.getAttribute("centerpoint-lon"));
			json.setString("coordinateSystem", "WGS84");
			json.setString("coordinateAccuracy", toValid(stationInfo.getNode("radius").getContents()));
			json.setString("coordinateMeasurementMethod", "MUU");
			json.setString("municipality", stationInfo.getNode("municipality").getContents());
			out.write(json.toString());
		} finally {
			if (client != null) client.close();
		}
		return new ResponseData().setOutputAlreadyPrinted();
	}

	private static final List<Integer> VALID_COORD_ACC = Utils.list(
			1, 10, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 5000, 10000, 20000, 50000, 100000);

	private String toValid(String contents) {
		try {
			int v = Integer.valueOf(contents);
			for (Integer valid : VALID_COORD_ACC) {
				if (v <= valid) return valid.toString();
			}
		} catch (NumberFormatException e) {}
		return null;
	}


}
