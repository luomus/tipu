package fi.luomus.rengastus.services;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.validators.BaseValidator;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/tuhoutunut-havinnyt/*"})
public class ReportingDestroyedOrLostService extends TipuBaseServlet {

	private static final long serialVersionUID = -3596073288749791489L;

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) throws Exception {
		return commonData.setViewName("event").setData("action", "INSERT");
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return validateAndExecute(req, res, Executor.EMPTY_EXECUTOR, new DestoyedLostShorcutValidator(), new DestoyedLostShorcutExecutor(), Executor.EMPTY_EXECUTOR);
	}

	private static class DestoyedLostShorcutValidator extends BaseValidator {

		@Override
		protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
			require(row.get("event.ringer"));
			require(row.get("event.ringStart"));
			require(row.get("event.ringNotUsedReason"));
			require(row.get("event.eventDate"));
		}
		
	}
	
	private static class DestoyedLostShorcutExecutor implements Executor {

		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			row.get("event.type").setValue("ringing");
			row.get("event.id").setValue(dao.getNextEventId()); 
			return responseData;
		}
		
	}
	
}
