package fi.luomus.rengastus.services;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.dao.SearchResult;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.EnumerationsContainer;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.Field;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.RowStructure;
import fi.luomus.rengastus.model.SearchResultBrowser;
import fi.luomus.rengastus.model.ValidationResponse;
import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.validators.BaseValidator;

public abstract class ResourceBaseServlet extends TipuBaseServlet {

	private static final long serialVersionUID = 7571171090574846306L;

	protected abstract String resourceIdFieldName();

	protected abstract String resourceName();

	protected abstract String resourceCommonName();

	protected abstract List<String> getTipuApiResourceNames();

	protected String frontpage() {
		return "yllapito";
	}

	protected String frontpageView() {
		return "yllapitoIndex";
	}

	protected void cleanResourceForCopy(@SuppressWarnings("unused") Row row) {

	}

	protected abstract Validator getValidator();

	protected abstract Validator getDeleteValidator();

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) {
		if (req.getRequestURI().contains("/haku")) {
			commonData.setData("search", "true");
		}
		if (IsInsertNew(req) || req.getRequestURI().contains("/kopioi")) {
			commonData.setData("addNew", "true");
		}
		return commonData
				.setViewName("yllapito")
				.setData("resourceFieldName", resourceIdFieldName())
				.setData("resourceName", resourceName())
				.setData("resourceCommonName", resourceCommonName());
	}

	public class BeforeExecution implements Executor {

		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			if (IsInsertNew(req)) {
				return responseData;
			}
			if (req.getRequestURI().contains("/haku")) {
				return responseData;
			}
			String resourceID = getResourceId(req);
			Column c = row.get(resourceIdFieldName());
			if (!c.hasValue() && given(resourceID)) {
				c.setValue(resourceID);
			}
			c.setValue(c.getValue().toUpperCase());
			return responseData;
		}

	}


	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		return validateAndExecute(req, res, new BeforeExecution(), new ResourceBaseServletGetValidator(), new GetExecutor(), Executor.EMPTY_EXECUTOR);
	}

	public class GetExecutor implements Executor {
		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			if (IsInsertNew(req)) {
				return responseData;
			}
			if (req.getRequestURI().contains("/haku")) {
				if ("true".equals(req.getParameter("historiasta"))) {
					List<Row> results = SearchResultBrowser.getSearchBrowserEntry(getSession(req)).getResults().getResults();
					return responseData.setData("rows", results).setViewName("yllapitoSearchListing");
				}
				return responseData;
			}

			if (req.getRequestURI().contains("/poista")) { // TODO pois getistä
				Column c = row.get(resourceIdFieldName());
				dao.deleteResourceFromDB(c);
				updateEnumeration(dao);
				return responseData.setRedirectLocation(config.baseURL()+"/"+frontpage()).setData("success", "Poistettu onnistuneesti.");
			}

			Column idColumn = row.get(resourceIdFieldName());
			dao.fillResourceByIdFields(row, idColumn);
			SearchResultBrowser.setActiveEntry(idColumn.getValue(), getSession(req));

			if (req.getRequestURI().contains("/kopioi")) {
				cleanForCopy(row);
			}

			return responseData;
		}

		private void cleanForCopy(Row row) {
			for (Field f: row.getTableFields(resourceName())) {
				if (f.getGroupName().equals("dataHistory")) {
					row.get(f.getName()).setValue("");
				}
			}
			cleanResourceForCopy(row);
		}
	}

	public class ResourceBaseServletGetValidator extends BaseValidator {
		@Override
		protected String viewIfErrors() {
			return frontpageView();
		}
		@Override
		protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
			if (IsInsertNew(req)) {
				return;
			}
			if (req.getRequestURI().contains("/haku")) {
				return;
			}

			Column c = row.get(resourceIdFieldName());
			if (!c.hasValue()) {
				setError(c, "Muokattavan resurssin ID täytyy antaa.");
			}
			typeValidate(c);
			if (hasErrors()) return;
			if (!dao.resourceExists(c)) {
				setError(c, "Tällä ID:llä ei löytynyt mitään muokattavaa...");
			}
			if (req.getRequestURI().contains("/poista")) {
				ValidationResponse validationResponse = getDeleteValidator().validate(row, req, dao);
				this.append(validationResponse);
			}
		}
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		return validateAndExecute(req, res, new BeforePostRequestHandlingExecutor(), new ResourceBaseServletPostValidator(), new PostExecutor(), Executor.EMPTY_EXECUTOR);
	}

	public class BeforePostRequestHandlingExecutor implements Executor {
		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			Column c = row.get(resourceIdFieldName());
			c.setValue(c.getValue().toUpperCase());
			return responseData;
		}
	}

	public class PostExecutor implements Executor {
		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			if (req.getRequestURI().contains("/haku")) {
				for (Column c : row) {
					if (!c.hasValue()) continue;
					if (c.getField().isEnumeration()) continue;
					if (c.getField().isNumeric()) continue;
					if (c.getField().isDate()) continue;
					if (c.getField().isIdField()) {
						c.setValue(c.getValue()+"%");
					} else {
						c.setValue("%"+c.getValue()+"%");
					}
				}
				SearchResult searchResult = dao.searchResources(row, resourceName());
				if (!searchResult.wasSuccessful()) {
					return responseData.setData("error", searchResult.getErrorMessage()).setViewName(frontpageView());
				}
				SearchResultBrowser.registerSearch(searchResult, getSession(req));
				if (searchResult.getResults().size() > 0) {
					return responseData.setData("rows", searchResult.getResults()).setViewName("yllapitoSearchListing");
				}
				return responseData.setData("message", "Haulla ei löytynyt mitään");
			}

			Column idColumn = null;
			if (IsInsertNew(req)) {
				idColumn = row.get(resourceIdFieldName());
			} else {
				String resourceID = getResourceId(req);
				Row originalData = dao.newRow();
				idColumn = originalData.get(resourceIdFieldName());
				idColumn.setValue(resourceID);
			}

			dao.storeResourceToDB(row, idColumn);

			Column newIdColumn = row.get(resourceIdFieldName());
			if (IsInsertNew(req)) {
				updateEnumeration(dao);
				responseData.setData("success", "Tiedot lisätty.");
			} else {
				responseData.setData("success", "Tiedot tallennettu.");
			}
			return redirectAfterSubmit(responseData, config, newIdColumn);
		}

	}

	protected ResponseData redirectAfterSubmit(ResponseData responseData, Config config, Column newIdColumn) {
		return responseData.setRedirectLocation(config.baseURL()+"/"+resourceName()+"/"+Utils.urlEncode(newIdColumn.getValue()).replace("+", "%20"));
	}

	private boolean IsInsertNew(HttpServletRequest req) {
		return req.getRequestURI().contains("/uusi");
	}
	private void updateEnumeration(DAO dao) throws Exception {
		if (getTipuApiResourceNames() == null) return;
		TipuAPIClient client = null;
		try {
			client = dao.getTipuApiClient();
			for (String resourceName : getTipuApiResourceNames()) {
				EnumerationsContainer enumerationsContainer = RowStructure.loadTipuApiEnumeration(client, resourceName);
				dao.getRowStructure().updateEnumeration(resourceName, enumerationsContainer);
			}
		} finally {
			if (client != null) client.close();
		}
	}

	public class ResourceBaseServletPostValidator extends ResourceBaseServletGetValidator {
		@Override
		protected String viewIfErrors() {
			return null;
		}
		@Override
		protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
			if (req.getRequestURI().contains("/haku")) {
				if (!row.tableHasValues(resourceName())) {
					setCustomFieldError("Jotain hakutekijöitä on annettava.");
				}
				validateAsSearchParams(row);
				return;
			}

			typeValidate(row);
			if (hasErrors()) return;

			if (isForInsert(req)) {
				Column idColumn = row.get(resourceIdFieldName());
				if (!idColumn.getField().isSequenceGivenIdField()) {
					if (!idColumn.hasValue()) {
						setError(idColumn, "ID on annettava.");
						return;
					}
					if (dao.resourceExists(idColumn)) {
						setError(idColumn, "ID on jo käytetty.");
					}
				}
			} else {
				String resourceID = getResourceId(req);
				Row originalData = dao.newRow();
				Column idColumn = originalData.get(resourceIdFieldName());
				idColumn.setValue(resourceID);

				if (!idColumn.hasValue()) {
					setError(idColumn, "Muokattavan resurssin ID täytyy antaa.");
				}
				typeValidate(idColumn);
				if (!dao.resourceExists(idColumn)) {
					setError(idColumn, "Tällä ID:llä ei löytynyt mitään muokattavaa...");
				}
			}
			if (hasErrors()) return;
			ValidationResponse validationResponse = getValidator().validate(row, req, dao);
			this.append(validationResponse);
		}
	}

}
