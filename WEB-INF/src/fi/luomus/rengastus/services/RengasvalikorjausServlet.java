package fi.luomus.rengastus.services;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Ring;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.dao.DAO.EventOperation;
import fi.luomus.rengastus.dao.SearchResult;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.validators.BaseValidator;

@WebServlet("/rengasvalikorjaus/*")
@MultipartConfig
public class RengasvalikorjausServlet extends TipuBaseServlet {

	private static final long serialVersionUID = -2311880185492522416L;

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) throws Exception {
		return commonData.setViewName("rengasvalikorjaus");
	}

	private static class Params {
		Map<Ring, Ring> data = new LinkedHashMap<>();
		Map<Ring, Row> updateRows = new LinkedHashMap<>();
		Ring lastNewRing = null;
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {

		final Params params = new Params();

		return validateAndExecute(req, res, Executor.EMPTY_EXECUTOR, new BaseValidator() {

			@Override
			protected void performValidations(Row notUsed, HttpServletRequest req, DAO dao) throws Exception {
				try {
					String series = req.getParameter("series");
					int currentStart = Integer.valueOf(req.getParameter("currentStart"));
					int currentEnd = Integer.valueOf(req.getParameter("currentEnd"));
					int newStart = Integer.valueOf(req.getParameter("newStart"));
					int newEnd = Integer.valueOf(req.getParameter("newEnd"));
					if (!given(series, currentStart, currentEnd, newStart, newEnd)) {
						setCustomFieldError("Anna kaikki parametrit");
						return;
					}
					if (currentStart >= currentEnd) {
						setCustomFieldError("Alku oltava pienempi kuin loppu");
						return;
					}
					if ( (currentEnd-currentStart) != (newEnd-newStart) ) {
						Utils.debug(currentEnd, currentStart, newEnd, newStart);
						setCustomFieldError("Vanhan ja uuden välin on oltava yhtä pitkät");
						return;
					}
					int newRingN = newStart;
					for (int currRingN = currentStart; currRingN<= currentEnd; currRingN++) {
						Ring currR = new Ring(series + currRingN);
						Ring newR = new Ring(series + newRingN);
						if (!currR.validFormat()) {
							setCustomFieldError("Epäkelpo rengas " + currR);
						}
						if (!newR.validFormat()) {
							setCustomFieldError("Epäkelpo rengas " + newR);
						}
						params.data.put(currR, newR);
						params.lastNewRing = newR;
						newRingN++;
					}
					if (params.data.size() <= 1) setCustomFieldError("Anna jokin sarja; yksittäiset voi korjata tavalliseen tapaan käyttöliittymällä");
				} catch (NumberFormatException nfe) {
					setCustomFieldError("Rengassarjan alku ja loppuvälien tulee olla numeroita");
					return;
				}

				for (Map.Entry<Ring, Ring> e : params.data.entrySet()) {
					Ring currentR = e.getKey();
					Ring newR = e.getValue();
					Row searchparams = dao.newRow();
					searchparams.get("event.nameRing").setValue(currentR.toString());
					SearchResult result = dao.searchEvent(searchparams);
					if (!result.wasSuccessful()) throw new IllegalStateException("Virhe haettessa rengasta " + currentR);
					if (!result.hasResults()) {
						setCustomFieldError("Rengastusta ei löydy: " + currentR);
						return;
					}
					if (result.getResults().size() > 1) {
						setCustomFieldError("Renkaalle " + currentR + " on tapaamisia. Ei voi päivittää.");
						return;
					}
					Row data = result.getResults().get(0);
					data.get("event.nameRing").setValue(newR.toString());
					data.get("event.legRing").setValue(newR.toString());
					data.get("event.ringStart").setValue(newR.toString());
					data.get("event.ringEnd").setValue(params.lastNewRing.toString());
					data.get("event.newLegRing").setValue(newR.toString());
					params.updateRows.put(currentR, data);
				}

			}

			private boolean given(Object ... params) {
				return RengasvalikorjausServlet.this.given(params);
			}

		}, new Executor() {

			@Override
			public ResponseData execute(Row notused, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
				StringBuilder errors = new StringBuilder();
				StringBuilder successRows = new StringBuilder();
				for (Map.Entry<Ring, Row> e : params.updateRows.entrySet()) {
					Ring currentRing = e.getKey();
					Row row = e.getValue();
					String id = row.get("event.id").getValue();
					String newRing = row.get("event.nameRing").getValue();
					try {
						JSONObject resultRow = EventUpdateServlet.update(row, dao, id, EventOperation.UPDATE);
						boolean success = EventUpdateServlet.updateSuccess(resultRow);
						if (!success) {
							String error = EventUpdateServlet.updateErrorMessage(resultRow);
							errors.append("Validointivirheitä! Vanha rengas: " + currentRing + " id: " + id + ". " + error +" \n");
						} else {
							successRows.append(currentRing +" -> " + newRing + "\n");
						}
					} catch (Exception ex) {
						getErrorReporter().report("Rengasvalikorjaus, event id: " + id, ex);
						errors.append("Tuntematon virhe! Rivi jäi päivittämättä! Vanha rengas: " + currentRing + " id: " + id + "\n");
					}

				}
				if (errors.length() != 0) {
					responseData.setData("error", errors);
				} else {
					responseData.setData("success", "Päivitysajo onnistui!");
				}
				return responseData.setData("done", "true").setData("successRows", successRows.toString());
			}

		}, Executor.EMPTY_EXECUTOR);
	}

}
