package fi.luomus.rengastus.services;

import java.sql.SQLException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.dao.DAO.EventOperation;
import fi.luomus.rengastus.model.BinRow;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.LoydosForm;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.ValidationResponse;
import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.validators.EventValidator;
import fi.luomus.rengastus.validators.MaallikkoValidator;

@WebServlet(urlPatterns = {"/uusi-rengastus", "/uusi-tapaaminen", "/uusi", "/kopioi/*", "/odottava-kasittelyyn/*"})
public class EventInsertServlet extends TipuBaseServlet {

	private static final String ID_TO_BE_INSERTED = "ID_TO_BE_INSERTED";
	private static final long serialVersionUID = -1890379789614969585L;

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) {
		boolean fromBin = fromBin(req);
		if (fromBin) {
			commonData.setData("fromBin", "true");
		}
		return commonData.setViewName("event").setData("action", "INSERT");
	}

	private static boolean fromBin(HttpServletRequest req) {
		return req.getParameter("fromBin") != null;
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		return validateAndExecute(req, res, EventInsertExecutor.EMPTY_EXECUTOR, Validator.EMPTY_VALIDATOR, Executor.EMPTY_EXECUTOR, new EventInsertGetExecutor());
	}

	private static class EventInsertGetExecutor implements Executor {
		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			if (req.getRequestURI().contains("kopioi")) {
				return copyEvent(responseData, dao, req);
			}
			if (req.getRequestURI().contains("odottava-kasittelyyn")) {
				return loadFromBinForInsert(responseData, dao, req);
			}
			row = dao.newRow();
			row.get("event.id").setValue(dao.getNextEventId());
			if (req.getRequestURI().contains("uusi-rengastus")) {
				row.get("event.type").setValue("ringing");
			} else {
				row.get("event.type").setValue("recovery");
			}
			responseData.setData("params", row);
			return responseData;
		}

		private ResponseData loadFromBinForInsert(ResponseData responseData, DAO dao, HttpServletRequest req) throws Exception {
			String eventId = getResourceId(req);
			BinRow binRow = dao.getEventFromBin(eventId);
			if (binRow == null) {
				return responseData.setViewName("index").setData("error", "ID:llä ei löytynyt odottavaa tapausta.");
			}
			Row row = binRow.getRow();
			EventUpdateServlet.loadRingHistory(responseData, dao, row);
			EventUpdateServlet.loadLaymanInfo(dao, row, responseData);

			if (row.get("event.type").getValue().equals("recovery")) {
				LoydosForm loydosForm = dao.getLoydosFormOriginal(eventId);
				if (loydosForm != null) {
					responseData.setData("loydosForm", loydosForm);
				}
			}

			responseData.setData("params", row);
			responseData.setData("binMoveNotes", binRow.getBinMoveNotes());
			responseData.setData("fromBin", "true");
			return responseData;
		}

		private ResponseData copyEvent(ResponseData responseData, DAO dao, HttpServletRequest req) throws Exception, SQLException {
			String id = getResourceId(req);
			Row row = dao.getEventById(id);
			row.get("event.id").setValue(dao.getNextEventId());
			row.get("event.diario").setValue("");
			row.get("event.ringStart").setValue("");
			row.get("event.ringEnd").setValue("");
			row.get("event.nameRing").setValue("");
			row.get("event.created").setValue("");
			row.get("event.modified").setValue("");
			row.get("event.modifiedBy").setValue("");
			EventUpdateServlet.loadRingHistory(responseData, dao, row);
			EventUpdateServlet.loadLaymanInfo(dao, row, responseData);
			responseData.setData("params", row);
			return responseData;
		}
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);

		boolean toWaitingBin = req.getParameter("toWaitingBin") != null;
		boolean toTrashBin = req.getParameter("toTrashBin") != null;
		boolean toDeletedBin = req.getParameter("toDeletedBin") != null;

		if (toWaitingBin || toTrashBin || toDeletedBin) {
			return validateAndExecute(req, res, Executor.EMPTY_EXECUTOR, Validator.EMPTY_VALIDATOR, new MoveToBinExecutor(), Executor.EMPTY_EXECUTOR);
		}

		return validateAndExecute(req, res, new PrePostExecutor(), new EventValidator(ID_TO_BE_INSERTED, EventOperation.INSERT), new EventInsertExecutor(), new PostEventInsertExecutor());
	}

	private static class MoveToBinExecutor implements Executor {

		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			boolean toWaitingBin = req.getParameter("toWaitingBin") != null;
			boolean toTrashBin = req.getParameter("toTrashBin") != null;
			boolean toDeletedBin = req.getParameter("toDeletedBin") != null;

			String eventId = row.get("event.id").getValue();
			String binNotes = req.getParameter("binMoveNotes");

			if (dao.inBin(eventId)) {
				if (toWaitingBin) {
					dao.updateEventToWaitingBin(row, binNotes);
					return responseData.setData("success", "Tapaus palautettiin odottaviin.").setRedirectLocation(config.baseURL()+"/etusivu");
				} else if (toTrashBin) {
					dao.updateEventToTrashBin(row, binNotes);
					return responseData.setData("success", "Tapaus palautettiin roskiin.").setRedirectLocation(config.baseURL()+"/etusivu");
				} else if (toDeletedBin) {
					dao.updateEventToDeletedBin(row, binNotes);
					return responseData.setData("success", "Tapaus palautettiin poistettuihin.").setRedirectLocation(config.baseURL()+"/etusivu");
				}
				throw new IllegalStateException();
			}

			if (toWaitingBin) {
				dao.insertEventToWaitingBin(row, binNotes);
				responseData.setData("success", "Tapaus siirettiin odottaviin.").setRedirectLocation(config.baseURL()+"/etusivu");
			} else if (toTrashBin) {
				dao.inertEventToTrashBin(row, binNotes);
				responseData.setData("success", "Tapaus siirettiin roskiin.").setRedirectLocation(config.baseURL()+"/etusivu");
			} else if (toDeletedBin) {
				dao.insertEventToDeletedBin(row, binNotes);
				responseData.setData("success", "Tapaus siirettiin poistettuihin.").setRedirectLocation(config.baseURL()+"/etusivu");
			} else {
				throw new IllegalStateException();
			}

			String loydosId = req.getParameter("loydosID");
			String loydosNotes = req.getParameter("loydosNotes");
			if (loydosId != null) {
				dao.marReceivedToLoydosStoreOriginal(eventId, loydosId, loydosNotes);
			}

			return responseData;
		}

	}

	public static class PrePostExecutor implements Executor {

		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			if (row.tableHasValues("layman")) {
				findExistingLaymanOrInsertOrUpdate(row, dao, responseData, req);
			}
			row.get("event.layman").setValue(row.get("layman.name").getValue());

			responseData.setData("binMoveNotes", req.getParameter("binMoveNotes"));
			return responseData;
		}

		private void findExistingLaymanOrInsertOrUpdate(Row row, DAO dao, ResponseData responseData, HttpServletRequest req) throws Exception {
			Column laymanID = row.get("event.laymanID");
			if (!laymanID.hasValue()) {
				tryToFindExistingLaymanOrInsertNewLayman(row, dao, responseData, laymanID, req);
			} else {
				if (validLaymanData(row, dao, req, responseData)) {
					updateLayman(row, dao, responseData);
				}
			}
		}

		private void updateLayman(Row row, DAO dao, ResponseData responseData) throws Exception {
			boolean updateCausedDbChanges = dao.storeResourceToDB(row, row.get("layman.id"));
			if (updateCausedDbChanges) {
				responseData.setData("success", "Maallikon tiedot muokattiin.");
			}
		}

		private void tryToFindExistingLaymanOrInsertNewLayman(Row row, DAO dao, ResponseData responseData, Column eventLaymanID, HttpServletRequest req) throws Exception {
			Row existingLayman = dao.getExistingLayman(row.get("layman.name").getValue(), row.get("layman.email").getValue());
			if (existingLayman != null) {
				useExistingLayman(row, responseData, eventLaymanID, existingLayman);
			} else {
				if (validLaymanData(row, dao, req, responseData)) {
					insertLayman(row, dao, responseData, eventLaymanID);
				}
			}
		}

		private void insertLayman(Row row, DAO dao, ResponseData responseData, Column eventLaymanID) throws Exception {
			dao.storeResourceToDB(row, row.get("layman.id"));
			String newId = row.get("layman.id").getValue();
			eventLaymanID.setValue(newId);
			responseData.setData("success", "Uusi maallikko lisättiin ID:llä " + newId + ".");
		}

		private void useExistingLayman(Row row, ResponseData responseData, Column eventLaymanID, Row existingLayman) {
			String existingID = existingLayman.get("layman.id").getValue();
			eventLaymanID.setValue(existingID);
			row.setTableValues("layman", existingLayman);
			responseData.setData("success", "Käytetään ennestään olemassa ollutta maallikkoa " + existingID + ". Syötetyt tiedot on yliajettu maallikon aiemmilla tiedoilla, tarkista ovatko tiedot muuttuneet.");
		}

	}

	public static boolean validLaymanData(Row row, DAO dao, HttpServletRequest req, ResponseData responseData) {
		Validator validator = new MaallikkoValidator();
		ValidationResponse validationResponse = validator.validate(row, req, dao);
		if (validationResponse.hasErrors()) {
			setErrorOrWarning(validationResponse.getErrors(), responseData, "error", "Mallikon tiedoissa oli virheitä", dao.getRowStructure());
			return false;
		}
		return true;
	}

	private static class PostEventInsertExecutor implements Executor {

		@Override
		public ResponseData execute(Row row, ResponseData response, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			if (row.get("event.type").getValue().equals("ringing")) return response;
			String loydosID = req.getParameter("loydosID");
			if (given(loydosID)) {
				LoydosForm form = dao.getLoydosForm(loydosID);
				response.setData("loydosForm", form);
			}
			EventUpdateServlet.loadRingHistory(response, dao, row);
			EventUpdateServlet.loadLaymanInfo(dao, row, response);
			return response;
		}

	}

	private static class EventInsertExecutor implements Executor {

		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			boolean fromBin = fromBin(req);
			String eventId = fromBin ? row.get("event.id").getValue() : ID_TO_BE_INSERTED;

			JSONObject data = EventUpdateServlet.rowToTipuApiJSON(eventId, row);
			JSONObject response = dao.storeEventUsingAPI(data, EventUpdateServlet.typeToModeParameterValue(row), EventOperation.INSERT);

			JSONObject resultRow = response.getObject("rows").getObject(eventId);
			boolean storedToDb = resultRow.hasKey("storedToDb") ? resultRow.getBoolean("storedToDb") : false;

			if (storedToDb) {
				String insertedEventId = getInsertedEventId(resultRow);
				String loydosID = req.getParameter("loydosID");
				String loydosNotes = req.getParameter("loydosNotes");
				if (!fromBin && loydosID != null) {
					dao.marReceivedToLoydosStoreOriginal(insertedEventId, loydosID, loydosNotes);
				}
				if (fromBin) {
					dao.deleteFromBin(eventId);
				}
				return responseData.setData("success", "Lisäys onnistui. " + getExistingMessage(responseData, "success")).setRedirectLocation(config.baseURL()+"/muokkaa/"+insertedEventId);
			}

			StringBuilder b = new StringBuilder();
			for (String key : resultRow.getObject("errors").getKeys()) {
				for (JSONObject error : resultRow.getObject("errors").getArray(key).iterateAsObject()) {
					b.append(error.getString("localizedErrorText")).append(" ");
				}
			}
			return	responseData.setData("error", "Lisäys epäonnistui tuntemattomasta syystä: " + b.toString());
		}

		private String getInsertedEventId(JSONObject resultRow) {
			String insertedEventID = resultRow.getArray("storedIds").iterator().next();
			return insertedEventID;
		}

	}

}
