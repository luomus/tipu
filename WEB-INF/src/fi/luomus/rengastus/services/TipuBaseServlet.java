package fi.luomus.rengastus.services;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.jdbc.pool.DataSource;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.reporting.FunnyOwlExceptionViewer;
import fi.luomus.commons.services.BaseServlet;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.session.SessionHandler;
import fi.luomus.commons.utils.URIBuilder;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.dao.DAOImple;
import fi.luomus.rengastus.dao.DatasourceDefinition;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.RowConstructor;
import fi.luomus.rengastus.model.RowStructure;
import fi.luomus.rengastus.model.SearchResultBrowser;
import fi.luomus.rengastus.model.ValidationError;
import fi.luomus.rengastus.model.ValidationResponse;
import fi.luomus.rengastus.model.Validator;

public abstract class TipuBaseServlet extends BaseServlet {

	private static final long serialVersionUID = -8866096686740253835L;

	@Override
	protected String configFileName() {
		return "rengastus.properties";
	}

	@Override
	protected void applicationInit() {
	}

	@Override
	protected void applicationInitOnlyOnce() {
	}

	@Override
	protected void applicationDestroy() {
		if (datasource != null) {
			try {
				datasource.close();
			} catch (Exception e) {}
		}
	}

	@Override
	protected boolean authorized(HttpServletRequest req) {
		SessionHandler session = getSession(req);
		return session.isAuthenticatedFor(getConfig().systemId());
	}

	@Override
	protected ResponseData notAuthorizedRequest(HttpServletRequest req, HttpServletResponse res) {
		Config config = getConfig();
		String service = config.developmentMode() ? "rengastus_localhost" : "rengastus";
		String requestedURI = Utils.getRequestURIAndQueryString(req);
		URIBuilder lintuvaaraURI = new URIBuilder(getConfig().get("LintuvaaraURL") + "sessions");
		lintuvaaraURI.addParameter("service", service).addParameter("service_uri", requestedURI);
		return redirectTo(lintuvaaraURI.toString());
	}

	protected ResponseData validateAndExecute(HttpServletRequest req, HttpServletResponse res, Executor beforeValidation, Validator validator, Executor afterSuccesfulValidation, Executor afterExecution) throws Exception {
		DAO dao = getDao(getUserId(req));
		ResponseData responseData = initResponseData(commonData(req), req);
		Row row = (Row) responseData.getDatamodel().get("params");
		Config config = getConfig();

		responseData = beforeValidation.execute(row, responseData, dao, config, req, res);

		ValidationResponse validationResponse = validator.validate(row, req, dao);

		boolean acceptWarnings = acceptWarnings(req);
		if (acceptWarnings) {
			responseData.setData("acceptWarnings", "true");
		}

		setErrors(validationResponse, responseData, dao.getRowStructure());
		if (!acceptWarnings) {
			setWarnings(validationResponse, responseData, dao.getRowStructure());
		}

		if (somethingWrong(validationResponse, acceptWarnings)) {
			if (validationResponse.hasCustomViewForErrors()) {
				responseData.setViewName(validationResponse.getViewForErrors());
			}
		} else {
			responseData = afterSuccesfulValidation.execute(row, responseData, dao, getConfig(), req, res);
		}

		responseData = afterExecution.execute(row, responseData, dao, config, req, res);

		if (responseData.hasBeenRequestedToBeRedirected()) {
			getSession(req).setFlashSuccess(getMessage("success", responseData));
			getSession(req).setFlashError(getMessage("error", responseData));
			getSession(req).setFlash(getMessage("message", responseData));
		} else {
			responseData.setData("searchBrowserEntry", SearchResultBrowser.getSearchBrowserEntry(getSession(req)));
		}
		return responseData;
	}

	private String getMessage(String messageType, ResponseData responseData) {
		if (responseData.getDatamodel().containsKey(messageType)) {
			return responseData.getDatamodel().get(messageType).toString();
		}
		return null;
	}

	private boolean somethingWrong(ValidationResponse validationResponse, boolean acceptWarnings) {
		return validationResponse.hasErrors() || (validationResponse.hasWarnings() && !acceptWarnings);
	}

	private void setErrors(ValidationResponse validationResponse, ResponseData responseData, RowStructure rowStructure) {
		setErrorOrWarning(validationResponse.getErrors(), responseData, "error", "Tarkistus löysi virheitä", rowStructure);
	}

	private void setWarnings(ValidationResponse validationResponse, ResponseData responseData, RowStructure rowStructure) {
		setErrorOrWarning(validationResponse.getWarnings(), responseData, "warning", "Tarkistus tuotti varoituksia", rowStructure);
	}

	public static void setErrorOrWarning(List<ValidationError> errorsOrWarnings, ResponseData responseData, String errorOrWarning, String message, RowStructure rowStructure) {
		StringBuilder b = new StringBuilder();
		for (ValidationError error : errorsOrWarnings) {
			responseData.setData(errorOrWarning + "_" + error.getFieldName(), error.getErrorText());
			String fieldName = "";
			if (rowStructure.hasField(error.getFieldName())) {
				fieldName = rowStructure.get(error.getFieldName()).getCommonName() + ": ";
			}
			b.append(fieldName).append(error.getErrorText()).append("<br/>");
		}
		if (!errorsOrWarnings.isEmpty()) {
			String existingMessage = getExistingMessage(responseData, errorOrWarning);
			if (given(existingMessage)) {
				existingMessage += "<br/>";
			}
			responseData.setData(errorOrWarning, existingMessage + message + ":<br/>" + b.toString());
		}
	}

	private boolean acceptWarnings(HttpServletRequest req) {
		String accept = req.getParameter("acceptWarnings");
		return  accept != null && accept.equals("true");
	}

	protected abstract ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) throws Exception;

	protected ResponseData commonData(HttpServletRequest req) throws Exception {
		ResponseData responseData = new ResponseData().setDefaultLocale("fi");
		SessionHandler session = getSession(req);
		if (session.hasSession() && session.isAuthenticatedFor(getConfig().systemId())) {
			DAO dao = getDao(getUserId(req));
			responseData.setData("userid", session.userId());
			responseData.setData("username", session.userName());
			responseData.setData("message", session.getFlash());
			responseData.setData("error", session.getFlashError());
			responseData.setData("success", session.getFlashSuccess());
			responseData.setData("rowStructure", dao.getRowStructure());
			responseData.setData("loydosCount", dao.getLoydosCount());
			responseData.setData("params", RowConstructor.construct(req, dao));
			responseData.setData("resourceID", getResourceId(req));
			responseData.setData("lintuvaaraURL", getConfig().get(Config.LINTUVAARA_URL));
			responseData.setData("loydosURI", getConfig().get("LOYDOS_URI"));
			responseData.setData("loydosUsername", getConfig().get("LOYDOS_USERNAME"));
			responseData.setData("loydosPassword", getConfig().get("LOYDOS_PASSWORD"));
		}
		return responseData;
	}

	protected String getUserId(HttpServletRequest req) {
		SessionHandler session = getSession(req);
		if (session.hasSession() && session.isAuthenticatedFor(getConfig().systemId())) {
			return session.userId();
		}
		return null;
	}

	protected DAO getDao(String userId) throws Exception {
		return new DAOImple(getConfig(), getErrorReporter(), getDatasource(), userId);
	}

	protected DAO getDao(HttpServletRequest req) throws Exception {
		return getDao(getUserId(req));
	}

	private static DataSource datasource = null;
	private DataSource getDatasource() {
		if (datasource == null) {
			datasource = DatasourceDefinition.initDataSource(getConfig());
		}
		return datasource;
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return initResponseData(commonData(req), req);
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	@Override
	protected ResponseData processPut(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	@Override
	protected ResponseData processDelete(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	protected boolean given(Object ... objects) {
		for (Object o : objects) {
			if (!given(o)) return false;
		}
		return true;
	}

	protected static boolean given(Object o) {
		return o != null && o.toString().length() > 0;
	}

	public static String getResourceId(HttpServletRequest req) {
		String path = req.getPathInfo();
		if (path == null || path.equals("/")) {
			return "";
		}
		String id = path.substring(path.lastIndexOf("/")+1);
		return id;
	}

	@Override
	protected void handleException(Exception e, HttpServletRequest req, HttpServletResponse res) throws ServletException {
		try {
			res.setContentType("text/html; charset=utf-8");
			new FunnyOwlExceptionViewer(getConfig().staticURL(), res.getWriter(), res).view(e);
		} catch (IOException e2) {
			e2.printStackTrace();
		}
	}

	protected static String getExistingMessage(ResponseData responseData, String messageType) {
		if (!responseData.getDatamodel().containsKey(messageType)) {
			return "";
		}
		return responseData.getDatamodel().get(messageType).toString();
	}

}
