package fi.luomus.rengastus.services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.dao.SearchResult;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.Field;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.RowStructure;
import fi.luomus.rengastus.model.SearchResultBrowser;
import fi.luomus.rengastus.model.SearchResultBrowser.SearchBrowserEntry;
import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.validators.HakuValidator;

@WebServlet("/haku")
public class SearchServlet extends TipuBaseServlet {

	private static final long serialVersionUID = -8354766101936883320L;

	private static class AfterExecution implements Executor {

		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			boolean advancedSearch = "true".equals(req.getParameter("showAdvancedSearch"));
			if (advancedSearch) {
				responseData.setData("showAdvancedSearch", "true");
			}
			
			responseData.setData("allFields", req.getParameter("allFields") != null);
			
			List<Field> resultFields = getResultFieldsOrDefaultFields(req, dao);
			responseData.setData("resultFields", resultFields);
			if (advancedSearch) {
				setResultFieldNames(responseData, resultFields);				
			}

			return responseData;
		}
		
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return validateAndExecute(req, res, Executor.EMPTY_EXECUTOR, Validator.EMPTY_VALIDATOR, new HakuGetExecutor(), new AfterExecution());
	}

	private class HakuGetExecutor implements Executor {

		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			if ("true".equals(req.getParameter("historiasta"))) {
				SearchBrowserEntry entry = SearchResultBrowser.getSearchBrowserEntry(getSession(req));
				if (entry != null) {
					SearchResult results = entry.getResults();
					return responseData.setData("searchResult", results);
				}
			}
			return responseData;
		}
		
	}
	
	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		return validateAndExecute(req, res, Executor.EMPTY_EXECUTOR, new HakuValidator(), new HakuExecutor(), new AfterExecution());
	}

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) {
		return commonData.setViewName("haku");
	}

	private class HakuExecutor implements Executor {

		@Override
		public ResponseData execute(Row searchParams, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws SQLException {
			boolean advancedSearch = "true".equals(req.getParameter("showAdvancedSearch"));

			SearchResult result = dao.searchEvent(searchParams);
			if (!result.wasSuccessful()) {
				return responseData.setData("error", result.getErrorMessage());
			}

			SearchResultBrowser.registerSearch(result, getSession(req));
			
			if (result.getResults().size() == 0) {
				return responseData.setData("message", "Haku ei tuottanut tuloksia.");
			}

			responseData.setData("searchResult", result);
			
			if (printToFile(req)) {
				generateSearchCriteriaText(responseData, searchParams);
				return responseData.setViewName("haku-file");
			}
			
			if (result.getResults().size() == 1) {
				responseData.setData("success", "Haku tuotti yhden osuman.");
				if (!advancedSearch) {
					return redirectToEventServlet(responseData, config, result);
				}	
			} else {
				
				responseData.setData("success", "Haku tuotti " + result.getResults().size() + " osumaa.");
			}
			return responseData;
		}

		private void generateSearchCriteriaText(ResponseData responseData, Row searchParams) {
			StringBuilder b = new StringBuilder();
			b.append("Tuotettu " + DateUtils.getCurrentDateTime("dd.MM.yyyy") + " parametreilla:\n");
			for (Column c : searchParams) {
				if (c.hasValue()) {
					b.append(c.getField().getCommonName()).append(":|").append(c.getValue()).append("\n");
				}
			}
			responseData.setData("searchCriteriaText", b.toString());
		}

		private boolean printToFile(HttpServletRequest req) {
			return req.getParameter("toFile") != null;
		}
		
	}

	private ResponseData redirectToEventServlet(ResponseData responseData, Config config, SearchResult result) {
		Row row = result.getResults().get(0);
		String id = row.get("event.id").getValue();
		return responseData.setRedirectLocation(config.baseURL()+"/muokkaa/"+id);
	}

	private static final List<String> DEFAULT_RESULT_COLUMN_NAMES = Utils.list("type", "nameRing", "diario", "legRing", "species", "eventDate", "municipality", "euringProvinceCode", "ringer", "layman", "fieldReadableCode", "mainColor");

	private static List<Field> defaultResultColumns = null;

	private static List<Field> defaultResultColumns(DAO dao) {
		if (defaultResultColumns != null) return defaultResultColumns;

		List<Field> resultColumns = new ArrayList<>();
		RowStructure rowStructure = dao.getRowStructure();
		for (String defaultColumnName : DEFAULT_RESULT_COLUMN_NAMES) {
			resultColumns.add(rowStructure.get("event."+defaultColumnName));
		}
		defaultResultColumns = resultColumns;
		return resultColumns;
	}


	private static List<Field> getResultFieldsOrDefaultFields(HttpServletRequest req, DAO dao) {
		if (req.getParameter("allFields") != null) {
			return dao.getRowStructure().getTableFields("event");
		}
		String requestedResultColumns = req.getParameter("resultColumns");
		if (!given(requestedResultColumns)) {
			return defaultResultColumns(dao);
		}
		List<Field> resultFields = new ArrayList<>();
		Set<String> givenFields = new HashSet<>();
		for (String givenFieldName : requestedResultColumns.split(",")) {
			givenFieldName = givenFieldName.trim();
			if (!given(givenFieldName)) continue;
			if (givenFieldName.endsWith("_yyyy")) givenFieldName = givenFieldName.replace("_yyyy", "");
			if (givenFieldName.endsWith("_mm")) givenFieldName = givenFieldName.replace("_mm", "");
			if (givenFieldName.endsWith("_dd")) givenFieldName = givenFieldName.replace("_dd", "");
			givenFields.add(givenFieldName);
		}
		for (Field f : dao.getRowStructure().getTableFields("event")) {
			if (givenFields.contains(f.getName())) {
				resultFields.add(f);
			}
		}
		return resultFields;
	}
	
	private static void setResultFieldNames(ResponseData responseData, List<Field> resultFields) {
		Set<String> resultFieldNames = new HashSet<>();
		for (Field f : resultFields) {
			resultFieldNames.add(f.getName());
		}
		responseData.setData("resultFieldsNames", resultFieldNames);
	}
	
}
