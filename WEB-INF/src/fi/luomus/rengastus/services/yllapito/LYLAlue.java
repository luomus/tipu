package fi.luomus.rengastus.services.yllapito;


import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.dao.SearchResult;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.services.ResourceBaseServlet;
import fi.luomus.rengastus.validators.BaseValidator;

import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

@WebServlet("/"+LYLAlue.RESOURCE_NAME+"/*")
public class LYLAlue extends ResourceBaseServlet {

	private static final long serialVersionUID = 7255726623412207694L;
	public static final String RESOURCE_NAME = "lylArea";
	
	@Override
	protected String resourceIdFieldName() {
		return "lylArea.id";
	}

	@Override
	protected String resourceName() {
		return RESOURCE_NAME;
	}

	@Override
	protected String resourceCommonName() {
		return "LYL-alueen";
	}

	@Override
	protected List<String> getTipuApiResourceNames() {
		return Utils.list(TipuAPIClient.LYL_AREAS);
	}

	@Override
	protected Validator getValidator() {
		return new BaseValidator() {
			@Override
			protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
				require(row.get("lylArea.id"));
				require(row.get("lylArea.name"));
			}
		};
	}

	@Override
	protected Validator getDeleteValidator() {
		return new BaseValidator() {
			
			@Override
			protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
				Column id = row.get("lylArea.id");
				boolean used = isUsed(id.getValue(), dao);
				if (used) {
					setError(id, "Jokin kunta on liitetty tähän alueeseen. Ei voi poistaa");
				}
			}

			private boolean isUsed(String id, DAO dao) {
				Row searchparams = dao.newRow();
				searchparams.get("municipality.lylArea").setValue(id);
				SearchResult searchResult = dao.searchResources(searchparams, "municipality");
				if (!searchResult.wasSuccessful()) throw new IllegalStateException(searchResult.getErrorMessage());
				return searchResult.hasResults();
			}
		};
	}
	
}
