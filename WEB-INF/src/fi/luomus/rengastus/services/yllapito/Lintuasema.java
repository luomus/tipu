package fi.luomus.rengastus.services.yllapito;


import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.services.ResourceBaseServlet;
import fi.luomus.rengastus.validators.BaseValidator;

import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

@WebServlet("/"+Lintuasema.RESOURCE_NAME+"/*")
public class Lintuasema extends ResourceBaseServlet {

	private static final long serialVersionUID = -8330862103063810203L;
	public static final String RESOURCE_NAME = "birdStation";

	@Override
	protected String resourceIdFieldName() {
		return "birdStation.id";
	}

	@Override
	protected String resourceName() {
		return RESOURCE_NAME;
	}

	@Override
	protected String resourceCommonName() {
		return "Lintuaseman";
	}

	@Override
	protected List<String> getTipuApiResourceNames() {
		return Utils.list(TipuAPIClient.BIRD_STATIONS);
	}

	@Override
	protected Validator getValidator() {
		return new BaseValidator() {
			@Override
			protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
				require(row.get("birdStation.id"));
				require(row.get("birdStation.name"));
				require(row.get("birdStation.fakeRingerNumber"));
				
				require(row.get("birdStation.municipality"));
				require(row.get("birdStation.contactPersonName"));
				require(row.get("birdStation.email"));
				require(row.get("birdStation.language"));
				require(row.get("birdStation.centerPointDegreeLat"));
				require(row.get("birdStation.centerPointDegreeLon"));
				require(row.get("birdStation.centerPointUniformLat"));
				require(row.get("birdStation.centerPointUniformLon"));
				require(row.get("birdStation.centerPointDecimalLat"));
				require(row.get("birdStation.centerPointDecimalLon"));
				require(row.get("birdStation.radius"));				
			}
		};
	}

	@Override
	protected Validator getDeleteValidator() {
		return new BaseValidator() {
			@Override
			protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
				Column idCol = row.get("birdStation.id");
				setError(idCol, "Lintuasemia ei saa poistaa.");
			}
		};
	}

}
