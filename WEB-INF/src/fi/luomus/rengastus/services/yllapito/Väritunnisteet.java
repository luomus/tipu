package fi.luomus.rengastus.services.yllapito;

import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.URIBuilder;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.services.MultiResourceBaseServlet;
import fi.luomus.rengastus.validators.VäritunnisteetDeleteValidator;
import fi.luomus.rengastus.validators.VäritunnisteetValidator;

@WebServlet("/"+Väritunnisteet.RESOURCE_NAME+"/*")
public class Väritunnisteet extends MultiResourceBaseServlet {

	private static final long serialVersionUID = 5987452744397128710L;
	public static final String RESOURCE_NAME = "fieldReadable";

	@Override
	protected String listingByFieldName() {
		return "fieldReadable.distributedTo";
	}

	@Override
	protected List<String> resourceIdFieldNames() {
		return Utils.list("fieldReadable.code", "fieldReadable.mainColor", "fieldReadable.forSpecies");
	}

	@Override
	protected String resourceName() {
		return RESOURCE_NAME;
	}

	@Override
	protected String frontpageView() {
		return "jakelu";
	}

	@Override
	protected String resourceCommonName() {
		return "Jaettujen väritunnisteiden";
	}

	@Override
	protected List<String> getTipuApiResourceNames() {
		return null;
	}

	@Override
	protected Validator getValidator() {
		return new VäritunnisteetValidator();
	}

	@Override
	protected Validator getDeleteValidator() {
		return new VäritunnisteetDeleteValidator();
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		return validateAndExecute(req, res, new FieldReadableBeforePostExecutionExecutor(), new ResourceBaseServletPostValidator(), new PostExecutor(), Executor.EMPTY_EXECUTOR);
	}

	private class FieldReadableBeforePostExecutionExecutor extends MultiResourceBaseServlet.BeforePostExecutionExecutor {
		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			super.execute(row, responseData, dao, config, req, res);
			Column doubleDistributedCount = row.get("fieldReadable.doubleDistributedCount");
			if (!doubleDistributedCount.hasValue()) doubleDistributedCount.setValue("1");
			return responseData;
		}
	}

	@Override
	protected String redirectAfterInsertURI(Row row, Config config) {
		URIBuilder redirectURI = new URIBuilder(config.baseURL()+"/"+resourceName()+"/kopioi");
		for (Column c : getIdColumns(row)) {
			redirectURI.addParameter(c.getFieldName(), c.getValue());
		}
		return redirectURI.toString();
	}
}
