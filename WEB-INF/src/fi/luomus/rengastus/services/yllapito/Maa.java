package fi.luomus.rengastus.services.yllapito;

import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.services.ResourceBaseServlet;
import fi.luomus.rengastus.validators.BaseValidator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

@WebServlet("/"+Maa.RESOURCE_NAME+"/*")
public class Maa extends ResourceBaseServlet {

	private static final long serialVersionUID = 5081995398125333111L;
	public static final String RESOURCE_NAME = "country";
	
	@Override
	protected String resourceIdFieldName() {
		return "country.id";
	}

	@Override
	protected String resourceName() {
		return RESOURCE_NAME;
	}

	@Override
	protected String resourceCommonName() {
		return "Maan";
	}
	
	@Override
	protected List<String> getTipuApiResourceNames() {
		return Utils.list(TipuAPIClient.COUNTRIES);
	}

	@Override
	protected Validator getValidator() {
		return new BaseValidator() {
			@Override
			protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
				require(row.get("country.id"));
				require(row.get("country.name"));
			}
		};
	}

	@Override
	protected Validator getDeleteValidator() {
		return new BaseValidator() {
			
			@Override
			protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
				Column idCol = row.get("country.id");
				int count = countOfUses(dao, idCol);
				if (count > 0) {
					setError(idCol, "Tämä maa on käytettynä schemen, euring provinssin tai maallikon maana.");
				}
			}

			private int countOfUses(DAO dao, Column idCol) throws SQLException {
				String id = idCol.getValue();
				TransactionConnection con = null;
				PreparedStatement p = null;
				ResultSet rs = null;
				try {
					con = dao.openConnection();
					String sql = "" +
							" 		select sum(c) from (														" + 
							"		select count(1) c from RENGASTUSKESKUS where RK_OMLYH = ?	and rownum = 1	" + 
							"		union																		" +
							"		select count(1) c from ULKOMAINEN_PAIKKA where UP_OMLYH = ? and rownum = 1  " +
							"		union																		" +
							"		select count(1) c from MAALLIKKO where MA_OMLYH = ? and rownum = 1      )   ";
					p = con.prepareStatement(sql);
					p.setString(1, id);
					p.setString(2, id);
					p.setString(3, id);
					rs = p.executeQuery();
					rs.next();
					int count = rs.getInt(1);
					return count;
				} finally {
					Utils.close(p, rs, con);
				}
			}
		};
	}
	
}
