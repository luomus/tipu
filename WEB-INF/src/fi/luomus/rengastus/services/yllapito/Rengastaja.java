package fi.luomus.rengastus.services.yllapito;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.dao.SearchResult;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.services.ResourceBaseServlet;
import fi.luomus.rengastus.validators.RengastajaValidator;

@WebServlet("/"+Rengastaja.RESOURCE_NAME+"/*")
public class Rengastaja extends ResourceBaseServlet {

	public static final String RESOURCE_NAME = "ringer";
	private static final long serialVersionUID = -5436551415582334520L;

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) {
		return super.initResponseData(commonData, req).setData("LintuvaaraURL", getConfig().get(Config.LINTUVAARA_URL));
	}

	@Override
	protected String resourceIdFieldName() {
		return "ringer.id";
	}

	@Override
	protected String resourceName() {
		return RESOURCE_NAME;
	}

	@Override
	protected String resourceCommonName() {
		return "Rengastajan";
	}

	@Override
	protected List<String> getTipuApiResourceNames() {
		return Utils.list(TipuAPIClient.RINGERS);
	}

	@Override
	protected Validator getValidator() {
		return new RengastajaValidator();
	}

	@Override
	protected Validator getDeleteValidator() {
		return new RengastajaValidator.DeleteValidator();
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		return validateAndExecute(req, res, new BeforeExecution(), new ResourceBaseServletGetValidator(), new RingerGetExecutor(), Executor.EMPTY_EXECUTOR);
	}

	private class RingerGetExecutor extends ResourceBaseServlet.GetExecutor {
		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			responseData = super.execute(row, responseData, dao, config, req, res);

			if (req.getRequestURI().contains("/uusi")) {
				return responseData;
			}
			if (req.getRequestURI().contains("/haku")) {
				return responseData;
			}
			if (req.getRequestURI().contains("/poista")) { // TODO pois getistä
				return responseData;	
			}

			Column idColumn = row.get(resourceIdFieldName());
			responseData.setData("ringerSynonyms", dao.getRingerSynonyms(idColumn.getValue()));

			return responseData;
		}
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		return validateAndExecute(req, res, new BeforePostRequestHandlingExecutor(), new ResourceBaseServletPostValidator(), new RingerPostExecutor(), new Executor() {
			@Override
			public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
				if (req.getRequestURI().contains("/haku")) return responseData;
				String ringerId = row.get("ringer.id").getValue();
				return responseData.setData("ringerSynonyms", dao.getRingerSynonyms(ringerId));
			}
		});
	}

	private class RingerPostExecutor extends ResourceBaseServlet.PostExecutor {
		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {

			if (req.getRequestURI().contains("/haku")) {
				Column ringerId = row.get("ringer.id");
				if (ringerId.hasValue()) {
					expandBySynonyms(dao, ringerId);
				}
			}

			responseData = super.execute(row, responseData, dao, config, req, res);

			if (req.getRequestURI().contains("/haku")) {
				return responseData;
			}

			String ringerId = row.get("ringer.id").getValue();
			Set<String> synonyms = new HashSet<>();
			if (req.getParameterValues("ringerSynonym") != null) {
				for (String synonym : req.getParameterValues("ringerSynonym")) {
					if (synonym.length() < 1) continue;
					synonyms.add(synonym);
				}
			}
			dao.updateRingerSynonyms(ringerId, synonyms);

			return responseData;
		}

		private void expandBySynonyms(DAO dao, Column ringerId) {
			List<String> synonymExpandedRingerIds = new ArrayList<>();
			searchByRingerId(dao, ringerId, synonymExpandedRingerIds);
			searchBySynonymIds(dao, ringerId, synonymExpandedRingerIds);
			catenadeValues(ringerId, synonymExpandedRingerIds);
		}

		private void catenadeValues(Column ringerId, List<String> synonymExpandedRingerIds) {
			StringBuilder value = new StringBuilder();
			for (String id : synonymExpandedRingerIds) {
				value.append(id).append("|");
			}
			Utils.removeLastChar(value);
			ringerId.setValue(value.toString());
		}

		private void searchBySynonymIds(DAO dao, Column ringerId, List<String> synonymExpandedRingerIds) {
			Row searchParams;
			SearchResult result;
			searchParams = dao.newRow();
			searchParams.get("ringerObserverSynonyms.synonymId").setValue(ringerId.getValue());
			result = dao.searchResources(searchParams, "ringerObserverSynonyms");
			for (Row resultRow : result.getResults()) {
				synonymExpandedRingerIds.add(resultRow.get("ringerObserverSynonyms.ringerId").getValue());
			}
		}

		private void searchByRingerId(DAO dao, Column ringerId, List<String> synonymExpandedRingerIds) {
			Row searchParams = dao.newRow();
			searchParams.get("ringer.id").setValue(ringerId.getValue());
			SearchResult result = dao.searchResources(searchParams, "ringer");
			for (Row resultRow : result.getResults()) {
				synonymExpandedRingerIds.add(resultRow.get("ringer.id").getValue());
			}
		}

	}

}
