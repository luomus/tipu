package fi.luomus.rengastus.services.yllapito;

import java.util.List;

import javax.servlet.annotation.WebServlet;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.services.ResourceBaseServlet;
import fi.luomus.rengastus.validators.JakeluDeleteValidator;
import fi.luomus.rengastus.validators.JakeluValidator;

@WebServlet("/"+Jakelu.RESOURCE_NAME+"/*")
public class Jakelu extends ResourceBaseServlet {

	private static final long serialVersionUID = -5136569378991706182L;
	public static final String RESOURCE_NAME = "distributed";

	@Override
	protected String resourceIdFieldName() {
		return "distributed.ringStart";
	}

	@Override
	protected String resourceName() {
		return RESOURCE_NAME;
	}

	@Override
	protected String resourceCommonName() {
		return "Jakelun";
	}

	@Override
	protected String frontpage() {
		return "jakelu";
	}

	@Override
	protected String frontpageView() {
		return "jakelu";
	}

	@Override
	protected List<String> getTipuApiResourceNames() {
		return null;
	}

	@Override
	protected Validator getValidator() {
		return new JakeluValidator();
	}

	@Override
	protected Validator getDeleteValidator() {
		return new JakeluDeleteValidator();
	}

	@Override
	protected void cleanResourceForCopy(Row row) {
		row.get("distributed.ringStart").setValue("");
		row.get("distributed.ringEnd").setValue("");
	}

	@Override
	protected ResponseData redirectAfterSubmit(ResponseData responseData, Config config, Column newIdColumn) {
		return responseData.setRedirectLocation(config.baseURL()+"/"+resourceName()+"/kopioi/"+Utils.urlEncode(newIdColumn.getValue()).replace("+", "%20"));
	}
}
