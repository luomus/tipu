package fi.luomus.rengastus.services.yllapito;

import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.services.ResourceBaseServlet;
import fi.luomus.rengastus.validators.RenkaatDeleteValidator;
import fi.luomus.rengastus.validators.RenkaatValidator;

import java.util.List;

import javax.servlet.annotation.WebServlet;

@WebServlet("/"+Renkaat.RESOURCE_NAME+"/*")
public class Renkaat extends ResourceBaseServlet {

	private static final long serialVersionUID = 2274343256940273073L;
	public static final String RESOURCE_NAME = "ring";
	
	@Override
	protected String resourceIdFieldName() {
		return "ring.ringStart";
	}

	@Override
	protected String resourceName() {
		return RESOURCE_NAME;
	}

	@Override
	protected String resourceCommonName() {
		return "Jaettavissa olevien renkaiden";
	}
	
	@Override
	protected String frontpage() {
		return "jakelu";
	}
	
	@Override
	protected String frontpageView() {
		return "jakelu";
	}

	@Override
	protected List<String> getTipuApiResourceNames() {
		return null;
	}

	@Override
	protected Validator getValidator() {
		return new RenkaatValidator();
	}
	
	@Override
	protected Validator getDeleteValidator() {
		return new RenkaatDeleteValidator();
	}
	
}
