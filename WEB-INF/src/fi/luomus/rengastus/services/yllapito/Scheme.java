package fi.luomus.rengastus.services.yllapito;

import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.services.ResourceBaseServlet;
import fi.luomus.rengastus.validators.BaseValidator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

@WebServlet("/"+Scheme.RESOURCE_NAME+"/*")
public class Scheme extends ResourceBaseServlet {

	private static final long serialVersionUID = -1069302127794629788L;
	public static final String RESOURCE_NAME = "scheme";
	
	@Override
	protected String resourceIdFieldName() {
		return "scheme.id";
	}

	@Override
	protected String resourceName() {
		return RESOURCE_NAME;
	}

	@Override
	protected String resourceCommonName() {
		return "Schemen";
	}

	@Override
	protected List<String> getTipuApiResourceNames() {
		return Utils.list(TipuAPIClient.SCHEMES);
	}
	
	@Override
	protected Validator getValidator() {
		return new BaseValidator() {
			@Override
			protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
				require(row.get("scheme.id"));
				require(row.get("scheme.name"));
				require(row.get("scheme.country"));
			}
		};
	}

	@Override
	protected Validator getDeleteValidator() {
		return new BaseValidator() {
			
			@Override
			protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
				Column idCol = row.get("scheme.id");
				int count = countOfUses(dao, idCol);
				if (count > 0) {
					setError(idCol, "Tämä scheme on käytettynä tapaamisessa.");
				}
			}

			private int countOfUses(DAO dao, Column idCol) throws SQLException {
				String id = idCol.getValue();
				TransactionConnection con = null;
				PreparedStatement p = null;
				ResultSet rs = null;
				try {
					con = dao.openConnection();
					String sql = " select count(1) c from event where schemeID = ? and rownum = 1 ";
					p = con.prepareStatement(sql);
					p.setString(1, id);
					rs = p.executeQuery();
					rs.next();
					int count = rs.getInt(1);
					return count;
				} finally {
					Utils.close(p, rs, con);
				}
			}
		};
	}
}
