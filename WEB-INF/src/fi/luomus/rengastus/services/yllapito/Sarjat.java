package fi.luomus.rengastus.services.yllapito;

import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.services.MultiResourceBaseServlet;

import java.util.List;

import javax.servlet.annotation.WebServlet;

@WebServlet("/"+Sarjat.RESOURCE_NAME+"/*")
public class Sarjat extends MultiResourceBaseServlet {

	private static final long serialVersionUID = -8450424517659808316L;
	public static final String RESOURCE_NAME = "series";
	
	@Override
	protected String listingByFieldName() {
		return "series.series";
	}
	
	@Override
	protected List<String> resourceIdFieldNames() {
		return Utils.list("series.series", "series.species");
	}
	
	@Override
	protected String resourceName() {
		return RESOURCE_NAME;
	}

	@Override
	protected String frontpageView() {
		return "jakelu";
	}
	
	@Override
	protected String resourceCommonName() {
		return "Sopivuuksien";
	}

	@Override
	protected List<String> getTipuApiResourceNames() {
		return null;
	}

	@Override
	protected Validator getValidator() {
		return Validator.EMPTY_VALIDATOR; // Ei mitään validoitavaa: key check estää jo duplikaattien lisäämisen
	}

	@Override
	protected Validator getDeleteValidator() {
		return Validator.EMPTY_VALIDATOR; // EI mitään validoitavaa
		// Voitaisiin validoida, että sarjan viimeistä sopivuutta ei saa poistaa jos sarja on käytetty jakelussa, mutta eipä kannata.. 
		// Ei haluta validoida, että sarja/lalyh ei ole käytetty rengastuksessa, koska jos halutaan muuttaa, ettei yhdistelmä se enää kelpaa, niin olemassa 
		// olevaa dataa ei voi lähteä siivoamaan: rengastettu mikä rengastettu.
	}
	
}
