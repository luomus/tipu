package fi.luomus.rengastus.services.yllapito;

import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.services.ResourceBaseServlet;
import fi.luomus.rengastus.validators.BaseValidator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

@WebServlet("/"+EuringProvinssit.RESOURCE_NAME+"/*")
public class EuringProvinssit extends ResourceBaseServlet {

	private static final long serialVersionUID = -2142229140655853746L;
	public static final String RESOURCE_NAME = "euringProvince";
	
	@Override
	protected String resourceIdFieldName() {
		return "euringProvince.id";
	}

	@Override
	protected String resourceName() {
		return RESOURCE_NAME;
	}

	@Override
	protected String resourceCommonName() {
		return "Euring-provinssin";
	}
	
	@Override
	protected List<String> getTipuApiResourceNames() {
		return Utils.list(TipuAPIClient.EURING_PROVINCES);
	}

	@Override
	protected Validator getValidator() {
		return new BaseValidator() {
			@Override
			protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
				require(row.get("euringProvince.id"));
				require(row.get("euringProvince.name"));
				require(row.get("euringProvince.country"));
				require(row.get("euringProvince.centerPointDegreeLat"));
				require(row.get("euringProvince.centerPointDegreeLon"));
				require(row.get("euringProvince.centerPointDecimalLat"));
				require(row.get("euringProvince.centerPointDecimalLon"));
				require(row.get("euringProvince.radius"));
			}
		};
	}

	@Override
	protected Validator getDeleteValidator() {
		return new BaseValidator() {
			
			@Override
			protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
				Column idCol = row.get("euringProvince.id");
				int count = countOfUses(dao, idCol);
				if (count > 0) {
					setError(idCol, "Tämä provinssi on käytettynä tapaamisessa tai liitetty kuntaan.");
				}
			}

			private int countOfUses(DAO dao, Column idCol) throws SQLException {
				String id = idCol.getValue();
				TransactionConnection con = null;
				PreparedStatement p = null;
				ResultSet rs = null;
				try {
					con = dao.openConnection();
					String sql = " select count(1) c from event where euringProvinceCode = ? and rownum = 1		    ";
					p = con.prepareStatement(sql);
					p.setString(1, id);
					rs = p.executeQuery();
					rs.next();
					int count = rs.getInt(1);
					return count;
				} finally {
					Utils.close(p, rs, con);
				}
			}
		};
	}
	
}
