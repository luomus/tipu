package fi.luomus.rengastus.services.yllapito;

import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.services.ResourceBaseServlet;
import fi.luomus.rengastus.validators.BaseValidator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

@WebServlet("/"+Laji.RESOURCE_NAME+"/*")
public class Laji extends ResourceBaseServlet {

	private static final long serialVersionUID = -6120551356370383052L;
	public static final String RESOURCE_NAME = "species";

	@Override
	protected String resourceIdFieldName() {
		return "species.id";
	}

	@Override
	protected String resourceName() {
		return RESOURCE_NAME;
	}

	@Override
	protected String resourceCommonName() {
		return "Lajin";
	}

	@Override
	protected List<String> getTipuApiResourceNames() {
		return Utils.list(TipuAPIClient.SPECIES, TipuAPIClient.SELECTABLE_SPECIES);
	}

	@Override
	protected Validator getValidator() {
		return new BaseValidator() {
			@Override
			protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
				Column speciesnumber = row.get("species.speciesnumber");

				require(row.get("species.id"));
				require(speciesnumber);
				require(row.get("species.allowedForReporting"));
				require(row.get("species.scientificName"));
				require(row.get("species.vernacularNameFi"));
				require(row.get("species.vernacularNameSv"));
				require(row.get("species.vernacularNameEn"));

				if (isForInsert(req) && speciesnumber.hasValue()) {
					Row searchparams = dao.newRow();
					searchparams.get("species.speciesnumber").setValue(speciesnumber.getValue());
					List<Row> results = dao.searchResources(searchparams, "species").getResults();
					if (results.size() > 0) {
						setError(speciesnumber, "Lajinumero on jo käytetty.");
					}
				}
			}
		};
	}

	@Override
	protected Validator getDeleteValidator() {
		return new BaseValidator() {

			@Override
			protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
				Column idCol = row.get("species.id");
				int count = countOfUses(dao, idCol);
				if (count > 0) {
					setError(idCol, "Tämä laji on käytettynä rengastuksessa, tapaamisessa, värijaossa tai renkaiden sopivuuksissa.");
				}
			}

			private int countOfUses(DAO dao, Column idCol) throws SQLException {
				String id = idCol.getValue();
				TransactionConnection con = null;
				PreparedStatement p = null;
				ResultSet rs = null;
				try {
					con = dao.openConnection();
					String sql = "" +
							" 		select sum(c) from (														" + 
							"		select count(1) c from event where species = ?	and rownum = 1			    " + 
							"		union																		" +
							"		select count(1) c from fieldReadable where forSpecies = ? and rownum = 1    " +
							"		union																		" +
							"		select count(1) c from series where species = ? and rownum = 1            ) ";
					p = con.prepareStatement(sql);
					p.setString(1, id);
					p.setString(2, id);
					p.setString(3, id);
					rs = p.executeQuery();
					rs.next();
					int count = rs.getInt(1);
					return count;
				} finally {
					Utils.close(p, rs, con);
				}
			}
		};
	}
}
