package fi.luomus.rengastus.services.yllapito;

import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.services.ResourceBaseServlet;
import fi.luomus.rengastus.validators.BaseValidator;
import fi.luomus.rengastus.validators.MaallikkoValidator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

@WebServlet("/"+Maallikko.RESOURCE_NAME+"/*")
public class Maallikko extends ResourceBaseServlet {

	private static final long serialVersionUID = -6608422397754224883L;
	public static final String RESOURCE_NAME = "layman";
	
	@Override
	protected String resourceIdFieldName() {
		return "layman.id";
	}

	@Override
	protected String resourceName() {
		return RESOURCE_NAME;
	}

	@Override
	protected String resourceCommonName() {
		return "Maallikon";
	}
	
	@Override
	protected List<String> getTipuApiResourceNames() {
		//return Utils.list(TipuAPIClient.LAYMEN);
		return null;
	}

	@Override
	protected Validator getValidator() {
		return new MaallikkoValidator();
	}

	@Override
	protected Validator getDeleteValidator() {
		return new BaseValidator() {
			
			@Override
			protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
				Column idCol = row.get("layman.id");
				int count = countOfUses(dao, idCol);
				if (count > 0) {
					setError(idCol, "Tämä maallikko on käytettynä tapaamisessa.");
				}
			}

			private int countOfUses(DAO dao, Column idCol) throws SQLException {
				String id = idCol.getValue();
				TransactionConnection con = null;
				PreparedStatement p = null;
				ResultSet rs = null;
				try {
					con = dao.openConnection();
					String sql = " select count(1) c from event where laymanID = ? and rownum = 1 ";
					p = con.prepareStatement(sql);
					p.setString(1, id);
					rs = p.executeQuery();
					rs.next();
					int count = rs.getInt(1);
					return count;
				} finally {
					Utils.close(p, rs, con);
				}
			}
		};
	}
	
}
