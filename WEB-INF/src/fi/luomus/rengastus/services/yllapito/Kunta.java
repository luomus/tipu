package fi.luomus.rengastus.services.yllapito;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.dao.SearchResult;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.services.ResourceBaseServlet;
import fi.luomus.rengastus.validators.BaseValidator;

@WebServlet("/"+Kunta.RESOURCE_NAME+"/*")
public class Kunta extends ResourceBaseServlet {

	private static final long serialVersionUID = 6760970701673501910L;
	public static final String RESOURCE_NAME = "municipality";

	@Override
	protected String resourceIdFieldName() {
		return "municipality.id";
	}

	@Override
	protected String resourceName() {
		return RESOURCE_NAME;
	}

	@Override
	protected String resourceCommonName() {
		return "Kunnan";
	}

	@Override
	protected List<String> getTipuApiResourceNames() {
		return Utils.list(TipuAPIClient.MUNICIPALITIES, TipuAPIClient.CURRENT_MUNICIPALITIES);
	}

	@Override
	protected Validator getValidator() {
		return new BaseValidator() {
			@Override
			protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
				boolean luovutettu = row.get("municipality.finnishOldCounty").getValue().equals("14") || row.get("municipality.finnishOldCounty").getValue().equals("15");

				Column id = row.get("municipality.id");
				Column name = row.get("municipality.name");

				require(id);
				require(name);
				require(row.get("municipality.nameSwedish"));
				require(row.get("municipality.centerPointDegreeLat"));
				require(row.get("municipality.centerPointDegreeLon"));
				require(row.get("municipality.centerPointUniformLat"));
				require(row.get("municipality.centerPointUniformLon"));
				require(row.get("municipality.centerPointDecimalLat"));
				require(row.get("municipality.centerPointDecimalLon"));
				require(row.get("municipality.radius"));
				require(row.get("municipality.euringProvince"));
				require(row.get("municipality.finnishOldCounty"));

				if (luovutettu) {
					mustBeEmpty(row.get("municipality.joinedTo"));
					mustBeEmpty(row.get("municipality.elyCentre"));
					mustBeEmpty(row.get("municipality.finnishProvince"));
					mustBeEmpty(row.get("municipality.lylArea"));
					mustBeEmpty(row.get("municipality.halalbArea"));
				} else {
					require(row.get("municipality.joinedTo"));
					require(row.get("municipality.elyCentre"));
					require(row.get("municipality.finnishProvince"));
					require(row.get("municipality.lylArea"));
					require(row.get("municipality.halalbArea"));
				}

				Column euringProvince = row.get("municipality.euringProvince");
				if (euringProvince.hasValue() && !euringProvince.getValue().startsWith("SF")) {
					setError(euringProvince, "Euring-paikan täytyy olla suomalainen paikka eli siis SF-alkuinen");
				}

				if (name.hasValue()) {
					validateNameNotUsedByOtherMunicipality(dao, id, name);
				}
			}

			private void validateNameNotUsedByOtherMunicipality(DAO dao, Column id, Column name) {
				Row searchParam = dao.newRow();
				searchParam.get("municipality.name").setValue(name.getValue());
				SearchResult result = dao.searchResources(searchParam, "municipality");
				if (!result.wasSuccessful()) {
					setCustomFieldError(result.getErrorMessage());
					return;
				}
				for (Row resultRow : result.getResults()) {
					String resultMunicipalityId = resultRow.get("municipality.id").getValue();
					if (resultMunicipalityId.equals(id.getValue())) continue;
					setError(name, "Nimi on jo käytössä toisessa kunnassa: " + resultMunicipalityId);
				}
			}
		};
	}


	@Override
	protected Validator getDeleteValidator() {
		return new BaseValidator() {

			@Override
			protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
				Column idCol = row.get("municipality.id");
				int count = countOfUses(dao, idCol);
				if (count > 0) {
					setError(idCol, "Tämä kunta on käytettynä rengastuksessa, tapaamisessa, kuntaliitoskuntana tai lintuaseman kuntana.");
				}
			}

			private int countOfUses(DAO dao, Column idCol) throws SQLException {
				String id = idCol.getValue();
				TransactionConnection con = null;
				PreparedStatement p = null;
				ResultSet rs = null;
				try {
					con = dao.openConnection();
					String sql = "" +
							" 		select sum(c) from (																" +
							"		select count(1) c from event where municipality = ?	and rownum = 1					" +
							"		union																				" +
							"		select count(1) c from municipality where joinedTo = ? and id != ? and rownum = 1   " +
							"		union																     			" +
							"		select count(1) c from birdStation where municipality = ? and rownum = 1  )         ";
					p = con.prepareStatement(sql);
					p.setString(1, id);
					p.setString(2, id);
					p.setString(3, id);
					p.setString(4, id);
					rs = p.executeQuery();
					rs.next();
					int count = rs.getInt(1);
					return count;
				} finally {
					Utils.close(p, rs, con);
				}
			}
		};
	}

}
