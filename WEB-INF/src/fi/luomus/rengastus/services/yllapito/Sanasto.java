package fi.luomus.rengastus.services.yllapito;

import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.services.MultiResourceBaseServlet;
import fi.luomus.rengastus.validators.BaseValidator;

import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

@WebServlet("/"+Sanasto.RESOURCE_NAME+"/*")
public class Sanasto extends MultiResourceBaseServlet {

	private static final long serialVersionUID = -17511040450761053L;
	public static final String RESOURCE_NAME = "codes";
	
	@Override
	protected String listingByFieldName() {
		return "codes.code";
	}
	
	@Override
	protected List<String> resourceIdFieldNames() {
		return Utils.list("codes.code", "codes.variable", "codes.language");
	}
	
	@Override
	protected String resourceName() {
		return RESOURCE_NAME;
	}

	@Override
	protected String frontpageView() {
		return "yllapitoIndex";
	}
	
	@Override
	protected String filterClause() {
		return " language IN ('S', 'R', 'E', 'L') ";
	}

	@Override
	protected String resourceCommonName() {
		return "Sanaston";
	}
	
	@Override
	protected List<String> getTipuApiResourceNames() {
		return Utils.list(TipuAPIClient.DESCRIPTIONS_OF_CODES);
	}

	@Override
	protected Validator getValidator() {
		return new BaseValidator() {
			@Override
			protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
				require(row.get("codes.description"));
				Column variable =  row.get("codes.variable");
				if (!variable.getValue().equals(variable.getValue().toUpperCase())) {
					setError(variable, "Täytyy olla isoilla kirjaimilla.");
					return;
				}
			}
		};
	}

	@Override
	protected Validator getDeleteValidator() {
		return new BaseValidator() {
			
			@Override
			protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
				setCustomFieldError("Sanastosta ei sallita poistoja.");
			}
		};
	}

	
}
