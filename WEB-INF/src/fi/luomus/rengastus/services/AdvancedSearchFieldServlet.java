package fi.luomus.rengastus.services;

import fi.luomus.commons.services.ResponseData;
import fi.luomus.rengastus.model.Field;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

@WebServlet("/advancedSearchField")
public class AdvancedSearchFieldServlet extends TipuBaseServlet {

	private static final long serialVersionUID = -2624702005350386L;

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) throws Exception {
		String fieldName = req.getParameter("fieldName");
		Field field = getDao(req).getRowStructure().get(fieldName);
		return commonData.setViewName("advancedSearchField").setData("field", field);
	}

}
