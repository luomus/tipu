package fi.luomus.rengastus.services;

import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.client.methods.HttpGet;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.URIBuilder;

@WebServlet("/permitsheet/*")
@MultipartConfig
public class Permitsheet extends TipuBaseServlet {

	private static final long serialVersionUID = -1221903290908745130L;

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) {
		return commonData;
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		res.setContentType("application/pdf; charset=utf-8");
		ServletOutputStream out = res.getOutputStream();
		String ringer = getId(req);
		HttpClientService client = null;
		try {
			client = getDao(req).getTipuApiAuthenenticatedClient();
			URIBuilder uri = new URIBuilder(getConfig().get(Config.TIPU_API_URI)+"/permitsheet/"+ringer);
			HttpGet request = new HttpGet(uri.toString());
			client.contentToStream(request, out);
		} finally {
			if (client != null) client.close();
		}
		return new ResponseData().setOutputAlreadyPrinted();
	}

}
