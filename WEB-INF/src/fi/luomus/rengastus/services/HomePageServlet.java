package fi.luomus.rengastus.services;

import fi.luomus.commons.services.ResponseData;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.model.WarehouseQueueInfo;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/etusivu")
public class HomePageServlet extends TipuBaseServlet {

	private static final long serialVersionUID = 6951900196961482768L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		if (req.getParameter("testException") != null) {
			throw new RuntimeException("Testing exception handling!");
		}
		reportWarehouseQueueErrors(req);
		return validateAndExecute(req, res, Executor.EMPTY_EXECUTOR, Validator.EMPTY_VALIDATOR, Executor.EMPTY_EXECUTOR, Executor.EMPTY_EXECUTOR);
	}

	private void reportWarehouseQueueErrors(HttpServletRequest req) {
		try {
			WarehouseQueueInfo info = getDao(req).getWarehouseQueueInfo();
			if (info.hasErrors()) {
				getErrorReporter().report("Warehouse queue has errors");
			} else if (info.getSize() > 2000) {
				getErrorReporter().report("Warehouse queue is not processing! Size: " + info.getSize());
			}
		} catch (Exception e) {
			getErrorReporter().report(e);
		}
	}

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) {
		return commonData.setViewName("index");
	}

}
