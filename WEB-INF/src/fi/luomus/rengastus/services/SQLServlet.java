package fi.luomus.rengastus.services;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.dao.DAO.CustomSQLTarget;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.Field;
import fi.luomus.rengastus.model.Report;
import fi.luomus.rengastus.model.Reports;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.validators.BaseValidator;

@WebServlet("/sql/*")
public class SQLServlet extends TipuBaseServlet {

	private static final String DEFAULT_SQL = "" +
			"SELECT * \n" +
			"FROM event \n" +
			"JOIN event ringing ON (ringing.nameRing = event.nameRing AND ringing.type = 'ringing') \n" +
			"LEFT JOIN species ringedSpecies ON (ringing.species = ringedSpecies.id) \n" +
			"WHERE ringedSpecies.speciesNumber BETWEEN 1 AND 100 \n" +
			"AND EXTRACT(year FROM event.eventDate) BETWEEN 2000 AND 2003 \n AND ... ;";

	private static final long serialVersionUID = 6888079421692525260L;

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) throws Exception {
		DAO dao = getDao(req);
		String sql = null;
		String reportId = req.getParameter("reportId");
		if (given(reportId)) {
			Report r = getReport(reportId, dao);
			sql = r.getSql();
			commonData.setData("showSQL", r.shouldShowSql());
		} else {
			sql = req.getParameter("sql");
			commonData.setData("showSQL", true);
		}
		if (!given(sql)) {
			sql =  DEFAULT_SQL;
		}
		return commonData
				.setViewName("sql")
				.setData("sql", sql)
				.setData("reports", Reports.getReports(dao.getRowStructure()));
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		return validateAndExecute(req, res, Executor.EMPTY_EXECUTOR, new SQLValidator(), new SQLExecutor(), Executor.EMPTY_EXECUTOR);
	}

	public static class SQLValidator extends BaseValidator {

		private static List<String> FORBIDDEN_WORDS = Utils.list("NEXTVAL", "ACCESS", "ADD", "ALTER", "AUDIT", "CHECK", "CLUSTER", "COMPRESS", "CONNECT", "CREATE", "DELETE", "DROP", "FILE", "GRANT", "IDENTIFIED", "IMMEDIATE", "INCREMENT", "INDEX", "INITIAL", "INSERT", "LOCK", "MAXEXTENTS", "MLSLABEL", "MODE", "MODIFY", "NESTED_TABLE_ID", "NOAUDIT", "NOCOMPRESS", "NOWAIT", "OFFLINE", "ONLINE", "OPTION", "PCTFREE", "PRIOR", "PRIVILEGES", "PUBLIC", "RAW", "RENAME", "RESOURCE", "REVOKE", "ROW", "ROWID", "SESSION", "SET", "SHARE", "START", "SYNONYM", "TABLE", "TRIGGER", "UID", "UPDATE", "USER", "VALIDATE", "VALUES", "VIEW", "WHENEVER");
		private static List<String> REQUIRED_WORDS = Utils.list("SELECT", "FROM");
		@Override
		protected void performValidations(Row row, HttpServletRequest req, DAO dao) {
			String reportId = req.getParameter("reportId");
			if (given(reportId)) {
				try {
					Report r = getReport(reportId, dao);
					validateReport(r, req);
				} catch (Exception e) {
					setCustomFieldError("Epäkelpo raportti");
				}
				return;
			}
			String sql = req.getParameter("sql");
			if (!given(sql)) {
				setCustomFieldError("sql", "Kysely on annettava");
				return;
			}
			sql = " " + clean(sql.toUpperCase()) + " ";
			for (String forbidden : FORBIDDEN_WORDS) {
				if (sql.contains(" " + forbidden + " ")) {
					setCustomFieldError("sql", "Komentoa " + forbidden + " ei sallita.");
					return;
				}
			}
			for (String required : REQUIRED_WORDS) {
				if (!sql.contains(" " + required + " ")) {
					setCustomFieldError("sql", "Kyselyn täytyy sisältää " + required + " komento.");
				}
			}
		}
		private void validateReport(Report r, HttpServletRequest req) {
			for (Field f : r.getDistinctParameters()) {
				String paramValue = req.getParameter(f.getName());
				if (!given(paramValue)) {
					setCustomFieldError("Anna kaikki raportin vaatimat parametrit.");
				}
			}
		}
	}

	private static Report getReport(String reportId, DAO dao) {
		int id = Integer.valueOf(reportId);
		for (Report r : Reports.getReports(dao.getRowStructure())) {
			if (r.getId() == id) return r;
		}
		throw new IllegalArgumentException();
	}

	private static String clean(String sql) {
		StringBuilder cleaned = new StringBuilder();
		for (char c : sql.toCharArray()) {
			if (Character.isAlphabetic(c)) {
				cleaned.append(c);
			} else {
				cleaned.append(" ");
			}
		}
		return cleaned.toString();
	}

	public static class SQLExecutor implements Executor {
		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) {
			String reportId = req.getParameter("reportId");
			String sql = null;
			Report report =  null;
			List<String> values = new ArrayList<>();
			if (given(reportId)) {
				report = getReport(reportId, dao);
				sql = report.getSql();
				for (Field f : report.getParameters()) {
					String value = req.getParameter(f.getName());
					values.add(value);
				}
			} else {
				sql = req.getParameter("sql").trim().replace(";", "");
			}
			try {
				CustomSQLTarget target = printToFile(req) ? CustomSQLTarget.FILE : CustomSQLTarget.UI;

				List<List<String>> results = dao.executeCustomSQLSearch(sql, target, values);

				if (results.isEmpty()) {
					return responseData.setData("message", "Kysely ei tuottanut osumia.");
				}

				if (report != null && report.getResultRowDecorator() != null) {
					for (List<String> resultRow : results) {
						report.getResultRowDecorator().decore(resultRow);
					}
				}

				Headers headers = headers(sql, results.get(0).size());
				if (!headers.sumIndexes.isEmpty()) {
					List<String> sumRow = new ArrayList<>();
					sumRow.add("Yhteensä");
					for (int i = 1; i < headers.columnNames.size(); i++) {
						if (headers.sumIndexes.contains(i)) {
							sumRow.add(countSum(results, i));
						} else {
							sumRow.add("");
						}
					}
					results.add(sumRow);
				}

				if (report != null && report.getResultRowDecorator() != null) {
					responseData.setData("colNames", report.getResultRowDecorator().getHeaders());
				} else {
					responseData.setData("colNames", headers.columnNames);
				}
				responseData.setData("queryResults", results);

				if (target == CustomSQLTarget.FILE) {
					return responseData.setViewName("sql-file").setData("currentTime", DateUtils.getCurrentDateTime("dd.MM.yyyy HH:mm"));
				}
				return responseData.setData("success", "Kysely suoritettu onnistuneesti. Rivejä: " + results.size());
			} catch (Exception e) {
				e.printStackTrace();
				return responseData.setData("error", e.getMessage());
			}
		}

		private String countSum(List<List<String>> results, int i) {
			int sum = 0;
			for (List<String> row : results) {
				int value = Integer.valueOf(row.get(i));
				sum += value;
			}
			return Integer.toString(sum);
		}

		private static class Headers {
			public List<String> columnNames;
			public List<Integer> sumIndexes;
			public Headers(List<String> columnNames, List<Integer> sumIndexes) {
				this.columnNames = columnNames;
				this.sumIndexes = sumIndexes;
			}
		}

		private boolean printToFile(HttpServletRequest req) {
			return req.getParameter("toFile") != null;
		}

		private Headers headers(String sql, int colCount) {
			List<String> colnames = new ArrayList<>(30);
			List<Integer> sumIndexes = new ArrayList<>();
			String selectedCols = sql.split(selectWord(sql))[1];
			selectedCols = selectedCols.split(fromWord(sql))[0];
			StringBuilder cleanedSelectedCols = new StringBuilder();
			boolean parenthesisOpen = false;
			for (char c : selectedCols.toCharArray()) {
				if (c == ',') {
					if (parenthesisOpen) {
						cleanedSelectedCols.append(",");
					} else {
						cleanedSelectedCols.append(",,,");
					}
					continue;
				}
				if (c == '(') {
					parenthesisOpen = true;
				} else if (c == ')') {
					parenthesisOpen = false;
				}
				cleanedSelectedCols.append(c);
			}
			int i = 0;
			for (String s : cleanedSelectedCols.toString().split(Pattern.quote(",,,"))) {
				s = s.trim();
				if (s.contains(" AS ")) {
					colnames.add(s.split(" AS ")[1].trim());
				} else if (s.contains(" as ")) {
					colnames.add(s.split(" as ")[1].trim());
				} else {
					colnames.add(s.trim());
				}

				if (s.startsWith("sum(") || s.startsWith("SUM(") || s.startsWith("count(") || s.startsWith("COUNT(")) {
					sumIndexes.add(i);
				}
				i++;
			}

			while (colnames.size() < colCount) {
				colnames.add("");
			}
			return new Headers(colnames, sumIndexes);
		}

		private String fromWord(String sql) {
			return upperOrLowerWord(sql, "FROM");
		}

		private String selectWord(String sql) {
			return upperOrLowerWord(sql, "SELECT");
		}

		private String upperOrLowerWord(String sql, String word) {
			String cleanedSql = " " + clean(sql) + " ";
			if (cleanedSql.contains(" " + word.toUpperCase() + " ")) {
				return word.toUpperCase();
			}
			if (cleanedSql.contains(" " + word.toLowerCase() + " ")) {
				return word.toLowerCase();
			}
			throw new IllegalStateException("No word " + word + " in " + sql);
		}
	}

}
