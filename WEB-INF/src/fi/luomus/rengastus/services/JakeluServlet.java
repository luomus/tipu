package fi.luomus.rengastus.services;

import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.Validator;

@WebServlet("/jakelu/*")
public class JakeluServlet extends TipuBaseServlet {

	private static final long serialVersionUID = 5313424716919033627L;

	private static class AfterExecutor implements Executor {

		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			String ringer = getResourceId(req);
			if (given(ringer)) {
				JSONObject distributedAndUsedRings = dao.getRingersDistributedAndUsedRings(ringer);
				List<JSONObject> rings = distributedAndUsedRings.getArray("distributedAndUsedRings").iterateAsObject();
				List<JSONObject> fieldReadables = distributedAndUsedRings.getArray("distributedAndUsedFieldReadables").iterateAsObject();
				return responseData.setViewName("jakelu-rengastaja").setData("distributedAndUsedRings", rings).setData("distributedAndUsedFieldReadables", fieldReadables);
			}
			return responseData.setViewName("jakelu");
		}
		
	}
	
	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		return validateAndExecute(req, res, Executor.EMPTY_EXECUTOR, Validator.EMPTY_VALIDATOR, Executor.EMPTY_EXECUTOR, new AfterExecutor());
	}

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) throws Exception {
		return commonData;
	}

}
