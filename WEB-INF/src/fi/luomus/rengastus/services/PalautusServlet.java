package fi.luomus.rengastus.services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Ring;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.Range;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.validators.PalautusValidator;

@WebServlet("/palautus/*")
public class PalautusServlet extends TipuBaseServlet {

	private static final long serialVersionUID = -5199996052087759894L;

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) throws Exception {
		return commonData.setViewName("jakelu");
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return validateAndExecute(req, res, Executor.EMPTY_EXECUTOR, new PalautusValidator(), new PalautusExecutor(), Executor.EMPTY_EXECUTOR);
	}

	private static class PalautusExecutor extends PalautusValidator implements Executor {

		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			Column ringer = row.get("returnRings.ringer");
			Ring start = new Ring(row.get("returnRings.ringStart").getValue());
			Ring end = new Ring(row.get("returnRings.ringEnd").getValue());

			List<Range> distributions = getRingersDistributedNumberRangesFromDb(start.getSeries(), ringer.getValue(), dao);

			RangeOperations operations = resolveRangeOperations(start.getNumbers(), end.getNumbers(), distributions);

			for (Range range : operations.rangesToDelete) {
				delete(start.getSeries() + range.start(), dao);
			}
			for (Map.Entry<Integer, Range> e : operations.rangesToCreate.entrySet()) {
				String original = start.getSeries() + e.getKey();
				String newStart = start.getSeries() + e.getValue().start();
				String newEnd = start.getSeries() + e.getValue().end();
				create(original, newStart, newEnd, dao);

			}
			for (Map.Entry<Integer, Range> e : operations.rangesToModify.entrySet()) {
				String original = start.getSeries() + e.getKey();
				String newStart = start.getSeries() + e.getValue().start();
				String newEnd = start.getSeries() + e.getValue().end();
				modify(original, newStart, newEnd, dao);
			}

			return responseData.setData("success", "Onnistuneesti poistettu jakelusta.");
		}

		private void modify(String originalRingStart, String newRingStart, String newRingEnd, DAO dao) throws Exception {
			Row newData = getOriginal(originalRingStart, dao);
			newData.get("distributed.ringStart").setValue(newRingStart);
			newData.get("distributed.ringEnd").setValue(newRingEnd);
			Column notes = newData.get("distributed.notes");
			String notesValue = notes.getValue() + " " + DateUtils.getCurrentDateTime("dd.MM.yyyy") +": Muokattu palautuksen yhteydessä automaattisesti.";
			if (notesValue.length() > 3800) {
				notesValue = notesValue.substring(0, 3700);
			}
			notes.setValue(notesValue);

			Row original = dao.newRow();
			Column idCol = original.get("distributed.ringStart");
			idCol.setValue(originalRingStart);

			dao.storeResourceToDB(newData, idCol);
		}

		private void create(String originalRingStartFromWhereSplitted, String ringStart, String ringEnd, DAO dao) throws Exception {
			Row originalData = getOriginal(originalRingStartFromWhereSplitted, dao);

			Row row = dao.newRow();
			row.get("distributed.ringStart").setValue(ringStart);
			row.get("distributed.ringEnd").setValue(ringEnd);
			row.get("distributed.distributedTo").setValue(originalData.get("distributed.distributedTo").getValue());
			row.get("distributed.distributedDate").setValue(originalData.get("distributed.distributedDate").getValue());
			row.get("distributed.notes").setValue(DateUtils.getCurrentDateTime("dd.MM.yyyy") +": Luotu palautuksen yhteydessä automaattisesti: jaettu rengasväli jakautui kahtia.");

			dao.storeResourceToDB(row, row.get("distributed.ringStart"));
		}

		private Row getOriginal(String ringStart, DAO dao) throws SQLException {
			Row originalData = dao.newRow();
			originalData.get("distributed.ringStart").setValue(ringStart);
			dao.fillResourceByIdFields(originalData, originalData.get("distributed.ringStart"));
			return originalData;
		}

		private void delete(String ringStart, DAO dao) throws Exception {
			Row row = dao.newRow();
			Column idCol = row.get("distributed.ringStart");
			idCol.setValue(ringStart);
			dao.deleteResourceFromDB(idCol);
		}

	}

	public static RangeOperations resolveRangeOperations(int start, int end, List<Range> distributions) {
		RangeOperations operations = new RangeOperations();

		for (Range distributed : distributions) {
			if (distributed.end() < start) continue;
			if (distributed.start() > end) continue;
			if (start > distributed.start() && end < distributed.end()) {
				// Poistettava väli on avoin väli jakelun sisällä
				operations.rangesToModify.put(distributed.start(), new Range(distributed.start(), start - 1));
				operations.rangesToCreate.put(distributed.start(), new Range(end + 1, distributed.end()));
				break;
			}
			if (distributed.start() >= start && distributed.end() <= end) {
				// Jakelu kokonaan poistettavassa välissä
				operations.rangesToDelete.add(distributed);
				continue;
			}
			if (start <= distributed.start()) {
				// Pätkäistaan jakelun alusta pala pois
				operations.rangesToModify.put(distributed.start(), new Range(end + 1, distributed.end()));
				continue;
			}
			if (end >= distributed.end()) {
				// Pätkäistään jakelun lopusta pala pois
				operations.rangesToModify.put(distributed.start(), new Range(distributed.start(), start - 1));
			}
		}
		return operations;
	}

	public static class RangeOperations {
		public List<Range> rangesToDelete = new ArrayList<>();
		public Map<Integer, Range> rangesToCreate = new LinkedHashMap<>();
		public Map<Integer, Range> rangesToModify = new LinkedHashMap<>();
	}
}
