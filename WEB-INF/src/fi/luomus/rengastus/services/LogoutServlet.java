package fi.luomus.rengastus.services;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.services.ResponseData;

@WebServlet("/logout")
public class LogoutServlet extends TipuBaseServlet {

	private static final long serialVersionUID = -6462630906588880052L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		getSession(req).removeAuthentication(getConfig().systemId());
		return redirectTo(getConfig().get("LintuvaaraURL"));
	}

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) {
		return commonData;
	}

}
