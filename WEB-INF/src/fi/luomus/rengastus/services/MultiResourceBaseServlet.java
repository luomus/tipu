package fi.luomus.rengastus.services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.URIBuilder;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.dao.SearchResult;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.EnumerationsContainer;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.Field;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.RowStructure;
import fi.luomus.rengastus.model.SearchResultBrowser;
import fi.luomus.rengastus.model.ValidationResponse;
import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.validators.BaseValidator;

public abstract class MultiResourceBaseServlet extends TipuBaseServlet {

	private static final long serialVersionUID = 788855994997259806L;

	protected abstract String resourceName();

	protected abstract String resourceCommonName();

	protected abstract String listingByFieldName();

	protected abstract List<String> resourceIdFieldNames();

	protected abstract String frontpageView();

	protected abstract List<String> getTipuApiResourceNames();

	protected String filterClause() {
		return null;
	}

	protected abstract Validator getValidator();

	protected abstract Validator getDeleteValidator();

	protected void cleanResourceForCopy(@SuppressWarnings("unused") Row row) {

	}

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) {
		if (req.getRequestURI().contains("/uusi") || req.getRequestURI().contains("/kopioi")) {
			commonData.setData("addNew", "true");
		}
		return commonData
				.setViewName("multiYllapito")
				.setData("resourceIdFieldNames", resourceIdFieldNames())
				.setData("resourceName", resourceName())
				.setData("resourceCommonName", resourceCommonName())
				.setData("listingByFieldName", listingByFieldName());
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		return validateAndExecute(req, res, Executor.EMPTY_EXECUTOR, new MultiResourceBaseServletGetValidator(), new GetExecutor(), Executor.EMPTY_EXECUTOR);
	}

	private class GetExecutor implements Executor {
		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {

			if ("true".equals(req.getParameter("historiasta"))) {
				List<Row> results = SearchResultBrowser.getSearchBrowserEntry(getSession(req)).getResults().getResults();
				return responseData.setData("rows", results);
			}

			if (req.getRequestURI().contains("/uusi")) {
				return responseData.setViewName("multiYllapitoOne");
			}

			if (req.getRequestURI().contains("/muokkaa") || req.getRequestURI().contains("/kopioi")) {
				Column[] idColumns = getIdColumns(row);
				dao.fillResourceByIdFields(row, idColumns);
				if (req.getRequestURI().contains("/kopioi")) {
					cleanForCopy(row);
				} else {
					List<String> ids = new ArrayList<>();
					for (Column c : idColumns) {
						ids.add(c.getValue());
					}
					SearchResultBrowser.setActiveEntry(ids, getSession(req));
				}
				return responseData.setViewName("multiYllapitoOne");
			}

			if (req.getRequestURI().contains("/poista")) { // TODO pois getistä
				dao.deleteResourceFromDB(getIdColumns(row));
				updateEnumeration(dao);
				return responseData.setRedirectLocation(config.baseURL()+"/"+frontpageView()).setData("success", "Poistettu onnistuneesti.");
			}

			for (Column c : row) {
				if (!c.hasValue()) continue;
				if (c.getField().isEnumeration()) continue;
				if (c.getField().isNumeric()) continue;
				if (c.getField().isDate()) continue;
				if (isIdColumn(c)) {
					c.setValue(c.getValue()+"%");
				} else {
					c.setValue("%"+c.getValue()+"%");
				}
			}
			SearchResult searchResult = dao.searchResources(row, resourceName(), filterClause());
			if (!searchResult.wasSuccessful()) {
				return responseData.setViewName(frontpageView()).setData("error", searchResult.getErrorMessage());
			}
			SearchResultBrowser.registerSearch(searchResult, resourceIdFieldNames(), getSession(req));
			if (searchResult.getResults().size() > 0) {
				return responseData.setData("rows", searchResult.getResults());
			}
			return responseData.setViewName(frontpageView()).setData("message", "Haulla ei löytynyt mitään");
		}

		private void cleanForCopy(Row row) {
			for (Field f: row.getTableFields(resourceName())) {
				if (f.getGroupName().equals("dataHistory")) {
					row.get(f.getName()).setValue("");
				}
			}
			cleanResourceForCopy(row);
		}

		private boolean isIdColumn(Column c) {
			return resourceIdFieldNames().contains(c.getFieldName());
		}

	}

	private class MultiResourceBaseServletGetValidator extends BaseValidator {
		@Override
		protected String viewIfErrors() {
			return frontpageView();
		}
		@Override
		protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
			if ("true".equals(req.getParameter("historiasta"))) {
				return;
			}

			if (req.getRequestURI().contains("/uusi")) {
				return;
			}

			if (req.getParameter("search") != null) {
				if (!row.tableHasValues(resourceName())) {
					setCustomFieldError("Jotain hakutekijöitä on annettava.");
				}
				return;
			}

			if (!exists(row, dao)) {
				for (Column c : getIdColumns(row)) {
					setError(c, "Tunnisteilla ei löydy mitään.");
				}
				return;
			}

			if (req.getRequestURI().contains("/poista")) {
				ValidationResponse validationResponse = getDeleteValidator().validate(row, req, dao);
				this.append(validationResponse);
			}
		}
	}

	private boolean exists(Row row, DAO dao) throws SQLException {
		Column[] idColumns = getIdColumns(row);
		for (Column c : idColumns) {
			if (!c.hasValue()) return false;
		}
		return dao.resourceExists(idColumns);
	}

	protected Column[] getIdColumns(Row row) {
		Column[] idColumns = new Column[resourceIdFieldNames().size()];
		int i = 0;
		for (String field : resourceIdFieldNames()) {
			idColumns[i++] = row.get(field);
		}
		return idColumns;
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		return validateAndExecute(req, res, new BeforePostExecutionExecutor(), new ResourceBaseServletPostValidator(), new PostExecutor(), Executor.EMPTY_EXECUTOR);
	}

	protected class BeforePostExecutionExecutor implements Executor {
		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			Column[] idColumns = getIdColumns(row);
			for (Column idColumn : idColumns) {
				idColumn.setValue(idColumn.getValue().toUpperCase());
			}
			return responseData;
		}
	}

	private String redirectAfterAlterURI(Row row, Config config) {
		URIBuilder redirectURI = new URIBuilder(config.baseURL()+"/"+resourceName());
		redirectURI.addParameter(listingByFieldName(), row.get(listingByFieldName()).getValue());
		redirectURI.addParameter("search", "true");
		return redirectURI.toString();
	}

	protected String redirectAfterInsertURI(Row row, Config config) {
		URIBuilder redirectURI = new URIBuilder(config.baseURL()+"/"+resourceName()+"/muokkaa");
		for (Column c : getIdColumns(row)) {
			redirectURI.addParameter(c.getFieldName(), c.getValue());
		}
		return redirectURI.toString();
	}

	public class PostExecutor implements Executor {
		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			dao.storeResourceToDB(row, getIdColumns(row));
			updateEnumeration(dao);
			if (req.getRequestURI().contains("/uusi")) {
				return responseData
						.setData("success", "Tiedot lisätty.")
						.setRedirectLocation(redirectAfterInsertURI(row, config));
				//muokkaa?fieldReadable.code=AA&fieldReadable.mainColor=KE&fieldReadable.forSpecies=PANHAL
			}
			return responseData
					.setData("success", "Tiedot tallennettu.")
					.setRedirectLocation(redirectAfterAlterURI(row, config));
		}

	}

	private void updateEnumeration(DAO dao) throws Exception {
		if (getTipuApiResourceNames() == null) return;
		TipuAPIClient client = null;
		try {
			client = dao.getTipuApiClient();
			for (String resourceName : getTipuApiResourceNames()) {
				EnumerationsContainer enumerationsContainer = RowStructure.loadTipuApiEnumeration(client, resourceName);
				dao.getRowStructure().updateEnumeration(resourceName, enumerationsContainer);
			}
		} finally {
			if (client != null) client.close();
		}
	}

	public class ResourceBaseServletPostValidator extends BaseValidator {
		@Override
		protected String viewIfErrors() {
			return "multiYllapitoOne";
		}
		@Override
		protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
			typeValidate(row);
			if (hasErrors()) return;

			if (isForInsert(req)) {
				Column[] idColumns = getIdColumns(row);
				for (Column idColumn : idColumns) {
					if (idColumn.getField().isSequenceGivenIdField()) continue;
					if (!idColumn.hasValue()) {
						setError(idColumn, "Tunnisteena toimiva muuttuja on annettava.");
					}
				}
				if (hasErrors()) return;
				if (dao.resourceExists(idColumns)) {
					for (Column c : idColumns) {
						setError(c, "Tämä tunnisteyhdistelmä on jo käytetty.");
					}
				}
			} else {
				if (!exists(row, dao)) {
					for (Column c : getIdColumns(row)) {
						setError(c, "Tunnisteilla ei löydy mitään.");
					}
				}
			}
			if (hasErrors()) return;

			ValidationResponse validationResponse = getValidator().validate(row, req, dao);
			this.append(validationResponse);
		}
	}

}
