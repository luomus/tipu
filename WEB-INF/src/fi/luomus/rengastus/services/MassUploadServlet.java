package fi.luomus.rengastus.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.io.IOUtils;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.dao.DAO.EventOperation;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.RowStructure;
import fi.luomus.rengastus.model.ValidationError;
import fi.luomus.rengastus.model.ValidationResponse;
import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.validators.EventValidator;

@WebServlet("/massatuonti/*")
@MultipartConfig
public class MassUploadServlet extends TipuBaseServlet {

	private static final long serialVersionUID = 3637992693925015299L;

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) throws Exception {
		return commonData.setViewName("massatuonti");
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return validateAndExecute(req, res, Executor.EMPTY_EXECUTOR, new Validator() {

			@Override
			public ValidationResponse validate(Row row, HttpServletRequest req, DAO dao) {
				try {
					Part filePart = req.getPart("file");
					if (filePart == null || filePart.getSize() == 0) {
						return new ValidationResponse().setError("file", "Anna tiedosto!");
					}
					return new ValidationResponse();
				} catch (Exception e) {
					return new ValidationResponse().setError("file", e.getMessage());
				} 
			}

		}, new Executor() {

			@Override
			public ResponseData execute(Row notused, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
				InputStream stream = null;
				try {
					Part filePart = req.getPart("file");
					stream = filePart.getInputStream();
					List<String> lines = getLines(stream);
					if (lines.size() < 2) {
						throw new IllegalArgumentException("Tiedostossa täytyy olla vähintään otsikkorivi ja yksi tapaaminen");
					}
					List<Row> rows = getRows(lines, dao);
					int i = 2;
					Map<String, ValidationResponse> validations = new LinkedHashMap<>();
					for (Row row : rows) {
						String id = String.valueOf(i++);
						ValidationResponse validationResponse = new EventValidator(id, EventOperation.INSERT).validate(row, req, dao);
						handleLayman(responseData, dao, req, row, validationResponse);
						validations.put(id, validationResponse);
					}
					boolean hasErrors = false;
					boolean hasWarnings = false;
					Map<String, String> fieldNames = new HashMap<>();
					RowStructure rowStructure = dao.getRowStructure();
					for (Map.Entry<String, ValidationResponse> e : validations.entrySet()) {
						if (e.getValue().hasErrors()) hasErrors = true;
						if (e.getValue().hasWarnings()) hasWarnings = true;
						fillFieldNames(fieldNames, rowStructure, e.getValue().getErrors());
						fillFieldNames(fieldNames, rowStructure, e.getValue().getWarnings());
					}
					boolean approveWarnings = "true".equals(req.getParameter("approveWarnings"));
					if (hasErrors || (hasWarnings && !approveWarnings)) {
						return responseData
								.setData("error", "Virheitä tai varoituksia")
								.setData("validations", validations)
								.setData("fieldNames", fieldNames)
								.setData("showApproveWarnings", !hasErrors);
					}

					JSONObject data = new JSONObject();
					i = 2;
					List<String> ids = new ArrayList<>();
					for (Row row : rows) {
						String id = String.valueOf(i++);
						EventUpdateServlet.rowToTipuApiJSON(id, row, data);
						ids.add(id);
						// TODO event.layman -> event.laymanID
					}

					JSONObject response = dao.storeEventUsingAPI(data, "recoveries", EventOperation.INSERT);

					List<String> unknownErrors = new ArrayList<>();
					int successCount = 0;
					String eventIdMin = null;
					String eventIdMax = null;

					for (String id : ids) {
						JSONObject resultRow = response.getObject("rows").getObject(id);
						boolean storedToDb = resultRow.hasKey("storedToDb") ? resultRow.getBoolean("storedToDb") : false;
						if (!storedToDb) {
							StringBuilder b = new StringBuilder();
							for (String key : resultRow.getObject("errors").getKeys()) {
								for (JSONObject error : resultRow.getObject("errors").getArray(key).iterateAsObject()) {
									b.append(error.getString("localizedErrorText")).append(" ");
								}
							}
							unknownErrors.add("Rivin " +id+ " lisäys epäonnistui tuntemattomasta syystä: " + b.toString());
						} else {
							successCount++;
							eventIdMax = resultRow.getArray("storedIds").iterator().next();
							if (eventIdMin == null) eventIdMin = eventIdMax;
						}
					}

					if (successCount > 0) {
						responseData.setData("diarioMin", getDiario(dao, eventIdMin));
						responseData.setData("diarioMax", getDiario(dao, eventIdMax));
					}

					if (unknownErrors.isEmpty()) {
						responseData.setData("success", "Lisätty onnistuneesti "+successCount+" löytöä.");
					} else {
						responseData.setData("message", "Lisätty onnistuneesti "+successCount+" löytöä.");
						responseData.setData("error", catenade(unknownErrors));
					}
					return responseData.setData("successCount", successCount);
				} catch (Exception e) {
					getErrorReporter().report("Mass upload", e);
					return responseData.setData("error", e.getMessage());
				}
				finally {
					if (stream !=  null) stream.close();
				}
			}

			private void handleLayman(ResponseData responseData, DAO dao, HttpServletRequest req, Row row, ValidationResponse validationResponse) throws Exception {
				if (!row.tableHasValues("layman")) return;
				if (!row.get("layman.language").hasValue()) {
					row.get("layman.language").setValue("E");
				}
				Row existingLayman = dao.getExistingLayman(row.get("layman.name").getValue(), row.get("layman.email").getValue());
				if (existingLayman != null) {
					String existingID = existingLayman.get("layman.id").getValue();
					row.get("event.laymanID").setValue(existingID);
				} else {
					if (EventInsertServlet.validLaymanData(row, dao, req, responseData)) {
						dao.storeResourceToDB(row, row.get("layman.id"));
						String newId = row.get("layman.id").getValue();
						row.get("event.laymanID").setValue(newId);
					} else {
						validationResponse.setError("Mallikko", "Maallikon tiedossa virheitä");
					}
				}
			}

			private String catenade(List<String> unknownErrors) {
				Iterator<String> i = unknownErrors.iterator();
				StringBuilder b = new StringBuilder();
				while (i.hasNext()) {
					b.append(i.next());
					if (i.hasNext()) b.append("; ");
				}
				return b.toString();
			}

			private String getDiario(DAO dao, String eventId) {
				return dao.getEventById(eventId).get("event.diario").getValue();
			}

			private void fillFieldNames(Map<String, String> fieldNames, RowStructure rowStructure, List<ValidationError> errors) {
				for (ValidationError error : errors) {
					if (rowStructure.hasField(error.getFieldName())) {
						fieldNames.put(error.getFieldName(), rowStructure.get(error.getFieldName()).getCommonName());
					} else {
						fieldNames.put(error.getFieldName(), error.getFieldName());
					}
				}
			}

			private List<Row> getRows(List<String> lines, DAO dao) {
				Iterator<String> i  = lines.iterator();
				Map<Integer, String> fieldIndexes = getFieldIndexesFromHeader(i.next());
				List<Row> rows = new ArrayList<>();
				while (i.hasNext()) {
					String line = i.next();
					rows.add(getRow(line, fieldIndexes, dao));
				}
				return rows;
			}

			private Row getRow(String line, Map<Integer, String> fieldIndexes, DAO dao) {
				Row row = dao.newRow();
				int i = 0;
				for (String value : line.split("\t")) {
					String fieldname = fieldIndexes.get(i);
					row.get(fieldname).setValue(value);
					i++;
				}
				row.get("event.type").setValue("recovery");
				return row;
			}

			private Map<Integer, String> getFieldIndexesFromHeader(String headerLine) {
				Map<Integer, String> indexes = new HashMap<>();
				int i = 0;
				for (String fieldName : headerLine.split("\t")) {
					if (!fieldName.startsWith("layman.")) {
						fieldName = "event." + fieldName;
					}
					indexes.put(i, fieldName);
					i++;
				}
				return indexes;
			}

			private List<String> getLines(InputStream stream) throws IOException {
				List<String> lines = IOUtils.readLines(stream, "utf-8");
				return lines;
			}

		}, Executor.EMPTY_EXECUTOR);
	}


}
