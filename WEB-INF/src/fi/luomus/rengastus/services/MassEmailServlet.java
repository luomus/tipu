package fi.luomus.rengastus.services;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.utils.EmailUtil;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.dao.SearchResult;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.validators.BaseValidator;

@WebServlet("/massapostitus/*")
@MultipartConfig
public class MassEmailServlet extends TipuBaseServlet {


	private static final long serialVersionUID = 2008988408203491385L;

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) throws Exception {
		return commonData.setViewName("massapostitus");
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return validateAndExecute(req, res, Executor.EMPTY_EXECUTOR, Validator.EMPTY_VALIDATOR, Executor.EMPTY_EXECUTOR, Executor.EMPTY_EXECUTOR);
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return validateAndExecute(req, res, Executor.EMPTY_EXECUTOR, new MassEmailSubmitValidator(), new MassEmailSubmitExecutor(), Executor.EMPTY_EXECUTOR);
	}

	private static class MassEmailSubmitValidator extends BaseValidator {

		@Override
		protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
			typeValidate(row);

			Column receiverType = row.get("massEmail.receiverType");
			Column receiverList = row.get("massEmail.receiverList");
			Column subject = row.get("massEmail.subject");
			Column message = row.get("massEmail.message");

			require(subject);
			require(message);

			if (!receiverList.hasValue()) {
				require(receiverType);
			}

			if (receiverType.hasValue() && receiverList.hasValue()) {
				setError(receiverList, "Joko vastaanottajaryhmä tai -lista on annettava, mutta molempia ei saa antaa.");
			}

			if (subject.getValue().length() < 10) {
				setError(subject, "Liian lyhyt otsikko.");
			}
			if (message.getValue().length() < 40) {
				setError(message, "Liian lyhyt viesti.");
			}

			if (receiverList.hasValue()) {
				validateReceiverList(dao, receiverList);
			}
		}

		private void validateReceiverList(DAO dao, Column receiverList) throws Exception {
			TipuApiResource ringers = dao.getTipuApiClient().get(TipuAPIClient.RINGERS);
			for (String receiver : receiverList.getValue().split(",")) {
				receiver = receiver.trim();
				if (!isInteger(receiver)) {
					setError(receiverList, "Täytyy olla lista rengastajanumeroita.");
					break;
				}
				if (!ringers.containsUnitById(receiver)) {
					setError(receiverList, "Täytyy olla lista olemassaolevia rengastajanumeroita.");
					break;
				}
			}
		}

	}

	private static class MassEmailSubmitExecutor implements Executor {

		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			Column receiverType = row.get("massEmail.receiverType");
			Column receiverList = row.get("massEmail.receiverList");
			Column subject = row.get("massEmail.subject");
			Column message = row.get("massEmail.message");

			List<String> receiverEmails = getReceiverEmails(receiverType, receiverList, config, dao);

			EmailUtil emailUtil = new EmailUtil("localhost");
			List<String> failedEmails = new ArrayList<>();

			for (String email : receiverEmails) {
				try {
					emailUtil.send(email, "rengastustoimisto@helsinki.fi", subject.getValue(), message.getValue());
				} catch (Exception e) {
					failedEmails.add(email);
				}
			}

			return responseData.setData("success", "Viesti lähetettiin "+receiverEmails.size()+":lle vastaanottajalle.").setData("failedEmails", failedEmails);
		}

		private List<String> getReceiverEmails(Column receiverType, Column receiverList, Config config, DAO dao) throws Exception {
			if (config.developmentMode() || config.stagingMode()) {
				return Utils.list("esko.piirainen@helsinki.fi");
			}

			if (receiverType.hasValue()) {
				return getEmails(getRingerNumbersByReceiverType(receiverType, dao), dao);
			}
			return getEmails(getRingerNumbersByReceiverList(receiverList), dao);
		}

		private List<String> getEmails(List<String> ringerNumbers, DAO dao) throws Exception {
			TipuApiResource ringers = dao.getTipuApiClient().get(TipuAPIClient.RINGERS);
			List<String> emails = new ArrayList<>();
			for (String ringerNumber : ringerNumbers) {
				if (!ringers.containsUnitById(ringerNumber)) throw new IllegalStateException("Rengastajaa ei löydy: " + ringerNumber);
				String email = ringers.getById(ringerNumber).getNode("email").getContents();
				if (email.length() > 0) {
					emails.add(email);
				}
			}
			return emails;
		}

		private List<String> getRingerNumbersByReceiverType(Column receiverType, DAO dao) throws Exception {
			Row searchParams = dao.newRow();
			searchParams.get("ringer.state").setValue(receiverType.getValue());
			SearchResult searchResult = dao.searchResources(searchParams, "ringer");
			if (!searchResult.wasSuccessful()) {
				throw new Exception(searchResult.getErrorMessage());
			}
			List<String> numbers = new ArrayList<>();
			for (Row result : searchResult.getResults()) {
				numbers.add(result.get("ringer.id").getValue());
			}
			return numbers;
		}

		public List<String> getRingerNumbersByReceiverList(Column receiverList) {
			List<String> numbers = new ArrayList<>();
			for (String receiver : receiverList.getValue().split(",")) {
				receiver = receiver.trim();
				if (receiver.length() > 0) {
					numbers.add(receiver);
				}
			}
			return numbers;
		}

	}

}
