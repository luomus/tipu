package fi.luomus.rengastus.services;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.io.IOUtils;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.model.Attachment;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.Validator;

@WebServlet("/attachments/*")
@MultipartConfig
public class AttachmentsServlet extends TipuBaseServlet {

	private static final long serialVersionUID = -8666012276767499969L;

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) throws Exception {
		String eventId = getResourceId(req);
		return commonData.setViewName("attachments").setData("eventId", eventId);
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return validateAndExecute(req, res, Executor.EMPTY_EXECUTOR, Validator.EMPTY_VALIDATOR, new AttachmentListExecutor(), Executor.EMPTY_EXECUTOR);
	}

	private static class AttachmentListExecutor implements Executor {

		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			if (req.getRequestURI().contains("/show")) {
				return showFile(responseData, dao, config, req, res);
			} if (req.getRequestURI().contains("/delete")) {
				return deleteFile(responseData, dao, config, req);
			}
			String eventId = getResourceId(req);
			if (!given(eventId)) {
				throw new IllegalStateException("Must have event id");
			}
			return responseData.setData("attachments", dao.getAttachments(eventId));
		}

		private ResponseData deleteFile(ResponseData responseData, DAO dao, Config config, HttpServletRequest req) throws Exception {
			String fileId = getResourceId(req);
			if (!given(fileId)) {
				throw new IllegalStateException("Must give file id");
			}
			Attachment attachment = dao.getAttachment(fileId);
			if (attachment == null) throw new IllegalStateException("Tiedostoa ei löydy.");

			dao.deleteAttachment(attachment);

			String eventId = attachment.getEventId();
			return responseData.setRedirectLocation(config.baseURL() + "/attachments/"+eventId);
		}

		private ResponseData showFile(ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			String fileId = getResourceId(req);
			if (!given(fileId)) {
				throw new IllegalStateException("Must give file id");
			}
			Attachment attachment = dao.getAttachment(fileId);
			if (attachment == null) return responseData.setRedirectLocation(config.baseURL()+"/etusivu").setData("error", "Tiedostoa ei löydy.");

			responseData.setOutputAlreadyPrinted();
			res.setContentType(attachment.getMimeType());
			res.setHeader("Content-Disposition", "attachment; filename=\"" + attachment.getFileName() + "\"");

			ServletOutputStream out = res.getOutputStream();
			InputStream data = new ByteArrayInputStream(attachment.getData());

			IOUtils.copy(data, out);
			out.flush();
			return responseData;
		}

	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return validateAndExecute(req, res, Executor.EMPTY_EXECUTOR, Validator.EMPTY_VALIDATOR, new AddAttachmentExecutor(), Executor.EMPTY_EXECUTOR);
	}

	private static class AddAttachmentExecutor implements Executor {

		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			String eventId = getResourceId(req);
			if (!given(eventId)) {
				throw new IllegalStateException("Must have event id");
			}
			Part filePart = req.getPart("attachment");
			if (filePart == null || filePart.getSize() == 0) {
				return responseData.setRedirectLocation(config.baseURL()+"/attachments/"+eventId).setData("error", "Tiedosto on annettava.");
			}
			addAttachment(dao, req, eventId);
			return responseData.setRedirectLocation(config.baseURL() + "/attachments/"+eventId);
		}

		private void addAttachment(DAO dao, HttpServletRequest req, String eventId) throws IOException, ServletException, Exception {
			InputStream stream = null;
			try {
				Part filePart = req.getPart("attachment");
				String fileName = parseFileName(filePart.getHeader("content-disposition"));
				String mimeType = filePart.getHeader("content-type");
				stream = filePart.getInputStream();
				byte[] data = IOUtils.toByteArray(stream);

				Attachment attachment = new Attachment(eventId, fileName, mimeType, data);
				dao.addAttachment(attachment);
			} finally {
				if (stream !=  null) stream.close();
			}
		}

		private String parseFileName(String header) {
			for (String part : header.split(Pattern.quote(";"))) {
				part = part.trim();
				if (part.startsWith("filename=")) {
					return part.split(Pattern.quote("="))[1].replace("\"", "");
				}
			}
			throw new IllegalStateException("Unable to parse filename from " + header);
		}

	}


}
