package fi.luomus.rengastus.services;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.URIBuilder;

import java.io.PrintWriter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.client.methods.HttpGet;

@WebServlet("/convert-coordinates")
public class CoordinateConversionServlet extends TipuBaseServlet {

	private static final long serialVersionUID = 6199785245954444187L;

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) {
		return commonData;
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		res.setContentType("application/json; charset=utf-8");
		PrintWriter out = res.getWriter();
		String lat = req.getParameter("lat");
		String lon = req.getParameter("lon");
		String system = req.getParameter("system");
		HttpClientService client = null;
		try {
			client = getDao(req).getTipuApiAuthenenticatedClient();
			URIBuilder uri = new URIBuilder(getConfig().get(Config.TIPU_API_URI)+"/coordinate-conversion-service");
			uri.addParameter("lat", lat).addParameter("lon", lon).addParameter("type", system).addParameter("format", "json");
			HttpGet request = new HttpGet(uri.toString());
			out.write(client.contentAsString(request));
		} finally {
			if (client != null) client.close();
		}
		return new ResponseData().setOutputAlreadyPrinted();
	}
	

}
