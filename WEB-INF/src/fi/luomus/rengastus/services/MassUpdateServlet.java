package fi.luomus.rengastus.services;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.io.IOUtils;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.dao.DAO.EventOperation;
import fi.luomus.rengastus.dao.MassUpdateBatch;
import fi.luomus.rengastus.dao.MassUpdateBatch.MassUpdateRow;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.Field;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.RowStructure;
import fi.luomus.rengastus.validators.BaseValidator;

@WebServlet("/massapaivitys/*")
@MultipartConfig
public class MassUpdateServlet extends TipuBaseServlet {

	private static final long serialVersionUID = -7646422221203025946L;

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) throws Exception {
		return commonData.setViewName("massapaivitys");
	}

	private static class MassUpdateData {
		MassUpdateBatch batch;
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		final MassUpdateData data = new MassUpdateData();

		return validateAndExecute(req, res, Executor.EMPTY_EXECUTOR, new BaseValidator() {

			@Override
			protected void performValidations(Row notUsed, HttpServletRequest req, DAO dao) throws Exception {
				Part filePart = req.getPart("file");
				if (filePart == null || filePart.getSize() == 0) {
					setCustomFieldError("file", "Anna tiedosto!");
					return;
				}

				String table = req.getParameter("table");
				if (!Utils.list("event", "ringer").contains(table)) {
					setCustomFieldError("table", "Tuetut taulut ovat event, ringer");
					return;
				}

				InputStream stream = null;
				try {
					stream = filePart.getInputStream();
					List<String> lines = getLines(stream);
					if (lines.size() < 2) {
						setCustomFieldError("file", "Tiedostossa täytyy olla vähintään otsikkorivi ja yksi päivitettävä rivi");
						return;
					}
					Iterator<String> lineI = lines.iterator();
					String headerLine = lineI.next().replace("|", "\t");
					Map<Integer, Field> idColumns = idFields(table, headerLine, dao.getRowStructure());
					Map<Integer, Field> updateColumns = updateFields(table, headerLine, dao.getRowStructure());
					MassUpdateBatch batch = new MassUpdateBatch(req.getParameter("table"), new ArrayList<>(idColumns.values()), new ArrayList<>(updateColumns.values()));

					while (lineI.hasNext()) {
						String line = lineI.next().replace("|", "\t");
						if (line.replace("\t", "").trim().isEmpty()) continue;
						Row row = dao.newRow();
						batch.add(values(idColumns, line, row), values(updateColumns, line, row));
						if (!typeValidate(row)) return;
					}

					data.batch = batch;
				} finally {
					if (stream !=  null) stream.close();
				}

			}

		}, new Executor() {

			@Override
			public ResponseData execute(Row notused, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
				System.out.println(" === MASS UPDATE STARTS === ");
				try {
					dao.massUpdate(data.batch);
				} catch (SQLException e) {
					getErrorReporter().report("Mass update", e);
					return responseData.setData("error", e.getMessage());
				} catch (Exception e) {
					e.printStackTrace();
					return responseData.setData("error", e.getMessage());
				}
				if (postUpdateNeeded(data)) {
					System.out.println(" ... post update");
					String errors = postUpdate(data, dao);
					if (!errors.isEmpty()) {
						System.out.println(" >>> MASS UPDATE POST PROCESSING ENDED WITH FAILURES!");
						return responseData.setData("error", errors);
					}
				}
				System.out.println(" >>> MASS UPDATE SUCCESS");
				return responseData.setData("done", "true").setData("success", "Päivitysajo onnistui!");
			}

			private String postUpdate(final MassUpdateData data, DAO dao) {
				StringBuilder errors = new StringBuilder();
				int rowCount = 1; // first line is header
				for (MassUpdateRow row : data.batch.getRows()) {
					rowCount++;
					String id = row.getIdValues().get(0).getValue();
					try {
						System.out.println("  ... reprocessing event " + id);
						Row dbRow = dao.getEventById(id);
						JSONObject resultRow = EventUpdateServlet.update(dbRow, dao, id, EventOperation.MASS_UPDATE);
						boolean success = EventUpdateServlet.updateSuccess(resultRow);
						if (!success) {
							String error = EventUpdateServlet.updateErrorMessage(resultRow);
							errors.append("Virhe tehdessä koordinaattimuunnoksia jne. Rivi jäi päivittämättä! Rivinro: " + rowCount + " id: " + id + ". " + error +" \n");
						}
					} catch (Exception e) {
						getErrorReporter().report("Mass update - post update phase, event id: " + id, e);
						errors.append("Tuntematon virhe laskettaessa koordinaattimuunnoksia jne. Rivi jäi päivittämättä! Rivinro: " + rowCount + " id: " + id + "\n");
					}
				}
				return errors.toString();
			}

			private boolean postUpdateNeeded(final MassUpdateData data) {
				boolean needToDoPostUpdate = false;
				if (data.batch.getTableName().equals("event")) {
					for (Field f : data.batch.getUpdateColumns()) {
						if (f.getName().equals("event.coordinateSystem") || f.getName().equals("event.eventDate")) {
							needToDoPostUpdate = true;
						}
					}
				}
				return needToDoPostUpdate;
			}

		}, Executor.EMPTY_EXECUTOR);
	}

	private Map<Integer, Field> idFields(String table, String headerLine, RowStructure rowStructure) {
		Map<Integer, Field> map = new LinkedHashMap<>();
		int i = 0;
		for (String fieldPart : headerLine.split("\t", -1)) {
			if (fieldPart.trim().isEmpty()) continue;
			Field f = rowStructure.get(table + "." + fieldPart);
			if (f.isIdField()) map.put(i, f);
			i++;
		}
		return map;
	}

	private Map<Integer, Field> updateFields(String table, String headerLine, RowStructure rowStructure) {
		Map<Integer, Field> map = new LinkedHashMap<>();
		int i = 0;
		for (String fieldPart : headerLine.split("\t", -1)) {
			if (fieldPart.trim().isEmpty()) continue;
			Field f = rowStructure.get(table + "." + fieldPart);
			if (f.isRingColumn()) throw new IllegalArgumentException("Can not be used to update ring columns: " + f.getName());
			if (f.isAggregated()) throw new IllegalArgumentException("Can not be used to update calculated columns: " + f.getName());
			if (f.getGroupName().equals("calculatedValues")) throw new IllegalArgumentException("Can not be used to update calcualted values: " + f.getName());
			if (!f.isIdField()) map.put(i, f);
			i++;
		}
		return map;
	}

	private List<Column> values(Map<Integer, Field> idColumns, String line, Row row) {
		List<Column> values = new ArrayList<>();
		String[] lineParts = line.split("\t", -1);
		for (Map.Entry<Integer, Field> i : idColumns.entrySet()) {
			String value = val(lineParts, i.getKey());
			Column c = row.get(i.getValue().getName());
			c.setValue(value);
			values.add(c);
		}
		return values;
	}

	private String val(String[] lineParts, int i) {
		try {
			return lineParts[i];
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	private List<String> getLines(InputStream stream) throws IOException {
		List<String> lines = IOUtils.readLines(stream, "utf-8");
		return lines;
	}

}
