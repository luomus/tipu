package fi.luomus.rengastus.services;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.dao.DAO.EventOperation;
import fi.luomus.rengastus.dao.SearchResult;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Executor;
import fi.luomus.rengastus.model.LoydosForm;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.SearchResultBrowser;
import fi.luomus.rengastus.model.ValidationResponse;
import fi.luomus.rengastus.model.Validator;
import fi.luomus.rengastus.validators.EventValidator;

@WebServlet(urlPatterns = {"/event/*", "/muokkaa/*"})
public class EventUpdateServlet extends TipuBaseServlet {

	private static final long serialVersionUID = 7290220452134002319L;

	@Override
	protected ResponseData initResponseData(ResponseData commonData, HttpServletRequest req) {
		return commonData.setViewName("event").setData("action", "UPDATE");
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		return validateAndExecute(req, res, Executor.EMPTY_EXECUTOR, new EventGetValidator(), new EventGetExecutor(), Executor.EMPTY_EXECUTOR);
	}

	private static class EventGetValidator implements Validator {

		@Override
		public ValidationResponse validate(Row row, HttpServletRequest req, DAO dao) {
			ValidationResponse validationResponse = new ValidationResponse();
			validationResponse.setViewIfErrors("haku");
			String id = getResourceId(req);
			if (!given(id)) {
				validationResponse.setError("event.id", "ID on annettava kun mennään muokkaukseen.");
			}
			return validationResponse;
		}

	}

	private class EventGetExecutor implements Executor {

		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			String id = getResourceId(req);
			row.get("event.id").setValue(id);
			SearchResult searchResult = dao.searchEvent(row);
			if (searchResult.wasSuccessful()) {
				if (searchResult.getResults().size() == 1) {
					SearchResultBrowser.setActiveEntry(id, getSession(req));
					return returnRowForUpdate(responseData, searchResult, dao);
				} else if (searchResult.getResults().size() == 0){
					return responseData.setViewName("haku").setData("error", "ID:llä ei löytynyt mitään.");
				} else {
					return responseData.setViewName("haku").setData("error", "ID:llä löytyi " + searchResult.getResults().size() + " riviä. Tämä on vallan tavatonta!");
				}
			}
			return responseData.setViewName("haku").setData("error", searchResult.getErrorMessage());
		}

		private ResponseData returnRowForUpdate(ResponseData responseData, SearchResult searchResult, DAO dao) throws Exception {
			Row row = searchResult.getResults().get(0);

			Column clutchN = row.get("event.clutchNumber");
			if (clutchN.hasValue()) {
				Column ringer = row.get("event.ringer");
				Column someoneElsesClutchN = row.get("event.someoneElsesClutchNumber");
				Column clutchNumberOwner = row.get("event.clutchNumberOwner");
				if (clutchNumberOwner.hasValue() && !clutchNumberOwner.getValue().equals(ringer.getValue())) {
					someoneElsesClutchN.setValue(clutchN.getValue());
					clutchN.setValue("");
				}
			}

			if (row.get("event.type").getValue().equals("recovery")) {
				LoydosForm loydosForm = dao.getLoydosFormOriginal(row.get("event.id").getValue());
				if (loydosForm != null) {
					responseData.setData("loydosForm", loydosForm);
				}
				String successMessage = getSuccessMessage(responseData);
				if (successMessage.startsWith("Lisäys onnistui")) {
					responseData.setData("success", successMessage + " Diarioksi tuli " + row.get("event.diario").getValue() + ".");
				}
			}

			loadRingHistory(responseData, dao, row);
			loadLaymanInfo(dao, row, responseData);
			responseData.setData("params", row);
			return responseData;
		}

		private String getSuccessMessage(ResponseData responseData) {
			Object success = responseData.getDatamodel().get("success");
			if (success == null) return "";
			return success.toString();
		}

	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		String id = getResourceId(req);
		ResponseData response = validateAndExecute(req, res, new EventInsertServlet.PrePostExecutor(), new EventValidator(id, EventOperation.UPDATE), new EventPostExecutor(), new AfterEventPostExecution());
		return response;
	}

	private static class AfterEventPostExecution implements Executor {
		@Override
		public ResponseData execute(Row row, ResponseData response, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			if (!response.hasBeenRequestedToBeRedirected()) {
				loadRingHistory(response, dao, row);
				loadLaymanInfo(dao, row, response);
			}
			return response;
		}
	}

	private static class EventPostExecutor implements Executor {

		@Override
		public ResponseData execute(Row row, ResponseData responseData, DAO dao, Config config, HttpServletRequest req, HttpServletResponse res) throws Exception {
			String id = getResourceId(req);

			JSONObject resultRow = update(row, dao, id, EventOperation.UPDATE);
			boolean storedToDb = updateSuccess(resultRow);

			if (storedToDb) {
				return responseData.setData("success", "Muokkaus onnistui. " + getExistingMessage(responseData, "success")).setRedirectLocation(config.baseURL()+"/muokkaa/"+id);
			}

			String error = updateErrorMessage(resultRow);
			return	responseData.setData("error", "Muokkaus epäonnistui tuntemattomasta syystä: " + error);
		}

	}

	public static String updateErrorMessage(JSONObject resultRow) {
		StringBuilder b = new StringBuilder();
		for (String key : resultRow.getObject("errors").getKeys()) {
			for (JSONObject error : resultRow.getObject("errors").getArray(key).iterateAsObject()) {
				b.append(error.getString("localizedErrorText")).append(" ");
			}
		}
		return b.toString();
	}

	public static boolean updateSuccess(JSONObject resultRow) {
		return resultRow.hasKey("storedToDb") ? resultRow.getBoolean("storedToDb") : false;
	}

	public static JSONObject update(Row row, DAO dao, String id, EventOperation operation) throws Exception {
		JSONObject data = EventUpdateServlet.rowToTipuApiJSON(id, row);
		JSONObject response = dao.storeEventUsingAPI(data, EventUpdateServlet.typeToModeParameterValue(row), operation);

		JSONObject resultRow = response.getObject("rows").getObject(id);
		return resultRow;
	}

	public static void loadRingHistory(ResponseData responseData, DAO dao, Row row) throws Exception {
		SearchResult searchResult = dao.getRingHistory(row);
		if (!searchResult.wasSuccessful() || searchResult.getResults().isEmpty()) {
			responseData.setData("message", "Tälle renkaalle ei löytynyt historiaa.");
		} else {
			responseData.setData("history", searchResult.getResults());
		}
	}

	public static void loadLaymanInfo(DAO dao, Row row, ResponseData responseData) {
		if (row.tableHasValues("layman")) return;
		Column laymanID = row.get("event.laymanID");
		if (!laymanID.hasValue()) return;
		row.get("layman.id").setValue(laymanID.getValue());
		try {
			dao.fillResourceByIdFields(row, row.get("layman.id"));
		} catch (Exception e) {
			responseData.setData("message", "Maallikon tietojen haku numerolla " + row.get("layman.id").getValue() + " epäonnistui.");
		}
	}

	public static JSONObject rowToTipuApiJSON(String id, Row row) {
		JSONObject data = new JSONObject();
		rowToTipuApiJSON(id, row, data);
		return data;
	}

	public static void rowToTipuApiJSON(String id, Row row, JSONObject data) {
		JSONObject rowData = data.getObject("rows").getObject(id);
		for (Column c : row) {
			if (c.hasValue()) {
				if (c.getField().isDecimal()) {
					c.setValue(Utils.trimToLength(c.getValue(), 10));
				}
				rowData.setString(c.getFieldName().replace("event.", ""), c.getValue());
			}
		}
	}

	public static String typeToModeParameterValue(Row row) {
		String type = row.get("event.type").getValue();
		if (type.equals("ringing")) return "ringings";
		if (type.equals("recovery")) return "recoveries";
		throw new IllegalStateException(type);
	}

}
