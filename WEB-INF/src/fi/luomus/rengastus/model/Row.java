package fi.luomus.rengastus.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Row implements Iterable<Column> {

	private final RowStructure rowStructure;
	private final Map<String, String> valueMap = new HashMap<>();

	public Row(RowStructure rowStructure) {
		this.rowStructure = rowStructure;
	}

	public Column get(String fieldName) {
		return new Column(valueMap, rowStructure.get(fieldName));
	}

	public boolean hasField(String fieldName) {
		return rowStructure.hasField(fieldName);
	}

	@Override
	public String toString() {
		return valueMap.toString();
	}

	@Override
	public Iterator<Column> iterator() {
		return new Iterator<Column>() {

			Iterator<Field> fieldIterator = rowStructure.getFieldIterator();

			@Override
			public boolean hasNext() {
				return fieldIterator.hasNext();
			}

			@Override
			public Column next() {
				return get(fieldIterator.next().getName());
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

	public boolean hasValues() {
		return !valueMap.isEmpty();
	}

	public boolean tableHasValues(String tableName) {
		for (Field f : rowStructure.getTableFields(tableName)) {
			Column c = get(f.getName());
			if (c.hasValue()) return true;
		}
		return false;
	}

	public boolean tableValuesEquals(String tableName, Row row) {
		for (Field f : rowStructure.getTableFields(tableName)) {
			Column c1 = get(f.getName());
			Column c2 = row.get(f.getName());
			if (!c1.getValue().equals(c2.getValue())) return false; 
		}
		return true;
	}

	public void setTableValues(String tableName, Row existing) {
		for (Field f : rowStructure.getTableFields(tableName)) {
			this.get(f.getName()).setValue(existing.get(f.getName()).getValue());
		}
	}

	public List<String> getTableNames() {
		return rowStructure.getTableNames();
	}

	public List<Field> getTableFields(String tableName) {
		return rowStructure.getTableFields(tableName);
	}
	
}
