package fi.luomus.rengastus.model;

import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;

import javax.servlet.http.HttpServletRequest;

public class RowConstructor {

	public static Row construct(HttpServletRequest req, DAO dao) {
		Row row = dao.newRow();
		for (Column c : row) {
			String[] values = req.getParameterValues(c.getFieldName());
			
			if (given(values)) {
				String value = parseValues(values);
				c.setValue(value);
			} else if (c.getField().isDate()) {
				String dd = req.getParameter(c.getFieldName() + "_dd");
				String mm = req.getParameter(c.getFieldName() + "_mm");
				String yyyy = req.getParameter(c.getFieldName() + "_yyyy");
				if (given(dd) || given(mm) || given(yyyy)) {
					c.setValue(DateUtils.catenateDateString(dd, mm, yyyy));
				}
			}
		}
		return row;
	}

	private static String parseValues(String[] values) {
		StringBuilder catenanedValues = new StringBuilder();
		for (String value : values) {
			value = value.trim();
			if (!given(value)) continue;
			catenanedValues.append(value).append("|");
		}
		Utils.removeLastChar(catenanedValues);
		return catenanedValues.toString();
	}

	private static boolean given(String[] values) {
		return values != null && values.length > 0;
	}

	private static boolean given(String param) {
		return param != null && param.length() > 0;
	}

}
