package fi.luomus.rengastus.model;

public class BinCounts {

	private final int waitingBinRingingsCount;
	private final int waitingBinRecoveriesCount;
	private final int trashBinRingingsCount;
	private final int trashBinRecoveriesCount;
	private final int deletedBinRingingsCount;
	private final int deletedBinRecoveriesCount;
	
	public BinCounts(int waitingBinRingingsCount, int waitingBinRecoveriesCount, int trashBinRingingsCount, int trashBinRecoveriesCount, int deletedBinRingingsCount, int deletedBinRecoveriesCount) {
		this.waitingBinRingingsCount = waitingBinRingingsCount;
		this.waitingBinRecoveriesCount = waitingBinRecoveriesCount;
		this.trashBinRingingsCount = trashBinRingingsCount;
		this.trashBinRecoveriesCount = trashBinRecoveriesCount;
		this.deletedBinRingingsCount = deletedBinRingingsCount;
		this.deletedBinRecoveriesCount = deletedBinRecoveriesCount;
	}

	public int getWaitingBinRingingsCount() {
		return waitingBinRingingsCount;
	}

	public int getWaitingBinRecoveriesCount() {
		return waitingBinRecoveriesCount;
	}

	public int getTrashBinRingingsCount() {
		return trashBinRingingsCount;
	}

	public int getTrashBinRecoveriesCount() {
		return trashBinRecoveriesCount;
	}

	public int getDeletedBinRingingsCount() {
		return deletedBinRingingsCount;
	}

	public int getDeletedBinRecoveriesCount() {
		return deletedBinRecoveriesCount;
	}
	
	
}
