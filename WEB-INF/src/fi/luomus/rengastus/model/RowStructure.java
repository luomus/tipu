package fi.luomus.rengastus.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.rengastus.model.Field.Type;


public class RowStructure {

	public static class RowStructureBuilder {

		private final Set<String> tablenames = new HashSet<>();
		private final Map<String, Field> fields = new LinkedHashMap<>(150);
		private final Map<String, EnumerationsContainer> enumerations = new HashMap<>(70);
		private final Map<String, String> fieldsEnumerationNames = new HashMap<>(70);
		private final Set<String> usedTipuApiResources = new HashSet<>(10);
		private final Map<String, String> groupDescriptions = new HashMap<>();

		public Map<String, Field> getFields() {
			return fields;
		}

		public void addGroupDesc(String tableGroupName, String desc) {
			groupDescriptions.put(tableGroupName, desc);
		}

		public void addField(String tableName, String groupName, String fieldName, String commonName, String desc, Type type, int inputSize) {
			this.addField(tableName, groupName, fieldName, commonName, desc, type, inputSize, false, fieldName);
		}

		public void addField(String tableName, String groupName, String fieldName, String commonName, String desc, Type type, int inputSize, boolean isAggregated, String dbName) {
			fields.put(tableName + "." + fieldName, new Field(tableName, groupName, fieldName, commonName, desc, type, inputSize, isAggregated, dbName));
			tablenames.add(tableName);
		}

		public void setFieldEnumValue(String fieldName, String enumValue, String enumDesc) {
			fieldsEnumerationNames.put(fieldName, fieldName);
			if (!enumerations.containsKey(fieldName)) {
				enumerations.put(fieldName, new EnumerationsContainer());
			}
			enumerations.get(fieldName).add(new EnumerationValue(enumValue, enumDesc));
		}

		public Map<String, String> getTablesGroupsDescriptions() {
			return groupDescriptions;
		}

		public void setFieldMode(String fieldName, String mode) {
			fields.get(fieldName).setMode(mode);
		}

		public Collection<String> getTableNames() {
			return tablenames;
		}

		public void reuseEnum(String fieldName, String enumName) {
			fieldsEnumerationNames.put(fieldName, enumName);
		}

		public void setTipuApiEnumName(String fieldName, String tipuApiResourceName) {
			fieldsEnumerationNames.put(fieldName, tipuApiResourceName);
			usedTipuApiResources.add(tipuApiResourceName);
		}

		public Set<String> getUsedTipuApiResources() {
			return usedTipuApiResources;
		}

		public Map<String, EnumerationsContainer> getEnumerations() {
			return enumerations;
		}

	}

	private static final Set<String> TABLE_FIELDS_THAT_ARE_NOT_ACTUAL_DATA = Utils.set("rowid");

	private final Map<String, Field> fields;
	private final List<Field> fieldList;
	private final Map<String, List<Field>> tableFields = new HashMap<>(20);
	private final Map<String, Map<String, List<Field>>> tablesGroupsFields = new HashMap<>(20);
	private final Map<String, String> tablesGroupsDescriptions;
	private final Map<String, EnumerationsContainer> enumerations;

	public RowStructure(RowStructureBuilder builder) {
		fields = builder.getFields();
		fieldList = Collections.unmodifiableList(new ArrayList<>(fields.values()));
		tablesGroupsDescriptions = builder.getTablesGroupsDescriptions();
		initTablesFields();
		initTablesGroupsFields();
		this.enumerations = builder.enumerations;
		initFieldsEnumerations(builder);
	}

	private void initFieldsEnumerations(RowStructureBuilder builder) {
		for (Map.Entry<String, String> e : builder.fieldsEnumerationNames.entrySet()) {
			String fieldName = e.getKey();
			String enumName = e.getValue();
			fields.get(fieldName).setEnumContainer(enumerations.get(enumName));
		}
	}

	public void updateEnumeration(String enumerationName, EnumerationsContainer enumerationsContainer) {
		if (!enumerations.containsKey(enumerationName)) {
			enumerations.put(enumerationName, enumerationsContainer);
		} else {
			enumerations.get(enumerationName).replaceValues(enumerationsContainer);
		}
	}

	private void initTablesGroupsFields() {
		for (Field f : fields.values()) {
			if (!tablesGroupsFields.containsKey(f.getTableName())) {
				tablesGroupsFields.put(f.getTableName(), new LinkedHashMap<String, List<Field>>(10));
			}
			if (!tablesGroupsFields.get(f.getTableName()).containsKey(f.getGroupName())) {
				tablesGroupsFields.get(f.getTableName()).put(f.getGroupName(), new ArrayList<Field>(10));
			}
			tablesGroupsFields.get(f.getTableName()).get(f.getGroupName()).add(f);
		}
	}

	private void initTablesFields() {
		for (Field f : fields.values()) {
			if (!tableFields.containsKey(f.getTableName())) {
				tableFields.put(f.getTableName(), new ArrayList<Field>(20));
			}
			tableFields.get(f.getTableName()).add(f);
		}
	}

	public Field get(String fieldName) {
		if (!fields.containsKey(fieldName)) throw new IllegalArgumentException("No field: "+ fieldName);
		return fields.get(fieldName);
	}

	public boolean hasField(String fieldName) {
		return fields.containsKey(fieldName);
	}

	public List<Field> getTableFields(String tableName) {
		if (!tableFields.containsKey(tableName)) throw new IllegalArgumentException("No table: " + tableName);
		return tableFields.get(tableName);
	}

	public List<Field> getActualDataTableFields(String tableName) {
		List<Field> list = new ArrayList<>();
		for (Field f : getTableFields(tableName)) {
			if (TABLE_FIELDS_THAT_ARE_NOT_ACTUAL_DATA.contains(f.getFieldNameOnly())) continue;
			if (f.isAggregated()) continue;
			list.add(f);
		}
		return list;
	}

	public List<String> getTablesGroupsNames(String tableName) {
		return new ArrayList<>(tablesGroupsFields.get(tableName).keySet());
	}

	public List<Field> getTableGroupsFields(String tableName, String groupName) {
		return tablesGroupsFields.get(tableName).get(groupName);
	}

	public String getGroupDesc(String tableName, String groupName) {
		String name = tableName + "." + groupName;
		if (!tablesGroupsDescriptions.containsKey(name)) throw new IllegalArgumentException("No group desc for " + name);
		return tablesGroupsDescriptions.get(name);
	}

	public Iterator<Field> getFieldIterator() {
		return fieldList.iterator();
	}

	public List<Field> getFields() {
		return fieldList;
	}

	public static EnumerationsContainer loadTipuApiEnumeration(TipuAPIClient client, String tipuApiResourceName) throws Exception {
		EnumerationsContainer enumerationsContainer = new EnumerationsContainer();
		Document resource = client.forceReload(tipuApiResourceName);
		for (Node resourceNode : resource.getRootNode()) {
			String value = resourceNode.getNode("id").getContents();
			String desc = resourceNode.getNode("name").getContents();
			enumerationsContainer.add(new EnumerationValue(value, desc));
		}
		return enumerationsContainer;
	}

	public List<String> getTableNames() {
		List<String> list = new ArrayList<>(tableFields.keySet());
		Collections.sort(list);
		return list;
	}

}
