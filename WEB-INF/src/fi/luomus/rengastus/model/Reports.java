package fi.luomus.rengastus.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.LegacyRingCodeHandlerUtil;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.model.Report.ResultRowDecorator;

public class Reports {

	private static List<Report> reports = null;

	public static List<Report> getReports(RowStructure row) {
		if (reports == null) {
			reports = initReports(row);
		}
		initReports(row);
		return reports;
	}

	private static List<Report> initReports(RowStructure row) {
		return Utils.list(
				Reports.getLinnutKirja(row),
				Reports.getTaulukointiLajeittain(row),
				Reports.getTaulukointiRengastajittain(row),
				Reports.getIkäennätysRaportti(row)
				);
	}

	private static Report getIkäennätysRaportti(final RowStructure row) {
		return new Report("Ikäennätysraportti", generateIkäennätysSql(row), new ArrayList<Field>())
				.dontShowSql()
				.setResultRowDecorator(new ResultRowDecorator() {
					@Override
					public void decore(List<String> resultRow) throws Exception {
						String speciesNumber = resultRow.get(0);
						String speciesId = resultRow.get(1);
						String numberOfDays = resultRow.get(2);
						String ring = resultRow.get(3);
						String ageAtRinging = resultRow.get(4);
						DateValue ringingDate = DateUtils.convertToDateValue(resultRow.get(5), "yyyy-MM-dd");
						DateValue recoveryDate = DateUtils.convertToDateValue(resultRow.get(6), "yyyy-MM-dd");
						DateValue yearsMonthsDays = DateUtils.getYearsMonthsDays(ringingDate, recoveryDate);
						String birdConditionEuring = resultRow.get(7);
						String birdConditionDesc = birdConditionEuring == null ? "" : row.get("event.birdConditionEURING").getEnumerationDescription(birdConditionEuring);
						String recoveryMethod = resultRow.get(8);
						String recoveryMethodDesc = recoveryMethod == null ? "" : row.get("event.recoveryMethod").getEnumerationDescription(recoveryMethod);
						String ringerId = resultRow.get(9);
						String ringerName = ringerId == null ? "" : row.get("event.ringer").getEnumerationDescription(ringerId);
						String validForLongevity = resultRow.get(10);

						resultRow.clear();
						resultRow.add(speciesNumber);
						resultRow.add(speciesId);
						resultRow.add(numberOfDays);
						resultRow.add(yearsMonthsDays.getYear());
						resultRow.add(yearsMonthsDays.getMonth());
						resultRow.add(yearsMonthsDays.getDay());
						resultRow.add(LegacyRingCodeHandlerUtil.dbToActual(ring));
						resultRow.add(ageAtRinging);
						resultRow.add(DateUtils.format(ringingDate, "dd.MM.yyyy"));
						resultRow.add(DateUtils.format(recoveryDate, "dd.MM.yyyy"));
						resultRow.add(birdConditionDesc);
						resultRow.add(recoveryMethodDesc);
						resultRow.add(ringerName);
						resultRow.add(validForLongevity);
					}

					@Override
					public List<String> getHeaders() {
						return Utils.list("Lajinro", "Laji", "Ero", "V.", "Kk.", "Pv,", "Rengas", "Ikä reng. hetkellä", "Rengastuspvm", "Tapaamispvm", "Linnun kunto", "Pyyntitapa", "Rengastaja", "Kelpaa ikäennätykseksi");
					}
				});
	}

	private static String generateIkäennätysSql(RowStructure row) {
		StringBuilder sql = new StringBuilder();
		sql.append(" select * from  ");
		Iterator<EnumerationValue> i = row.get("event.species").getEnumerationValues().iterator();
		while (i.hasNext()) {
			String species = i.next().getValue();
			sql.append("" +
					" ( SELECT " +
					"  species.speciesnumber, species.id as species, " +
					"  (recovery.eventDate - ringing.eventDate) AS daysBetween, " +
					"  ringing.nameRing, " +
					"  ringing.age, " +
					"  ringing.eventDate as ringingDate, " +
					"   recovery.eventDate as recoveryDate, " +
					"   recovery.BIRDCONDITIONEURING, " +
					"   recovery.RECOVERYMETHOD, " +
					"   ringing.ringer, " +
					"   recovery.validForLongevity " +
					" FROM event recovery " +
					" JOIN event ringing ON (ringing.nameRing = recovery.nameRing AND ringing.type = 'ringing') " +
					" JOIN species on (ringing.species = species.id ) " +
					" WHERE recovery.type = 'recovery'  " +
					" AND (recovery.eventDate  - ringing.eventDate) > 365 " +
					" AND ringing.species = '"+species+"' " +
					" AND ( " +
					"   ( "+
					" 		(ringing.speciesAccuracy IS NULL OR ringing.speciesAccuracy != 'E') " +
					" 		AND (recovery.birdConditionEURING IS NULL OR recovery.birdConditionEURING != '0') " +
					" 		AND (recovery.validForLongevity IS NULL OR recovery.validForLongevity != 'E') " +
					"   ) " +
					"   OR recovery.validForLongevity = 'K' " +
					" ) " +
					" order by daysBetween DESC " +
					" FETCH FIRST 10 ROWS WITH TIES \n ) ");
			if (i.hasNext()) {
				sql.append(" UNION \n ");
			}
		}
		sql.append(" order by 1, 3 DESC ");
		return sql.toString();
	}

	private static Report getLinnutKirja(RowStructure row) {
		return new Report(
				"Linnut-kirja",
				""+
						"SELECT 																\n" +
						" 	species.vernacularNameFi as Laji, 									\n" +
						" 	species.scientificname as Laji,										\n" +
						" 	NVL(vuosikohtaiset.Rengastukset_poikaset, 0) as Rengastukset_pull, 	\n" +
						" 	NVL(vuosikohtaiset.Rengastukset_aikuiset, 0) as Rengastukset_ad, 	\n" +
						" 	NVL(vuosikohtaiset.Tapaamiset, 0) as Tapaamiset, 					\n" +
						" 	NVL(oldsums.ringingcount, 0) + NVL(newsums.Rengastukset, 0) as Rengastukset_total, 	\n" +
						" 	NVL(Tapaamissummat.Tapaamiset, 0) as Tapaamiset_total 				\n" +
						" FROM species 			\n" +
						" 						\n" +
						" LEFT JOIN 			\n" +
						" ( 					\n" +
						"   SELECT 									\n" +
						"     ringing.species as LajiID, 			\n" +
						"     sum( 									\n" +
						"       case  								\n" +
						"         when event.type = 'ringing' 		\n" +
						"           and event.age like 'P%' 		\n" +
						"         then 1 							\n" +
						"         else 0  							\n" +
						"       end 								\n" +
						"     ) as Rengastukset_poikaset, 			\n" +
						"     sum( 									\n" +
						"       case  								\n" +
						"         when event.type = 'ringing' 		\n" +
						"           and (event.age is null OR event.age not like 'P%') 	\n" +
						"         then 1 							\n" +
						"         else 0  							\n" +
						"       end 								\n" +
						"     ) as Rengastukset_aikuiset, 			\n" +
						"     sum( 									\n" +
						"       case 								\n" +
						"         when event.type = 'recovery' 		\n" +
						"         then 1  							\n" +
						"         else 0  							\n" +
						"       end 								\n" +
						"     ) as Tapaamiset 						\n" +
						"   FROM event ringing 						\n" +
						"   LEFT JOIN event ON (event.nameRing = ringing.nameRing) 		\n" +
						"   WHERE ringing.type ='ringing' 								\n" +
						"   AND   to_number(to_char(event.eventDate, 'YYYY')) = ?	 	\n" +
						"   GROUP BY ringing.species 									\n" +
						" ) Vuosikohtaiset ON (Vuosikohtaiset.LajiID = species.id) 		\n" +
						" 																\n" +
						" LEFT JOIN ringing_sums_1913_1973 oldsums ON (oldsums.speciesnumber = species.speciesnumber) 	\n" +
						" 																\n" +
						" LEFT JOIN ( 													\n" +
						"   SELECT 														\n" +
						"     ringing.species as LajiID,					 			\n" +
						"     count(ringing.id) as Rengastukset			 				\n" +
						"   FROM event ringing 											\n" +
						"   WHERE ringing.type = 'ringing' and 							\n" +
						"   to_number(to_char(ringing.eventDate, 'YYYY')) between 1974 and ? 	\n" +
						"   GROUP BY ringing.species 									\n" +
						" ) newsums ON (newsums.LajiID = species.id) 					\n" +
						" 																\n" +
						" LEFT JOIN ( 													\n" +
						"   SELECT 														\n" +
						"     ringing.species as LajiID, 								\n" +
						"     count(recovery.id) as Tapaamiset 							\n" +
						"   FROM event ringing 											\n" +
						"   LEFT JOIN event recovery ON ( 								\n" +
						"     recovery.type ='recovery' AND 							\n" +
						"     recovery.nameRing = ringing.nameRing AND 					\n" +
						"     to_number(to_char(recovery.eventDate, 'YYYY')) <= ?	 	\n" +
						"   ) 															\n" +
						"   WHERE ringing.type = 'ringing' 								\n" +
						"   GROUP BY ringing.species 									\n" +
						" ) Tapaamissummat ON (Tapaamissummat.LajiID = species.id) 		\n" +
						" 																\n" +
						" WHERE (species.speciesnumber < 50000 OR species.speciesnumber >= 90000)	 \n"+
						" AND NVL(oldsums.ringingcount, 0) + NVL(newsums.Rengastukset, 0) > 0	 \n"+
						" ORDER BY species.speciesnumber                                  ",
						Utils.list(row.get("event.eventYear"), row.get("event.eventYear"), row.get("event.eventYear"))
				);
	}

	private static Report getTaulukointiRengastajittain(RowStructure row) {
		return new Report(
				"Taulukointi rengastajittain",
				"			\n" +
						" SELECT																	\n" +
						" 	trim(ringer.lastname || ' ' || ringer.firstname) as Nimi,				\n" +
						" 	ringer.id as Rengastajanumero,											\n" +
						" 	sum(case when age like 'P%' then 1 else 0 end) as Poikasia,				\n" +
						" 	sum(case when age like 'P%' then 0 else 1 end) as Aikuisia,				\n" +
						" 	count(1) as Yht															\n" +
						" FROM event																\n" +
						" JOIN municipality ON (event.municipality = municipality.id)				\n" +
						" JOIN ringer ON (ringer.id = event.ringer)									\n" +
						" WHERE 																	\n" +
						" 	type = 'ringing'														\n" +
						" 	and to_number(to_char(eventdate,'YYYY')) = ?							\n" +
						" 	and municipality.elycentre = ?											\n" +
						" GROUP BY trim(ringer.lastname || ' ' || ringer.firstname), ringer.id		\n" +
						" HAVING count(1) > 0														\n" +
						" ORDER BY Nimi, Rengastajanumero 										",
						Utils.list(row.get("event.eventYear"), row.get("municipality.elyCentre"))
				);
	}

	private static Report getTaulukointiLajeittain(RowStructure row) {
		return new Report(
				"Taulukointi lajeittain",
				"			\n" +
						" SELECT																			\n" +
						" 	species.scientificname as Laji, 												\n" +
						" 	species.vernacularnamefi as Laji,												\n" +
						" 	sum(case when age like 'P%' then 1 else 0 end) as Poikasia,						\n" +
						" 	sum(case when age like 'P%' then 0 else 1 end) as Aikuisia,						\n" +
						" 	count(1) as Yht																	\n" +
						" FROM event																		\n" +
						" JOIN municipality ON (event.municipality = municipality.id)						\n" +
						" JOIN species ON (event.species = species.id)			 							\n" +
						" WHERE 																			\n" +
						" 	type = 'ringing'																\n" +
						" 	and to_number(to_char(eventdate,'YYYY')) = ?									\n" +
						" 	and municipality.elycentre = ?													\n" +
						" GROUP BY species.speciesnumber, species.scientificname, species.vernacularnamefi	\n" +
						" HAVING count(1) > 0																\n" +
						" ORDER BY species.speciesnumber 										",
						Utils.list(row.get("event.eventYear"), row.get("municipality.elyCentre"))
				);
	}

}
