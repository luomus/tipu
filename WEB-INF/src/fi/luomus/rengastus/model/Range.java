package fi.luomus.rengastus.model;


public class Range {

	private final int start;
	private final int end;
	
	public Range(int start, int end) {
		this.start = start;
		this.end = end;
	}

	public int start() {
		return start;
	}

	public int end() {
		return end;
	}
	
	@Override
	public String toString() {
		return start + "-" + end;
	}
}
