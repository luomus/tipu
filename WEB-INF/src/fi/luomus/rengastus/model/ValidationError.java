package fi.luomus.rengastus.model;

public class ValidationError {

	private final String fieldName;
	private final String errorText;

	public ValidationError(String fieldName, String errorText) {
		this.fieldName = fieldName;
		this.errorText = errorText;
	}

	public String getFieldName() {
		return fieldName;
	}

	public String getErrorText() {
		return errorText;
	}

	@Override
	public String toString() {
		return "ValidationError [fieldName=" + fieldName + ", errorText=" + errorText + "]";
	}

}
