package fi.luomus.rengastus.model;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Report {

	private static int nextid = 0;

	private final int id;
	private final String name;
	private final String sql;
	private final List<Field> parameters;
	private boolean showSql = true;
	private ResultRowDecorator decorator = null;

	public Report(String name, String sql, List<Field> parameters) {
		nextid++;
		this.id = nextid;
		this.name = name;
		this.sql = sql;
		this.parameters = parameters;
	}

	public String getName() {
		return name;
	}

	public String getSql() {
		return sql;
	}

	public List<Field> getParameters() {
		return Collections.unmodifiableList(parameters);
	}

	public Set<Field> getDistinctParameters() {
		return new HashSet<>(parameters);
	}

	public int getId() {
		return id;
	}

	public boolean shouldShowSql() {
		return showSql;
	}

	public Report dontShowSql() {
		showSql = false;
		return this;
	}

	public ResultRowDecorator getResultRowDecorator() {
		return decorator;
	}

	public Report setResultRowDecorator(ResultRowDecorator decorator) {
		this.decorator = decorator;
		return this;
	}

	public interface ResultRowDecorator {
		void decore(List<String> resultRow) throws Exception;
		List<String> getHeaders();
	}

}
