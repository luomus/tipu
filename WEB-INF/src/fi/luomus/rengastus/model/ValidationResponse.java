package fi.luomus.rengastus.model;

import java.util.ArrayList;
import java.util.List;

public class ValidationResponse {

	private final List<ValidationError> errors = new ArrayList<>();
	private final List<ValidationError> warnings = new ArrayList<>();
	private String viewIfErrors = null;

	public boolean hasErrors() {
		return !errors.isEmpty();
	}

	public boolean hasWarnings() {
		return !warnings.isEmpty();
	}

	public List<ValidationError> getErrors() {
		return errors;
	}

	public List<ValidationError> getWarnings() {
		return warnings;
	}

	public ValidationResponse setError(String fieldName, String errorText) {
		errors.add(new ValidationError(fieldName, errorText));
		return this;
	}

	public ValidationResponse setWarning(String fieldName, String errorText) {
		warnings.add(new ValidationError(fieldName, errorText));
		return this;
	}

	public ValidationResponse setViewIfErrors(String viewIfErrors) {
		this.viewIfErrors = viewIfErrors;
		return this;
	}

	public boolean hasCustomViewForErrors() {
		return viewIfErrors != null;
	}

	public String getViewForErrors() {
		return viewIfErrors;
	}

	public void append(ValidationResponse other) {
		this.errors.addAll(other.getErrors());
		this.warnings.addAll(other.getWarnings());
		if (other.viewIfErrors != null) {
			this.viewIfErrors = other.viewIfErrors;
		}
	}

}

