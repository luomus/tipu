package fi.luomus.rengastus.model;

import fi.luomus.rengastus.dao.DAO;

import javax.servlet.http.HttpServletRequest;


public interface Validator {

	public static Validator EMPTY_VALIDATOR = new Validator() {
		
		@Override
		public ValidationResponse validate(Row row, HttpServletRequest req, DAO dao) {
			return new ValidationResponse();
		}
	};
	
	public ValidationResponse validate(Row row, HttpServletRequest req, DAO dao);

}
