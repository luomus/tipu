package fi.luomus.rengastus.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import fi.luomus.commons.session.SessionHandler;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.SearchResult;

public class SearchResultBrowser {

	private static final Map<String, SearchBrowserEntry> sessions = new HashMap<>();

	public static void registerSearch(SearchResult searchResult, SessionHandler session) {
		String internalSessionId = sessionId(session);
		if (searchResult.wasSuccessful() && searchResult.getResults().size() > 1) {
			sessions.put(internalSessionId, new SearchBrowserEntry(searchResult));
		} else {
			sessions.remove(internalSessionId);
		}
	}

	public static void registerSearch(SearchResult searchResult, List<String> idColumnNames, SessionHandler session) {
		String internalSessionId = sessionId(session);
		if (searchResult.wasSuccessful() && searchResult.getResults().size() > 1) {
			sessions.put(internalSessionId, new SearchBrowserEntry(searchResult, idColumnNames));
		} else {
			sessions.remove(internalSessionId);
		}
	}
	
	private static String sessionId(SessionHandler session) {
		return session.get("internal_session_id");
	}

	public static SearchBrowserEntry getSearchBrowserEntry(SessionHandler session) {
		return sessions.get(sessionId(session));
	}

	public static void setActiveEntry(String id, SessionHandler session) {
		setActiveEntry(Utils.list(id), session);
	}
	
	public static void setActiveEntry(List<String> ids, SessionHandler session) {
		String internalSessionId = sessionId(session);
		SearchBrowserEntry entry = sessions.get(internalSessionId);
		if (entry != null) {
			entry.updateCurrent(ids);
		}
	}

	public static class SearchBrowserEntry {

		private final SearchResult results;
		private final List<String> idColumnNames;
		private final String resourceName;
		private Row previous = null;
		private Row next = null;
		private Row current = null;

		public SearchBrowserEntry(SearchResult searchResult, List<String> idColumnNames) {
			this.results = searchResult;
			this.next = results.getResults().iterator().next();
			this.idColumnNames = idColumnNames;
			this.resourceName = idColumnNames.get(0).split(Pattern.quote("."))[0];
		}
		
		public SearchBrowserEntry(SearchResult searchResult) {
			this.results = searchResult;
			this.next = results.getResults().iterator().next();
			this.idColumnNames = resolveIdColumn();
			if (idColumnNames != null) {
				resourceName = idColumnNames.get(0).split(Pattern.quote("."))[0];
			} else {
				resourceName = null;
			}
		}

		private List<String> resolveIdColumn() {
			for (String tableName : next.getTableNames()) {
				String fieldName = next.getTableFields(tableName).get(0).getName();
				if (next.get(fieldName).hasValue()) {
					return Utils.list(fieldName);
				}
			}
			return null;
		}

		public void updateCurrent(List<String> ids) {
			if (idColumnNames == null) return;
			Row prev = null;
			Iterator<Row> i = results.getResults().iterator();
			while (i.hasNext()) {
				Row row = i.next();
				if (rowMatches(ids, row)) {
					this.current = row;
					this.previous = prev;
					if (i.hasNext()) {
						this.next = i.next();
					} else {
						this.next = null;
					}
				} else {
					prev = row;
				}
			}
		}

		private boolean rowMatches(List<String> ids, Row row) {
			int i = 0;
			for (String id : ids) {
				Column c = row.get(idColumnNames.get(i++));
				if (!c.getValue().equals(id)) return false;
			}
			return true;
		}

		public Row getPrevious() {
			return previous;
		}

		public Row getNext() {
			return next;
		}

		public Row getCurrent() {
			return current;
		}

		public List<String> getIdColumns() {
			return idColumnNames;
		}

		public String getResourceName() {
			return resourceName;
		}

		public SearchResult getResults() {
			return results;
		}
	}
}
