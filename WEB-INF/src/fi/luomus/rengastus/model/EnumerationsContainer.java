package fi.luomus.rengastus.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EnumerationsContainer {

	private final Map<String, EnumerationValue> map = new HashMap<>();
	private final List<EnumerationValue> list = new ArrayList<>();

	public Map<String, EnumerationValue> getValueMap() {
		synchronized (map) {
			return Collections.unmodifiableMap(map);
		}
	}

	public List<EnumerationValue> getEnumerationValues() {
		synchronized (map) {
			return Collections.unmodifiableList(list);
		}
	}

	public void add(EnumerationValue enumerationValue) {
		map.put(enumerationValue.getValue(), enumerationValue);
		list.add(enumerationValue);
	}

	public void replaceValues(EnumerationsContainer enumerationsContainer) {
		synchronized (map) {
			map.clear();
			list.clear();
			for (EnumerationValue enumerationValue : enumerationsContainer.getEnumerationValues()) {
				add(enumerationValue);
			}
		}
	}

}
