package fi.luomus.rengastus.model;

public class WarehouseQueueInfo {

	private int size = 0;
	private boolean hasErrors = false;

	public int getSize() {
		return size;
	}

	public WarehouseQueueInfo setSize(int size) {
		this.size = size;
		return this;
	}

	public boolean hasErrors() {
		return hasErrors;
	}

	public WarehouseQueueInfo setHasErrors() {
		this.hasErrors = true;
		return this;
	}

}
