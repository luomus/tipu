package fi.luomus.rengastus.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.luomus.commons.utils.Utils;

public class Field {

	public static enum Type { VARCHAR, INTEGER, DECIMAL, DATE, ENUMERATION, DECIMAL_ENUMERATION, UNIQUE_ID_USER_GIVEN_VARCHAR, UNIQUE_ID_USER_GIVEN_INTEGER, UNIQUE_ID_SEQUENCE_GIVEN, YEAR }
	private static final Set<String> RING_FIELDS = Utils.set(
			"event.nameRing", "event.legRing", "event.ringStart", "event.ringEnd", "event.newLegRing",
			"event.fieldReadableCode", "event.removedFieldReadableCode",
			"distributed.ringStart", "distributed.ringEnd", "ring.ringStart", "ring.ringEnd",
			"returnRings.ringStart", "returnRings.ringEnd", "fieldReadable.code");

	private static final Set<String> SHOW_FOR_BOTH = Utils.set("event.nameRing", "event.created", "event.modified", "event.modifiedBy", "event.uniformLat","event.uniformLon","event.eurefLat", "event.eurefLon", "event.wgs84DecimalLat","event.wgs84DecimalLon","event.wgs84DegreeLat", "event.wgs84DegreeLon", "event.elyCentre", "event.lylArea", "event.currentMunicipality", "event.finnishProvince");

	private static final Set<String> SHOW_FOR_RINGING_FIELDS = Utils.set(SHOW_FOR_BOTH, "event.nameRing", "event.legacySomethingUncertain", "event.euringProvinceCode");

	private static final Set<String> SHOW_FOR_RECOVERY_FIELDS = Utils.set(SHOW_FOR_BOTH, "event.diario", "event.fieldReadableChangeEuringCode", "event.distanceToRingingInKilometers", "event.directionToRingingInDegrees", "event.timeToRingingInDays", "event.country", "event.ringedSpecies", "event.ringedSpeciesAccuracy");

	private final String tableName;
	private final String groupName;
	private final String name;
	private final String fieldNameOnly;
	private final String commonName;
	private final String desc;
	private final Type type;
	private EnumerationsContainer enumerationsContainer = new EnumerationsContainer();
	private final Collection<String> modes = new ArrayList<>(5);
	private final int inputSize;
	private final boolean isAggregated;
	private final String dbName;

	public Field(String tableName, String groupName, String fieldName, String commonName, String desc, Type type, int inputSize, boolean isAggregated, String dbName) {
		this.tableName = tableName;
		this.groupName = groupName;
		this.fieldNameOnly = fieldName;
		this.name = tableName + "." + fieldName;
		this.dbName = dbName;
		this.commonName = commonName;
		this.desc = desc;
		this.type = type;
		this.inputSize = inputSize < 1 ? 1 : inputSize;
		this.isAggregated = isAggregated;
	}

	public void setEnumContainer(EnumerationsContainer enumerationsContainer) {
		this.enumerationsContainer = enumerationsContainer;
	}

	public boolean isIdField() {
		return type == Type.UNIQUE_ID_SEQUENCE_GIVEN || type == Type.UNIQUE_ID_USER_GIVEN_INTEGER || type == Type.UNIQUE_ID_USER_GIVEN_VARCHAR;
	}

	public boolean isDate() {
		return type == Type.DATE;
	}

	public boolean isEnumeration() {
		return type == Type.ENUMERATION || type == Type.DECIMAL_ENUMERATION;
	}

	public boolean isDecimal() {
		return type == Type.DECIMAL || type == Type.DECIMAL_ENUMERATION;
	}

	public int getInputSize() {
		return inputSize;
	}

	public boolean hasEnumerationDefined(String value) {
		return getEnumerationValueMap().containsKey(value);
	}

	public String getEnumerationDescription(String enumerationValue) {
		if (!hasEnumerationDefined(enumerationValue)) {
			throw new IllegalArgumentException("No enum value: " + enumerationValue + " in " + this.getName());
		}
		return getEnumerationValueMap().get(enumerationValue).getDesc();
	}

	private Map<String, EnumerationValue> getEnumerationValueMap() {
		return enumerationsContainer.getValueMap();
	}

	public List<EnumerationValue> getEnumerationValues() {
		return enumerationsContainer.getEnumerationValues();
	}

	public String getName() {
		return name;
	}

	public String getCommonName() {
		return commonName;
	}

	public String getDesc() {
		return desc;
	}

	public String getTableName() {
		return tableName;
	}

	public String getGroupName() {
		return groupName;
	}

	public boolean isInteger() {
		return type == Type.INTEGER || type == Type.UNIQUE_ID_SEQUENCE_GIVEN || type == Type.UNIQUE_ID_USER_GIVEN_INTEGER;
	}

	public boolean isYear() {
		return type == Type.YEAR;
	}

	public void setMode(String mode) {
		modes.add(mode);
	}

	public boolean showForRingingMode() {
		return inputForRingingMode() || SHOW_FOR_RINGING_FIELDS.contains(getName());
	}

	public boolean showForRecoveryMode() {
		return inputForRecoveryMode() ||  SHOW_FOR_RECOVERY_FIELDS.contains(getName());
	}

	public boolean inputForRingingMode() {
		return modes.contains("ADMIN_RINGINGS");
	}

	public boolean inputForRecoveryMode() {
		return modes.contains("ADMIN_RECOVERIES");
	}

	public boolean isSequenceGivenIdField() {
		return type == Type.UNIQUE_ID_SEQUENCE_GIVEN;
	}

	public boolean isNumeric() {
		return isInteger() || isDecimal();
	}

	public boolean isRingColumn() {
		return RING_FIELDS.contains(this.getName());
	}

	public String getDbType() {
		if (type == Type.VARCHAR || type == Type.ENUMERATION || type == Type.UNIQUE_ID_USER_GIVEN_VARCHAR) {
			return "String (" + getInputSize() + ")";
		}
		if (type == Type.DECIMAL || type == Type.DECIMAL_ENUMERATION) {
			return "Decimal (" + getInputSize() + ")";
		}
		if (type == Type.INTEGER || type == Type.UNIQUE_ID_SEQUENCE_GIVEN || type == Type.UNIQUE_ID_USER_GIVEN_INTEGER || type == Type.YEAR) {
			return "Integer (" + getInputSize() + ")";
		}
		if (type == Type.DATE) {
			return "Date";
		}
		throw new IllegalStateException("Unknown type " + type);
	}

	public String getFieldNameOnly() {
		return fieldNameOnly;
	}

	public boolean isAggregated() {
		return isAggregated;
	}

	public String getDbName() {
		return dbName;
	}

	@Override
	public int hashCode() {
		return getName().hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Field) {
			Field other = (Field) o;
			return this.getName().equals(other.getName());
		}
		throw new UnsupportedOperationException("Can only compare to another " + this.getClass().getName());
	}
}
