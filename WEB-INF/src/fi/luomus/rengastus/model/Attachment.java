package fi.luomus.rengastus.model;

public class Attachment {

	private String fileId;
	private final String eventId;
	private final String fileName;
	private final String mimeType;
	private final byte[] data;
	
	public Attachment(String eventId, String fileName, String mimeType, byte[] data) {
		this.eventId = eventId;
		this.fileName = fileName;
		this.mimeType = mimeType;
		this.data = data;
	}

	public String getEventId() {
		return eventId;
	}

	public String getFileName() {
		return fileName;
	}

	public String getMimeType() {
		return mimeType;
	}

	public byte[] getData() {
		return data;
	}

	public String getFileId() {
		return fileId;
	}

	public Attachment setFileId(String fileId) {
		this.fileId = fileId;
		return this;
	}
	
	
	
}
