package fi.luomus.rengastus.model;

import fi.luomus.commons.utils.DateUtils;

import java.text.ParseException;

public class BinRow {

	private final String eventId;
	private final String binMoveNotes;
	private final String modifiedDate;
	private final String lastModifiedUserId;
	private final Row row;
	
	public BinRow(String eventId, String binMoveNotes, String modifiedDate, String lastModifieduserId, Row row) {
		this.eventId = eventId;
		this.binMoveNotes = binMoveNotes;
		this.modifiedDate = modifiedDate;
		this.lastModifiedUserId = lastModifieduserId;
		this.row = row;
	}

	public String getEventId() {
		return eventId;
	}

	public String getBinMoveNotes() {
		return binMoveNotes;
	}

	public String getModifiedDate() throws ParseException {
		return DateUtils.format(DateUtils.convertToDate(modifiedDate), "yyyy-MM-dd");
	}

	public String getLastModifiedUserId() {
		return lastModifiedUserId;
	}

	public Row getRow() {
		return row;
	}
	
	
	
}
