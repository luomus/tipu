package fi.luomus.rengastus.model;

public class EnumerationValue {

	private final String value;
	private final String desc;
	
	public EnumerationValue(String value, String desc) {
		this.value = value;
		this.desc = desc;
	}

	public String getValue() {
		return value;
	}

	public String getDesc() {
		return desc;
	}
	
	
}
