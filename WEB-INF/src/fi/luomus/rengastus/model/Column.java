package fi.luomus.rengastus.model;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class Column {

	private final Map<String, String> valueMap;
	private final Field field;

	public Column(Map<String, String> valueMap, Field field) {
		this.valueMap = valueMap;
		this.field = field;
	}

	public boolean hasValue() {
		return valueMap.containsKey(field.getName());
	}

	public Column setValue(String value) {
		if (value != null) {
			value = value.trim();
			if (field.isDecimal()) {
				value = value.replace(".", ",");
			}
			if (field.isRingColumn()) {
				value = removeSpecialChars(value.toUpperCase());
			}
		}
		if (value == null || value.length() == 0) {
			valueMap.remove(field.getName());
		} else {
			valueMap.put(field.getName(), value);
		}
		return this;
	} 

	private static final Set<Character> ALLOWED_RING_CHARS = initAllowedRingChars();
	private static Set<Character> initAllowedRingChars() {
		Set<Character> allowedRingChars = new HashSet<>();
		String allowed = "%_ ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		for (char c : allowed.toCharArray()) {
			allowedRingChars.add(c);
		}
		return allowedRingChars;
	}

	private String removeSpecialChars(String ring) {
		StringBuilder b = new StringBuilder();
		for (char c : ring.toCharArray()) {
			if (ALLOWED_RING_CHARS.contains(c)) b.append(c);
		}
		return b.toString();
	}

	public String getValue() {
		if (!hasValue()) {
			return "";
		}
		return valueMap.get(field.getName());
	}

	public Set<String> getSelectedValues() {
		String value = getValue();
		if (value.length() < 1) return Collections.emptySet();
		if (!value.contains("|")) return Utils.set(value);
		return Utils.set(value.split(Pattern.quote("|")));
	}

	public String getFieldName() {
		return field.getName();
	}

	public String getDbName() {
		return field.getDbName();
	}

	public DateValue getDateValue() {
		return DateUtils.convertToDateValue(getValue());
	}

	public Field getField() {
		return field;
	}

	public int getIntValue() {
		return Integer.valueOf(getValue());
	}

	public double getDoubleValue() {
		return Double.valueOf(getValue().replace(",", "."));
	}

	@Override
	public String toString() {
		return getFieldName() + ": " + getValue();
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean equals(Object obj) {
		throw new UnsupportedOperationException("Compare to value not to column");
	}

}
