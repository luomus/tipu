package fi.luomus.rengastus.model;

import java.util.ArrayList;
import java.util.List;

import fi.luomus.commons.json.JSONObject;

public class LoydosForm implements Comparable<LoydosForm> {

	private final String id;
	private final boolean validated;
	private boolean received;
	private boolean deleted;
	private final String submissionTime;
	private final String sortString;
	private final List<String> attachments = new ArrayList<>();
	private final Row row;
	private final JSONObject jsonData;
	private String species;
	private String age;
	private String ringInfo;
	private Person submitter;
	private Person discoverer;
	private String tipuNotes;

	public LoydosForm(String id, boolean validated, List<String> attachments, String submissionTime, String sortString, Row row, JSONObject data) {
		this.id = id;
		this.validated = validated;
		this.submissionTime = submissionTime;
		if (attachments != null) {
			this.attachments.addAll(attachments);
		}
		this.sortString = sortString;
		this.row = row;
		this.jsonData = data;
	}

	public String getId() {
		return id;
	}

	public boolean isValidated() {
		return validated;
	}

	public List<String> getAttachments() {
		return attachments;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public void setRingInfo(String ringInfo) {
		this.ringInfo = ringInfo;
	}

	public Row getRow() {
		return row;
	}

	public String getSpecies() {
		return species;
	}

	public String getAge() {
		return age;
	}

	public String getRingInfo() {
		return ringInfo;
	}

	public void setSubmitter(Person submitter) {
		this.submitter = submitter;
	}

	public void setDiscoverer(Person discoverer) {
		this.discoverer = discoverer;
	}


	public Person getSubmitter() {
		return submitter;
	}

	public Person getDiscoverer() {
		return discoverer;
	}

	public String getSubmissionTime() {
		return submissionTime;
	}

	public JSONObject getJsonData() {
		return jsonData;
	}

	public boolean isReceived() {
		return received;
	}

	public void setReceived(boolean received) {
		this.received = received;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public static class Person {
		private final String firstName;
		private final String lastName;
		private final String address;
		private final String postCode;
		private final String city;
		private final String email;
		private final String phone;
		private String id;

		public Person(String firstName, String lastName, String address, String postCode, String city, String email, String phone) {
			this.firstName = firstName;
			this.lastName = lastName;
			this.address = address;
			this.postCode = postCode;
			this.city = city;
			this.email = email;
			this.phone = phone;
		}
		public String getFirstName() {
			return firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public String getAddress() {
			return address;
		}
		public String getPostCode() {
			return postCode;
		}
		public String getCity() {
			return city;
		}
		public String getEmail() {
			return email;
		}
		public String getPhone() {
			return phone;
		}
		public String getFullName() {
			return (firstName + " " + lastName).trim();
		}
		public boolean hasValues() {
			return (firstName + lastName + address + postCode + city + email + phone).trim().length() > 0;
		}
		public String getID() {
			return id;
		}
		public void setID(String id) {
			this.id = id;
		}
	}

	@Override
	public int compareTo(LoydosForm other) {
		return this.sortString.compareTo(other.sortString);
	}

	public String getTipuNotes() {
		return tipuNotes;
	}

	public void setTipuNotes(String tipuNotes) {
		this.tipuNotes = tipuNotes;
	}

}
