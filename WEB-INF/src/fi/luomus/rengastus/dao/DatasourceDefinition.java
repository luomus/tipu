package fi.luomus.rengastus.dao;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.db.connectivity.ConnectionDescription;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

public class DatasourceDefinition {

	public static DataSource initDataSource(Config config) {
		ConnectionDescription desc = config.connectionDescription();
		PoolProperties p = new PoolProperties();
		p.setUrl(desc.url());
		p.setDriverClassName(desc.driver());
		p.setUsername(desc.username());
		p.setPassword(desc.password());
		
		p.setDefaultAutoCommit(false); // transaction mode
		p.setMaxActive(5);
		p.setMaxIdle(5);
		p.setMinIdle(1);
		p.setInitialSize(2);
		p.setRemoveAbandonedTimeout(20*60); // 20 minutes -- "the longest possible query"
		p.setTestOnBorrow(true);
		p.setValidationQuery("SELECT 1 FROM DUAL");
		p.setValidationInterval(5 * 60 * 1000); // 5 minutes
		p.setJdbcInterceptors(
				"org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"+
				"org.apache.tomcat.jdbc.pool.interceptor.StatementCache");
		
		DataSource datasource = new DataSource();
		datasource.setPoolProperties(p);
		return datasource;
	}

}
