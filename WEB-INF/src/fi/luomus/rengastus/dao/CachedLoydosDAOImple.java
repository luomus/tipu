package fi.luomus.rengastus.dao;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import fi.luomus.commons.utils.SingleObjectCache;
import fi.luomus.commons.utils.SingleObjectCache.CacheLoader;
import fi.luomus.rengastus.model.LoydosForm;

public class CachedLoydosDAOImple implements LoydosDAO {

	private final LoydosDAOImple dao;
	private final SingleObjectCache<Map<String, LoydosForm>> cache;

	public CachedLoydosDAOImple(LoydosDAOImple dao) {
		this.dao = dao;
		this.cache = new SingleObjectCache<>(new Loader(dao), 30, TimeUnit.MINUTES);
	}

	private static class Loader implements CacheLoader<Map<String, LoydosForm>> {

		private final LoydosDAOImple dao;
		public Loader(LoydosDAOImple dao) {
			this.dao = dao;
		}

		@Override
		public Map<String, LoydosForm> load() {
			try {
				Map<String, LoydosForm> map = new LinkedHashMap<>();
				List<LoydosForm> forms = dao.getAll();
				Collections.sort(forms, new Comparator<LoydosForm>() {
					@Override
					public int compare(LoydosForm f1, LoydosForm f2) {
						return f2.compareTo(f1);
					}
				});
				for (LoydosForm form : forms) {
					map.put(form.getId(), form);
				}
				return map;
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

	}

	@Override
	public Collection<LoydosForm> getAll() {
		return cache.get().values();
	}

	@Override
	public LoydosForm getOne(String id) throws Exception {
		LoydosForm form = cache.get().get(id);
		if (form == null) {
			form = dao.getOne(id);
			cache.get().put(id, form);
		}
		return form;
	}

	@Override
	public void delete(String id) throws Exception {
		dao.delete(id);
		update(id);
	}

	@Override
	public void receive(String id) throws Exception {
		dao.receive(id);
		update(id);
	}

	private void update(String id) throws Exception {
		cache.get().put(id, dao.getOne(id));
	}

	public void markUpdated(String id) throws Exception {
		update(id);
	}

	@Override
	public void temporarilyReceive(String id) throws Exception {
		cache.get().remove(id);
	}

}
