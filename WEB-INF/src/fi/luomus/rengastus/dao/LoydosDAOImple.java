package fi.luomus.rengastus.dao;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.Charsets;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.LoydosForm;
import fi.luomus.rengastus.model.LoydosForm.Person;
import fi.luomus.rengastus.model.Row;

public class LoydosDAOImple implements LoydosDAO {

	private final String apiURL;
	private final String apiUsername;
	private final String apiPassword;
	private final DAO dao;

	public LoydosDAOImple(Config config, DAO dao) {
		apiURL = config.get("LOYDOS_URI") + "admin/api/submissions/rengasloyto/";
		apiUsername  = config.get("LOYDOS_USERNAME");
		apiPassword = config.get("LOYDOS_PASSWORD");
		if (apiURL == null || apiUsername == null || apiPassword == null) {
			throw new RuntimeException("Löydös settings are not configured!");
		}
		this.dao = dao;
	}

	@Override
	public List<LoydosForm> getAll() throws Exception {
		JSONArray array = getAllFromAPI();
		List<LoydosForm> rows = new ArrayList<>(array.size());
		Map<String, String> notes = dao.getLoydosNotes();
		for (JSONObject data : array.iterateAsObject()) {
			LoydosForm loydosForm = parseLoydosForm(data);
			loydosForm.setTipuNotes(notes.get(loydosForm.getId()));
			rows.add(loydosForm);
		}
		return rows;
	}

	@Override
	public LoydosForm getOne(String id) throws Exception {
		LoydosForm form = parseLoydosForm(getFromAPI(id));
		form.setTipuNotes(dao.getLoydosNotes(id));
		return form;
	}

	@Override
	public void delete(String id) {
		post(id, "deleted");
	}

	@Override
	public void receive(String id) {
		post(id, "received");
	}

	private void post(String id, String action) {
		URI uri;
		try {
			uri = new URI(apiURL + id + "/" + action);
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}

		HttpPost request = new HttpPost(uri);
		HttpClientService client = null;
		try {
			client = open();
			request.setEntity(new StringEntity("true", Charsets.UTF_8));
			try {
				int response = client.execute(request).getStatusLine().getStatusCode();
				if (response != 200) {
					throw new RuntimeException("Api response status code was " + response + " for " + uri);
				}
			} catch (IOException e) {
				throw new RuntimeException("Error while executing api command", e);
			}
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		} finally {
			if (client != null) client.close();
		}

	}

	private JSONObject getFromAPI(String id) {
		HttpGet request = new HttpGet(apiURL + id);
		HttpClientService clientService = null;
		try {
			clientService = open();
			String result = clientService.contentAsString(request);
			if (result == null) {
				throw new RuntimeException("Received no response: " + apiURL + id);
			}
			return new JSONObject(result);
		} catch (Exception e) {
			throw new RuntimeException(" Get single : " + apiURL + id, e);
		} finally {
			if (clientService != null) clientService.close();
		}
	}

	private HttpClientService open() throws URISyntaxException {
		return new HttpClientService(apiURL, apiUsername, apiPassword);
	}

	private JSONArray getAllFromAPI() {
		HttpGet request = new HttpGet(apiURL);
		HttpClientService clientService = null;
		try {
			clientService = open();
			String result = clientService.contentAsString(request);
			if (result == null) {
				throw new RuntimeException("Received no response: " + apiURL);
			}
			return new JSONArray(result);
		} catch (Exception e) {
			throw new RuntimeException(" Get all : " + apiURL, e);
		} finally {
			if (clientService != null) clientService.close();
		}
	}

	private LoydosForm parseLoydosForm(JSONObject data) {
		String id = data.getString("id");
		String submissionTime = formatDate(data.getString("submissionTime"));
		boolean validated = data.getBoolean("validated");
		List<String> attachments = new ArrayList<>();
		for (String attachment : data.getArray("attachments")) {
			attachments.add(attachment);
		}
		Row row = dao.newRow();

		String sortString = data.getString("submissionTime");
		LoydosForm loydosForm = new LoydosForm(id, validated, attachments, submissionTime, sortString, row, data);
		loydosForm.setDeleted(data.getBoolean("deleted"));
		loydosForm.setReceived(data.getBoolean("received"));
		loydosForm.setSpecies(data.getObject("observation").getString("species"));
		loydosForm.setAge(data.getObject("observation").getString("age"));
		loydosForm.setRingInfo(data.getObject("observation").getString("ringInfo"));

		row.get("event.intermediaryRinger").setValue(data.getString("submitterRingerNumber"));
		row.get("event.ringer").setValue(data.getString("discovererRingerNumber"));
		addNotes(row, "Notes", data.getString("additionalInfo"));

		String country = data.getObject("location").getString("country");
		String municipalityOrProvince = data.getObject("location").getString("municipality");
		String locality = data.getObject("location").getString("description");

		String lat = null;
		String lon = null;
		JSONArray coordinates = data.getObject("location").getArray("coordinates");
		if (!coordinates.isEmpty()) {
			lat = Utils.trimToLength(String.valueOf(coordinates.iterateAsObject().get(0).getDouble("latitude")), 7);
			lon = Utils.trimToLength(String.valueOf(coordinates.iterateAsObject().get(0).getDouble("longitude")), 7);
		}

		if (country.equals("FI")) {
			row.get("event.municipality").setValue(municipalityOrProvince);
			row.get("event.locality").setValue(locality);
		} else {
			row.get("event.locality").setValue(country + " " + municipalityOrProvince + " " + locality);
		}
		if (given(lat) || given(lon)) {
			row.get("event.lat").setValue(lat);
			row.get("event.lon").setValue(lon);
			row.get("event.coordinateSystem").setValue("WGS84");
			row.get("event.coordinateMeasurementMethod").setValue("KARTTA");
		}

		row.get("event.eventDate").setValue(formatDate(data.getObject("observation").getString("date")));
		row.get("event.eventDateAccuracy").setValue(data.getObject("observation").getString("accuracy"));
		String birdConditionEuring = data.getObject("observation").getString("birdState");
		row.get("event.birdConditionEURING").setValue(convertBirdConditionEuring(birdConditionEuring));
		row.get("event.sex").setValue(data.getObject("observation").getString("gender"));

		addNotes(row, "Bird condition", birdConditionEuring);
		addNotes(row, "Death duration", data.getObject("observation").getString("deadDuration"));
		addNotes(row, "Notes", data.getObject("observation").getString("additionalInfo"));
		addNotes(row, "Discovery/death reason", data.getObject("observation").getString("discoveryOrDeathReason"));

		Person submitter = parsePerson(data.getObject("submitter"));
		Person discoverer = parsePerson(data.getObject("discoverer"));
		loydosForm.setSubmitter(submitter);
		loydosForm.setDiscoverer(discoverer);
		return loydosForm;
	}

	private Person parsePerson(JSONObject object) {
		String firstName = object.getString("firstName");
		String lastName = object.getString("lastName");
		String address = object.getString("street");
		String postCode = object.getString("postalCode");
		String city = object.getString("city");
		String email = object.getString("email");
		String phone = object.getString("phone");
		Person person = new Person(firstName, lastName, address, postCode, city, email, phone);
		return person;
	}

	private void addNotes(Row row, String fieldDesc, String contents) {
		if (!given(contents.trim())) return;
		Column notes = row.get("event.notes");
		if (notes.hasValue()) {
			notes.setValue((notes.getValue() + " " + fieldDesc + ": " + contents).trim());
		} else {
			notes.setValue(fieldDesc + ": " + contents);
		}
	}

	private String convertBirdConditionEuring(String birdConditionEuring) {
		if (birdConditionEuring.contains(".")) {
			return birdConditionEuring.substring(0, birdConditionEuring.indexOf('.'));
		}
		return birdConditionEuring;
	}

	private String formatDate(String date) {
		// 2014-12-31 or 2014-09-16T18:55:21.955
		try {
			if (date.contains("T")) date = date.substring(0, date.indexOf("T"));
			return DateUtils.format(DateUtils.convertToDate(date, "yyyy-MM-dd"), "dd.MM.yyyy");
		} catch (Exception e) {
			return "";
		}
	}

	private boolean given(String value) {
		return value != null && value.length() > 0;
	}

	@Override
	public void temporarilyReceive(String id) throws Exception {
		throw new UnsupportedOperationException();
	}

}
