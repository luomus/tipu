package fi.luomus.rengastus.dao;

import java.io.File;
import java.net.URISyntaxException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;

import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.tomcat.jdbc.pool.DataSource;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.Ring;
import fi.luomus.commons.db.connectivity.SimpleTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.http.Param;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuAPIClientImple;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Logger;
import fi.luomus.commons.utils.SingleObjectCache;
import fi.luomus.commons.utils.SingleObjectCache.CacheLoader;
import fi.luomus.commons.utils.URIBuilder;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.MassUpdateBatch.MassUpdateRow;
import fi.luomus.rengastus.dao.RowBatch.Entry;
import fi.luomus.rengastus.model.Attachment;
import fi.luomus.rengastus.model.BinCounts;
import fi.luomus.rengastus.model.BinRow;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Field;
import fi.luomus.rengastus.model.Field.Type;
import fi.luomus.rengastus.model.LoydosForm;
import fi.luomus.rengastus.model.LoydosForm.Person;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.RowStructure;
import fi.luomus.rengastus.model.RowStructure.RowStructureBuilder;
import fi.luomus.rengastus.model.WarehouseQueueInfo;

public class DAOImple implements DAO {

	private static final Object LOCK = new Object();

	private static Config config;
	private static ErrorReporter errorReporter;
	private static DataSource datasource;
	private final String userId;
	private final Logger logger;

	public DAOImple(Config config, ErrorReporter errorReporter, DataSource dataSource, String userId) throws Exception {
		if (DAOImple.config == null) DAOImple.config = config;
		if (DAOImple.errorReporter == null) DAOImple.errorReporter = errorReporter;
		if (DAOImple.datasource == null) DAOImple.datasource = dataSource;
		this.userId = userId;
		File logFile = new File(config.logFolder() + File.separator + "db-log_" + DateUtils.getCurrentDateTime("yyyy-MM") + ".txt");
		this.logger = new Logger(logFile);
	}

	private static CachedLoydosDAOImple loydosDAO = null;

	private CachedLoydosDAOImple getLoydosDAO() {
		if (loydosDAO == null) {
			loydosDAO = new CachedLoydosDAOImple(new LoydosDAOImple(config, this));
		}
		return loydosDAO;
	}

	private static RowStructure rowStructure = null;

	@Override
	public RowStructure getRowStructure() {
		if (rowStructure == null) {
			rowStructure = initRowStructure();
		}
		return rowStructure;
	}

	private RowStructure initRowStructure() {
		synchronized (LOCK) {
			if (rowStructure != null) return rowStructure;
			System.out.println("Building row structure...");
			HttpClientService client = null;
			try {
				client = getTipuApiAuthenenticatedClient();
				JSONObject fields = client.contentAsJson(new HttpGet(tipuApiURI() + "/ringing/fields?forceReload=true"));
				return buildStructureFromFieldsApiFieldsAndAdditionalResources(fields);
			} catch (Exception e) {
				throw new RuntimeException("Init row structure", e);
			} finally {
				if (client != null) client.close();
				System.out.println("Row structure build!");
			}
		}
	}

	private String tipuApiURI() {
		return config.get(Config.TIPU_API_URI);
	}

	@Override
	public HttpClientService getTipuApiAuthenenticatedClient() throws URISyntaxException {
		return new HttpClientService(tipuApiURI(), config.get(Config.TIPU_API_USERNAME), config.get(Config.TIPU_API_PASSWORD));
	}

	private RowStructure buildStructureFromFieldsApiFieldsAndAdditionalResources(JSONObject fields) throws Exception {
		RowStructureBuilder builder = new RowStructureBuilder();
		buildEventTableStructureFromFieldsApiFields(fields, builder);
		buildStructureFromAdditionalResources(builder);
		return new RowStructure(builder);
	}

	private void buildStructureFromAdditionalResources(RowStructureBuilder builder) throws Exception {
		new ManuallyDefinedTablesRowBuilder(builder, config).add();
	}

	private void buildEventTableStructureFromFieldsApiFields(JSONObject fields, RowStructureBuilder builder) {
		for (JSONObject groupJson : fields.getArray("groups").iterateAsObject()) {
			String groupName = groupJson.getString("name");
			String desc = groupJson.getString("description");
			builder.addGroupDesc("event."+groupName, desc);
			addFields(groupJson, builder, groupName);
		}
	}

	private void addFields(JSONObject groupJson, RowStructureBuilder builder, String groupName) {
		for (JSONObject fieldJson : groupJson.getArray("fields").iterateAsObject()) {
			String fieldName = fieldJson.getString("field");
			String commonName = fieldJson.getString("name");
			String desc = fieldJson.getString("description");
			String type = fieldJson.getString("type");
			String dbType = fieldJson.getString("dbType");
			String dbName = fieldJson.getString("dbName");
			int inputSize = fieldJson.hasKey("inputSize") ? fieldJson.getInteger("inputSize") : 0;

			Type typeResolved = resolveType(type, dbType);
			if (fieldName.equals("id")) typeResolved = Type.UNIQUE_ID_SEQUENCE_GIVEN;

			builder.addField("event", groupName, fieldName, commonName, desc, typeResolved, inputSize, fieldJson.getBoolean("isAggregated"), dbName);
			if (type.equals("ENUMERATION")) {
				addEnumerationValues(builder, fieldJson, fieldName);
			}
			for (String mode : fieldJson.getArray("modes")) {
				builder.setFieldMode("event."+fieldName, mode);
			}
		}
	}

	private Type resolveType(String type, String dbType) {
		if (type.equalsIgnoreCase(Type.ENUMERATION.toString())) {
			if (dbType.equalsIgnoreCase(Type.DECIMAL.toString())) {
				return Type.DECIMAL_ENUMERATION;
			}
			return Type.ENUMERATION;
		}
		for (Type candidate : Type.values()) {
			if (candidate.toString().equalsIgnoreCase(type)) return candidate;
		}
		throw new IllegalStateException("Unknown type: " + type);
	}

	private void addEnumerationValues(RowStructureBuilder builder, JSONObject fieldJson, String fieldName) {
		for (JSONObject enumJson : fieldJson.getArray("enumerationValues").iterateAsObject()) {
			String enumValue = enumJson.getString("value");
			String enumDesc = enumJson.getString("detailedDescription");
			builder.setFieldEnumValue("event."+fieldName, enumValue, enumDesc);
		}
	}

	@Override
	public JSONObject getRingersDistributedAndUsedRings(String ringer) {
		HttpClientService client = null;
		try {
			client = getTipuApiAuthenenticatedClient();
			URIBuilder uri = new URIBuilder(tipuApiURI()+ "/ringing/distributed-and-used-rings");
			uri.addParameter("ringer", ringer);
			uri.addParameter("userid", userId);
			return client.contentAsJson(new HttpGet(uri.getURI()));
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			if (client != null) client.close();
		}
	}

	@Override
	public List<List<String>> executeCustomSQLSearch(String sql, CustomSQLTarget target, List<String> values) throws SQLException {
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs =  null;
		try {
			con = getConnection();

			int count = getCount(sql, values, con);

			if (target == CustomSQLTarget.UI) {
				if (count > 2000) {
					throw new SQLException("Kysely tuottaisi " + count + " osumaa. Näytölle tulostettaessa maksimi on 2000. Rajaa kyselyä tai tulosta tiedostoon.");
				}
			}
			if (target == CustomSQLTarget.FILE) {
				if (count > 1000000) {
					throw new SQLException("Kysely tuottaisi " + count + " osumaa. Maksimi on 1 000 000. Rajaa kyselyä.");
				}
			}

			List<List<String>> rows = new ArrayList<>(500);
			p = con.prepareStatement(sql);
			int i = 1;
			for (String value : values) {
				p.setString(i++, value);
			}
			rs = p.executeQuery();
			while (rs.next()) {
				List<String> cols = new ArrayList<>(30);
				int columnIndex = 1;
				while (true) {
					try {
						String val = rs.getString(columnIndex++);
						if (val !=  null && val.endsWith(" 00:00:00")) {
							val = val.replace(" 00:00:00", "");
						}
						cols.add(val);
					} catch (SQLException noMoreCols) {
						break;
					}
				}
				rows.add(cols);
			}
			return rows;
		} finally {
			Utils.close(p, rs, con);
		}
	}

	private int getCount(String sql, List<String> values, TransactionConnection con) throws SQLException {
		PreparedStatement p;
		ResultSet rs;
		p = con.prepareStatement(" SELECT COUNT(1) FROM ( " + sql + " )");
		int i = 1;
		for (String value : values) {
			p.setString(i++, value);
		}
		rs = p.executeQuery();
		rs.next();
		int count = rs.getInt(1);
		rs.close();
		p.close();
		return count;
	}

	private static TransactionConnection getConnection() throws SQLException {
		return new SimpleTransactionConnection(datasource.getConnection());
	}

	@Override
	public boolean resourceExists(Column ... idColumns) throws SQLException {
		for (Column idColumn : idColumns) {
			if (!idColumn.hasValue()) throw new IllegalArgumentException("Resource id not given.");
		}
		String dbTableName = idColumns[0].getField().getTableName();
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs =  null;
		try {
			con = getConnection();
			StringBuilder sql = new StringBuilder();
			List<String> values = new ArrayList<>(idColumns.length);
			sql.append(" SELECT COUNT(1) FROM  " + dbTableName + " WHERE 1=1 ");
			for (Column idColumn : idColumns) {
				sql.append(" AND ").append(idColumn.getFieldName()).append(" =  ? ");
				if (ringColumn(idColumn)) {
					values.add(ringToDbFormat(idColumn));
				} else {
					values.add(idColumn.getValue().toUpperCase());
				}
			}

			p = con.prepareStatement(sql.toString());
			int i = 1;
			for (String value : values) {
				p.setString(i++, value);
			}
			rs = p.executeQuery();
			rs.next();
			return rs.getInt(1) > 0;
		} finally {
			Utils.close(p, rs, con);
		}
	}

	@Override
	public void fillResourceByIdFields(Row row, Column ... idColumns) throws SQLException {
		String dbTableName = idColumns[0].getField().getTableName();
		String tableName = idColumns[0].getField().getTableName();
		List<Field> fields = getRowStructure().getTableFields(tableName);

		StringBuilder sql = new StringBuilder("SELECT ");
		for (Field f : fields) {
			if (f.getName().endsWith(".rowid")) {
				sql.append("rowid").append(",");
			} else {
				sql.append(f.getName()).append(",");
			}
		}
		Utils.removeLastChar(sql);
		sql.append(" FROM ").append(dbTableName).append(" WHERE 1=1 ");
		List<String> values = new ArrayList<>(idColumns.length);
		for (Column idColumn : idColumns) {
			if (!idColumn.hasValue()) throw new IllegalArgumentException("No value for " + idColumn.getFieldName());
			if (idColumn.getFieldName().endsWith(".rowid")) {
				sql.append(" AND rowid =  ? ");
				values.add(idColumn.getValue());
				continue;
			}
			sql.append(" AND ").append(idColumn.getFieldName()).append(" =  ? ");
			if (ringColumn(idColumn)) {
				values.add(ringToDbFormat(idColumn));
			} else {
				values.add(idColumn.getValue().toUpperCase());
			}
		}

		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs =  null;
		try {
			con = getConnection();
			p = con.prepareStatement(sql.toString());
			int i = 1;
			for (String value : values) {
				p.setString(i++, value);
			}
			rs = p.executeQuery();
			if (!rs.next()) throw new IllegalArgumentException("Nothing found with ids: " + Utils.debugS((Object[])idColumns));
			resultSetToRow(row, rs, fields);
			if (rs.next()) throw new IllegalArgumentException("More than one row found with ids: " + Utils.debugS((Object[])idColumns));
		} finally {
			Utils.close(p, rs, con);
		}
	}

	private void resultSetToRow(Row row, ResultSet rs, List<Field> tableFields) throws SQLException {
		int i = 1;
		for (Field f : tableFields) {
			if (f.isDate()) {
				row.get(f.getName()).setValue(DateUtils.sqlDateToString(rs.getDate(i++)));
			} else {
				row.get(f.getName()).setValue(rs.getString(i++));
			}
		}
	}

	@Override
	public List<String> massUpdate(MassUpdateBatch batch) throws SQLException {
		TransactionConnection con = null;
		PreparedStatement p = null;

		try {
			con = openConnection();
			con.startTransaction();
			p = con.prepareStatement(constructMassUpdateStatement(batch));
			int rowCount = 1; // first line is header
			for (MassUpdateRow row : batch.getRows()) {
				rowCount++;
				int i = 1;
				for (Column c : row.getUpdateValues()) {
					Field f = batch.getUpdateColumns().get(i-1);
					if (!c.hasValue()) p.setString(i++, null);
					else if (f.isDate()) p.setDate(i++, toDate(c));
					else if (f.isDecimal()) p.setDouble(i++, c.getDoubleValue());
					else if (f.isRingColumn()) p.setString(i++, ringToDbFormat(c));
					else p.setString(i++, c.getValue());
				}
				for (Column id : row.getIdValues()) {
					if (!id.hasValue()) throw new IllegalArgumentException("No id value for row number " + rowCount);
					p.setString(i++, id.getValue());
				}
				int upd = p.executeUpdate();
				if (upd != 1) throw new IllegalArgumentException("Row not found: " + row.getIdValues() + " (row number " + rowCount + ")");
			}
			con.commitTransaction();
		} finally {
			Utils.close(p, con);
		}
		return null;
	}

	private Date toDate(Column c) {
		if (!c.hasValue()) return null;
		return toDate(c.getValue());
	}

	private String constructMassUpdateStatement(MassUpdateBatch batch) {
		StringBuilder b = new StringBuilder();
		b.append(" UPDATE ").append(batch.getTableName()).append(" SET ");
		Iterator<Field> i = batch.getUpdateColumns().iterator();
		while (i.hasNext()) {
			Field f = i.next();
			b.append(f.getName()).append(" = ? ");
			if (i.hasNext()) b.append(", ");
		}
		b.append(" WHERE 1=1 ");
		for (Field f : batch.getIdColumns()) {
			b.append(" AND ").append(f.getName()).append(" = ? ");
		}
		String sql = b.toString();
		System.out.println(sql);
		return sql;
	}

	@Override
	public void storeBatchToDB(RowBatch batch) throws Exception {
		TransactionConnection con = null;
		try {
			con = openConnection();
			con.startTransaction();
			for (Entry e : batch.get()) {
				if (e.getIdColumns()[0].hasValue() && resourceExists(e.getIdColumns())) {
					updateResourceToDB(con, e.getRow(), e.getIdColumns());
				} else {
					insertResourceToDB(con, e.getRow(), e.getIdColumns());
				}
			}
			con.commitTransaction();
		} finally {
			Utils.close(con);
		}
	}

	@Override
	public boolean storeResourceToDB(Row row, Column ... idColumns) throws Exception {
		for (Column c : idColumns) {
			c.setValue(c.getValue().toUpperCase());
		}

		TransactionConnection con = null;
		try {
			con = openConnection();
			con.startTransaction();
			if (idColumns[0].hasValue() && resourceExists(idColumns)) {
				return updateResourceToDB(con, row, idColumns);
			}
			insertResourceToDB(con, row, idColumns);
			con.commitTransaction();
			return true;
		} finally {
			Utils.close(con);
		}
	}

	@Override
	public void deleteResourceFromDB(Column ... idColumns) throws Exception {
		if (!resourceExists(idColumns)) throw new IllegalStateException("Resource does not exist.");
		Row originalData = getRowByIds(idColumns);

		Field idColumnField = idColumns[0].getField();
		String tableName = idColumnField.getTableName();

		List<String> values = new ArrayList<>();
		StringBuilder sql = new StringBuilder(" DELETE FROM ").append(tableName).append(" WHERE 1=1 ");
		for (Column idColumn : idColumns) {
			sql.append(" AND ").append(idColumn.getFieldName()).append(" =  ? ");
			if (ringColumn(idColumn)) {
				values.add(ringToDbFormat(idColumn));
			} else {
				values.add(idColumn.getValue().toUpperCase());
			}
		}

		TransactionConnection con = null;
		PreparedStatement p = null;
		try {
			con = openConnection();
			con.startTransaction();
			p = con.prepareStatement(sql.toString());
			int i = 1;
			for (String value : values) {
				p.setString(i++, value);
			}
			int c = p.executeUpdate();
			if (c != 1) throw new IllegalStateException("Delete alterted " + c + " rows instead of 1 ");
			con.commitTransaction();
		} finally {
			Utils.close(p, con);
		}

		logger.log("DELETE from " +idColumns[0].getField().getTableName()+ ": " + serializeRow(originalData, idColumns));
		logger.commit(Utils.generateGUID(), userId);
	}

	private boolean updateResourceToDB(TransactionConnection con, Row row, Column ... idColumns) throws Exception {
		Field idColumnField = idColumns[0].getField();
		String tableName = idColumnField.getTableName();

		StringBuilder sql = new StringBuilder(" UPDATE ").append(tableName).append(" SET ");
		List<String> values = new ArrayList<>();
		for (Field f : rowStructure.getTableFields(idColumnField.getTableName())) {
			if (f.getName().endsWith(".created")) continue;
			if (f.getName().endsWith(".rowid")) continue;

			sql.append(f.getName()).append(" = ?,");

			if (f.getName().endsWith(".modified")) {
				values.add(DATE_VALUE + DateUtils.getCurrentDateTime("dd.MM.yyyy"));
			} else if (f.getName().endsWith(".modifiedBy")) {
				values.add(userId);
			} else {
				values.add(getValueByType(row, f));
			}
		}
		Utils.removeLastChar(sql);
		sql.append(" WHERE 1=1 ");
		for (Column idColumn : idColumns) {
			sql.append(" AND ").append(idColumn.getFieldName()).append(" =  ? ");
			if (ringColumn(idColumn)) {
				values.add(ringToDbFormat(idColumn));
			} else {
				values.add(idColumn.getValue().toUpperCase());
			}
		}

		PreparedStatement p = null;
		try {
			Row originalData = getRowByIds(idColumns);

			p = con.prepareStatement(sql.toString());
			int i = 1;
			for (String value : values) {
				i = setValueByType(p, i, value);
			}

			int c = p.executeUpdate();
			if (c != 1) throw new IllegalStateException("Updated " + i + " rows instead of 1.");
			String comparison = generateRowComparison(originalData, row, idColumns);
			if (comparison.length() > 0) {
				logger.log("UPDATE "+idColumns[0].getField().getTableName()+ " for IDs [" + logColumns(idColumns) + "]: " + comparison);
				logger.commit(Utils.generateGUID(), userId);
				con.commitTransaction();
				return true;
			}
			return false;
		} finally {
			Utils.close(p);
		}
	}

	private String logColumns(Column[] idColumns) {
		StringBuilder b = new StringBuilder();
		for (Column c : idColumns) {
			b.append(" { ").append(c.getFieldName()).append(": ").append(c.getValue()).append(" } ");
		}
		return b.toString().trim();
	}

	private String generateRowComparison(Row original, Row row, Column ... idColumns) {
		StringBuilder b = new StringBuilder();
		Iterator<Field> i = getRowStructure().getTableFields(idColumns[0].getField().getTableName()).iterator();
		while (i.hasNext()) {
			Field field = i.next();
			if (field.getName().contains(".modified") || field.getName().endsWith(".created") || field.getName().endsWith(".rowid")) continue;
			Column originalData = original.get(field.getName());
			Column newData = row.get(field.getName());

			if (field.isDate()) {
				if (!originalData.getDateValue().equals(newData.getDateValue())) {
					String originalDate = originalData.hasValue() ? originalData.getDateValue().toString() : "";
					String newDate = newData.hasValue() ? newData.getDateValue().toString() : "";
					generateFieldComparisonLine(b, field, originalDate, newDate);
				}
			} else if (field.isDecimal()) {
				String roundedOriginal = Utils.trimToLength(originalData.getValue(), field.getInputSize());
				String roundedNew = Utils.trimToLength(newData.getValue(), field.getInputSize());
				if (!roundedOriginal.equals(roundedNew)) {
					generateFieldComparisonLine(b, field, roundedOriginal, roundedNew);
				}
			} else {
				if (!originalData.getValue().equals(newData.getValue())) {
					generateFieldComparisonLine(b, field, originalData.getValue(), newData.getValue());
				}
			}
		}
		Utils.removeLastChar(b);
		return b.toString().trim();
	}

	private void generateFieldComparisonLine(StringBuilder b, Field field, String originalValue, String newValue) {
		b.append("{").append(field.getName()).append(":").append(originalValue).append("=>").append(newValue).append("} ");
	}

	private String serializeRow(Row row, Column ... idColumns) {
		StringBuilder b = new StringBuilder();
		for (Field f : getRowStructure().getTableFields(idColumns[0].getField().getTableName())) {
			Column c = row.get(f.getName());
			if (c.hasValue()) {
				String value = c.getValue().replace("{", "(").replace("}", ")");
				b.append(" { ").append(f.getName()).append(": ").append(value).append(" } ");
			}
		}
		return b.toString();
	}

	private Row getRowByIds(Column ... idColumns) throws SQLException {
		Row originalData = newRow();
		fillResourceByIdFields(originalData, idColumns);
		return originalData;
	}

	private int setValueByType(PreparedStatement p, int i, String value) throws SQLException {
		if (!given(value)) {
			p.setString(i, null);
			return ++i;
		}
		if (value.startsWith(DATE_VALUE)) {
			value = value.replace(DATE_VALUE, "");
			p.setDate(i, toDate(value));
			return ++i;
		}
		if (value.startsWith(DECIMAL_VALUE)) {
			value = value.replace(DECIMAL_VALUE, "").replace(",", ".");
			p.setDouble(i, Double.valueOf(value));
			return ++i;
		}
		p.setString(i, value);
		return ++i;
	}

	private Date toDate(String value) {
		if (!given(value)) return null;
		try {
			return new Date(DateUtils.getEpoch(DateUtils.convertToDateValue(value)) *1000L);
		} catch (Exception e) {
			throw new IllegalArgumentException("Invalid date: " + value);
		}
	}

	private boolean given(String value) {
		return value != null && value.length() > 0;
	}

	private static final String DATE_VALUE = "date_value_type#";
	private static final String DECIMAL_VALUE = "decimal_value_type#";

	private String getValueByType(Row row, Field f) {
		Column c = row.get(f.getName());
		String value = c.getValue();
		if (!given(value)) return null;
		if (f.isDecimal()) {
			return DECIMAL_VALUE + value;
		}
		if (f.isDate()) {
			return DATE_VALUE + value;
		}
		if (f.isIdField()) {
			value = value.toUpperCase();
			c.setValue(value);
			return value;
		} if (ringColumn(c)) {
			return ringToDbFormat(c);
		}
		return value;
	}

	private String ringToDbFormat(Column c) {
		if (!c.hasValue()) return null;
		String value = ringToDbFormat(c.getValue());
		c.setValue(value);
		return value;
	}

	private boolean ringColumn(Column c) {
		return c.getFieldName().endsWith(".ringStart") || c.getFieldName().endsWith(".ringEnd");
	}

	private String ringToDbFormat(String ringValue) {
		Ring ring = new Ring(ringValue);
		String series = ring.getSeries();
		String numbers = Integer.toString(ring.getNumbers());
		if (series.length() == 1) {
			series += " ";
		}
		while (numbers.length() < 7) {
			numbers = "0" + numbers;
		}
		return series + numbers;
	}

	private void insertResourceToDB(TransactionConnection con, Row row, Column ... idColumns) throws Exception {
		Field idColumnField = idColumns[0].getField();
		String tableName = idColumnField.getTableName();
		StringBuilder sql = new StringBuilder(" INSERT INTO ").append(tableName).append(" ( ");
		List<String> values = new ArrayList<>();
		String newId = null;
		StringBuilder bindVariables = new StringBuilder();
		for (Field f : rowStructure.getTableFields(idColumnField.getTableName())) {
			if (f.getName().endsWith(".rowid")) continue;
			sql.append(f.getName()).append(",");
			if (f.isSequenceGivenIdField()) {
				newId = getNextId(f);
				values.add(newId);
			} else if (f.getName().endsWith(".created")) {
				values.add(DATE_VALUE + DateUtils.getCurrentDateTime("dd.MM.yyyy"));
			} else if (f.getName().endsWith(".modifiedBy")) {
				values.add(userId);
			} else {
				values.add(getValueByType(row, f));
			}
			bindVariables.append("?,");
		}
		Utils.removeLastChar(sql);
		Utils.removeLastChar(bindVariables);
		sql.append(" ) VALUES ( ").append(bindVariables).append(" ) ");

		PreparedStatement p = null;
		try {
			p = con.prepareStatement(sql.toString());
			int i = 1;
			for (String value : values) {
				i = setValueByType(p, i, value);
			}
			int c = p.executeUpdate();
			if (c != 1) throw new IllegalStateException("Inserted " + i + " rows instead of 1.");

			if (newId != null) {
				idColumns[0].setValue(newId);
			}

			logger.log("INSERT to "+idColumns[0].getField().getTableName()+": " + serializeRow(row, idColumns));
			logger.commit(Utils.generateGUID(), userId);
		} finally {
			Utils.close(p);
		}
	}

	@Override
	public String getNextId(Field f) throws SQLException {
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs =  null;
		try {
			con = getConnection();
			p = con.prepareStatement( " SELECT seq_" + f.getTableName() + ".NEXTVAL FROM dual ");
			rs = p.executeQuery();
			rs.next();
			return rs.getString(1);
		} finally {
			Utils.close(p, rs, con);
		}
	}

	@Override
	public SearchResult searchEvent(Row searchParams) {
		HttpClientService client = null;
		try {
			client = getTipuApiAuthenenticatedClient();
			JSONObject response = buildRequestAndExecute(searchParams, client);
			return parseRowsResponse(response);
		} catch (Exception e) {
			return new SearchResult(e.getMessage());
		} finally {
			if (client != null) client.close();
		}
	}

	private SearchResult parseRowsResponse(JSONObject response) {
		if (!response.getBoolean("success")) {
			return returnErrors(response);
		}
		List<Row> results = new ArrayList<>(30);
		for (JSONObject rowJson : response.getArray("rows").iterateAsObject()) {
			Row row = newRow();
			results.add(row);
			for (String fieldName : rowJson.getKeys()) {
				row.get("event."+fieldName).setValue(rowJson.getString(fieldName));
			}
		}
		return new SearchResult(results);
	}

	private JSONObject buildRequestAndExecute(Row searchParams, HttpClientService client) throws Exception {
		URIBuilder uri = new URIBuilder(tipuApiURI() + "/ringing/rows");
		for (Column c : searchParams) {
			if (c.hasValue()) {
				uri.addParameter(c.getFieldName().replace("event.", ""), c.getValue());
			}
		}
		uri.addParameter("userid", userId);
		uri.addParameter("maxResultCount", "100000");

		JSONObject response = client.contentAsJson(new HttpGet(uri.getURI()));
		return response;
	}

	private SearchResult returnErrors(JSONObject response) {
		StringBuilder errors = new StringBuilder();
		for (JSONObject error : response.getArray("errors").iterateAsObject()) {
			errors.append(error.getString("localizedErrorText")).append(" ");
		}
		return new SearchResult(errors.toString());
	}

	@Override
	public Row newRow() {
		return new Row(getRowStructure());
	}

	@Override
	public JSONObject validateEventUsingAPI(JSONObject data, String ringingRecoveryMode, EventOperation eventOperation) throws Exception {
		return validateOrStore("admin-validate", data, ringingRecoveryMode, eventOperation);
	}

	@Override
	public JSONObject storeEventUsingAPI(JSONObject data, String ringingRecoveryMode, EventOperation eventOperation) throws Exception {
		return validateOrStore("rows-to-db", data, ringingRecoveryMode, eventOperation);
	}

	private JSONObject validateOrStore(String serviceName, JSONObject data, String ringingRecoveryMode, EventOperation eventOperation) throws Exception {
		HttpClientService client = null;
		try {
			client = getTipuApiAuthenenticatedClient();
			HttpPost postRequest = new HttpPost(tipuApiURI()+ "/ringing/"+serviceName);
			client.setParams(postRequest,
					new Param("data", data.toString()),
					new Param("mode", ringingRecoveryMode),
					new Param("action", eventOperation.toString()),
					new Param("acceptWarnings","true"),
					new Param("userid", userId));
			return client.contentAsJson(postRequest);
		} finally {
			if (client != null) client.close();
		}
	}


	@Override
	public int getLoydosCount() {
		try {
			Callable<Integer> call = new Callable<Integer>() {
				@Override
				public Integer call() throws Exception {
					int count = 0;
					for (LoydosForm f : getLoydosDAO().getAll()) {
						if (f.isDeleted()) continue;
						if (f.isReceived()) continue;
						count++;
					}
					return count;
				}
			};
			Utils.executeWithTimeOut(call, 5, TimeUnit.SECONDS);
			return call.call();
		} catch (TimeoutException e) {
			return -1;
		} catch (Exception e) {
			errorReporter.report("Loydos count", e);
			return -1;
		}
	}

	@Override
	public List<LoydosForm> getLoydosForms(Set<String> onlyTypes, int page) throws Exception {
		List<LoydosForm> filtered = new ArrayList<>();
		int start = 100 * page;
		int end = start + 99;
		int c = 0;
		for (LoydosForm form : getLoydosDAO().getAll()) {
			if (matchesFilters(onlyTypes, form)) {
				if (c >= start) {
					filtered.add(form);
				}
				c++;
				if (c >= end) return filtered;
			}
		}
		return filtered;
	}

	@Override
	public int getLoydosFormPageCount(Set<String> onlyTypes) throws Exception {
		int c = 0;
		for (LoydosForm form : getLoydosDAO().getAll()) {
			if (matchesFilters(onlyTypes, form)) {
				c++;
			}
		}
		return (c+100-1)/100;
	}

	private boolean matchesFilters(Set<String> onlyTypes, LoydosForm form) {
		if (onlyTypes.isEmpty()) {
			return !form.isReceived() && !form.isDeleted();
		}
		if (onlyTypes.contains("received") && form.isReceived()) return true;
		if (onlyTypes.contains("deleted") && form.isDeleted()) return true;
		return false;
	}

	@Override
	public LoydosForm getLoydosForm(String loydosID) throws Exception {
		return getLoydosDAO().getOne(loydosID);
	}

	@Override
	public void marReceivedToLoydosStoreOriginal(String eventId, String loydosID, String notes) throws Exception {
		LoydosForm form = getLoydosForm(loydosID);
		storeLoydosFormOriginal(eventId, form.getJsonData(), notes);
		getLoydosDAO().receive(loydosID);
	}

	private void storeLoydosFormOriginal(String eventId, JSONObject jsonData, String notes) throws SQLException {
		TransactionConnection con = null;
		PreparedStatement p = null;
		try {
			con = getConnection();
			con.startTransaction();
			p = con.prepareStatement("INSERT INTO loydos_originals (event_id, data, notes) VALUES (?, ?, ?)");
			p.setString(1, eventId);
			p.setString(2, jsonData.toString());
			p.setString(3, notes);
			int i = p.executeUpdate();
			if (i != 1) throw new IllegalStateException("Inserted to loydos alkup " + i + " rows instead of 1");
			con.commitTransaction();
		} finally {
			Utils.close(p, con);
		}
	}

	@Override
	public void markLoydosFormTemporarilyReceived(String loydosID) throws Exception {
		getLoydosDAO().temporarilyReceive(loydosID);
	}

	@Override
	public LoydosForm getLoydosFormOriginal(String eventId) throws Exception {
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			p = con.prepareStatement(" SELECT data, notes FROM loydos_originals WHERE event_id = ? ");
			p.setString(1, eventId);
			rs = p.executeQuery();
			if (!rs.next()) return null;
			String id = new JSONObject(rs.getString(1)).getString("id");
			LoydosForm form = getLoydosForm(id); // Koska tuotantokannassa on dataa ennen Löydöksen kuvatunnisteuudistusta, täytyy meidän pyytää uudelleen apista tätä lähetystä koskevat tiedot, vaikka JSON periaatteessa onkin tallessa Tipun omassa tietokannassa.
			form.setTipuNotes(rs.getString(2));
			return form;
		} finally {
			Utils.close(p, rs, con);
		}
	}

	@Override
	public Row getExistingLayman(String fullname, String email) throws Exception {
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			StringBuilder sql = new StringBuilder();
			List<String> values = new ArrayList<>();
			sql.append(" SELECT id FROM layman WHERE TRIM(UPPER(email)) = ? ");
			values.add(email.toUpperCase());

			for (String namePart : fullname.split(" ")) {
				if (!given(namePart)) continue;
				sql.append("AND UPPER(name) LIKE ? ");
				values.add("%"+namePart.toUpperCase()+"%");
			}
			p = con.prepareStatement(sql.toString());
			int i = 1;
			for (String value : values) {
				p.setString(i++, value);
			}
			rs = p.executeQuery();
			if (rs.next()) {
				String id = rs.getString(1);
				return getRowByIds(newRow().get("layman.id").setValue(id));
			}
			return null;
		} finally {
			Utils.close(p, rs, con);
		}
	}

	@Override
	public void fillExistingLaymanOrInsertAndFill(Person laymanCandidate, Row row) throws Exception {
		if (given(laymanCandidate.getEmail()) && given(laymanCandidate.getFullName())) {
			Row existing = getExistingLayman(laymanCandidate.getFullName(), laymanCandidate.getEmail());
			if (existing != null) {
				row.setTableValues("layman", existing);
				row.get("event.laymanID").setValue(row.get("layman.id").getValue());
				row.get("event.layman").setValue(row.get("layman.name").getValue());
				return;
			}
		}
		row.get("layman.name").setValue(laymanCandidate.getFullName());
		row.get("layman.email").setValue(laymanCandidate.getEmail());
		row.get("layman.phone").setValue(laymanCandidate.getPhone());
		row.get("layman.addressLine1").setValue(laymanCandidate.getAddress());
		row.get("layman.addressLine2").setValue(laymanCandidate.getPostCode());
		row.get("layman.addressLine3").setValue(laymanCandidate.getCity());
		trimToMaxLength(row);
		TransactionConnection con = null;
		try {
			con = openConnection();
			con.startTransaction();
			insertResourceToDB(con, row, row.get("layman.id"));
			con.commitTransaction();
		} finally {
			Utils.close(con);
		}

		row.get("event.laymanID").setValue(row.get("layman.id").getValue());
		row.get("event.layman").setValue(row.get("layman.name").getValue());
	}

	private void trimToMaxLength(Row row) {
		for (Column c : row) {
			if (c.hasValue()) {
				trimToMaxLength(c);
			}
		}
	}

	private void trimToMaxLength(Column c) {
		if (c.getField().isEnumeration() || c.getField().isDate()) return;
		int maxLenght = c.getField().getInputSize();
		if (c.getValue().length() > maxLenght) {
			c.setValue(c.getValue().substring(0, maxLenght));
		}
	}

	@Override
	public SearchResult getRingHistory(Row row) throws Exception {
		JSONObject searchData = new JSONObject();
		for (Column c : row) {
			if (c.hasValue()) {
				searchData.setString(c.getFieldName().replace("event.", ""), c.getValue());
			}
		}
		HttpClientService client = null;
		try {
			client = getTipuApiAuthenenticatedClient();
			URIBuilder uri = new URIBuilder(tipuApiURI()+"/ringing/bird-history");
			uri.addParameter("data", searchData.toString());
			uri.addParameter("userid", userId);
			HttpGet request = new HttpGet(uri.getURI());
			JSONObject response = client.contentAsJson(request);
			return parseRowsResponse(response);
		} finally {
			if (client != null) client.close();
		}
	}

	@Override
	public void markDeletedToLoydos(String idToRemove) throws Exception {
		getLoydosDAO().delete(idToRemove);
	}

	private static final String UPDATE_LOYDOS_NOTES_SQL = " UPDATE loydos_notes set notes = ? WHERE loydos_id = ? ";
	private static final String SAVE_LOYDOS_NOTES_SQL = " INSERT INTO loydos_notes (notes, loydos_id) VALUES (?, ?) ";

	@Override
	public void saveLoydosNotes(String loydosId, String notes) throws Exception {
		TransactionConnection con = null;
		PreparedStatement p = null;
		try {
			con = getConnection();
			con.startTransaction();
			if (getLoydosNotes(loydosId) != null) {
				p = con.prepareStatement(UPDATE_LOYDOS_NOTES_SQL);
			} else {
				p = con.prepareStatement(SAVE_LOYDOS_NOTES_SQL);
			}
			p.setString(1, notes);
			p.setString(2, loydosId);
			p.executeUpdate();
			con.commitTransaction();
		} finally {
			Utils.close(p, con);
		}
		getLoydosDAO().markUpdated(loydosId);
	}

	private static final String GET_LOYDOS_NOTES_SQL = " SELECT loydos_id, notes FROM loydos_notes ";

	@Override
	public Map<String, String> getLoydosNotes() throws Exception {
		Map<String, String> notes = new HashMap<>();
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			p = con.prepareStatement(GET_LOYDOS_NOTES_SQL);
			rs = p.executeQuery();
			while (rs.next()) {
				notes.put(rs.getString(1), rs.getString(2));
			}
		} finally {
			Utils.close(p, rs, con);
		}
		return notes;
	}

	private static final String GET_LOYDOS_NOTE_SQL = " SELECT notes FROM loydos_notes WHERE loydos_id = ? ";

	@Override
	public String getLoydosNotes(String loydosId) throws Exception {
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			p = con.prepareStatement(GET_LOYDOS_NOTE_SQL);
			p.setString(1, loydosId);
			rs = p.executeQuery();
			if (rs.next()) {
				String s = rs.getString(1);
				if (s == null) return "";
				return s;
			}
			return null;
		} finally {
			Utils.close(p, rs, con);
		}
	}

	@Override
	public JSONObject deleteEventUsingAPI(String id) throws Exception {
		HttpClientService client = null;
		try {
			client = getTipuApiAuthenenticatedClient();
			URIBuilder uri = new URIBuilder(tipuApiURI() + "/ringing/delete-row/" + id);
			uri.addParameter("userid", userId);
			HttpDelete deleteRequest = new HttpDelete(uri.getURI());
			return client.contentAsJson(deleteRequest);
		} finally {
			if (client != null) client.close();
		}
	}

	private static final int WAITING_BIN = 1;
	private static final int TRASH_BIN = 2;
	private static final int DELETED_BIN = 3;

	private static final String IN_BIN_SQL = " SELECT count(1) FROM bin WHERE event_id = ? ";

	@Override
	public boolean inBin(String eventId) throws SQLException {
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs =  null;
		try {
			con = getConnection();
			p = con.prepareStatement(IN_BIN_SQL);
			p.setString(1, eventId);
			rs = p.executeQuery();
			rs.next();
			return rs.getInt(1) > 0;
		} finally {
			Utils.close(p, rs, con);
		}
	}

	@Override
	public void insertEventToWaitingBin(Row row, String binNotes) throws SQLException {
		insertEventToBin(WAITING_BIN, row, binNotes);
	}

	@Override
	public void inertEventToTrashBin(Row row, String binNotes) throws SQLException {
		insertEventToBin(TRASH_BIN, row, binNotes);
	}

	@Override
	public void insertEventToDeletedBin(Row row, String binNotes) throws SQLException {
		insertEventToBin(DELETED_BIN, row, binNotes);
	}

	@Override
	public void updateEventToWaitingBin(Row row, String binNotes) throws SQLException {
		updateEventToBin(WAITING_BIN, row, binNotes);
	}

	@Override
	public void updateEventToTrashBin(Row row, String binNotes) throws SQLException {
		updateEventToBin(TRASH_BIN, row, binNotes);
	}

	@Override
	public void updateEventToDeletedBin(Row row, String binNotes) throws SQLException {
		updateEventToBin(DELETED_BIN, row, binNotes);
	}

	private static final String INSERT_EVENT_TO_BIN_SQL = " INSERT INTO bin (event_id, bin_type, event_type, data, notes, userid, modified) VALUES (?, ?, ?, ?, ?, ?, ?) ";

	private void insertEventToBin(int binType, Row row, String notes) throws SQLException {
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs =  null;
		try {
			con = getConnection();
			con.startTransaction();
			String eventId = row.get("event.id").getValue();
			p = con.prepareStatement(INSERT_EVENT_TO_BIN_SQL);
			p.setString(1, eventId);
			p.setInt(2, binType);
			p.setString(3, row.get("event.type").getValue());
			p.setString(4, serializeRow(row, row.get("event.id")));
			p.setString(5, notes);
			p.setString(6, userId);
			p.setDate(7, new Date(DateUtils.getCurrentEpoch() * 1000L));
			int i = p.executeUpdate();
			if (i != 1) throw new IllegalStateException("Inserted " + i + " rows instead of 1");
			con.commitTransaction();
		} finally {
			Utils.close(p, rs, con);
		}
		binCountCache.getForceReload();
	}

	private static final String UPDATE_EVENT_TO_BIN_SQL = " UPDATE bin SET bin_type = ?, data = ?, notes = ?, userid = ?, modified = sysdate WHERE event_id = ? ";

	private void updateEventToBin(int binType, Row row, String notes) throws SQLException {
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs =  null;
		try {
			con = getConnection();
			con.startTransaction();
			String eventId = row.get("event.id").getValue();
			p = con.prepareStatement(UPDATE_EVENT_TO_BIN_SQL);
			p.setInt(1, binType);
			p.setString(2, serializeRow(row, row.get("event.id")));
			p.setString(3, notes);
			p.setString(4, userId);
			p.setString(5, eventId);
			int i = p.executeUpdate();
			if (i != 1) throw new IllegalStateException("Updated " + i + " rows instead of 1");
			con.commitTransaction();
		} finally {
			Utils.close(p, rs, con);
		}
		binCountCache.getForceReload();
	}

	private static final String GET_NEXT_EVENT_ID_SQL = "SELECT event_id_seq.NEXTVAL FROM dual";

	@Override
	public String getNextEventId() throws SQLException {
		TransactionConnection con = null;
		try {
			con = getConnection();
			return getNextEventId(con);
		} finally {
			Utils.close(con);
		}
	}

	private String getNextEventId(TransactionConnection con) throws SQLException {
		PreparedStatement p = null;
		ResultSet rs =  null;
		try {
			p = con.prepareStatement(GET_NEXT_EVENT_ID_SQL);
			rs = p.executeQuery();
			rs.next();
			return rs.getString(1);
		} finally {
			Utils.close(p, rs);
		}
	}

	@Override
	public List<BinRow> getWaitingBinEvents(String eventType) throws Exception {
		return getBinEvents(WAITING_BIN, eventType);
	}

	@Override
	public List<BinRow> getTrashBinEvents(String eventType) throws Exception {
		return getBinEvents(TRASH_BIN, eventType);
	}

	@Override
	public List<BinRow> getDeletedBinEvents(String eventType) throws Exception {
		return getBinEvents(DELETED_BIN, eventType);
	}

	private static final String GET_BIN_EVENTS_SQL = " SELECT event_id, data, notes, userid, modified FROM bin WHERE bin_type = ? AND event_type = ? ORDER BY modified DESC ";

	private List<BinRow> getBinEvents(int binType, String eventType) throws SQLException {
		List<BinRow> list = new ArrayList<>(100);
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			p = con.prepareStatement(GET_BIN_EVENTS_SQL);
			p.setInt(1, binType);
			p.setString(2, eventType);
			rs = p.executeQuery();
			while (rs.next()) {
				BinRow binRow = buildBinRow(rs);
				list.add(binRow);
			}
		} finally {
			Utils.close(p, rs, con);
		}
		return list;
	}

	private BinRow buildBinRow(ResultSet rs) throws SQLException {
		String eventId = rs.getString(1);
		String data = rs.getString(2);
		String notes = rs.getString(3);
		String userId = rs.getString(4);
		String date = DateUtils.sqlDateToString(rs.getDate(5));
		Row row = newRow();
		BinRowParser.parseRowFromBinData(row, data);
		BinRow binRow = new BinRow(eventId, notes, date, userId, row);
		return binRow;
	}

	private static final String GET_ONE_BIN_EVENT_SQL = " SELECT event_id, data, notes, userid, modified FROM bin WHERE event_id = ? ";

	@Override
	public BinRow getEventFromBin(String eventId) throws Exception {
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			p = con.prepareStatement(GET_ONE_BIN_EVENT_SQL);
			p.setString(1, eventId);
			rs = p.executeQuery();
			if (!rs.next()) return null;
			return buildBinRow(rs);
		} finally {
			Utils.close(p, rs, con);
		}
	}

	@Override
	public BinCounts getBinCounts() throws Exception {
		return binCountCache.get();
	}

	private static final SingleObjectCache<BinCounts> binCountCache = new SingleObjectCache<>(new CacheLoader<BinCounts>() {
		@Override
		public BinCounts load() {
			try {
				return loadBinCounts();
			} catch (Exception e) {
				errorReporter.report("Loading bin counts", e);
				return new BinCounts(-1, -1, -1, -1, -1, -1);
			}
		}
	}, 1, TimeUnit.MINUTES);

	private static final String LOAD_BIN_COUNTS_SQL = " SELECT bin_type, event_type, COUNT(1) FROM bin GROUP BY bin_type, event_type ";

	private static BinCounts loadBinCounts() throws SQLException {
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			p = con.prepareStatement(LOAD_BIN_COUNTS_SQL);
			rs = p.executeQuery();
			int waitingRingings = 0;
			int waitingRecoveries = 0;
			int trashRingings = 0;
			int trashRecoveries = 0;
			int deletedRingings = 0;
			int deletedRecoveries = 0;
			while (rs.next()) {
				int bin = rs.getInt(1);
				String type = rs.getString(2);
				int count = rs.getInt(3);
				if (bin == WAITING_BIN) {
					if (type.equals("ringing")) {
						waitingRingings = count;
					} else {
						waitingRecoveries = count;
					}
				} else if (bin == TRASH_BIN){
					if (type.equals("ringing")) {
						trashRingings = count;
					} else {
						trashRecoveries = count;
					}
				} else if (bin == DELETED_BIN) {
					if (type.equals("ringing")) {
						deletedRingings = count;
					} else {
						deletedRecoveries = count;
					}
				} else {
					throw new IllegalStateException("Unknown bin type: " + bin);
				}
			}
			return new BinCounts(waitingRingings, waitingRecoveries, trashRingings, trashRecoveries, deletedRingings, deletedRecoveries);
		} finally {
			Utils.close(p, rs, con);
		}
	}

	private static final String DELETE_FROM_BIN_SQL = " DELETE FROM bin WHERE event_id = ? ";

	@Override
	public void deleteFromBin(String eventId) throws Exception {
		TransactionConnection con = null;
		PreparedStatement p = null;
		try {
			con = getConnection();
			con.startTransaction();
			p = con.prepareStatement(DELETE_FROM_BIN_SQL);
			p.setString(1, eventId);
			int i = p.executeUpdate();
			if (i != 1) throw new IllegalStateException("Delete from bin deleted " + i + " rows instead of 1");
			con.commitTransaction();
			binCountCache.getForceReload();
		} finally {
			Utils.close(p, con);
		}
	}

	@Override
	public TransactionConnection openConnection() throws SQLException {
		return getConnection();
	}

	@Override
	public SearchResult searchResources(Row searchParams, String tableName) {
		return searchResources(searchParams, tableName, null);
	}

	@Override
	public SearchResult searchResources(Row searchParams, String tableName, String filterClause) {
		List<Field> tableFields = getRowStructure().getTableFields(tableName);
		StringBuilder sql = new StringBuilder("SELECT ");
		for (Field f : tableFields) {
			if (f.getName().endsWith(".rowid")) {
				sql.append("rowid").append(",");
			} else {
				sql.append(f.getName()).append(",");
			}
		}
		Utils.removeLastChar(sql);
		sql.append(" FROM ").append(tableName).append(" WHERE 1=1 ");
		List<String> values = new ArrayList<>();
		for (Field f : tableFields) {
			if (f.getName().endsWith(".rowid")) continue;
			Column c = searchParams.get(f.getName());
			if (!c.hasValue()) continue;
			if (c.getField().isNumeric() && c.getValue().contains("/")) {
				sql.append(" AND ").append(f.getName()).append(" BETWEEN ? AND ? ");
				String[] parts = c.getValue().split(Pattern.quote("/"));
				values.add(parts[0].replace(",", "."));
				values.add(parts[1].replace(",", "."));
			} else if (c.getField().isDate()) {
				DateValue dateValue = DateUtils.convertToDateValue(c.getValue());
				if (dateValue.hasEmptyFields() || c.getValue().contains("/")) {
					dateFieldSearchSQL(sql, values, f, dateValue.getYear(), "year");
					dateFieldSearchSQL(sql, values, f, dateValue.getMonth(), "month");
					dateFieldSearchSQL(sql, values, f, dateValue.getDay(), "day");
				} else {
					sql.append(" AND TRUNC(").append(f.getName()).append(") = ? ");
					values.add(DATE_VALUE + c.getValue());
				}
			} else if (c.getValue().contains("|")) {
				sql.append(" AND UPPER(").append(f.getName()).append(") IN ( ");
				for (String value : c.getSelectedValues()) {
					sql.append("?,");
					values.add(value);
				}
				Utils.removeLastChar(sql);
				sql.append(" ) ");
			} else {
				sql.append(" AND UPPER(").append(f.getName()).append(") LIKE ? ");
				values.add(c.getValue().toUpperCase());
			}
		}

		if (given(filterClause)) {
			sql.append(" AND ").append(filterClause);
		}

		sql.append(" ORDER BY ");
		for (Field f : tableFields) {
			if (f.getName().endsWith(".rowid")) continue;
			sql.append(f.getName()).append(",");
		}
		Utils.removeLastChar(sql);

		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			List<Row> results = new ArrayList<>();
			con = getConnection();
			p = con.prepareStatement(sql.toString());
			int i = 1;
			for (String value : values) {
				if (value.startsWith(DATE_VALUE)) {
					value = value.replace(DATE_VALUE, "");
					p.setDate(i++, toDate(value));
				} else {
					p.setString(i++, value);
				}
			}
			rs = p.executeQuery();
			while (rs.next()) {
				Row row = newRow();
				resultSetToRow(row, rs, tableFields);
				results.add(row);
			}
			return new SearchResult(results);
		} catch (Exception e) {
			return new SearchResult(e.getMessage());
		} finally {
			Utils.close(p, rs, con);
		}
	}

	private void dateFieldSearchSQL(StringBuilder sql, List<String> values, Field f, String datePart, String sqlDateFormat) {
		if (!given(datePart)) return;
		if (datePart.contains("/")) {
			sql.append(" AND extract(").append(sqlDateFormat).append(" from ").append(f.getName()).append(") BETWEEN ? AND ? ");
			String[] parts = datePart.split(Pattern.quote("/"));
			values.add(removeFrontZero(parts[0]));
			values.add(removeFrontZero(parts[1]));
		} else {
			sql.append(" AND extract(").append(sqlDateFormat).append(" from ").append(f.getName()).append(") = ? ");
			values.add(removeFrontZero(datePart));
		}
	}

	private String removeFrontZero(String datePart) {
		while (datePart.startsWith("0")) {
			datePart = datePart.substring(1);
		}
		return datePart;
	}

	@Override
	public TipuAPIClient getTipuApiClient() throws Exception {
		return new TipuAPIClientImple(config);
	}

	@Override
	public void refreshRowStructure() {
		synchronized (LOCK) {
			rowStructure = null;
			rowStructure = initRowStructure();
		}
	}

	@Override
	public Row getResourceByRowId(String rowid, String tableName) throws Exception {
		Row row = newRow();
		Column rowidColumn = row.get(tableName+".rowid");
		rowidColumn.setValue(rowid);
		fillResourceByIdFields(row, rowidColumn);
		return row;
	}

	@Override
	public void refreshValidations() throws Exception {
		HttpClientService client = null;
		try {
			client = getTipuApiAuthenenticatedClient();
			JSONObject response = client.contentAsJson(new HttpGet(tipuApiURI() +"/ringing/force-reload"));
			if (!response.getBoolean("success")) {
				throw new Exception(response.getArray("errors").toString());
			}
		} finally {
			if (client != null) client.close();
		}
	}

	@Override
	public Row getEventById(String id) {
		Row searchParam = newRow();
		searchParam.get("event.id").setValue(id);
		SearchResult result = searchEvent(searchParam);
		if (!result.wasSuccessful()) throw new IllegalStateException(id + ": " + result.getErrorMessage());
		if (result.getResults().isEmpty()) throw new IllegalStateException("No event found with id " + id);
		Row row = result.getResults().get(0);
		return row;
	}

	@Override
	public ErrorReporter getErrorReporter() {
		return errorReporter;
	}

	@Override
	public Row toWhomDistributed(Ring ring) throws SQLException {
		TransactionConnection con = null;
		PreparedStatement p =  null;
		ResultSet rs = null;
		try {
			con = getConnection();
			p = con.prepareStatement("" +
					" SELECT distributedTo, ringStart, ringEnd FROM distributed " +
					" WHERE ringStart LIKE ? " +
					" AND to_number(substr(ringStart,3)) <= ? AND to_number(substr(ringEnd,3)) >= ? "
					);
			String series = ring.getSeries();
			if (series.length() == 1) {
				series += " ";
			}
			p.setString(1, series+"%");
			p.setInt(2, ring.getNumbers());
			p.setInt(3, ring.getNumbers());
			rs = p.executeQuery();
			if (!rs.next()) return null;
			Row row = newRow();
			row.get("distributed.distributedTo").setValue(rs.getString(1));
			row.get("distributed.ringStart").setValue(rs.getString(2));
			row.get("distributed.ringEnd").setValue(rs.getString(3));
			return row;
		} finally {
			Utils.close(p, rs, con);
		}
	}

	@Override
	public List<Attachment> getAttachments(String eventId) throws SQLException {
		List<Attachment> attachments = new ArrayList<>();
		TransactionConnection con = null;
		PreparedStatement p =  null;
		ResultSet rs = null;
		try {
			con = getConnection();
			p = con.prepareStatement(" SELECT id, filename, mime_type, data FROM eventAttachment WHERE event_id = ? ");
			p.setString(1, eventId);
			rs = p.executeQuery();
			while (rs.next()) {
				attachments.add(new Attachment(eventId, rs.getString(2), rs.getString(3), null).setFileId(rs.getString(1)));
			}
		} finally {
			Utils.close(p, rs, con);
		}
		return attachments;
	}

	@Override
	public Attachment getAttachment(String fileId) throws SQLException {
		TransactionConnection con = null;
		PreparedStatement p =  null;
		ResultSet rs = null;
		try {
			con = getConnection();
			p = con.prepareStatement(" SELECT event_id, filename, mime_type, data FROM eventAttachment WHERE id = ? ");
			p.setString(1, fileId);
			rs = p.executeQuery();
			if (rs.next()) {
				return new Attachment(rs.getString(1), rs.getString(2), rs.getString(3), rs.getBytes(4)).setFileId(fileId);
			}
			return null;
		} finally {
			Utils.close(p, rs, con);
		}
	}

	@Override
	public void addAttachment(Attachment attachment) throws SQLException {
		TransactionConnection con = null;
		PreparedStatement p =  null;
		try {
			con = getConnection();
			con.startTransaction();
			p = con.prepareStatement(" INSERT INTO eventAttachment (id, event_id, filename, mime_type, data) values (?,?,?,?,?) ");
			p.setString(1, getNextAttachmentId(con));
			p.setString(2, attachment.getEventId());
			p.setString(3, attachment.getFileName());
			p.setString(4, attachment.getMimeType());
			p.setBytes(5, attachment.getData());
			int i = p.executeUpdate();
			if (i != 1) throw new IllegalStateException("Inserted "+ i +" rows instead of 1");
			con.commitTransaction();
		} finally {
			Utils.close(p, con);
		}
	}

	private static final String GET_NEXT_ATTACHMENT_ID_SQL = "SELECT attachment_id_seq.NEXTVAL FROM dual";

	private String getNextAttachmentId(TransactionConnection con) throws SQLException {
		PreparedStatement p = null;
		ResultSet rs =  null;
		try {
			p = con.prepareStatement(GET_NEXT_ATTACHMENT_ID_SQL);
			rs = p.executeQuery();
			rs.next();
			return rs.getString(1);
		} finally {
			Utils.close(p, rs);
		}
	}

	@Override
	public void deleteAttachment(Attachment attachment) throws SQLException {
		TransactionConnection con = null;
		PreparedStatement p =  null;
		try {
			con = getConnection();
			con.startTransaction();
			p = con.prepareStatement(" DELETE FROM eventAttachment WHERE id = ? ");
			p.setString(1, attachment.getFileId());
			int i = p.executeUpdate();
			if (i != 1) throw new IllegalStateException("Deleted "+ i +" rows instead of 1");
			con.commitTransaction();
		} finally {
			Utils.close(p, con);
		}
	}

	@Override
	public Set<String> getRingerSynonyms(String ringerId) throws SQLException {
		Set<String> synonyms = new LinkedHashSet<>();
		TransactionConnection con = null;
		PreparedStatement p =  null;
		ResultSet rs = null;
		try {
			con = getConnection();
			con.startTransaction();
			p = con.prepareStatement(" SELECT synonymId FROM ringerObserverSynonyms WHERE ringerId = ? ORDER BY synonymId ");
			p.setString(1, ringerId);
			rs = p.executeQuery();
			while (rs.next()) {
				synonyms.add(rs.getString(1));
			}
			con.commitTransaction();
		} finally {
			Utils.close(p, con);
		}
		return synonyms;
	}

	@Override
	public void updateRingerSynonyms(String ringerId, Set<String> synonyms) throws Exception {
		Set<String> existingSynonyms = getRingerSynonyms(ringerId);

		if (similiar(synonyms, existingSynonyms)) {
			return;
		}

		TransactionConnection con = null;
		PreparedStatement p =  null;
		try {
			con = getConnection();
			con.startTransaction();
			p = con.prepareStatement(" DELETE FROM ringerObserverSynonyms WHERE ringerId = ? ");
			p.setString(1, ringerId);
			p.executeUpdate();

			for (String existing : existingSynonyms) {
				logger.log("DELETE from ringerObserverSynonyms: { ringerId: "+ringerId+" } " + "{ synonymId: "+existing+" }");
			}
			p.close();
			p = con.prepareStatement("INSERT INTO ringerObserverSynonyms (ringerId, synonymId) VALUES (?, ?) ");
			p.setString(1, ringerId);
			for (String synonym: synonyms) {
				p.setString(2, synonym);
				int i = p.executeUpdate();
				if (i != 1) throw new IllegalStateException("Insterted " + i + " rows instead of 1.");
				logger.log("INSERT to ringerObserverSynonyms: { ringerId: "+ringerId+" } " + "{ synonymId: "+synonym+" }");
			}
			con.commitTransaction();
			logger.commit(Utils.generateGUID(), userId);
		} finally {
			Utils.close(p, con);
		}
	}

	private boolean similiar(Set<String> synonyms, Set<String> existingSynonyms) {
		if (synonyms.size() != existingSynonyms.size()) return false;
		for (String s : synonyms) {
			if (!existingSynonyms.contains(s)) return false;
		}
		return true;
	}

	private static final SingleObjectCache<WarehouseQueueInfo> WAREHOUSE_QUEUE_INFO_CACHE = new SingleObjectCache<>(new SingleObjectCache.CacheLoader<WarehouseQueueInfo>() {

		@Override
		public WarehouseQueueInfo load() {
			TransactionConnection con = null;
			PreparedStatement p =  null;
			ResultSet rs = null;
			try {
				con = getConnection();
				p = con.prepareStatement("" +
						" SELECT attempted, count(1) " +
						" FROM warehouse_queue " +
						" WHERE etl_success = 0 " +
						" GROUP BY attempted ");
				rs = p.executeQuery();
				WarehouseQueueInfo info = new WarehouseQueueInfo();
				int unattemptedCount = 0;
				while (rs.next()) {
					boolean attempted = rs.getInt(1) > 0;
					if (attempted) {
						info.setHasErrors();
					} else {
						unattemptedCount += rs.getInt(2);
					}
				}
				info.setSize(unattemptedCount);
				return info;
			} catch (SQLException e) {
				errorReporter.report("Loading WarehouseQueue info", e);
				return new WarehouseQueueInfo().setHasErrors();
			} finally {
				Utils.close(p, rs, con);
			}
		}}, 1, TimeUnit.MINUTES);

	@Override
	public WarehouseQueueInfo getWarehouseQueueInfo() throws SQLException {
		return WAREHOUSE_QUEUE_INFO_CACHE.get();
	}

}
