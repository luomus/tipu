package fi.luomus.rengastus.dao;

import java.util.Collection;

import fi.luomus.rengastus.model.LoydosForm;

public interface LoydosDAO {

	Collection<LoydosForm> getAll() throws Exception;

	LoydosForm getOne(String id) throws Exception;

	void delete(String id) throws Exception;

	void receive(String id) throws Exception;

	void temporarilyReceive(String id) throws Exception;

}
