package fi.luomus.rengastus.dao;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import fi.luomus.rengastus.model.Row;

public class BinRowParser {

	public static void parseRowFromBinData(Row row, String data) {
		Map<String, String> parsedData = parse(data);
		for (Map.Entry<String, String> e : parsedData.entrySet()) {
			if (row.hasField(e.getKey())) {
				row.get(e.getKey()).setValue(e.getValue());
			}
		}
	}

	public static Map<String, String> parse(String data) {
		Map<String, String> parsedData = new HashMap<>();
		// { event.id: 18471586 }  { event.type: ringing }  { event.measurer: eli siis (ei haittaa) mitkään, kummat = merkit }
		boolean first = true;
		for (String part : data.split(Pattern.quote("{"))) {
			if (first) { first = false; continue; }
			part = part.replace("}", "").trim();
			int i = part.indexOf(":");
			String fieldName = part.substring(0, i);
			String value = part.substring(i+1, part.length()).trim();
			parsedData.put(fieldName, value);
		}
		return parsedData;
	}

}
