package fi.luomus.rengastus.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Field;

public class MassUpdateBatch {

	private final String tableName;
	private final List<Field> idColumns;
	private final List<Field> updateColumns;
	private final List<MassUpdateRow> rows = new ArrayList<>();

	public static class MassUpdateRow {
		private final List<Column> idValues;
		private final List<Column> updateValues;
		private MassUpdateRow(List<Column> idValues, List<Column> updateValues) {
			this.idValues = idValues;
			this.updateValues = updateValues;
		}
		public List<Column> getIdValues() {
			return Collections.unmodifiableList(idValues);
		}
		public List<Column> getUpdateValues() {
			return Collections.unmodifiableList(updateValues);
		}
	}

	public MassUpdateBatch(String tableName, List<Field> idColumns, List<Field> updateColumns) {
		this.tableName = tableName;
		this.idColumns = idColumns;
		this.updateColumns = updateColumns;
		if (idColumns.isEmpty()) throw new IllegalArgumentException("Must have at least one id column");
		if (updateColumns.isEmpty()) throw new IllegalArgumentException("Must have at least one update column");
		for (Field idColumn : idColumns) {
			for (Field updateColumn : updateColumns) {
				if (idColumn.getName().equals(updateColumn.getName())) throw new IllegalArgumentException("Must not update id columns: " + idColumn.getName());
			}
		}
	}

	public MassUpdateBatch add(List<Column> idValues, List<Column> updateValues) {
		rows.add(new MassUpdateRow(idValues, updateValues));
		return this;
	}

	public List<Field> getIdColumns() {
		return Collections.unmodifiableList(idColumns);
	}

	public List<Field> getUpdateColumns() {
		return Collections.unmodifiableList(updateColumns);
	}

	public List<MassUpdateRow> getRows() {
		return Collections.unmodifiableList(rows);
	}

	public String getTableName() {
		return tableName;
	}


}
