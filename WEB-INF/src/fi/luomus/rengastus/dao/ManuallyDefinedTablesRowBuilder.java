package fi.luomus.rengastus.dao;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuAPIClientImple;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.rengastus.model.EnumerationsContainer;
import fi.luomus.rengastus.model.Field.Type;
import fi.luomus.rengastus.model.RowStructure;
import fi.luomus.rengastus.model.RowStructure.RowStructureBuilder;

public class ManuallyDefinedTablesRowBuilder {

	private static final String[] SERIES = ("" +
			"A,AA,AI,AQ,AT,B,BA,BB,BI,BQ,BT,BV,C,CA,CE,CH,CI,CQ,CR,CT,CX," +
			"D,D+,DI,DQ,DT,DX,E,EH,EI,EJ,F,FF,FL,GA,GS," +
			"H,HA,HB,HC,HD,HE,HF,HH,HJ,HK,HL,HM,HN,HP,HR,HT,HU,HV,HW,HY,HX," +
			"J,JL,K,KT,L,LL,M,ML,MM,MT,N,NL,P,PA,PC,PD,PT,R,RL,RT,S,SM,ST,SX," +
			"T,TL,U,UL,UX,V,W,VL,VR,X,Y,YL," +
			"AZ,AO,BZ,BO,CZ,CO,DZ,DO,EZ,HZ,KZ,KO,PZ,RZ,XZ").split(",");

	private final RowStructureBuilder builder;
	private final Config config;
	private TipuAPIClient client;

	public ManuallyDefinedTablesRowBuilder(RowStructureBuilder builder, Config config) {
		this.builder = builder;
		this.config = config;
	}

	public void add() throws Exception {
		try {
			client = new TipuAPIClientImple(config);
			addWithClient();
		} finally {
			if (client != null) {
				client.close();
			}
		}
	}

	private void addWithClient() throws Exception {

		// distributed
		builder.addGroupDesc("distributed.identifiers", "Renkaat");
		builder.addField("distributed", "identifiers", "ringStart", "Rengas-", "Rengasvälin alku", Type.VARCHAR, 9);
		builder.addField("distributed", "identifiers","ringEnd", "-rengas", "Rengasvälin loppu", Type.VARCHAR, 9);
		builder.addGroupDesc("distributed.additional", "Jakelu");
		builder.addField("distributed", "additional","distributedTo", "Kenelle", "Kenelle jaettu", Type.ENUMERATION, 0);
		builder.setTipuApiEnumName("distributed.distributedTo", TipuAPIClient.RINGERS);
		builder.addField("distributed", "additional", "distributedDate", "Jakopäivämäärä", "Jakopäivämäärä", Type.DATE, 0);
		builder.addField("distributed", "additional", "notes", "Kommentti", "", Type.VARCHAR, 4000);

		// municipality
		builder.addGroupDesc("municipality.common", "Perustiedot");
		builder.addField("municipality", "common", "id", "Lyhenne", "Kunnan lyhenne", Type.UNIQUE_ID_USER_GIVEN_VARCHAR, 7);
		builder.addField("municipality", "common", "name", "Nimi", "Nimi", Type.VARCHAR, 50);
		builder.addField("municipality", "common", "nameSwedish", "Nimi ruotsiksi", "Nimi ruotsiksi", Type.VARCHAR, 50);
		builder.addField("municipality", "common", "joinedTo", "Kuntaliitos", "Mihin kuntaan liitetty - itseensä jos ei mihinkään muuhun", Type.ENUMERATION, 0);
		builder.setTipuApiEnumName("municipality.joinedTo", TipuAPIClient.CURRENT_MUNICIPALITIES);
		builder.addGroupDesc("municipality.centerpoint", "Validointeja varten");
		builder.addField("municipality", "centerpoint", "centerPointDegreeLat", "Kekipiste - aste - lev", "", Type.INTEGER, 4);
		builder.addField("municipality", "centerpoint", "centerPointDegreeLon", "Kekipiste - aste - pit", "", Type.INTEGER, 4);
		builder.addField("municipality", "centerpoint", "centerPointUniformLat", "Kekipiste - yht - lev", "", Type.INTEGER, 7);
		builder.addField("municipality", "centerpoint", "centerPointUniformLon", "Kekipiste - yht - pit", "", Type.INTEGER, 7);
		builder.addField("municipality", "centerpoint", "centerPointDecimalLat", "Kekipiste - des - lev", "", Type.DECIMAL, 7);
		builder.addField("municipality", "centerpoint", "centerPointDecimalLon", "Kekipiste - des - pit", "", Type.DECIMAL, 7);
		builder.addField("municipality", "centerpoint", "radius", "Säde km", "Säde kilometreinä", Type.INTEGER, 4);
		builder.addGroupDesc("municipality.areas", "Hallinnolliset alueet");
		builder.addField("municipality", "areas", "euringProvince", "EURING-paikka", "", Type.ENUMERATION, 0);
		builder.addField("municipality", "areas", "elyCentre", "ELY-keskus", "", Type.ENUMERATION, 0);
		builder.addField("municipality", "areas", "finnishProvince", "Maakunta", "", Type.ENUMERATION, 0);
		builder.addField("municipality", "areas", "finnishOldCounty", "Vanha lääni", "", Type.ENUMERATION, 0);
		builder.addField("municipality", "areas", "lylArea", "LYL-alue", "", Type.ENUMERATION, 0);
		builder.addField("municipality", "areas", "halalbArea", "Merikotka suuralue", "", Type.ENUMERATION, 0);
		builder.setTipuApiEnumName("municipality.id", TipuAPIClient.MUNICIPALITIES);
		builder.setTipuApiEnumName("municipality.elyCentre", TipuAPIClient.ELY_CENTRES);
		builder.setTipuApiEnumName("municipality.finnishProvince", TipuAPIClient.PROVINCES);
		builder.setTipuApiEnumName("municipality.finnishOldCounty", TipuAPIClient.OLD_COUNTIES);
		builder.setTipuApiEnumName("municipality.lylArea", TipuAPIClient.LYL_AREAS);
		builder.setTipuApiEnumName("municipality.euringProvince", TipuAPIClient.EURING_PROVINCES);
		builder.setFieldEnumValue("municipality.halalbArea", "A", "Ahvenanmaa");
		builder.setFieldEnumValue("municipality.halalbArea", "M", "Merenkurkku");
		builder.setFieldEnumValue("municipality.halalbArea", "P", "Perämeri");
		builder.setFieldEnumValue("municipality.halalbArea", "U", "Länsi-Uusimaa");
		builder.setFieldEnumValue("municipality.halalbArea", "I", "Itä-Uusimaa");
		builder.setFieldEnumValue("municipality.halalbArea", "K", "Kymenlaakso");
		builder.setFieldEnumValue("municipality.halalbArea", "S", "Satakunta");
		builder.setFieldEnumValue("municipality.halalbArea", "N", "Länsi-Suomi, sisämaa");
		builder.setFieldEnumValue("municipality.halalbArea", "T", "Itä-Suomi, sisämaa");
		builder.setFieldEnumValue("municipality.halalbArea", "E", "Etelä-Lappi");
		builder.setFieldEnumValue("municipality.halalbArea", "L", "Keski-Lappi");
		builder.setFieldEnumValue("municipality.halalbArea", "H", "Pohjois-Lappi");
		builder.setFieldEnumValue("municipality.halalbArea", "R", "Varsinais-Suomi");

		// finnishOldCounty
		builder.addGroupDesc("finnishOldCounty.common", "Perustiedot");
		builder.addField("finnishOldCounty", "common", "id", "Numero", "Numero", Type.UNIQUE_ID_USER_GIVEN_INTEGER, 2);
		builder.addField("finnishOldCounty", "common", "name", "Nimi", "Nimi", Type.VARCHAR, 40);
		builder.addField("finnishOldCounty", "common", "nameSwedish", "Nimi ruotsi", "Nimi ruotsi", Type.VARCHAR, 40);
		builder.setTipuApiEnumName("finnishOldCounty.id", TipuAPIClient.OLD_COUNTIES);

		// species
		builder.addGroupDesc("species.common", "Perustiedot");
		builder.addField("species", "common", "id", "Lajin koodi", "Lajin koodi", Type.UNIQUE_ID_USER_GIVEN_VARCHAR, 7);
		builder.addField("species", "common", "speciesnumber", "Lajin numero", "Lajin numero", Type.INTEGER, 5);
		builder.addField("species", "common", "protectionStatus", "Suojelu", "", Type.ENUMERATION, 0);
		builder.setFieldEnumValue("species.protectionStatus", "", "Ei suojeltu");
		builder.setFieldEnumValue("species.protectionStatus", "1", "Suojeltu");
		builder.setFieldEnumValue("species.protectionStatus", "2", "Erityisesti suojeltu");
		builder.addField("species", "common", "allowedForReporting", "Käytössä", "Saako käyttää ilmoittamiseen esim Sulassa?", Type.ENUMERATION, 0);
		builder.setFieldEnumValue("species.allowedForReporting", "K", "Kyllä");
		builder.setFieldEnumValue("species.allowedForReporting", "E", "Ei");
		builder.addField("species", "common", "winterbird", "Talvilintu", "Saako käyttää ilmoittamiseen ruokintapaikkaseurannassa?", Type.ENUMERATION, 0);
		builder.setFieldEnumValue("species.winterbird", "K", "Kyllä");
		builder.setFieldEnumValue("species.winterbird", "E", "Ei");
		builder.addGroupDesc("species.names", "Nimet");
		builder.addField("species", "names", "scientificName", "Tieteellinen nimi", "", Type.VARCHAR, 80);
		builder.addField("species", "names", "vernacularNameFi", "Suomi", "", Type.VARCHAR, 80);
		builder.addField("species", "names", "vernacularNameSv", "Ruotsi", "", Type.VARCHAR, 80);
		builder.addField("species", "names", "vernacularNameEn", "Englanti", "", Type.VARCHAR, 80);
		builder.addGroupDesc("species.adultMeasurements", "Aikuinen");
		builder.addField("species", "adultMeasurements", "adultWeightMin", "Paino min.", "", Type.DECIMAL, 6);
		builder.addField("species", "adultMeasurements", "adultWeightMax", "Paino max.", "", Type.DECIMAL, 6);
		builder.addField("species", "adultMeasurements", "adultWingLengthMin", "Siipi min.", "", Type.DECIMAL, 6);
		builder.addField("species", "adultMeasurements", "adultWingLengthMax", "Siipi max.", "", Type.DECIMAL, 6);
		builder.addField("species", "adultMeasurements", "adultThirdFeatherMin", "3ks min.", "", Type.DECIMAL, 6);
		builder.addField("species", "adultMeasurements", "adultThirdFeatherMax", "3ks max.", "", Type.DECIMAL, 6);
		builder.addGroupDesc("species.youngMeasurements", "Nuori");
		builder.addField("species", "youngMeasurements", "youngWeightMin", "Paino min.", "", Type.DECIMAL, 6);
		builder.addField("species", "youngMeasurements", "youngWeightMax", "Paino max.", "", Type.DECIMAL, 6);
		builder.addField("species", "youngMeasurements", "youngWingLengthMin", "Siipi min.", "", Type.DECIMAL, 6);
		builder.addField("species", "youngMeasurements", "youngWingLengthMax", "Siipi  max.", "", Type.DECIMAL, 6);
		builder.addField("species", "youngMeasurements", "youngThirdFeatherMin", "3ks min.", "", Type.DECIMAL, 6);
		builder.addField("species", "youngMeasurements", "youngThirdFeatherMax", "3ks max.", "", Type.DECIMAL, 6);
		builder.setTipuApiEnumName("species.id", TipuAPIClient.SPECIES);

		// birdStation
		builder.addGroupDesc("birdStation.common", "Perustiedot");
		builder.addField("birdStation", "common", "id", "Lyhenne", "Lyhenne", Type.UNIQUE_ID_USER_GIVEN_VARCHAR, 6);
		builder.addField("birdStation", "common", "name", "Nimi", "", Type.VARCHAR, 40);
		builder.addField("birdStation", "common", "fakeRingerNumber", "Valerengastajanro", "Se epäaito rengastaja jolle tämän lintuaseman renkaat on jaettu", Type.ENUMERATION, 0);
		builder.setTipuApiEnumName("birdStation.fakeRingerNumber", TipuAPIClient.RINGERS);
		builder.addField("birdStation", "common", "municipality", "Kunta", "Minkä kunnan alueella lintuasema on", Type.ENUMERATION, 0);
		builder.setTipuApiEnumName("birdStation.municipality", TipuAPIClient.CURRENT_MUNICIPALITIES);
		builder.addField("birdStation", "common", "contactPersonName", "Yhteyshenkilön nimi", "", Type.VARCHAR, 20);
		builder.addField("birdStation", "common", "email", "Yhteyshenkilön email", "", Type.VARCHAR, 40);
		builder.addField("birdStation", "common", "language", "", "Kieli", Type.ENUMERATION, 0);
		builder.setFieldEnumValue("birdStation.language", "S", "Suomi");
		builder.setFieldEnumValue("birdStation.language", "R", "Ruotsi");
		builder.setFieldEnumValue("birdStation.language", "E", "Englanti");
		builder.addGroupDesc("birdStation.primaryContact", "Yhteystiedot");
		builder.addField("birdStation", "primaryContact", "address", "Katuosoite", "", Type.VARCHAR, 60);
		builder.addField("birdStation", "primaryContact", "postCode", "Postinumero", "", Type.VARCHAR, 5);
		builder.addField("birdStation", "primaryContact", "city", "Postitoimipaikka", "", Type.VARCHAR, 20);
		builder.addField("birdStation", "primaryContact", "phone", "Puhelin", "Puhelin", Type.VARCHAR, 20);
		builder.addGroupDesc("birdStation.secondaryContact", "Toissijaiset yhteystiedot");
		builder.addField("birdStation", "secondaryContact", "secondAddress", "Katuosoite", "", Type.VARCHAR, 60);
		builder.addField("birdStation", "secondaryContact", "secondPostCode", "Postinumero", "", Type.VARCHAR, 5);
		builder.addField("birdStation", "secondaryContact", "secondCity", "Postitoimipaikka", "", Type.VARCHAR, 20);
		builder.addField("birdStation", "secondaryContact", "secondPhone", "Puhelin", "", Type.VARCHAR, 20);
		builder.addGroupDesc("birdStation.centerpoint", "Validointeja varten");
		builder.addField("birdStation", "centerpoint", "centerPointDegreeLat", "Keskipiste - aste - lev", "", Type.INTEGER, 4);
		builder.addField("birdStation", "centerpoint", "centerPointDegreeLon", "Keskipiste - aste - pit", "", Type.INTEGER, 4);
		builder.addField("birdStation", "centerpoint", "centerPointUniformLat", "Keskipiste - yht - lev", "", Type.INTEGER, 7);
		builder.addField("birdStation", "centerpoint", "centerPointUniformLon", "Keskipiste - yht - pit", "", Type.INTEGER, 7);
		builder.addField("birdStation", "centerpoint", "centerPointDecimalLat", "Keskipiste - des - lev", "", Type.DECIMAL, 7);
		builder.addField("birdStation", "centerpoint", "centerPointDecimalLon", "Keskipiste - des - pit", "", Type.DECIMAL, 7);
		builder.addField("birdStation", "centerpoint", "radius", "Säde km", "", Type.INTEGER, 4);
		builder.setTipuApiEnumName("birdStation.id", TipuAPIClient.BIRD_STATIONS);

		// lylArea
		builder.addGroupDesc("lylArea.common", "Perustiedot");
		builder.addField("lylArea", "common", "id", "Numero", "", Type.UNIQUE_ID_USER_GIVEN_INTEGER, 3);
		builder.addField("lylArea", "common", "name", "Nimi", "", Type.VARCHAR, 70);
		builder.addField("lylArea", "common", "address", "Katuosoite", "", Type.VARCHAR, 40);
		builder.addField("lylArea", "common", "postCode", "Postikoodi", "", Type.VARCHAR, 5);
		builder.addField("lylArea", "common", "city", "Postitoimipaikka", "", Type.VARCHAR, 20);
		builder.addField("lylArea", "common", "phone", "Puhelin", "", Type.VARCHAR, 20);
		builder.setTipuApiEnumName("lylArea.id", TipuAPIClient.LYL_AREAS);

		// finnishProvince
		builder.addGroupDesc("finnishProvince.common", "Perustiedot");
		builder.addField("finnishProvince", "common", "id", "Numero", "", Type.UNIQUE_ID_USER_GIVEN_INTEGER, 3);
		builder.addField("finnishProvince", "common", "name", "Nimi", "", Type.VARCHAR, 70);
		builder.setTipuApiEnumName("finnishProvince.id", TipuAPIClient.PROVINCES);

		// layman
		builder.addGroupDesc("layman.common", "Perustiedot");
		builder.addField("layman", "common", "id", "Numero", "ID", Type.UNIQUE_ID_SEQUENCE_GIVEN, 6);
		builder.addField("layman", "common", "name", "Nimi", "Etu- ja sukunimi", Type.VARCHAR, 80);
		builder.addField("layman", "common", "language", "Kieli", "kieli", Type.ENUMERATION, 0);
		builder.addField("layman", "common", "email", "Email", "Email", Type.VARCHAR, 80);
		builder.addField("layman", "common", "phone", "Puhelin", "Puhelin", Type.VARCHAR, 20);
		builder.addField("layman", "common", "secondPhone", "Puhelin 2", "Puhelin 2", Type.VARCHAR, 20);
		builder.addField("layman", "common", "notes", "Kommentti", "Kommentti", Type.VARCHAR, 500);
		builder.addGroupDesc("layman.address", "Osoite");
		builder.addField("layman", "address", "addressLine1", "Osoite 1", "Rivi 1", Type.VARCHAR, 80);
		builder.addField("layman", "address", "addressLine2", "Osoite 2", "Rivi 2", Type.VARCHAR, 80);
		builder.addField("layman", "address", "addressLine3", "Osoite 3", "Rivi 3", Type.VARCHAR, 80);
		builder.addField("layman", "address", "addressLine4", "Osoite 4", "Rivi 4", Type.VARCHAR, 80);
		builder.addField("layman", "address", "country", "Maa", "Maa", Type.ENUMERATION, 0);
		builder.setTipuApiEnumName("layman.country", TipuAPIClient.COUNTRIES);
		builder.setFieldEnumValue("layman.language", "S", "Suomi");
		builder.setFieldEnumValue("layman.language", "R", "Ruotsi");
		builder.setFieldEnumValue("layman.language", "E", "Englanti");

		// country
		builder.addGroupDesc("country.common", "Perustiedot");
		builder.addField("country", "common", "id", "Lyhenne", "", Type.UNIQUE_ID_USER_GIVEN_VARCHAR, 2);
		builder.addField("country", "common", "name", "Nimi", "", Type.VARCHAR, 50);
		builder.addField("country", "common", "language", "Kieli", "", Type.ENUMERATION,0);
		builder.addField("country", "common", "notes", "", "", Type.VARCHAR, 500);
		builder.setTipuApiEnumName("country.id", TipuAPIClient.COUNTRIES);
		builder.setFieldEnumValue("country.language", "S", "Suomi");
		builder.setFieldEnumValue("country.language", "E", "Englanti");
		builder.setFieldEnumValue("country.language", "R", "Ruotsi");

		// ring
		builder.addGroupDesc("ring.identifiers", "Rengasväli");
		builder.addField("ring", "identifiers", "ringStart", "Rengas-", "Rengasvälin alku", Type.VARCHAR, 9);
		builder.addField("ring", "identifiers", "ringEnd", "-rengas", "Rengasvälin loppu", Type.VARCHAR, 9);
		builder.addGroupDesc("ring.additional", "Lisätiedot");
		builder.addField("ring", "additional", "diameter", "Läpimitta", "Renkaan läpimitta", Type.DECIMAL, 4);
		builder.addField("ring", "additional", "height", "Korkeus", "Renkaan korkeus", Type.DECIMAL, 4);
		builder.addField("ring", "additional", "material", "Materiaali", "Renkaan materiaali", Type.ENUMERATION, 0);
		builder.setFieldEnumValue("ring.material", "A", "Alumiini");
		builder.setFieldEnumValue("ring.material", "T", "Teräs");
		builder.setFieldEnumValue("ring.material", "S", "S?");
		builder.setFieldEnumValue("ring.material", "M", "M?");

		// ringer
		builder.addGroupDesc("ringer.common", "Perustiedot");
		builder.addField("ringer", "common", "id", "Rengastajanumero", "Rengastajanumero", Type.UNIQUE_ID_USER_GIVEN_INTEGER, 5);
		builder.addField("ringer", "common", "lastName", "Sukunimi", "Sukunimi", Type.VARCHAR, 40);
		builder.addField("ringer", "common", "firstName", "Etunimi", "Etunimi", Type.VARCHAR, 20);
		builder.addField("ringer", "common", "email", "Sähköposti", "Sähköpostiosoite", Type.VARCHAR, 80);
		builder.addField("ringer", "common", "birthYear", "Syntymävuosi", "Syntymävuosi", Type.YEAR, 4);
		builder.addField("ringer", "common", "gender", "Sukupuoli", "Sukupuoli", Type.ENUMERATION, 0);
		builder.addField("ringer", "common", "language", "Kieli", "Kieli", Type.ENUMERATION, 0);
		builder.addField("ringer", "common", "state", "Tila/tyyppi", "Rengastajan tila tai tyyppi", Type.ENUMERATION, 0);
		builder.addField("ringer", "common", "letters", "Kirjeet", "Lähetetäänkö kirjeitä ja miten", Type.ENUMERATION, 0);
		builder.addField("ringer", "common", "yearbook", "Vuosikirja", "Haluaako rengastajan vuosikirjan", Type.ENUMERATION, 0);
		builder.addField("ringer", "common", "startDate", "Aloituspvm", "Aloituspäivämäärä", Type.DATE, 0);
		builder.addField("ringer", "common", "endDate", "Lopetuspvm", "Lopetuspäivämäärä", Type.DATE, 0);
		builder.addGroupDesc("ringer.permissions", "Luvat");
		builder.addField("ringer", "permissions", "permissionYearStamp", "Vuosileima", "Vuosileima", Type.YEAR, 4);
		builder.addField("ringer", "permissions", "permissionTypes", "Lupatyypit", "Lupatyypit", Type.VARCHAR, 6);
		builder.addField("ringer", "permissions", "permissionNotes", "Lupatyypin kommentti", "Lupatyypin kommentti", Type.VARCHAR, 1024);
		builder.addField("ringer", "permissions", "completedTestsNotes", "Tentit", "Suoritetut tentit", Type.VARCHAR, 500);
		builder.addField("ringer", "permissions", "disclosePermission", "Birdlife -luovutus", "", Type.ENUMERATION, 0);
		builder.addField("ringer", "permissions", "resultServicePermission", "Tulospalvelulupa", "Lupa näyttää tulospalvelussa (rengastus.helsinki.fi/tuloksia ja vastaavat) nimi julkisesti", Type.ENUMERATION, 0);
		builder.addField("ringer", "permissions", "FinBIFPermission", "Laji.fi lupa", "Lupa näyttää Lajitietokeskuksessa nimi julkisesti", Type.ENUMERATION, 0);
		builder.addGroupDesc("ringer.seekingPermissions", "Luvan hakeminen");
		builder.addField("ringer", "seekingPermissions", "signedCommitmentReceived", "Sitoumus", "Allekirjoitettu sitoomus vastaanotettu", Type.ENUMERATION, 0);
		builder.addField("ringer", "seekingPermissions", "signedPowerOfAttorneyReceived", "Valtakirja", "Allekirjoitettu valtakirja vastaanotettu", Type.ENUMERATION, 0);
		builder.addField("ringer", "seekingPermissions", "previousYearDataReported", "Edellisen vuoden tiedot ilmoitettu", "", Type.ENUMERATION, 0);
		builder.addGroupDesc("ringer.contact", "Yhteystiedot");
		builder.addField("ringer", "contact", "address", "Katuosoite", "Katuosoite", Type.VARCHAR, 40);
		builder.addField("ringer", "contact", "postCode", "Postinumero", "Postinumero", Type.VARCHAR, 15);
		builder.addField("ringer", "contact", "city", "Postitoimipaikka", "Postitoimipaikka", Type.VARCHAR, 20);
		builder.addField("ringer", "contact", "mobilePhone", "Kännykkä", "Matkapuhelinnumero", Type.VARCHAR, 20);
		builder.addField("ringer", "contact", "homePhone", "Kotipuhelin", "Kotipuhelin", Type.VARCHAR, 20);
		builder.addField("ringer", "contact", "workPhone", "Työpuhelin", "Työpuhelin", Type.VARCHAR, 20);
		builder.addField("ringer", "contact", "hatikkaUsername", "Hatikan käyttäjätunnus", "Poistumassa käytöstä", Type.VARCHAR, 80);
		builder.addGroupDesc("ringer.secondaryContact", "Toissijaiset yhteystiedot");
		builder.addField("ringer", "secondaryContact", "secondAddress", "2. Katuosoite", "Toissijainen katuosoite", Type.VARCHAR, 40);
		builder.addField("ringer", "secondaryContact", "secondPostCode", "2. Postinumero", "Toissijainen postinumero", Type.VARCHAR, 15);
		builder.addField("ringer", "secondaryContact", "secondCity", "2. Postitoimipaikka", "Toissijainen postitoimipaikka", Type.VARCHAR, 20);
		builder.addGroupDesc("ringer.notes", "Kommentti");
		builder.addField("ringer", "notes", "notes", "Kommentti", "Kommentti", Type.VARCHAR, 500);
		builder.addField("ringer", "dataHistory", "modifiedByRinger", "Oma muokkaus", "Pvm jolloin rengastaja viimeksi itse muokannut tietojaan", Type.DATE, 0);
		builder.addField("ringer", "dataHistory", "lastLogin", "Viim. sisäänkirj.", "Pvm jona rengastaja viimeksi kirjautunut Lintuvaaraan", Type.DATE, 0);

		builder.setTipuApiEnumName("ringer.id", TipuAPIClient.RINGERS);
		builder.setFieldEnumValue("ringer.state", "0", "Aktiivinen rengastaja");
		builder.setFieldEnumValue("ringer.state", "1", "Lopettanut rengastaja");
		builder.setFieldEnumValue("ringer.state", "2", "Lopettanut rengastaja - jotain puuttuu");
		builder.setFieldEnumValue("ringer.state", "3", "Aktiivinen lepakkorengastaja (vain lepakoita)");
		builder.setFieldEnumValue("ringer.state", "9", "Massarengastaja");
		builder.setFieldEnumValue("ringer.state", "8", "Linnustonseurantoihin osallistuva havainnoija (ei rengastaja)");
		builder.setFieldEnumValue("ringer.language", "S", "Suomi");
		builder.setFieldEnumValue("ringer.language", "R", "Ruotsi");
		builder.setFieldEnumValue("ringer.language", "E", "Englanti");
		builder.setFieldEnumValue("ringer.gender", "M", "Mies");
		builder.setFieldEnumValue("ringer.gender", "F", "Nainen");
		builder.setFieldEnumValue("ringer.resultServicePermission", "1", "Sallii");
		builder.setFieldEnumValue("ringer.resultServicePermission", "0", "Ei salli");
		builder.setFieldEnumValue("ringer.FinBIFPermission", "1", "Sallii");
		builder.setFieldEnumValue("ringer.FinBIFPermission", "0", "Ei salli");
		builder.setFieldEnumValue("ringer.disclosePermission", "B", "Luovutetaan osoitteita Birdlifelle");
		builder.setFieldEnumValue("ringer.letters", "0", "Paperikirjeet");
		builder.setFieldEnumValue("ringer.letters", "1", "Sähköpostitse");
		builder.setFieldEnumValue("ringer.signedCommitmentReceived", "1", "Kyllä - Allekirjoitettu sitoumus saatu");
		builder.setFieldEnumValue("ringer.signedCommitmentReceived", "0", "Ei - Allekirjoitettua sitoumusta ei ole saatu");
		builder.setFieldEnumValue("ringer.signedPowerOfAttorneyReceived", "1", "Kyllä - Allekirjoitettu valtakirja saatu");
		builder.setFieldEnumValue("ringer.signedPowerOfAttorneyReceived", "0", "Ei - Allekirjoitettua valtakirjaa ei ole saatu");
		builder.setFieldEnumValue("ringer.previousYearDataReported", "1", "Kyllä");
		builder.setFieldEnumValue("ringer.previousYearDataReported", "0", "Ei");
		builder.setFieldEnumValue("ringer.yearbook", "S", "Kyllä - Sähköisenä");
		builder.setFieldEnumValue("ringer.yearbook", "P", "Kyllä - Paperisena");
		builder.setFieldEnumValue("ringer.yearbook", "E", "Ei");

		// scheme
		builder.addGroupDesc("scheme.common", "Perustiedot");
		builder.addField("scheme", "common", "id", "Lyhenne", "", Type.UNIQUE_ID_USER_GIVEN_VARCHAR, 3);
		builder.addField("scheme", "common", "name", "Nimi", "", Type.VARCHAR, 70);
		builder.addField("scheme", "common", "country", "Maa", "", Type.ENUMERATION, 0);
		builder.setTipuApiEnumName("scheme.country", TipuAPIClient.COUNTRIES);
		builder.addField("scheme", "common", "notes", "Kommentti", "", Type.VARCHAR, 500);
		builder.addGroupDesc("scheme.contact", "Yhteystiedot");
		builder.addField("scheme", "contact", "email", "Email", "", Type.VARCHAR, 55);
		builder.addField("scheme", "contact", "addressLine1", "Osoite 1", "", Type.VARCHAR, 60);
		builder.addField("scheme", "contact", "addressLine2", "Osoite 2", "", Type.VARCHAR, 60);
		builder.addField("scheme", "contact", "addressLine3", "Osoite 3", "", Type.VARCHAR, 60);
		builder.addField("scheme", "contact", "addressLine4", "Osoite 4", "", Type.VARCHAR, 60);
		builder.addField("scheme", "contact", "phone", "Puhelin", "", Type.VARCHAR, 20);
		builder.setTipuApiEnumName("scheme.id", TipuAPIClient.SCHEMES);

		// series
		builder.addGroupDesc("series.common", "Tiedot");
		builder.addField("series", "common", "series", "Rengassarja", "Rengassarja", Type.ENUMERATION, 0);
		for (String series : SERIES) {
			builder.setFieldEnumValue("series.series", series, series);
		}
		builder.addField("series", "common", "species", "Laji", "Laji", Type.ENUMERATION, 0);
		builder.setTipuApiEnumName("series.species", TipuAPIClient.SPECIES);
		builder.addField("series", "common", "warning", "Varoitus", "Renkaan kätöstä lajille varoitetaan", Type.ENUMERATION, 0);
		builder.setFieldEnumValue("series.warning", "", "Sallittu");
		builder.setFieldEnumValue("series.warning", "H", "Hyväksyttävä varoituksella");

		// codes
		builder.addGroupDesc("codes.identifiers", "Muuttuja");
		builder.addField("codes", "identifiers", "code", "Muuttujan numero", "Muuttujan numero", Type.ENUMERATION, 4);
		builder.setTipuApiEnumName("codes.code", TipuAPIClient.DESCRIPTIONS_OF_CODES);
		builder.addField("codes", "identifiers", "variable", "Muuttujan arvo", "Muuttujan arvo", Type.VARCHAR, 50);
		builder.addField("codes", "identifiers", "language", "Muuttujan kuvauksen kieli", "Muuttujan kuvauksen kieli", Type.ENUMERATION, 0);
		builder.setFieldEnumValue("codes.language", "S", "Suomi");
		builder.setFieldEnumValue("codes.language", "R", "Ruotsi");
		builder.setFieldEnumValue("codes.language", "E", "Englanti");
		builder.setFieldEnumValue("codes.language", "L", "Latina");
		builder.addGroupDesc("codes.descriptions", "Kuvaukset");
		builder.addField("codes", "descriptions", "description", "Lyhyt kuvaus", "Tämä on annettava. Pidempi kuvaus ei ole pakollinen.", Type.VARCHAR, 500);
		builder.addField("codes", "descriptions", "detailedDescription", "Pidempi kuvaus", "Ei pakollinen, pidempänä kuvauksena käytetään lyhyttä, jos pitkää ei ole erikseen annettu.", Type.VARCHAR, 500);
		builder.addField("codes", "descriptions", "metadata", "Metadata", "Asettaa koodin järjestyksen, ym filteröintiä varten", Type.VARCHAR, 40);

		// euringProvince
		builder.addGroupDesc("euringProvince.common", "Perustiedot");
		builder.addField("euringProvince", "common", "id", "Lyhenne", "", Type.UNIQUE_ID_USER_GIVEN_VARCHAR, 4);
		builder.addField("euringProvince", "common", "name", "Nimi", "", Type.VARCHAR, 40);
		builder.addField("euringProvince", "common", "country", "Maa", "", Type.ENUMERATION, 0);
		builder.setTipuApiEnumName("euringProvince.country", TipuAPIClient.COUNTRIES);
		builder.addGroupDesc("euringProvince.centerpoint", "Validointeja varten");
		builder.addField("euringProvince", "centerpoint", "centerPointDegreeLat", "Keskipiste - aste - lev", "", Type.INTEGER, 5);
		builder.addField("euringProvince", "centerpoint", "centerPointDegreeLon", "Keskipiste - aste - pit", "", Type.INTEGER, 5);
		builder.addField("euringProvince", "centerpoint", "centerPointDecimalLat", "Keskipiste - des - lev", "", Type.DECIMAL, 7);
		builder.addField("euringProvince", "centerpoint", "centerPointDecimalLon", "Keskipiste - des - pit", "", Type.DECIMAL, 7);
		builder.addField("euringProvince", "centerpoint", "radius", "Säde km", "", Type.INTEGER, 4);
		builder.setTipuApiEnumName("euringProvince.id", TipuAPIClient.EURING_PROVINCES);

		// fieldReadable
		builder.addGroupDesc("fieldReadable.identifiers", "Yksilöivät tiedot");
		builder.addField("fieldReadable", "identifiers", "code", "Merkit", "", Type.VARCHAR, 10);
		builder.addField("fieldReadable", "identifiers", "mainColor", "Taustan väri", "", Type.ENUMERATION, 0);
		builder.addField("fieldReadable", "identifiers", "forSpecies", "Mille lajille", "", Type.ENUMERATION, 0);
		builder.addGroupDesc("fieldReadable.distribution", "Jakotiedot");
		builder.addField("fieldReadable", "distribution", "distributedTo", "Kenelle jaettu", "", Type.ENUMERATION, 0);
		builder.addField("fieldReadable", "distribution", "distributedDate", "Jakopäivämäärä", "", Type.DATE, 0);
		builder.addField("fieldReadable", "distribution", "doubleDistributedCount", "Tuplajako (jakojen lkm)", "", Type.INTEGER, 1);
		builder.addGroupDesc("fieldReadable.additional", "Muut");
		builder.addField("fieldReadable", "additional", "attachmentPoint", "Kiinnityskohta", "", Type.ENUMERATION, 0);
		builder.addField("fieldReadable", "additional", "codeColor", "Merkkien väri", "", Type.ENUMERATION, 0);
		builder.addField("fieldReadable", "additional", "codeAlign", "Merkkien asento", "", Type.ENUMERATION, 0);
		builder.addField("fieldReadable", "additional", "codeRepeatedCount", "Monta kertaa koodi", "Kuinka monta kertaa koodi on väritunnisteessa", Type.INTEGER, 1);
		builder.addField("fieldReadable", "additional", "heightInMillimeters", "Korkeus (mm)", "", Type.DECIMAL, 5);
		builder.addField("fieldReadable", "additional", "diameterInMillimeters", "Halkaisija (mm)", "", Type.DECIMAL, 5);
		builder.addField("fieldReadable", "additional", "material", "Materiaali", "", Type.VARCHAR, 40);
		builder.addField("fieldReadable", "additional", "otherInfo", "Muut", "", Type.VARCHAR, 80);
		builder.addField("fieldReadable", "additional", "font", "Fontti", "", Type.VARCHAR, 80);
		builder.addField("fieldReadable", "additional", "checksumCharacterFormula", "Tarkistemerkin kaava", "", Type.VARCHAR, 80);
		builder.addField("fieldReadable", "additional", "notes", "Kommentti", "", Type.VARCHAR, 500);

		builder.setTipuApiEnumName("fieldReadable.forSpecies", TipuAPIClient.SELECTABLE_SPECIES);
		builder.setTipuApiEnumName("fieldReadable.distributedTo", TipuAPIClient.RINGERS);
		code("fieldReadable.mainColor", 303);
		code("fieldReadable.codeColor", 305);
		code("fieldReadable.codeAlign", 307);
		code("fieldReadable.attachmentPoint", 308);

		// elyCentre
		builder.addGroupDesc("elyCentre.common", "Perustiedot");
		builder.addField("elyCentre", "common", "id", "Numero", "", Type.UNIQUE_ID_USER_GIVEN_INTEGER, 2);
		builder.addField("elyCentre", "common", "name", "Nimi", "", Type.VARCHAR, 50);
		builder.setTipuApiEnumName("elyCentre.id", TipuAPIClient.ELY_CENTRES);

		// ringerObeserverSynonyms
		builder.addField("ringerObserverSynonyms", null, "ringerId", "Rengastaja", "", Type.INTEGER, 5);
		builder.addField("ringerObserverSynonyms", null, "synonymId", "Synonyymi", "", Type.INTEGER, 5);

		// palautus dummy
		builder.addField("returnRings", null, "ringer", "Rengastaja", "", Type.ENUMERATION, 0);
		builder.addField("returnRings", null, "ringStart", "Rengas-", "", Type.VARCHAR, 9);
		builder.addField("returnRings", null, "ringEnd", "-rengas", "", Type.VARCHAR, 9);
		builder.setTipuApiEnumName("returnRings.ringer", TipuAPIClient.RINGERS);

		// jakeluvisualisointi dummy
		builder.addField("distributionVisualization", null, "ringer", "Rengastaja", "", Type.ENUMERATION, 0);
		builder.setTipuApiEnumName("distributionVisualization.ringer", TipuAPIClient.RINGERS);

		// massapostitus dummy
		builder.addField("massEmail", null, "receiverType", "Vastaanottajaryhmät", "Vastaanottajaryhmien valinta", Type.ENUMERATION, 0);
		builder.reuseEnum("massEmail.receiverType", "ringer.state");
		builder.addField("massEmail", null, "receiverList", "Rengastajanumerot", "Pilkulla eroteltu lista rengastajanumeroista", Type.VARCHAR, 2500);
		builder.addField("massEmail", null, "subject", "Otsikko", "", Type.VARCHAR, 250);
		builder.addField("massEmail", null, "message", "Viesti", "", Type.VARCHAR, 4500);

		for (String tableName : builder.getTableNames()) {
			if (tableName.equals("event")) continue;
			if (tableName.equals("ringerObserverSynonyms")) continue;
			builder.addGroupDesc(tableName+".dataHistory", "Tiedon lisäys/muokkaus");
			builder.addField(tableName, "dataHistory", "created", "Lisätty", "Pvm jolloin tieto lisätty tietokantaan", Type.DATE, 0);
			builder.addField(tableName, "dataHistory", "modified", "Muokattu", "Pvm jolloin tietoa on viimeksi muokattu", Type.DATE, 0);
			builder.addField(tableName, "dataHistory", "modifiedBy", "Muokkaaja", "Käyttäjätunnus joka on lisännyt tai joka on viimeksi muokannut tiedot", Type.ENUMERATION, 20);
			builder.setTipuApiEnumName(tableName+".modifiedBy", TipuAPIClient.ADMINS);
			builder.addField(tableName, "dataHistory", "rowid", "rowid", "rowid", Type.VARCHAR, 40);
		}

		for (String tipuApiEnumName : builder.getUsedTipuApiResources()) {
			EnumerationsContainer enumerationsContainer = RowStructure.loadTipuApiEnumeration(client, tipuApiEnumName);
			builder.getEnumerations().put(tipuApiEnumName, enumerationsContainer);
		}
	}



	private void code(String fieldName, int code) throws Exception {
		Document codes = client.getAsDocument(TipuAPIClient.CODES, Integer.toString(code));
		for (Node codeNode : codes.getRootNode()) {
			String value = codeNode.getNode("code").getContents();
			String desc = "";
			for (Node descNode : codeNode.getChildNodes("desc")) {
				String lang = descNode.getAttribute("lang").toLowerCase();
				if (lang.equals("fi")) {
					desc = descNode.getContents();
					break;
				}
			}
			builder.setFieldEnumValue(fieldName, value, desc);
		}
	}

}
