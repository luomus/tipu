package fi.luomus.rengastus.dao;

import fi.luomus.rengastus.model.Row;

import java.util.List;

public class SearchResult {

	private final List<Row> results;
	private final String errorMessage;
	
	public SearchResult(String errorMessage) {
		this.results = null;
		this.errorMessage = errorMessage;
	}
	
	public SearchResult(List<Row> results) {
		this.results = results;
		this.errorMessage = null;
	}
	
	public List<Row> getResults() {
		return results;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public boolean wasSuccessful() {
		return errorMessage == null;
	}

	public boolean hasResults() {
		return wasSuccessful() && results.size() > 0;
	}
}
