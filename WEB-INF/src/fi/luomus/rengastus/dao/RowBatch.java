package fi.luomus.rengastus.dao;

import java.util.ArrayList;
import java.util.List;

import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Row;

public class RowBatch {

	public static class Entry {
		private final Row row;
		private final Column[] idColumns;
		public Entry(Row row, Column[] idColumns) {
			this.row = row;
			this.idColumns = idColumns;
		}
		public Row getRow() {
			return row;
		}
		public Column[] getIdColumns() {
			return idColumns;
		}
	}

	private final List<Entry> batch = new ArrayList<>();

	public RowBatch add(Row row, Column ... idColumns) {
		batch.add(new Entry(row, idColumns));
		return this;
	}

	public List<Entry> get() {
		return batch;
	}

}
