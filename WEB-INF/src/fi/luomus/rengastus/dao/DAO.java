package fi.luomus.rengastus.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.luomus.commons.containers.Ring;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.rengastus.model.Attachment;
import fi.luomus.rengastus.model.BinCounts;
import fi.luomus.rengastus.model.BinRow;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Field;
import fi.luomus.rengastus.model.LoydosForm;
import fi.luomus.rengastus.model.LoydosForm.Person;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.RowStructure;
import fi.luomus.rengastus.model.WarehouseQueueInfo;

public interface DAO {

	public static enum EventOperation { INSERT, UPDATE, DELETE, MASS_UPDATE }

	public static enum CustomSQLTarget { UI, FILE }

	// Structures and rows

	RowStructure getRowStructure();

	Row newRow();

	void refreshRowStructure();


	// Event APIs

	Row getEventById(String id);

	SearchResult searchEvent(Row searchParams);

	SearchResult getRingHistory(Row row) throws Exception;

	JSONObject validateEventUsingAPI(JSONObject data, String ringingRecoveryMode, EventOperation eventOperation) throws Exception;

	JSONObject storeEventUsingAPI(JSONObject data, String ringingRecoveryMode, EventOperation eventOperation) throws Exception;

	JSONObject deleteEventUsingAPI(String id) throws Exception;

	String getNextEventId() throws Exception;


	// Event bins

	boolean inBin(String eventId) throws Exception;

	void insertEventToWaitingBin(Row row, String binNotes) throws Exception;

	void inertEventToTrashBin(Row row, String binNotes) throws Exception;

	void insertEventToDeletedBin(Row row, String binNotes) throws Exception;

	void updateEventToWaitingBin(Row row, String binNotes) throws Exception;

	void updateEventToTrashBin(Row row, String binNotes) throws Exception;

	void updateEventToDeletedBin(Row row, String binNotes) throws Exception;

	List<BinRow> getWaitingBinEvents(String eventType) throws Exception;

	List<BinRow> getTrashBinEvents(String eventType) throws Exception;

	List<BinRow> getDeletedBinEvents(String eventType) throws Exception;

	BinCounts getBinCounts() throws Exception;

	BinRow getEventFromBin(String eventId) throws Exception;

	void deleteFromBin(String eventId) throws Exception;


	// Löydös APIs

	int getLoydosCount();

	List<LoydosForm> getLoydosForms(Set<String> onlyTypes, int page) throws Exception;

	int getLoydosFormPageCount(Set<String> onlyTypes) throws Exception;

	LoydosForm getLoydosForm(String loydosID) throws Exception;

	void marReceivedToLoydosStoreOriginal(String diario, String loydosID, String notes) throws Exception;

	void markLoydosFormTemporarilyReceived(String loydosID) throws Exception;

	LoydosForm getLoydosFormOriginal(String diario) throws Exception;

	void markDeletedToLoydos(String idToRemove) throws Exception;

	void saveLoydosNotes(String loydosId, String notes) throws Exception;

	Map<String, String> getLoydosNotes() throws Exception;

	String getLoydosNotes(String loydosId) throws Exception;


	// Other APIs

	HttpClientService getTipuApiAuthenenticatedClient() throws Exception;

	TipuAPIClient getTipuApiClient() throws Exception;

	JSONObject getRingersDistributedAndUsedRings(String ringer);

	void refreshValidations() throws Exception;


	// Internal DB resources

	boolean resourceExists(Column ... idColumns) throws SQLException;

	void fillResourceByIdFields(Row row, Column ... idColumns) throws SQLException;

	Row getResourceByRowId(String rowid, String tableName) throws Exception;

	SearchResult searchResources(Row row, String tableName);

	SearchResult searchResources(Row row, String tableName, String filterClause);

	/**
	 *
	 * @param row
	 * @param idColumns
	 * @return true if database state was affected (for insert always returns true (or exception) and for update if the resource was altered or not)
	 * @throws Exception
	 */
	boolean storeResourceToDB(Row row, Column ... idColumns) throws Exception;

	void storeBatchToDB(RowBatch batch) throws Exception;

	void deleteResourceFromDB(Column ... idColumns) throws Exception;

	/**
	 * Update rows
	 * @param batch
	 * @return null on success, list of not-found ids if no row was found to update
	 * @throws IllegalArgumentException on validation error / data error
	 * @throws SQLException on unknown error
	 */
	List<String> massUpdate(MassUpdateBatch batch) throws SQLException;

	// Misc

	TransactionConnection openConnection() throws SQLException;

	List<List<String>> executeCustomSQLSearch(String sql, CustomSQLTarget target, List<String> values) throws SQLException;

	void fillExistingLaymanOrInsertAndFill(Person laymanCandidate, Row row) throws Exception;

	Row getExistingLayman(String fullname, String email) throws Exception;

	ErrorReporter getErrorReporter();

	Row toWhomDistributed(Ring ring) throws SQLException;

	String getNextId(Field f) throws SQLException;

	Set<String> getRingerSynonyms(String ringerId) throws Exception;

	void updateRingerSynonyms(String ringerId, Set<String> synonyms) throws Exception;

	WarehouseQueueInfo getWarehouseQueueInfo() throws Exception;


	// Event attachments

	void addAttachment(Attachment attachment) throws Exception;

	List<Attachment> getAttachments(String eventId) throws Exception;

	Attachment getAttachment(String fileId) throws Exception;

	void deleteAttachment(Attachment attachment) throws Exception;

}
