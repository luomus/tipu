package fi.luomus.rengastus.validators;

import fi.luomus.commons.containers.Ring;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Range;
import fi.luomus.rengastus.model.Row;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class JakeluValidator extends RenkaatValidator {

	@Override
	protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
		validateRingRange("distributed", row, dao);
		if (hasErrors()) return;

		require(row.get("distributed.distributedDate"));
		require(row.get("distributed.distributedTo"));

		Column ringStart = row.get("distributed.ringStart");
		Ring start = new Ring(ringStart.getValue());
		Ring end = new Ring(row.get("distributed.ringEnd").getValue());

		List<Range> definedRanges = getExistingSeriesNumberRangesFromDb(start.getSeries(), dao);
		for (int ringNumber = start.getNumbers(); ringNumber <= end.getNumbers(); ringNumber++) {
			if (!contains(definedRanges, ringNumber)) {
				setError(ringStart, "Ainakaan tätä rengasta ei ole määritelty jaettavissa olevissa renkaissa: " + start.getSeries() + " " + ringNumber);
				return;
			}
		}

		if (isForInsert(req)) {
			validateInsert(row, dao);
		} else {
			validateUpdate(row, dao);
		}

	}

	protected boolean contains(List<Range> ranges, int ringNumber) {
		for (Range range : ranges) {
			if (contains(range, ringNumber)) {
				return true;
			}
		}
		return false;
	}

	protected boolean contains(Range range, int ringNumber) {
		return ringNumber >= range.start() && ringNumber <= range.end();
	}

	private void validateInsert(Row row, DAO dao) throws SQLException {
		Column ringStart = row.get("distributed.ringStart");
		Ring start = new Ring(ringStart.getValue());
		Ring end = new Ring(row.get("distributed.ringEnd").getValue());
		
		List<Range> distributedRanges = getDistributedNumberRangesFromDb(start.getSeries(), dao);
		
		for (int ringNumber = start.getNumbers(); ringNumber <= end.getNumbers(); ringNumber++) {
			if (contains(distributedRanges, ringNumber)) {
				setError(ringStart, "Ainakin tämä rengas on jo jaettu jollekulle: " + start.getSeries() + " " + ringNumber);
				return;
			}
		}
	}

	private void validateUpdate(Row row, DAO dao) throws Exception {
		String rowid = row.get("distributed.rowid").getValue();
		Row originalRow = dao.getResourceByRowId(rowid, "distributed");

		Column ringStart = row.get("distributed.ringStart");
		Column ringEnd = row.get("distributed.ringEnd");
		Ring start = new Ring(ringStart.getValue());
		Ring end = new Ring(ringEnd.getValue());
		Ring originalStart = new Ring(originalRow.get("distributed.ringStart").getValue());
		Ring originalEnd = new Ring(originalRow.get("distributed.ringEnd").getValue());

		if (notChanged(start, end, originalStart, originalEnd)) {
			return;
		}

		List<Integer> expandedWith = thoseOfTheFirstRangeThatAreNotInTheSecondRange(start, end, originalStart, originalEnd);
		
		if (!expandedWith.isEmpty()) {
			List<Range> distributedRanges = getDistributedNumberRangesFromDb(start.getSeries(), dao);
			for (int ringNumber : expandedWith) {
				if (contains(distributedRanges, ringNumber)) {
					setError(ringStart, "Rengasväliä ollaan laajentamassa, mutta ainakin tämä rengas on jo jaettu jollekulle: " + start.getSeries() + " " + ringNumber);
					return;
				}
			}
		}

		// Ei validoida onko käytetty, koska jos jokin rengasväli pitääkin pilkkoa toiselle rengastajalle, täytyy sallia se välivaihe, että renkaat ovat hetken jakamatta
	}

}
