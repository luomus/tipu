package fi.luomus.rengastus.validators;


import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Row;

import javax.servlet.http.HttpServletRequest;

public class VäritunnisteetValidator extends BaseValidator {

	@Override
	public void performValidations(Row row, HttpServletRequest req, DAO dao) {
		Column code = row.get("fieldReadable.code");
		Column mainColor = row.get("fieldReadable.mainColor");
		Column forSpecies = row.get("fieldReadable.forSpecies");
		Column distributedTo = row.get("fieldReadable.distributedTo");
		Column distributedDate = row.get("fieldReadable.distributedDate");
		Column doubleDistributedCount = row.get("fieldReadable.doubleDistributedCount");
		Column attachmentPoint = row.get("fieldReadable.attachmentPoint");
		Column codeColor = row.get("fieldReadable.codeColor");
		Column codeAlign = row.get("fieldReadable.codeAlign");
		
		require(code);
		require(mainColor);
		require(forSpecies);
		require(distributedTo);
		require(distributedDate);
		require(doubleDistributedCount);
		require(attachmentPoint);
		require(codeColor);
		require(codeAlign);
				
		if (hasErrors()) return;
		
		if (forSpecies.getValue().length() > 6) {
			setError(forSpecies, "Laji ei saa olla rotu: maksimipituus kuusi merkkiä.");
		}
		
		if (doubleDistributedCount.getIntValue() < 1) {
			setError(doubleDistributedCount, "Jakolkm oltava 1 tai suurempi");
		}
	}


}
