package fi.luomus.rengastus.validators;

import javax.servlet.http.HttpServletRequest;

import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.dao.DAO.EventOperation;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.services.EventUpdateServlet;

public class EventValidator extends BaseValidator {

	private final String id;
	private final EventOperation eventOperation;

	public EventValidator(String id, EventOperation eventOperation) {
		this.id = id;
		this.eventOperation = eventOperation;
	}

	@Override
	protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
		if (!given(id)) {
			setCustomFieldError("ID:tä ei annettu.");
			return;
		}

		Column euringProvince = row.get("event.euringProvinceCode");
		Column country = row.get("event.countryCode"); 
		if (euringProvince.hasValue() && !country.hasValue()) {
			country.setValue(Utils.trimToLength(euringProvince.getValue(), 2));
		}

		for (Column c : row) {
			if (c.getField().inputForRecoveryMode() || c.getField().inputForRingingMode() || c.getField().getTableName().equals("layman")) {
				typeValidate(c);
			}
		}

		Column type = row.get("event.type");
		require(type);

		if (type.getValue().equals("recovery")) {
			recoveryValidate(row);
		} else {
			ringingValidate(row);
		}

		if (hasErrors()) return;

		validateUsingValidationAPI(id, row, dao);
	}

	private void ringingValidate(Row row) {
		if (eventOperation == EventOperation.UPDATE) {
			require(row.get("event.legRing"));
		} else if (eventOperation == EventOperation.INSERT) {
			Column legRing = row.get("event.legRing");
			Column ringStart = row.get("event.ringStart");
			Column ringEnd = row.get("event.ringEnd");
			if (!legRing.hasValue() && !ringStart.hasValue()) {
				setError(legRing, "Joko jalkarengas tai rengasväli on annettava.");
				setError(ringStart, "Joko jalkarengas tai rengasväli on annettava.");
				return;
			}
			if (legRing.hasValue() && (ringStart.hasValue() || ringEnd.hasValue())) {
				setError(legRing, "Saa antaa joko jalkarenkaan tai rengasvälin.");
				setError(ringStart, "Saa antaa joko jalkarenkaan tai rengasvälin.");
			}
		}
	}

	private void recoveryValidate(Row row) {
		Column sourceOfRecovery = row.get("event.sourceOfRecovery");
		if (!sourceOfRecovery.getValue().equals("1") && !sourceOfRecovery.getValue().equals("2")) {
			setError(sourceOfRecovery, "Tapaamisen lähde oltava joko  kontrolli tai löytö.");
		}
		require(row.get("event.recoveryMethod"));
		require(row.get("event.birdConditionEURING"));
		//require(row.get("event.recoveryMethodAccuracy"));
	}

	private void validateUsingValidationAPI(String id, Row row, DAO dao) throws Exception {
		JSONObject data = EventUpdateServlet.rowToTipuApiJSON(id, row);
		JSONObject validationResponse = dao.validateEventUsingAPI(data, EventUpdateServlet.typeToModeParameterValue(row), eventOperation);

		JSONObject errors = validationResponse.getObject("rows").getObject(id).getObject("errors"); 
		for (String errorField : errors.getKeys()) {
			for (JSONObject error : errors.getArray(errorField).iterateAsObject()) {
				setError(row.get("event."+errorField), error.getString("localizedErrorText"));
			}
		}
		JSONObject warnings = validationResponse.getObject("rows").getObject(id).getObject("warnings"); 
		for (String errorField :warnings.getKeys()) {
			for (JSONObject warning : warnings.getArray(errorField).iterateAsObject()) {
				if (warning.getString("errorName").equals("old_event")) continue;
				setWarning(row.get("event."+errorField), warning.getString("localizedErrorText"));
			}
		}
	}

}
