package fi.luomus.rengastus.validators;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import fi.luomus.commons.containers.Ring;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Row;

public class RenkaatDeleteValidator extends RenkaatValidator {

	@Override
	protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
		Column ringStart = row.get("ring.ringStart");
		Column ringEnd = row.get("ring.ringEnd");

		require(ringStart);
		if (hasErrors()) return;
		
		dao.fillResourceByIdFields(row, ringStart);
		
		Ring start = new Ring(ringStart.getValue());
		Ring end = new Ring(ringEnd.getValue());
		
		List<Integer> decreasedBy = new ArrayList<>();
		for (int i = start.getNumbers(); i <= end.getNumbers(); i++) {
			decreasedBy.add(i);
		}
		
		checkNoneIsDistributed(ringStart, start, decreasedBy, dao);
	}

}
