package fi.luomus.rengastus.validators;


import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.model.Row;

import javax.servlet.http.HttpServletRequest;

public class MaallikkoValidator extends BaseValidator {

	@Override
	protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
		require(row.get("layman.name"));
		require(row.get("layman.language"));
	}

}
