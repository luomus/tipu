package fi.luomus.rengastus.validators;


import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.dao.SearchResult;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Row;

import javax.servlet.http.HttpServletRequest;

public class VäritunnisteetDeleteValidator extends BaseValidator {

	@Override
	protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
		Column code = row.get("fieldReadable.code");
		Column mainColor = row.get("fieldReadable.mainColor");
		Column forSpecies = row.get("fieldReadable.forSpecies");
		
		require(code);
		require(mainColor);
		require(forSpecies);
		
		if (hasErrors()) return;
		
		if (isUsed(dao, code, mainColor, forSpecies)) {
			setError(code, "Väritunniste on käytetty.");
			setError(mainColor, "Väritunniste on käytetty.");
			setError(forSpecies, "Väritunniste on käytetty.");
		}
	}

	private boolean isUsed(DAO dao, Column code, Column mainColor, Column forSpecies) throws Exception {
		Row searchParams = dao.newRow();
		searchParams.get("event.fieldReadableCode").setValue(code.getValue());
		searchParams.get("event.mainColor").setValue(mainColor.getValue());
		searchParams.get("event.species").setValue(forSpecies.getValue());
		
		SearchResult searchResult = dao.searchEvent(searchParams);
		if (!searchResult.wasSuccessful()) {
			throw new Exception(searchResult.getErrorMessage());
		}
		return !searchResult.getResults().isEmpty();
	}

}
