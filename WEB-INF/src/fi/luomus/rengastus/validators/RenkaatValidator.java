package fi.luomus.rengastus.validators;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import fi.luomus.commons.containers.Ring;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Range;
import fi.luomus.rengastus.model.Row;

public class RenkaatValidator extends BaseValidator {

	@Override
	protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
		validateRingRange("ring", row, dao);
		if (hasErrors()) return;
		
		if (isForInsert(req)) {
			validateInsert(row, dao);
		} else {
			validateUpdate(row, dao);
		}
	}

	protected void validateRingRange(String tableName, Row row, DAO dao) throws SQLException {
		Column ringStart = row.get(tableName+".ringStart");
		Column ringEnd = row.get(tableName+".ringEnd");

		require(ringStart);
		require(ringEnd);
		if (hasErrors()) return;

		Ring start = new Ring(ringStart.getValue());
		Ring end = new Ring(ringEnd.getValue());
		if (!start.validFormat()) {
			setError(ringStart, "Epäkelpo rengasformaatti");
		}
		if (!end.validFormat()) {
			setError(ringEnd, "Epäkelpo rengasformaatti");
		}
		if (hasErrors()) return;

		if (!start.getSeries().equals(end.getSeries())) {
			setError(ringStart, "Renkaat eivät ole samasta sarjasta.");
			setError(ringEnd, "Renkaat eivät ole samasta sarjasta.");
		}
		if (end.getNumbers() < start.getNumbers()) {
			setError(ringStart, "Rengasväli väärin päin?");
			setError(ringEnd, "Rengasväli väärin päin?");
		}
		if (hasErrors()) return;	

		if (!ringSeriesExists(start.getSeries(), dao)) {
			setError(ringStart, "Sarjaa ei ole määritelty.");
			setError(ringEnd, "Sarjaa ei ole määritelty.");
			return;
		}
	}

	private void validateUpdate(Row row, DAO dao) throws Exception {
		String rowid = row.get("ring.rowid").getValue();
		Row originalRow = dao.getResourceByRowId(rowid, "ring");

		Column ringStart = row.get("ring.ringStart");
		Column ringEnd = row.get("ring.ringEnd");
		Ring start = new Ring(ringStart.getValue());
		Ring end = new Ring(ringEnd.getValue());
		Ring originalStart = new Ring(originalRow.get("ring.ringStart").getValue());
		Ring originalEnd = new Ring(originalRow.get("ring.ringEnd").getValue());

		if (notChanged(start, end, originalStart, originalEnd)) {
			return;
		}

		List<Integer> expandedWith = thoseOfTheFirstRangeThatAreNotInTheSecondRange(start, end, originalStart, originalEnd);
		List<Integer> decreasedBy = thoseOfTheFirstRangeThatAreNotInTheSecondRange(originalStart, originalEnd, start, end);

		if (!expandedWith.isEmpty()) {
			checkNoneAlreadyDefined(ringEnd, start, expandedWith, dao);
		}

		if (!decreasedBy.isEmpty()) {
			checkNoneIsDistributed(ringEnd, start, decreasedBy, dao);
		}
	}

	protected boolean notChanged(Ring start, Ring end, Ring originalStart, Ring originalEnd) {
		return start.getNumbers() == originalStart.getNumbers() && end.getNumbers() == originalEnd.getNumbers();
	}

	protected void checkNoneIsDistributed(Column ringStart, Ring start, List<Integer> decreasedBy, DAO dao) throws SQLException {
		List<Range> distributedNumberRanges = getDistributedNumberRangesFromDb(start.getSeries(), dao);
		String errorMessage = "Rengasväliä ollaan supistamassa, mutta ainakin tämä rengas on jo jaettu: ";
		checkGivenRangeNotInSecondRanges(ringStart, start, decreasedBy, distributedNumberRanges, errorMessage);
	}

	private void checkNoneAlreadyDefined(Column ringStart, Ring start, List<Integer> expandedWith, DAO dao) throws SQLException {
		List<Range> existingNumberRanges = getExistingSeriesNumberRangesFromDb(start.getSeries(), dao);
		String errorMessage = "Ainakin tämä rengas on jo määritelty ennestään: ";
		checkGivenRangeNotInSecondRanges(ringStart, start, expandedWith, existingNumberRanges, errorMessage);
	}

	protected void checkGivenRangeNotInSecondRanges(Column ringStart, Ring start, List<Integer> firstRange, List<Range> secondRanges, String errorMessage) {
		for (int firstRangeNumber : firstRange) {
			for (Range secondRange : secondRanges) {
				for (int secondRangeNumber = secondRange.start(); secondRangeNumber <= secondRange.end(); secondRangeNumber++) {
					if (secondRangeNumber == firstRangeNumber) {
						setError(ringStart, errorMessage + start.getSeries() + " " + firstRangeNumber);
						return;
					}
				}
			}
		}
	}

	private static final String DISTRIBUTED_SERIES_RANGES_SQL = " SELECT ringStart, ringEnd FROM distribution_view WHERE series = ? ";

	protected List<Range> getDistributedNumberRangesFromDb(String series, DAO dao) throws SQLException {
		return getNumberRange(DISTRIBUTED_SERIES_RANGES_SQL, dao, series);
	}

	private static final String EXISTING_SERIES_RANGES_SQL = " SELECT to_number(substr(ringStart, 3)), to_number(substr(ringEnd, 3)) FROM ring WHERE trim(substr(ringStart, 1, 2)) = ? ";

	protected List<Range> getExistingSeriesNumberRangesFromDb(String series, DAO dao) throws SQLException {
		return getNumberRange(EXISTING_SERIES_RANGES_SQL, dao, series);
	}

	protected List<Range> getNumberRange(String sql, DAO dao, String ... values) throws SQLException {
		List<Range> existing = new ArrayList<>();
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			con = dao.openConnection();
			p = con.prepareStatement(sql);
			int i = 1;
			for (String value : values) {
				p.setString(i++, value.trim().toUpperCase());
			}
			rs = p.executeQuery();
			while (rs.next()) {
				int start = rs.getInt(1);
				int end = rs.getInt(2);
				existing.add(new Range(start, end));
			}
			return existing;
		} finally {
			Utils.close(p, rs, con);
		}
	}

	protected List<Integer> thoseOfTheFirstRangeThatAreNotInTheSecondRange(Ring start, Ring end, Ring originalStart, Ring originalEnd) {
		List<Integer> difference = new ArrayList<>();
		for (int i = start.getNumbers(); i <= end.getNumbers(); i++) {
			if (i < originalStart.getNumbers() || i > originalEnd.getNumbers()) {
				difference.add(i);
			}
		}
		return difference;
	}

	private void validateInsert(Row row, DAO dao) throws SQLException {
		Column ringStart = row.get("ring.ringStart");
		Column ringEnd = row.get("ring.ringEnd");
		Ring start = new Ring(ringStart.getValue());
		Ring end = new Ring(ringEnd.getValue());
		
		for (Range numberRange : getExistingSeriesNumberRangesFromDb(start.getSeries(), dao)) {
			for (int existing = numberRange.start(); existing <= numberRange.end(); existing++) {
				for (int toBeAdded = start.getNumbers(); toBeAdded <= end.getNumbers(); toBeAdded++) {
					if (existing == toBeAdded) {
						setError(ringStart, "Ainakin tämä rengas on jo määritelty ennestään: " + start.getSeries() + " " + toBeAdded);
						return;
					}
				}
			}
		}
	}

	private boolean ringSeriesExists(String series, DAO dao) throws SQLException {
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			con = dao.openConnection();
			p = con.prepareStatement(" SELECT count(1) FROM series WHERE series = ? AND rownum = 1 ");
			p.setString(1, series.trim().toUpperCase());
			rs = p.executeQuery();
			rs.next();
			return rs.getInt(1) > 0;
		} finally {
			Utils.close(p, rs, con);
		}
	}

}
