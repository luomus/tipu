package fi.luomus.rengastus.validators;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Field;
import fi.luomus.rengastus.model.Row;
import fi.luomus.rengastus.model.ValidationResponse;
import fi.luomus.rengastus.model.Validator;

public abstract class BaseValidator implements Validator {

	protected static final int MIN_YEAR = 1880;
	protected static final int MAX_YEAR = 2080;

	protected final ValidationResponse response = new ValidationResponse();

	/**
	 * Must be overriden by implementing validator to perform the actual validations
	 * @param row
	 * @param req 
	 * @param dao
	 * @throws Exception
	 */
	protected abstract void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception;

	/**
	 * May be overriden by implementing validator
	 * @return
	 */
	protected String viewIfErrors() {
		return null;
	}

	@Override
	public ValidationResponse validate(Row row, HttpServletRequest req, DAO dao) {
		try {
			performValidations(row, req, dao);
		} catch (Exception e) {
			response.setError("Tuntematon virhe:", e.getMessage());
			dao.getErrorReporter().report(e);
		}
		if (viewIfErrors() != null) {
			response.setViewIfErrors(viewIfErrors());
		}
		return response;
	}

	protected void setCustomFieldError(String field, String errorText) {
		response.setError(field, errorText);
	}

	protected void setCustomFieldError(String errorText) {
		setCustomFieldError(null, errorText);
	}

	protected void setError(Column column, String errorText) {
		response.setError(column.getFieldName(), errorText);
	}

	protected void setWarning(Column column, String errorText) {
		response.setWarning(column.getFieldName(), errorText);
	}

	protected boolean given(String s) {
		return s != null && s.length() > 0;
	}

	protected boolean hasErrors() {
		return response.hasErrors();
	}

	protected boolean typeValidate(Row row) {
		boolean ok = true;
		for (Column c : row) {
			if (!typeValidate(c)) ok = false;
		}
		return ok;
	}

	protected boolean typeValidate(Column c) {
		if (!c.hasValue()) return true;
		Field field = c.getField();
		if (field.isDate()) {
			return validateDate(c);
		} else if (field.isInteger()) {
			return validateInteger(c);
		} else if (field.isDecimal()) {
			return validateDecimal(c);
		} else if (field.isEnumeration()) {
			return validateEnumeration(c);
		} else if (field.isYear()) {
			return validateYear(c);
		} else if (field.getName().endsWith("email")) {
			return validateEmail(c);
		}
		return validateLength(c);
	}

	protected boolean validateAsSearchParams(Row searchParams) {
		boolean ok = true;
		for (Column c : searchParams) {
			if (!typeValidateAsSearchParam(c)) ok = false;
		}
		return ok;
	}

	protected boolean typeValidateAsSearchParam(Column c) {
		if (!c.hasValue()) return true;
		Field field = c.getField();
		if (field.isDate()) {
			return validateDateSearchParam(c);
		} else if (field.isInteger()) {
			return validateIngegerSearchParam(c);
		} else if (field.isDecimal()) {
			return validateDecimalSearchParam(c);
		} else if (field.isEnumeration()) {
			return validateEnumerationSearchParam(c);
		} else if (field.isYear()) {
			return validateYearSearchParam(c);
		}
		return true;
	}

	protected boolean validateYearSearchParam(Column c) {
		return validateIngegerSearchParam(c);
	}

	protected boolean validateEnumerationSearchParam(Column c) {
		for (String value : c.getValue().split(Pattern.quote("|"))) {
			if (!validateEnumeration(c, value)) return false;
		}
		return true;
	}

	protected boolean validateDecimalSearchParam(Column c) {
		String value = c.getValue();
		value = value.replace("%", "");
		value = value.replace("_", "");
		int countOfSlashes = Utils.countNumberOf("/", value); 
		if (countOfSlashes == 0) {
			return validateDecimal(c, value);
		}
		if (countOfSlashes > 1) {
			setError(c, "Epäkelpo numeerinen arvoväli");
		}
		if (value.startsWith("/") || value.endsWith("/")) {
			setError(c, "Epäkelpo numeerinen arvoväli");
		}
		for (String splittedValue : value.split(Pattern.quote("/"))) {
			if (!validateDecimal(c, splittedValue)) return false;
		}
		return true;
	}

	protected boolean validateIngegerSearchParam(Column c) {
		String value = c.getValue();
		value = value.replace("%", "");
		value = value.replace("_", "");
		int countOfSlashes = Utils.countNumberOf("/", value); 
		if (countOfSlashes == 0) {
			return validateInteger(c, value);
		}
		if (countOfSlashes > 1) {
			setError(c, "Epäkelpo numeerinen arvoväli");
		}
		if (value.startsWith("/") || value.endsWith("/")) {
			setError(c, "Epäkelpo numeerinen arvoväli");
		}
		for (String splittedValue : value.split(Pattern.quote("/"))) {
			if (!validateInteger(c, splittedValue)) return false;
		}
		return true;
	}

	protected boolean validateEmail(Column c) {
		if (!validateLength(c)) return false;
		String email = c.getValue();
		if (invalidEmailChars(email)) {
			setError(c, "Epäkelpo sähköpostiosoite.");
			return false;
		}
		if (!email.contains(".") || !email.contains("@")) {
			setError(c, "Epäkelpo sähköpostiosoite.");
			return false;
		}
		if (Utils.countNumberOf("@", email) != 1) {
			setError(c, "Epäkelpo sähköpostiosoite.");
			return false;
		}
		String localPart = email.split("@")[0];
		String domain = email.split("@")[1];
		if (localPart.length() < 1 || domain.length() < 4) {
			setError(c, "Epäkelpo sähköpostiosoite.");
			return false;
		}
		if (localPart.startsWith(".") || localPart.endsWith(".") || domain.startsWith(".") || domain.endsWith(".")) {
			setError(c, "Epäkelpo sähköpostiosoite.");
			return false;
		}
		if (!domain.contains(".")) {
			setError(c, "Epäkelpo sähköpostiosoite.");
			return false;
		}
		return true;
	}


//    Uppercase and lowercase English letters (a-z, A-Z)
//    Digits 0 to 9
//    Characters ! # $ % & ' * + - / = ? ^ _ ` { | } ~
//    Character . (dot, period, full stop) provided that it is not the first or last character, and provided also that it does not appear two or more times consecutively.
//    And @ to separate local parts
	
    private static final Set<Character> VALID_EMAIL_CHARS;
    static {
    	VALID_EMAIL_CHARS = new HashSet<>();
    	for (char c : "abcdefghijklmnopqrstuvwxyz".toCharArray()) {
    		VALID_EMAIL_CHARS.add(c);
    		VALID_EMAIL_CHARS.add(Character.toUpperCase(c));
    	}
    	for (char c : "0123456789@.!#$%&'*+-/=?^_`{|}".toCharArray()) {
    		VALID_EMAIL_CHARS.add(c);
    	}
    }
	private boolean invalidEmailChars(String email) {
		for (char c : email.toCharArray()) {
			if (!VALID_EMAIL_CHARS.contains(Character.valueOf(c))) {
				return true;
			}
		}
		return false;
	}

	protected boolean validateYear(Column c) {
		if (!validateInteger(c)) return false;
		int yyyy = c.getIntValue();

		int maxValue = DateUtils.getCurrentYear();
		if (c.getFieldName().equals("ringer.permissionYearStamp")) {
			maxValue++;
		}

		if ((yyyy < MIN_YEAR || yyyy > maxValue)) {
			setError(c, "Epäkelpo vuosi.");
			return false;
		}
		return true;
	}

	protected boolean validateEnumeration(Column c) {
		for (String value : c.getSelectedValues()) {
			if (!validateEnumeration(c, value)) return false;
		}
		return true;
	}

	protected boolean validateEnumeration(Column c, String value) {
		if (c.getFieldName().equals("event.species")) return true; // This gets validated in validaton api anyway; we have to validate against all species not just TipuAPIClient.SELECTABLE_SPECIES
		if (!c.getField().hasEnumerationDefined(value)) {
			setError(c, "Epäkelpo valintalistan arvo.");
			return false;
		}
		return true;
	}

	protected boolean validateDecimal(Column c) {
		return validateDecimal(c, c.getValue());
	}

	protected boolean validateDecimal(Column c, String value) {
		try {
			Double.valueOf(value.replace(",", "."));
		} catch (NumberFormatException e) {
			setError(c, "Epäkelpo desimaaliluku.");
			return false;
		}

		int maxLength = c.getField().getInputSize();
		value = negativeSignRemovedValue(c, value);
		String[] values = Utils.splitDecimal(value);
		if (values[0].length() > maxLength - 1)  { // Tämä on puutteellinen; maxlength on joka tulee tipu-api/ringing/rows on "input size" johon on lisätty kaiketi tilaa pilkulle ja desimaaleille, eli tämä sallii arvon joka ei mahdu tietokantakenttään
			setError(c, "Liian suuri luku.");
			return false;
		}
		return true;
	}

	protected boolean validateInteger(Column c) {
		return validateInteger(c, c.getValue());
	}

	protected boolean validateInteger(Column c, String value) {
		if (isInteger(value)) {
			return validateLength(c, value);
		}
		setError(c, "Epäkelpo kokonaisluku.");
		return false;
	}

	protected boolean validateLength(Column c) {
		return validateLength(c, c.getValue());
	}

	protected boolean validateLength(Column c, String value) {
		int maxLength = c.getField().getInputSize();
		if (maxLength < 1) {
			throw new IllegalStateException("No length defined for " + c.getFieldName());
		}
		value = negativeSignRemovedValue(c, value);
		if (value.length() > maxLength) {
			setError(c, "Liian pitkä.");
			return false;
		}
		return true;
	}

	protected String negativeSignRemovedValue(Column c, String value) {
		if (c.getField().isDecimal() || c.getField().isInteger()) {
			if (value.startsWith("-")) {
				value = value.replaceFirst("-", "");
			}
		}
		return value;
	}

	protected boolean isInteger(String value) {
		try {
			Integer.valueOf(value);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	protected boolean validateDate(Column c) {
		return validateDate(c, DateUtils.convertToDateValue(c.getValue()));
	}

	protected boolean validateDateSearchParam(Column c) {
		return validateDateSearchParam(c, DateUtils.convertToDateValue(c.getValue()));
	}

	protected boolean validateDate(Column c, DateValue date) {
		if (date.isEmpty()) return true;
		if (date.hasEmptyFields()) {
			setError(c, "Epäkelpo päivämäärä.");
			return false;
		}
		if (!containsIntegers(Utils.collection(date.getYear(), date.getMonth(), date.getDay()))) {
			setError(c, "Epäkelpo päivämäärä.");
			return false;
		}
		int dd = Integer.parseInt(date.getDay());
		int mm = Integer.parseInt(date.getMonth());
		int yyyy = Integer.parseInt(date.getYear());

		if (dd < 1 || dd > 31) {
			setError(c, "Epäkelpo päivämäärä.");
			return false;
		}
		if (mm < 1 || mm > 12) {
			setError(c, "Epäkelpo päivämäärä.");
			return false;
		}
		if ((yyyy < MIN_YEAR || yyyy > MAX_YEAR)) {
			setError(c, "Epäkelpo päivämäärä.");
			return false;
		}

		Calendar k = Calendar.getInstance();
		k.setLenient(false);
		try {
			k.set(yyyy, mm - 1, dd);
			k.getTime();
		} catch (IllegalArgumentException e) {
			setError(c, "Päivämäärää ei ole kalenterissa.");
			return false;
		}
		if (k.after(now())) {
			setError(c, "Ei saa olla tulevaisuudessa.");
			return false;
		}
		return true;
	}

	protected boolean validateDateSearchParam(Column c, DateValue date) {
		if (date.isEmpty()) return true;

		if (!validDatePart(date.getYear(), MIN_YEAR, MAX_YEAR, c)) {
			return false;
		}
		if (!validDatePart(date.getMonth(), 1, 12, c)) {
			return false;
		}
		if (!validDatePart(date.getDay(), 1, 31, c)) {
			return false;
		}
		return true;
	}

	private boolean validDatePart(String part, int minValue, int maxValue, Column c) {
		if (!given(part)) return true;

		int countOfSeparators = Utils.countNumberOf("/", part);
		if (countOfSeparators > 1) {
			setError(c, "Epäkelpo päivämääräväli");
			return false;
		}
		if (part.startsWith("/") || part.endsWith("/")) {
			setError(c, "Epäkelpo päivämääräväli");
			return false;
		}

		String[] parts = part.split(Pattern.quote("/"));
		for (String value : parts) {
			if (!isInteger(value)) {
				setError(c, "Epäkelpo päivämäärä.");
				return false;
			}
			int intValue = Integer.parseInt(value);
			if (intValue < minValue || intValue > maxValue) {
				setError(c, "Epäkelpo päivämäärä.");
				return false;
			}
		}
		return true;
	}

	protected boolean containsIntegers(Collection<String> values) {
		for (String value : values) {
			if (!isInteger(value)) return false;
		}
		return true;
	}

	protected Calendar now() {
		Calendar k = Calendar.getInstance();
		k.add(Calendar.SECOND, 1); // So that <somedate>.after(now()) will retun true if <somedate> and <now()> are the exact same moment
		return k;
	}

	protected void require(Column c) {
		if (!c.hasValue()) {
			setError(c, "Pakollinen kenttä.");
		}
	}

	protected void mustBeEmpty(Column c) {
		if (c.hasValue()) {
			setError(c, "Tietoa ei saa antaa.");
		}	
	}

	protected void append(ValidationResponse validationResponse) {
		this.response.append(validationResponse);
	}

	protected boolean isForInsert(HttpServletRequest req) {
		return req.getRequestURI().contains("/uusi");
	}

	protected boolean isForUpdate(HttpServletRequest req) {
		return !isForInsert(req);
	}

}
