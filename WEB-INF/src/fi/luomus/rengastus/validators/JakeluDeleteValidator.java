package fi.luomus.rengastus.validators;


import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Row;

import javax.servlet.http.HttpServletRequest;


public class JakeluDeleteValidator extends JakeluValidator {

	@Override
	protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
		Column ringStart = row.get("distributed.ringStart");

		require(ringStart);
		// Ei validoida onko käytetty, koska jos jokin rengasväli pitääkin pilkkoa toiselle rengastajalle, täytyy sallia se välivaihe, että renkaat ovat hetken jakamatta
	}

}
