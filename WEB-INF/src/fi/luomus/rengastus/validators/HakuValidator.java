package fi.luomus.rengastus.validators;

import fi.luomus.commons.containers.Ring;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Row;

import javax.servlet.http.HttpServletRequest;

public class HakuValidator extends BaseValidator {

	@Override
	protected void performValidations(Row searchParams, HttpServletRequest req, DAO dao) throws Exception {
		if (!searchParams.hasValues()) {
			setCustomFieldError("Jotain hakutekijöitä on annettava.");
		}
		validateAsSearchParams(searchParams);

		validateRingFormat(searchParams.get("event.ringStart"));
		validateRingFormat(searchParams.get("event.ringEnd"));
		validateRingFormat(searchParams.get("event.legRing"));
		validateRingFormat(searchParams.get("event.nameRing"));		
	}

	private void validateRingFormat(Column ring) {
		if (!ring.hasValue()) return;
		if (ring.getValue().contains("%") || ring.getValue().contains("_")) return;
		Ring start = new Ring(ring.getValue());
		if (!start.validFormat()) {
			setError(ring, "Epäkelpo rengasformaatti");
		}
	}

}
