package fi.luomus.rengastus.validators;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.utils.Utils;
import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.dao.SearchResult;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Row;

public class RengastajaValidator extends BaseValidator {

	@Override
	protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
		Column id = row.get("ringer.id");
		Column email = row.get("ringer.email");

		require(id);
		require(row.get("ringer.firstName"));
		require(row.get("ringer.lastName"));
		require(row.get("ringer.language"));
		require(row.get("ringer.letters"));
		require(row.get("ringer.state"));
		require(row.get("ringer.resultServicePermission"));
		require(row.get("ringer.FinBIFPermission"));
		
		if (email.hasValue()) {
			validateEmailNotUsedByOtherPerson(dao, id, email);
		}
	}

	private void validateEmailNotUsedByOtherPerson(DAO dao, Column id, Column email) {
		Row searchParam = dao.newRow();
		searchParam.get("ringer.email").setValue(email.getValue());
		SearchResult result = dao.searchResources(searchParam, "ringer");
		if (!result.wasSuccessful()) {
			setCustomFieldError(result.getErrorMessage());
			return;
		}
		for (Row resultRow : result.getResults()) {
			String resultRingerId = resultRow.get("ringer.id").getValue(); 
			if (resultRingerId.equals(id.getValue())) continue;
			setError(email, "Email on jo käytössä toisella henkilöllä: " + resultRingerId);
		}
	}

	public static class DeleteValidator extends BaseValidator {

		@Override
		protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
			Column idCol = row.get("ringer.id");
			int count = countOfUses(dao, idCol);
			if (count > 0) {
				setError(idCol, "Tämä rengastajanumero on käytössä rengastuksissa, tapaamisissa, jakelussa tai synonyyminä.");
			}
		}

		private int countOfUses(DAO dao, Column idCol) throws SQLException {
			String renro = idCol.getValue();
			TransactionConnection con = null;
			PreparedStatement p = null;
			ResultSet rs = null;
			try {
				con = dao.openConnection();
				String sql = "" +
						" 		select sum(c) from (														" + 
						"		select count(1) c from event where ringer = ?	and rownum = 1			    " + 
						"		union																		" +
						"		select count(1) c from event where intermediaryRinger = ? and rownum = 1	" +
						"		union																		" +
						"		select count(1) c from distributed where distributedTo = ? and rownum = 1	" +
						"		union																		" +
						"		select count(1) from ringerObserverSynonyms where ringerId = ? and rownum = 1 ) ";
				p = con.prepareStatement(sql);
				p.setString(1, renro);
				p.setString(2, renro);
				p.setString(3, renro);
				p.setString(4, renro);
				rs = p.executeQuery();
				rs.next();
				int count = rs.getInt(1);
				return count;
			} finally {
				Utils.close(p, rs, con);
			}
		}

	}

}
