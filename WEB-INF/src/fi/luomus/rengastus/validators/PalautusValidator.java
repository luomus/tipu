package fi.luomus.rengastus.validators;

import fi.luomus.commons.containers.Ring;

import fi.luomus.rengastus.dao.DAO;
import fi.luomus.rengastus.dao.SearchResult;
import fi.luomus.rengastus.model.Column;
import fi.luomus.rengastus.model.Range;
import fi.luomus.rengastus.model.Row;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class PalautusValidator extends JakeluValidator {

	@Override
	protected void performValidations(Row row, HttpServletRequest req, DAO dao) throws Exception {
		validateRingRange("returnRings", row, dao);
		Column ringer = row.get("returnRings.ringer"); 
		require(ringer);
		if (hasErrors()) return;
		
		Column ringStart = row.get("returnRings.ringStart");
		Ring start = new Ring(ringStart.getValue());
		Ring end = new Ring(row.get("returnRings.ringEnd").getValue());

		List<Range> distributedRanges = getRingersDistributedNumberRangesFromDb(start.getSeries(), ringer.getValue(), dao);
		
		for (int ringNumber = start.getNumbers(); ringNumber <= end.getNumbers(); ringNumber++) {
			if (!contains(distributedRanges, ringNumber)) {
				setError(ringStart, "Ainakaan tätä rengasta ei ole merkitty jaetuksi tälle rengastajalle: " + start.getSeries() + " " + ringNumber);
				return;
			}
			if (isUsed(dao, start.getSeries()+ringNumber)) {
				setError(ringStart, "Ainakin tämä rengas on käytetty: " + start.getSeries() + " " + ringNumber);
				return;
			}
		}
	}
	
	private boolean isUsed(DAO dao, String ring) throws Exception {
		Row searchParams = dao.newRow();
		searchParams.get("event.legRing").setValue(ring);
		
		SearchResult searchResult = dao.searchEvent(searchParams);
		if (!searchResult.wasSuccessful()) {
			throw new Exception(searchResult.getErrorMessage());
		}
		return !searchResult.getResults().isEmpty();
	}

	private static final String RINGERS_DISTRIBUTED_SERIES_RANGES_SQL = " select ringStart, ringEnd from distribution_view where series = ? and ringer = ? ";

	protected List<Range> getRingersDistributedNumberRangesFromDb(String series, String ringer, DAO dao) throws SQLException {
		return getNumberRange(RINGERS_DISTRIBUTED_SERIES_RANGES_SQL, dao, series, ringer);
	}
	
	
}
